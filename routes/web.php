<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/que-es-homex','GenericController@aboutUs')->name('aboutUs');
Route::get('/preguntas-frecuentes','GenericController@faq')->name('faq');
Route::get('/politicas-de-privacidad','GenericController@privacyPolicies')->name('privacyPolicies');
Route::get('/terminos-y-condiciones','GenericController@termsAndConditions')->name('termsAndConditions');
Route::get('/delete-help','GenericController@deleteHelp')->name('deleteHelp');

Route::group(['prefix' => 'users'], function (){
    Route::get('/profile/{slug}','UserController@profile')
        ->name('users.profile');
    Route::get('/create','UserController@create')
        ->name('users.create');
    Route::post('/store','UserController@store')
        ->name('users.store');


    Route::group(['middleware' => ['auth']], function () {
        Route::put('/update/{user}', 'UserController@update')
            ->name('users.update');
        Route::put('/update/personal/{user}', 'UserController@updatePersonal')
            ->name('users.updatePersonal');
        Route::put('/update/document/{user}', 'UserController@updateDocument')
            ->name('users.updateDocument');
        Route::post('/report/interested/{user}', 'UserController@reportInterested')
            ->name('users.reportInterested');
        Route::post('/report/payed/{user}', 'UserController@reportPayed')
            ->name('users.reportPayed');
        Route::get('/list', 'UserController@list')
            ->name('users.list');
    });
});
Route::group(['prefix' => 'properties'], function (){
    Route::get('/list','PropertyController@list')
        ->name('properties.list');
    Route::get('/myList/{user}','PropertyController@myList')
        ->name('properties.myList');
    Route::get('/ver/{id}','PropertyController@view')
        ->name('properties.view');
    Route::get('/addSearch','PropertyController@addSearch')
        ->name('properties.addSearch');

    Route::group(['middleware' => ['auth']], function (){
        Route::get('/user/searched/{id}','PropertyController@userSearched')
            ->name('properties.userSearched');
        Route::get('/application/{property}','PropertyController@application')
            ->name('properties.application');
        Route::get('/createSimilar/{property}','PropertyController@createSimilar')
            ->name('properties.createSimilar');
        Route::get('/create','PropertyController@create')
            ->name('properties.create');
        Route::post('/store','PropertyController@store')
            ->name('properties.store');
        Route::get('/edit/{property}','PropertyController@edit')
            ->name('properties.edit');
        Route::put('/update/{property}','PropertyController@update')
            ->name('properties.update');
        Route::get('/admin','PropertyController@admin')
            ->name('properties.admin');
    });

});


Route::group(['middleware' => ['auth']], function (){

    Route::group(['prefix' => 'lessees'], function (){
        Route::put('/update/{lessee}','LesseeController@update')
            ->name('lessees.update');
    });
    Route::group(['prefix' => 'propietaries'], function (){
        Route::put('/update/{propietary}','PropietaryController@update')
            ->name('propietaries.update');
    });
    Route::group(['prefix' => 'applications'], function (){
        Route::get('/list','ApplicationController@list')
            ->name('applications.list');
        Route::get('/list/lessee/{id}','ApplicationController@listLessee')
            ->name('applications.listLessee');
        Route::get('/list/propietary/{id}','ApplicationController@listPropietary')
            ->name('applications.listPropietary');
        Route::get('/list/property/{id}','ApplicationController@listProperty')
            ->name('applications.listProperty');
        Route::get('/status/{id}','ApplicationController@status')
            ->name('applications.status');
        Route::get('/updatePostulation','ApplicationController@updatePostulation')
            ->name('applications.updatePostulation');
    });
    Route::group(['prefix' => 'leases'], function (){

        Route::get('/list','LeaseController@list')
            ->name('leases.list');
        Route::get('/list/lessee/{id}','LeaseController@listLessee')
            ->name('leases.listLessee');
        Route::get('/list/propietary/{id}','LeaseController@listPropietary')
            ->name('leases.listPropietary');

    });
    Route::group(['prefix' => 'contracts'], function () {
        Route::get('/list/lease/{id}', 'ContractController@listLease')
            ->name('contracts.listLease');
        Route::get('/download/{id}','ContractController@getDownload')
            ->name('contracts.download');
        Route::get('/create/{lease}','ContractController@create')
            ->name('contracts.create');
        Route::post('/store','ContractController@store')
            ->name('contracts.store');
        Route::get('/edit/{contract}','ContractController@edit')
            ->name('contracts.edit');
        Route::put('/update/{contract}','ContractController@update')
            ->name('contracts.update');
    });
    Route::group(['prefix' => 'payments'], function () {
        Route::get('/list/lease/{id}', 'PaymentController@listLease')
            ->name('payments.listLease');
        Route::get('/download/{id}','PaymentController@getDownload')
            ->name('payments.download');
        Route::get('/updatePayment','PaymentController@updatePayment')
            ->name('payments.updatePayment');
    });
    Route::group(['prefix' => 'property-images'], function () {
        Route::get('/edit/{property}','PropertyImageController@edit')
            ->name('propertyImages.edit');
        Route::post('/store','PropertyImageController@store')
            ->name('propertyImages.store');
        Route::delete('/delete/{property}','PropertyImageController@delete')
            ->name('propertyImages.delete');
    });


});


Route::get('images/{path}/{attachment}', function ($path, $attachment){
    $file = sprintf('storage/%s/%s', $path, $attachment);

    if(File::exists($file)){
        return \Intervention\Image\Facades\Image::make($file)->response();
    }
});
