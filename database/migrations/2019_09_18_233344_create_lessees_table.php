<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLesseesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->float('monthly_salary')->nullable();
            $table->integer('emotional_situation')->nullable();
            $table->unsignedBigInteger('occupation_type_id')->nullable();
            $table->foreign('occupation_type_id')->references('id')->on('occupation_types');
            $table->string('occupation_center')->nullable();
            $table->unsignedBigInteger('lifestyle_type_id')->nullable();
            $table->foreign('lifestyle_type_id')->references('id')->on('lifestyle_types');
            $table->string('address')->nullable();
            $table->unsignedBigInteger('district_id')->nullable();
            $table->foreign('district_id')->references('id')->on('districts');
            $table->integer('quantity_members')->nullable();
            $table->integer('dependent_status')->nullable();
            $table->integer('debtor_status')->nullable();
            $table->integer('other_postulation')->nullable();
            $table->integer('bond_interested_status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessees');
    }
}
