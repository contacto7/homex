<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table){
            $table->increments('id');
            $table->string('name')->comment('nombre del rol del usuario');
            $table->text('description');
        });
        Schema::create('document_types', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('code');
        });
        Schema::create('nacionalities', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('code');
        });


        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('role_id')->default(\App\Role::LESSEE);
            $table->foreign('role_id')->references('id')->on('roles');
            $table->string('name');
            $table->string('last_name');
            $table->string('slug');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->unsignedInteger('document_type_id')->nullable()->default(1);//DNI = 1
            $table->foreign('document_type_id')->references('id')->on('document_types');
            $table->string('document_number')->nullable();
            $table->unsignedInteger('nacionality_id')->nullable()->default(170);//PERU = 170
            $table->foreign('nacionality_id')->references('id')->on('nacionalities');
            $table->string('cellphone');
            $table->string('picture')->nullable()->default('user.png');
            $table->enum('sex', [
                \App\User::FEMALE,
                \App\User::MALE
            ])->nullable();
            $table->enum('state', [
                \App\User::ACTIVE,
                \App\User::INACTIVE
            ])->default(\App\User::ACTIVE);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('roles');
    }
}
