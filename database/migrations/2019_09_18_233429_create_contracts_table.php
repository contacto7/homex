<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('pay_date');
            $table->integer('months_date');
            $table->integer('type');
            $table->unsignedBigInteger('currency_id');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->float('amount');
            $table->unsignedBigInteger('lease_id');
            $table->foreign('lease_id')->references('id')->on('leases');
            $table->string('document')->nullable();
            $table->enum('state', [
                \App\Contract::ACTIVE,
                \App\Contract::INACTIVE
            ])->default(\App\Contract::ACTIVE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
