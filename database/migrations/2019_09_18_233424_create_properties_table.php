<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description');
            $table->integer('rooms');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('district_id');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->unsignedBigInteger('property_type_id');
            $table->foreign('property_type_id')->references('id')->on('property_types');
            $table->integer('parking_slots');
            $table->integer('total_area');
            $table->integer('bathrooms');
            $table->integer('half_bathrooms');
            $table->integer('year');
            $table->unsignedBigInteger('currency_rental_amount');
            $table->foreign('currency_rental_amount')->references('id')->on('currencies');
            $table->float('rental_amount');
            $table->unsignedBigInteger('currency_maintenance_amount');
            $table->foreign('currency_maintenance_amount')->references('id')->on('currencies');
            $table->float('maintenance_amount')->nullable();
            $table->unsignedBigInteger('currency_guaranty_amount');
            $table->foreign('currency_guaranty_amount')->references('id')->on('currencies');
            $table->float('guaranty_amount')->nullable();
            $table->float('latitude');
            $table->float('longitude');
            $table->string('address');
            $table->enum('pet_allowed', [
                \App\Property::PET_ALLOWED,
                \App\Property::PET_NOT_ALLOWED
            ])->default(\App\Property::PET_NOT_ALLOWED);
            $table->enum('state', [
                \App\Property::ACTIVE,
                \App\Property::INACTIVE
            ])->default(\App\Property::ACTIVE);
            $table->enum('only_genre', [
                \App\Property::ALL,
                \App\Property::WOMEN_ONLY,
                \App\Property::MAN_ONLY,
                \App\Property::WOMAN_PREFERRED,
                \App\Property::MAN_PREFERRED
            ])->default(\App\Property::ALL);
            $table->enum('only_students', [
                \App\Property::ALL,
                \App\Property::STUDENTS_ONLY,
                \App\Property::STUDENTS_PREFERRED,
            ])->default(\App\Property::ALL);
            $table->enum('young_bonus', [
                \App\Property::YB_QUALIFIED,
                \App\Property::YB_NOT_QUALIFIED,
                \App\Property::YB_PREFERRED,
            ])->default(\App\Property::YB_NOT_QUALIFIED);
            $table->enum('family_status', [
                \App\Property::ALL,
                \App\Property::COUPLE_WITH_KIDS_ONLY,
                \App\Property::COUPLE_WITH_KIDS_PREFERRED,
            ])->default(\App\Property::ALL);
            $table->enum('age_range', [
                \App\Property::ALL,
                \App\Property::AGE_18_30_ONLY,
                \App\Property::AGE_31_45_ONLY,
                \App\Property::AGE_45_ONLY,
                \App\Property::AGE_18_30_PREFERRED,
                \App\Property::AGE_31_45_PREFERRED,
                \App\Property::AGE_45_PREFERRED,
            ])->default(\App\Property::ALL);
            $table->unsignedBigInteger('rent_limit_id');
            $table->foreign('rent_limit_id')->references('id')->on('rent_limits');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
