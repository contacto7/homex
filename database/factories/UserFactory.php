<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $name = $faker->name;
    $last_name =  $faker->lastName;
    $id_fake =  $faker->numberBetween(10000,99999);

    return [
        'role_id'=>\App\Role::all()->random()->id,
        'name' => $name,
        'last_name' => $last_name,
        'slug' => Str::slug($name." ".$last_name." ".$id_fake,'-'),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'document_type_id'=>\App\DocumentType::all()->random()->id,
        'document_number'=> $faker->bothify('##??###??'),
        'nacionality_id'=>\App\Nacionality::all()->random()->id,
        'cellphone'=> $faker->phoneNumber,
        'picture'=> $faker->image(storage_path() . '/app/public/users',200,200, 'people', false),
        'sex'=> $faker->numberBetween(1,2),
        'state'=> $faker->numberBetween(1,2),
        'remember_token' => Str::random(10),
    ];
});
