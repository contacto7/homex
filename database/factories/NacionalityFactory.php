<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Nacionality;
use Faker\Generator as Faker;

$factory->define(Nacionality::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'code' => $faker->sentence
    ];
});
