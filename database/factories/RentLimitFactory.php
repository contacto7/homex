<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\RentLimit;
use Faker\Generator as Faker;

$factory->define(RentLimit::class, function (Faker $faker) {
    return [
        'lower_rent' => $faker->numberBetween(747,780),
        'upper_rent' => $faker->numberBetween(1558,1700)
    ];
});
