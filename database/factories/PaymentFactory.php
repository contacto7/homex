<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Payment;
use Faker\Generator as Faker;

$factory->define(Payment::class, function (Faker $faker) {
    return [
        'contract_id'=>\App\Contract::all()->random()->id,
        'emitted_date' => $faker->dateTimeBetween('-90 days', '-30 days'),
        'payed_date' => $faker->dateTimeBetween('+30 days', '+90 days'),
        'currency_id'=>\App\Currency::all()->random()->id,
        'amount_payed' => $faker->numberBetween(0, 500),
        'amount_total' => $faker->numberBetween(500,1000),
        'state'=> $faker->numberBetween(1,3),
    ];
});
