<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Property;
use Faker\Generator as Faker;

$factory->define(Property::class, function (Faker $faker) {
    return [
        'name'=> $faker->sentence,
        'description'=> $faker->text,
        'rooms'=> $faker->numberBetween(1,2),
        'user_id'=>\App\User::all()->random()->id,
        'district_id'=>\App\District::all()->random()->id,
        'property_type_id'=>\App\PropertyType::all()->random()->id,
        'parking_slots'=> $faker->numberBetween(1,4),
        'total_area'=> $faker->numberBetween(70,1200),
        'bathrooms'=> $faker->numberBetween(1,5),
        'half_bathrooms'=> $faker->numberBetween(1,5),
        'year'=> $faker->dateTimeBetween('-30 years', 'now'),
        'currency_rental_amount'=>\App\Currency::all()->random()->id,
        'rental_amount'=> $faker->numberBetween(600,5000),
        'currency_maintenance_amount'=>\App\Currency::all()->random()->id,
        'maintenance_amount'=> $faker->numberBetween(600,5000),
        'currency_guaranty_amount'=>\App\Currency::all()->random()->id,
        'guaranty_amount'=> $faker->numberBetween(600,5000),
        'latitude'=> $faker->latitude,
        'longitude'=> $faker->longitude,
        'address'=> $faker->address,
        'pet_allowed'=> $faker->numberBetween(1,2),
        'state'=> $faker->numberBetween(1,2),
        'only_genre'=> $faker->numberBetween(1,5),
        'only_students'=> $faker->numberBetween(1,3),
        'young_bonus'=> $faker->numberBetween(1,3),
        'family_status'=> $faker->numberBetween(1,3),
        'age_range'=> $faker->numberBetween(1,7),
        'rent_limit_id'=>\App\RentLimit::all()->random()->id,
    ];
});
