<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\LifestyleType;
use Faker\Generator as Faker;

$factory->define(LifestyleType::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence,
    ];
});
