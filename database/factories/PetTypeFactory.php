<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PetType;
use Faker\Generator as Faker;

$factory->define(PetType::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
