<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Search;
use Faker\Generator as Faker;

$factory->define(Search::class, function (Faker $faker) {
    return [
        'user_id'=>\App\User::all()->random()->id,
        'property_id'=>\App\Property::all()->random()->id,
        'searched_at'=>$faker->dateTimeBetween('-90 days', '-2 days'),
    ];
});
