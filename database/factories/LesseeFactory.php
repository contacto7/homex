<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Lessee;
use Faker\Generator as Faker;

$factory->define(Lessee::class, function (Faker $faker) {
    return [
        'user_id'=>\App\User::all()->random()->id,
        'monthly_salary'=> $faker->numberBetween(900,9000),
        'emotional_situation'=> $faker->numberBetween(1,3),
        'occupation_type_id'=> \App\OccupationType::all()->random()->id,
        'occupation_center'=> $faker->company,
        'lifestyle_type_id'=> \App\LifestyleType::all()->random()->id,
        'address'=> $faker->address,
        'district_id'=> \App\District::all()->random()->id,
        'quantity_members'=> $faker->numberBetween(0,10),
        'dependent_status'=> $faker->numberBetween(0,Lessee::DEPENDENT),
        'debtor_status'=> $faker->numberBetween(0,Lessee::DEBTOR),
        'other_postulation'=> $faker->numberBetween(0,Lessee::OTHER_POSTULATION),
        'bond_interested_status'=> $faker->numberBetween(0,Lessee::BOND_NOT_INTERESTED),
    ];
});
