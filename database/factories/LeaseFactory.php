<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Lease;
use Faker\Generator as Faker;

$factory->define(Lease::class, function (Faker $faker) {
    return [
        'application_id'=>\App\Application::all()->random()->id,
    ];
});
