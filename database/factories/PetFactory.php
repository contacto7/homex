<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Pet;
use Faker\Generator as Faker;

$factory->define(Pet::class, function (Faker $faker) {
    return [
        'size'=> $faker->numberBetween(1,3),
        'pet_type_id'=>\App\PetType::all()->random()->id,
        'lessees_id'=>\App\Lessee::all()->random()->id,
    ];
});
