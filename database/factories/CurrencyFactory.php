<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Currency;
use Faker\Generator as Faker;

$factory->define(Currency::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'symbol' => $faker->asciify('*'),
        'currency_code' => $faker->currencyCode,
    ];
});
