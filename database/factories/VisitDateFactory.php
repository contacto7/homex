<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\VisitDate;
use Faker\Generator as Faker;

$factory->define(VisitDate::class, function (Faker $faker) {
    return [
        'day' => $faker->dateTimeBetween('-90 days', '-2 days'),
        'duration' => $faker->numberBetween(15,60),
        'application_id' => \App\Role::all()->random()->id,
    ];
});
