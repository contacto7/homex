<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contract;
use Faker\Generator as Faker;

$factory->define(Contract::class, function (Faker $faker) {
    return [
        'start_date' => $faker->dateTimeBetween('-90 days', '-30 days'),
        'end_date' => $faker->dateTimeBetween('+30 days', '+90 days'),
        'pay_date' => $faker->numberBetween(1,30),
        'months_date' => $faker->numberBetween(3,12),
        'type' => $faker->numberBetween(1,2),
        'currency_id'=>\App\Currency::all()->random()->id,
        'amount' => $faker->numberBetween(450,5000),
        'lease_id'=>\App\Lease::all()->random()->id,
        'document' => 'contract_model.pdf',
        'state' => 1,
    ];
});
