<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Application;
use Faker\Generator as Faker;

$factory->define(Application::class, function (Faker $faker) {
    return [
        'date' => $faker->dateTimeBetween('-90 days', '-2 days'),
        'property_id' => \App\Property::all()->random()->id,
        'user_id' => \App\User::all()->random()->id,
        'stage' => $faker->numberBetween(1,5),
        'stage_state' => $faker->numberBetween(Application::INITIAL,Application::DISAPPROVED),
    ];
});
