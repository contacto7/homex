<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OccupationType;
use Faker\Generator as Faker;

$factory->define(OccupationType::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
