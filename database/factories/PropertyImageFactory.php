<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PropertyImage;
use Faker\Generator as Faker;

$factory->define(PropertyImage::class, function (Faker $faker) {
    return [
        'picture'=> \Faker\Provider\Image::image(storage_path(). '/app/public/properties',600,350,'city',false),
        'order'=> $faker->numberBetween(1,4),
        'property_id'=>\App\Property::all()->random()->id,
    ];
});
