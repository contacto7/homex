<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Propietary;
use Faker\Generator as Faker;

$factory->define(Propietary::class, function (Faker $faker) {
    return [
        'user_id'=>\App\User::all()->random()->id,
        'biography'=> $faker->text,
    ];
});
