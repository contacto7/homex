<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        if (!File::exists('users')) {
            Storage::makeDirectory('users');
        }
        if (!File::exists('properties')) {
            Storage::makeDirectory('properties');
        }
        if (!File::exists('contracts')) {
            Storage::makeDirectory('contracts');
        }


        //////////NACIONALITIES
        factory(\App\Nacionality::class, 1)->create(['name' => 'Andorra', 'code' => 'AD']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Emiratos Árabes Unidos', 'code' => 'AE']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Afganistán', 'code' => 'AF']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Antigua y Barbuda', 'code' => 'AG']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Anguila', 'code' => 'AI']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Albania', 'code' => 'AL']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Armenia', 'code' => 'AM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Antillas Neerlandesas', 'code' => 'AN']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Angola', 'code' => 'AO']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Antártida', 'code' => 'AQ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Argentina', 'code' => 'AR']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Samoa Americana', 'code' => 'AS']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Austria', 'code' => 'AT']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Australia', 'code' => 'AU']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Aruba', 'code' => 'AW']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Áland', 'code' => 'AX']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Azerbaiyán', 'code' => 'AZ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Bosnia y Herzegovina', 'code' => 'BA']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Barbados', 'code' => 'BB']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Bangladesh', 'code' => 'BD']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Bélgica', 'code' => 'BE']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Burkina Faso', 'code' => 'BF']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Bulgaria', 'code' => 'BG']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Bahréin', 'code' => 'BH']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Burundi', 'code' => 'BI']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Benin', 'code' => 'BJ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'San Bartolomé', 'code' => 'BL']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Bermudas', 'code' => 'BM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Brunéi', 'code' => 'BN']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Bolivia', 'code' => 'BO']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Brasil', 'code' => 'BR']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Bahamas', 'code' => 'BS']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Bhután', 'code' => 'BT']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Isla Bouvet', 'code' => 'BV']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Botsuana', 'code' => 'BW']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Belarús', 'code' => 'BY']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Belice', 'code' => 'BZ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Canadá', 'code' => 'CA']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Cocos', 'code' => 'CC']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'República Centro-Africana', 'code' => 'CF']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Congo', 'code' => 'CG']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Suiza', 'code' => 'CH']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Costa de Marfil', 'code' => 'CI']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Cook', 'code' => 'CK']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Chile', 'code' => 'CL']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Camerún', 'code' => 'CM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'China', 'code' => 'CN']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Colombia', 'code' => 'CO']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Costa Rica', 'code' => 'CR']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Cuba', 'code' => 'CU']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Cabo Verde', 'code' => 'CV']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Christmas', 'code' => 'CX']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Chipre', 'code' => 'CY']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'República Checa', 'code' => 'CZ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Alemania', 'code' => 'DE']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Yibuti', 'code' => 'DJ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Dinamarca', 'code' => 'DK']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Domínica', 'code' => 'DM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'República Dominicana', 'code' => 'DO']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Argel', 'code' => 'DZ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Ecuador', 'code' => 'EC']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Estonia', 'code' => 'EE']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Egipto', 'code' => 'EG']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Sahara Occidental', 'code' => 'EH']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Eritrea', 'code' => 'ER']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'España', 'code' => 'ES']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Etiopía', 'code' => 'ET']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Finlandia', 'code' => 'FI']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Fiji', 'code' => 'FJ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Malvinas', 'code' => 'FK']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Micronesia', 'code' => 'FM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Faroe', 'code' => 'FO']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Francia', 'code' => 'FR']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Gabón', 'code' => 'GA']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Reino Unido', 'code' => 'GB']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Granada', 'code' => 'GD']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Georgia', 'code' => 'GE']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Guayana Francesa', 'code' => 'GF']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Guernsey', 'code' => 'GG']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Ghana', 'code' => 'GH']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Gibraltar', 'code' => 'GI']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Groenlandia', 'code' => 'GL']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Gambia', 'code' => 'GM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Guinea', 'code' => 'GN']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Guadalupe', 'code' => 'GP']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Guinea Ecuatorial', 'code' => 'GQ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Grecia', 'code' => 'GR']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Georgia del Sur e Islas Sandwich del Sur', 'code' => 'GS']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Guatemala', 'code' => 'GT']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Guam', 'code' => 'GU']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Guinea-Bissau', 'code' => 'GW']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Guayana', 'code' => 'GY']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Hong Kong', 'code' => 'HK']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Heard y McDonald', 'code' => 'HM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Honduras', 'code' => 'HN']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Croacia', 'code' => 'HR']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Haití', 'code' => 'HT']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Hungría', 'code' => 'HU']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Indonesia', 'code' => 'ID']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Irlanda', 'code' => 'IE']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Israel', 'code' => 'IL']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Isla de Man', 'code' => 'IM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'India', 'code' => 'IN']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Territorio Británico del Océano Índico', 'code' => 'IO']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Irak', 'code' => 'IQ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Irán', 'code' => 'IR']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islandia', 'code' => 'IS']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Italia', 'code' => 'IT']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Jersey', 'code' => 'JE']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Jamaica', 'code' => 'JM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Jordania', 'code' => 'JO']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Japón', 'code' => 'JP']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Kenia', 'code' => 'KE']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Kirguistán', 'code' => 'KG']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Camboya', 'code' => 'KH']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Kiribati', 'code' => 'KI']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Comoros', 'code' => 'KM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'San Cristóbal y Nieves', 'code' => 'KN']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Corea del Norte', 'code' => 'KP']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Corea del Sur', 'code' => 'KR']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Kuwait', 'code' => 'KW']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Caimán', 'code' => 'KY']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Kazajstán', 'code' => 'KZ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Laos', 'code' => 'LA']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Líbano', 'code' => 'LB']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Santa Lucía', 'code' => 'LC']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Liechtenstein', 'code' => 'LI']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Sri Lanka', 'code' => 'LK']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Liberia', 'code' => 'LR']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Lesotho', 'code' => 'LS']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Lituania', 'code' => 'LT']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Luxemburgo', 'code' => 'LU']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Letonia', 'code' => 'LV']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Libia', 'code' => 'LY']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Marruecos', 'code' => 'MA']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Mónaco', 'code' => 'MC']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Moldova', 'code' => 'MD']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Montenegro', 'code' => 'ME']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Madagascar', 'code' => 'MG']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Marshall', 'code' => 'MH']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Macedonia', 'code' => 'MK']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Mali', 'code' => 'ML']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Myanmar', 'code' => 'MM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Mongolia', 'code' => 'MN']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Macao', 'code' => 'MO']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Martinica', 'code' => 'MQ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Mauritania', 'code' => 'MR']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Montserrat', 'code' => 'MS']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Malta', 'code' => 'MT']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Mauricio', 'code' => 'MU']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Maldivas', 'code' => 'MV']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Malawi', 'code' => 'MW']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'México', 'code' => 'MX']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Malasia', 'code' => 'MY']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Mozambique', 'code' => 'MZ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Namibia', 'code' => 'NA']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Nueva Caledonia', 'code' => 'NC']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Níger', 'code' => 'NE']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Norkfolk', 'code' => 'NF']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Nigeria', 'code' => 'NG']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Nicaragua', 'code' => 'NI']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Países Bajos', 'code' => 'NL']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Noruega', 'code' => 'NO']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Nepal', 'code' => 'NP']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Nauru', 'code' => 'NR']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Niue', 'code' => 'NU']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Nueva Zelanda', 'code' => 'NZ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Omán', 'code' => 'OM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Panamá', 'code' => 'PA']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Perú', 'code' => 'PE']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Polinesia Francesa', 'code' => 'PF']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Papúa Nueva Guinea', 'code' => 'PG']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Filipinas', 'code' => 'PH']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Pakistán', 'code' => 'PK']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Polonia', 'code' => 'PL']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'San Pedro y Miquelón', 'code' => 'PM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Pitcairn', 'code' => 'PN']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Puerto Rico', 'code' => 'PR']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Palestina', 'code' => 'PS']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Portugal', 'code' => 'PT']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Palaos', 'code' => 'PW']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Paraguay', 'code' => 'PY']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Qatar', 'code' => 'QA']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Reunión', 'code' => 'RE']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Rumanía', 'code' => 'RO']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Serbia y Montenegro', 'code' => 'RS']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Rusia', 'code' => 'RU']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Ruanda', 'code' => 'RW']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Arabia Saudita', 'code' => 'SA']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Solomón', 'code' => 'SB']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Seychelles', 'code' => 'SC']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Sudán', 'code' => 'SD']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Suecia', 'code' => 'SE']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Singapur', 'code' => 'SG']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Santa Elena', 'code' => 'SH']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Eslovenia', 'code' => 'SI']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Svalbard y Jan Mayen', 'code' => 'SJ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Eslovaquia', 'code' => 'SK']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Sierra Leona', 'code' => 'SL']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'San Marino', 'code' => 'SM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Senegal', 'code' => 'SN']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Somalia', 'code' => 'SO']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Surinam', 'code' => 'SR']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Santo Tomé y Príncipe', 'code' => 'ST']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'El Salvador', 'code' => 'SV']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Siria', 'code' => 'SY']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Suazilandia', 'code' => 'SZ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Turcas y Caicos', 'code' => 'TC']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Chad', 'code' => 'TD']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Territorios Australes Franceses', 'code' => 'TF']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Togo', 'code' => 'TG']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Tailandia', 'code' => 'TH']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Tanzania', 'code' => 'TH']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Tayikistán', 'code' => 'TJ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Tokelau', 'code' => 'TK']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Timor-Leste', 'code' => 'TL']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Turkmenistán', 'code' => 'TM']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Túnez', 'code' => 'TN']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Tonga', 'code' => 'TO']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Turquía', 'code' => 'TR']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Trinidad y Tobago', 'code' => 'TT']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Tuvalu', 'code' => 'TV']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Taiwán', 'code' => 'TW']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Ucrania', 'code' => 'UA']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Uganda', 'code' => 'UG']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Estados Unidos de América', 'code' => 'US']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Uruguay', 'code' => 'UY']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Uzbekistán', 'code' => 'UZ']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Ciudad del Vaticano', 'code' => 'VA']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'San Vicente y las Granadinas', 'code' => 'VC']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Venezuela', 'code' => 'VE']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Vírgenes Británicas', 'code' => 'VG']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Islas Vírgenes de los Estados Unidos de América', 'code' => 'VI']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Vietnam', 'code' => 'VN']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Vanuatu', 'code' => 'VU']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Wallis y Futuna', 'code' => 'WF']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Samoa', 'code' => 'WS']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Yemen', 'code' => 'YE']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Mayotte', 'code' => 'YT']);
        factory(\App\Nacionality::class, 1)->create(['name' => 'Sudáfrica', 'code' => 'ZA']);

        //////////DOCUMENT TYPES
        factory(\App\DocumentType::class, 1)->create([
            'name' => 'Documento Nacional de Identidad',
            'code' => 'DNI'
        ]);
        factory(\App\DocumentType::class, 1)->create([
            'name' => 'Carné de Extranjería',
            'code' => 'CE'
        ]);
        factory(\App\DocumentType::class, 1)->create([
            'name' => 'Pasaporte',
            'code' => 'PA'
        ]);

        //////////ROLES
        factory(\App\Role::class, 1)->create([
            'name' => 'admin'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'propietary'
        ]);
        factory(\App\Role::class, 1)->create([
            'name' => 'lessee'
        ]);

        //////////USERS
        factory(\App\User::class, 1)->create([
            'name' => 'Fernando',
            'email' => 'fvivancol@gmail.com',
            'password' => 'secret',
            'role_id' => \App\Role::ADMIN,
            'state' => \App\User::ACTIVE,
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Invitado',
            'email' => 'guest@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::ADMIN,
            'state' => \App\User::ACTIVE,
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Admin',
            'email' => 'admin@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::ADMIN,
            'state' => \App\User::ACTIVE,
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Propietario',
            'email' => 'propietario@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::PROPIETARY,
            'state' => \App\User::ACTIVE,
        ]);
        factory(\App\User::class, 1)->create([
            'name' => 'Arrendatario',
            'email' => 'arrendatario@mail.com',
            'password' => 'secret',
            'role_id' => \App\Role::LESSEE,
            'state' => \App\User::ACTIVE,
        ]);
        
        //////////CURRENCIES
        factory(\App\Currency::class, 1)->create([
            'name' => 'Soles',
            'symbol' => 'S/',
            'currency_code' => 'PEN',
        ]);
        factory(\App\Currency::class, 1)->create([
            'name' => 'Dólares',
            'symbol' => '$',
            'currency_code' => 'USD',
        ]);

        //////////OCCUPATION TYPE
        factory(\App\OccupationType::class, 1)->create([
            'name' => 'Estudiante'
        ]);
        factory(\App\OccupationType::class, 1)->create([
            'name' => 'Trabajador'
        ]);

        //////////LIFESTYLE TYPE
        factory(\App\LifestyleType::class, 1)->create([
            'name' => 'Social',
            'description' => 'Hago reuniones en casa.'
        ]);
        factory(\App\LifestyleType::class, 1)->create([
            'name' => 'Hogareño',
            'description' => 'Suelo estar en casa todo el día y noche.'
        ]);
        factory(\App\LifestyleType::class, 1)->create([
            'name' => 'Oficinista',
            'description' => 'Salgo temprano al trabajo y regreso en la noche.'
        ]);
        factory(\App\LifestyleType::class, 1)->create([
            'name' => 'Saludable',
            'description' => 'Salgo a correr temprano.'
        ]);
        factory(\App\LifestyleType::class, 1)->create([
            'name' => 'Estudiante',
            'description' => 'Salgo a menudo a la universidad.'
        ]);
        factory(\App\LifestyleType::class, 1)->create([
            'name' => 'Fiestero',
            'description' => 'Salgo los fines de semana a divertirme.'
        ]);

        //////////PET TYPES
        factory(\App\PetType::class, 1)->create([
            'name' => 'Perro'
        ]);
        factory(\App\PetType::class, 1)->create([
            'name' => 'Gato'
        ]);
        factory(\App\PetType::class, 1)->create([
            'name' => 'Loro'
        ]);
        factory(\App\PetType::class, 1)->create([
            'name' => 'Conejo'
        ]);
        factory(\App\PetType::class, 1)->create([
            'name' => 'Hamster'
        ]);
        factory(\App\PetType::class, 1)->create([
            'name' => 'Pez'
        ]);
        factory(\App\PetType::class, 1)->create([
            'name' => 'Otro'
        ]);

        //////////RENT LIMITS
        factory(\App\RentLimit::class, 1)->create([
            'lower_rent' => 747.60,
            'upper_rent' => 1558.20
        ]);

        //////////PROPERTY TYPES
        factory(\App\PropertyType::class, 1)->create([
            'name' => 'departamento'
        ]);
        factory(\App\PropertyType::class, 1)->create([
            'name' => 'Casa'
        ]);
        factory(\App\PropertyType::class, 1)->create([
            'name' => 'Dúplex'
        ]);
        factory(\App\PropertyType::class, 1)->create([
            'name' => 'Triplex'
        ]);
        factory(\App\PropertyType::class, 1)->create([
            'name' => 'Oficina'
        ]);
        factory(\App\PropertyType::class, 1)->create([
            'name' => 'PentHouse'
        ]);
        factory(\App\PropertyType::class, 1)->create([
            'name' => 'Casa de Playa'
        ]);
        factory(\App\PropertyType::class, 1)->create([
            'name' => 'Departamento de Playa'
        ]);

        //DEPARTMENTS
        factory(\App\Department::class, 1)->create([
            'id' => 1,
            'name' => 'Amazonas',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 2,
            'name' => 'Ancash',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 3,
            'name' => 'Apurimac',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 4,
            'name' => 'Arequipa',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 5,
            'name' => 'Ayacucho',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 6,
            'name' => 'Cajamarca',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 7,
            'name' => 'Callao',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 8,
            'name' => 'Cusco',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 9,
            'name' => 'Huancavelica',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 10,
            'name' => 'Huanuco',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 11,
            'name' => 'Ica',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 12,
            'name' => 'Junin',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 13,
            'name' => 'La Libertad',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 14,
            'name' => 'Lambayeque',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 15,
            'name' => 'Lima',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 16,
            'name' => 'Loreto',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 17,
            'name' => 'Madre de Dios',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 18,
            'name' => 'Moquegua',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 19,
            'name' => 'Pasco',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 20,
            'name' => 'Piura',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 21,
            'name' => 'Puno',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 22,
            'name' => 'San Martin',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 23,
            'name' => 'Tacna',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 24,
            'name' => 'Tumbes',
        ]);
        factory(\App\Department::class, 1)->create([
            'id' => 25,
            'name' => 'Ucayali',
        ]);

        //PROVINCES
        factory(\App\Province::class, 1)->create([
            'id' => 1,
            'department_id' => 1,
            'name' => 'Chachapoyas',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 2,
            'department_id' => 1,
            'name' => 'Bagua',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 3,
            'department_id' => 1,
            'name' => 'Bongara',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 4,
            'department_id' => 1,
            'name' => 'Condorcanqui',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 5,
            'department_id' => 1,
            'name' => 'Luya',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 6,
            'department_id' => 1,
            'name' => 'Rodriguez de Mendoza',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 7,
            'department_id' => 1,
            'name' => 'Utcubamba',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 8,
            'department_id' => 2,
            'name' => 'Huaraz',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 9,
            'department_id' => 2,
            'name' => 'Aija',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 10,
            'department_id' => 2,
            'name' => 'Antonio Raymondi',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 11,
            'department_id' => 2,
            'name' => 'Asuncion',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 12,
            'department_id' => 2,
            'name' => 'Bolognesi',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 13,
            'department_id' => 2,
            'name' => 'Carhuaz',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 14,
            'department_id' => 2,
            'name' => 'Carlos Fermin Fitzca',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 15,
            'department_id' => 2,
            'name' => 'Casma',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 16,
            'department_id' => 2,
            'name' => 'Corongo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 17,
            'department_id' => 2,
            'name' => 'Huari',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 18,
            'department_id' => 2,
            'name' => 'Huarmey',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 19,
            'department_id' => 2,
            'name' => 'Huaylas',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 20,
            'department_id' => 2,
            'name' => 'Mariscal Luzuriaga',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 21,
            'department_id' => 2,
            'name' => 'Ocros',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 22,
            'department_id' => 2,
            'name' => 'Pallasca',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 23,
            'department_id' => 2,
            'name' => 'Pomabamba',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 24,
            'department_id' => 2,
            'name' => 'Recuay',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 25,
            'department_id' => 2,
            'name' => 'Santa',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 26,
            'department_id' => 2,
            'name' => 'Sihuas',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 27,
            'department_id' => 2,
            'name' => 'Yungay',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 28,
            'department_id' => 3,
            'name' => 'Abancay',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 29,
            'department_id' => 3,
            'name' => 'Andahuaylas',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 30,
            'department_id' => 3,
            'name' => 'Antabamba',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 31,
            'department_id' => 3,
            'name' => 'Aymaraes',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 32,
            'department_id' => 3,
            'name' => 'Cotabambas',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 33,
            'department_id' => 3,
            'name' => 'Chincheros',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 34,
            'department_id' => 3,
            'name' => 'Grau',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 35,
            'department_id' => 4,
            'name' => 'Arequipa',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 36,
            'department_id' => 4,
            'name' => 'Camana',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 37,
            'department_id' => 4,
            'name' => 'Caraveli',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 38,
            'department_id' => 4,
            'name' => 'Castilla',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 39,
            'department_id' => 4,
            'name' => 'Caylloma',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 40,
            'department_id' => 4,
            'name' => 'Condesuyos',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 41,
            'department_id' => 4,
            'name' => 'Islay',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 42,
            'department_id' => 4,
            'name' => 'La Union',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 43,
            'department_id' => 5,
            'name' => 'Huamanga',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 44,
            'department_id' => 5,
            'name' => 'Cangallo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 45,
            'department_id' => 5,
            'name' => 'Huanca Sancos',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 46,
            'department_id' => 5,
            'name' => 'Huanta',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 47,
            'department_id' => 5,
            'name' => 'La Mar',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 48,
            'department_id' => 5,
            'name' => 'Lucanas',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 49,
            'department_id' => 5,
            'name' => 'Parinacochas',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 50,
            'department_id' => 5,
            'name' => 'Paucar del Sara Sara',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 51,
            'department_id' => 5,
            'name' => 'Sucre',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 52,
            'department_id' => 5,
            'name' => 'Victor Fajardo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 53,
            'department_id' => 5,
            'name' => 'Vilcas Huaman',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 54,
            'department_id' => 6,
            'name' => 'Cajamarca',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 55,
            'department_id' => 6,
            'name' => 'Cajabamba',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 56,
            'department_id' => 6,
            'name' => 'Celendin',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 57,
            'department_id' => 6,
            'name' => 'Chota',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 58,
            'department_id' => 6,
            'name' => 'Contumaza',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 59,
            'department_id' => 6,
            'name' => 'Cutervo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 60,
            'department_id' => 6,
            'name' => 'Hualgayoc',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 61,
            'department_id' => 6,
            'name' => 'Jaen',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 62,
            'department_id' => 6,
            'name' => 'San Ignacio',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 63,
            'department_id' => 6,
            'name' => 'San Marcos',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 64,
            'department_id' => 6,
            'name' => 'San Miguel',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 65,
            'department_id' => 6,
            'name' => 'San Pablo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 66,
            'department_id' => 6,
            'name' => 'Santa Cruz',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 67,
            'department_id' => 7,
            'name' => 'Callao',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 68,
            'department_id' => 8,
            'name' => 'Cusco',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 69,
            'department_id' => 8,
            'name' => 'Acomayo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 70,
            'department_id' => 8,
            'name' => 'Anta',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 71,
            'department_id' => 8,
            'name' => 'Calca',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 72,
            'department_id' => 8,
            'name' => 'Canas',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 73,
            'department_id' => 8,
            'name' => 'Canchis',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 74,
            'department_id' => 8,
            'name' => 'Chumbivilcas',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 75,
            'department_id' => 8,
            'name' => 'Espinar',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 76,
            'department_id' => 8,
            'name' => 'La Convencion',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 77,
            'department_id' => 8,
            'name' => 'Paruro',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 78,
            'department_id' => 8,
            'name' => 'Paucartambo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 79,
            'department_id' => 8,
            'name' => 'Quispicanchi',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 80,
            'department_id' => 8,
            'name' => 'Urubamba',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 81,
            'department_id' => 9,
            'name' => 'Huancavelica',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 82,
            'department_id' => 9,
            'name' => 'Acobamba',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 83,
            'department_id' => 9,
            'name' => 'Angaraes',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 84,
            'department_id' => 9,
            'name' => 'Castrovirreyna',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 85,
            'department_id' => 9,
            'name' => 'Churcampa',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 86,
            'department_id' => 9,
            'name' => 'Huaytara',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 87,
            'department_id' => 9,
            'name' => 'Tayacaja',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 88,
            'department_id' => 10,
            'name' => 'Huanuco',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 89,
            'department_id' => 10,
            'name' => 'Ambo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 90,
            'department_id' => 10,
            'name' => 'Dos de Mayo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 91,
            'department_id' => 10,
            'name' => 'Huacaybamba',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 92,
            'department_id' => 10,
            'name' => 'Huamalies',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 93,
            'department_id' => 10,
            'name' => 'Leoncio Prado',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 94,
            'department_id' => 10,
            'name' => 'Marañon',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 95,
            'department_id' => 10,
            'name' => 'Pachitea',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 96,
            'department_id' => 10,
            'name' => 'Puerto Inca',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 97,
            'department_id' => 10,
            'name' => 'Lauricocha',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 98,
            'department_id' => 10,
            'name' => 'Yarowilca',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 99,
            'department_id' => 11,
            'name' => 'Ica',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 100,
            'department_id' => 11,
            'name' => 'Chincha',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 101,
            'department_id' => 11,
            'name' => 'Nazca',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 102,
            'department_id' => 11,
            'name' => 'Palpa',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 103,
            'department_id' => 11,
            'name' => 'Pisco',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 104,
            'department_id' => 12,
            'name' => 'Huancayo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 105,
            'department_id' => 12,
            'name' => 'Concepcion',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 106,
            'department_id' => 12,
            'name' => 'Chanchamayo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 107,
            'department_id' => 12,
            'name' => 'Jauja',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 108,
            'department_id' => 12,
            'name' => 'Junin',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 109,
            'department_id' => 12,
            'name' => 'Satipo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 110,
            'department_id' => 12,
            'name' => 'Tarma',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 111,
            'department_id' => 12,
            'name' => 'Yauli',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 112,
            'department_id' => 12,
            'name' => 'Chupaca',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 113,
            'department_id' => 13,
            'name' => 'Trujillo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 114,
            'department_id' => 13,
            'name' => 'Ascope',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 115,
            'department_id' => 13,
            'name' => 'Bolivar',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 116,
            'department_id' => 13,
            'name' => 'Chepen',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 117,
            'department_id' => 13,
            'name' => 'Julcan',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 118,
            'department_id' => 13,
            'name' => 'Otuzco',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 119,
            'department_id' => 13,
            'name' => 'Pacasmayo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 120,
            'department_id' => 13,
            'name' => 'Pataz',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 121,
            'department_id' => 13,
            'name' => 'Sanchez Carrion',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 122,
            'department_id' => 13,
            'name' => 'Santiago de Chuco',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 123,
            'department_id' => 13,
            'name' => 'Gran Chimu',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 124,
            'department_id' => 13,
            'name' => 'Viru',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 125,
            'department_id' => 14,
            'name' => 'Chiclayo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 126,
            'department_id' => 14,
            'name' => 'Ferreñafe',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 127,
            'department_id' => 14,
            'name' => 'Lambayeque',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 128,
            'department_id' => 15,
            'name' => 'Lima',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 129,
            'department_id' => 15,
            'name' => 'Barranca',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 130,
            'department_id' => 15,
            'name' => 'Cajatambo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 131,
            'department_id' => 15,
            'name' => 'Canta',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 132,
            'department_id' => 15,
            'name' => 'Cañete',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 133,
            'department_id' => 15,
            'name' => 'Huaral',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 134,
            'department_id' => 15,
            'name' => 'Huarochiri',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 135,
            'department_id' => 15,
            'name' => 'Huaura',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 136,
            'department_id' => 15,
            'name' => 'Oyon',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 137,
            'department_id' => 15,
            'name' => 'Yauyos',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 138,
            'department_id' => 16,
            'name' => 'Maynas',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 139,
            'department_id' => 16,
            'name' => 'Alto Amazonas',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 140,
            'department_id' => 16,
            'name' => 'Loreto',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 141,
            'department_id' => 16,
            'name' => 'Mariscal Ramon Castilla',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 142,
            'department_id' => 16,
            'name' => 'Requena',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 143,
            'department_id' => 16,
            'name' => 'Ucayali',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 144,
            'department_id' => 16,
            'name' => 'Datem del Marañon',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 145,
            'department_id' => 16,
            'name' => 'Maynas',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 146,
            'department_id' => 17,
            'name' => 'Tambopata',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 147,
            'department_id' => 17,
            'name' => 'Manu',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 148,
            'department_id' => 17,
            'name' => 'Tahuamanu',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 149,
            'department_id' => 18,
            'name' => 'Mariscal Nieto',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 150,
            'department_id' => 18,
            'name' => 'General Sanchez Cerr',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 151,
            'department_id' => 18,
            'name' => 'Ilo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 152,
            'department_id' => 19,
            'name' => 'Pasco',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 153,
            'department_id' => 19,
            'name' => 'Daniel Alcides Carri',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 154,
            'department_id' => 19,
            'name' => 'Oxapampa',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 155,
            'department_id' => 20,
            'name' => 'Piura',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 156,
            'department_id' => 20,
            'name' => 'Ayabaca',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 157,
            'department_id' => 20,
            'name' => 'Huancabamba',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 158,
            'department_id' => 20,
            'name' => 'Morropon',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 159,
            'department_id' => 20,
            'name' => 'Paita',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 160,
            'department_id' => 20,
            'name' => 'Sullana',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 161,
            'department_id' => 20,
            'name' => 'Talara',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 162,
            'department_id' => 20,
            'name' => 'Sechura',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 163,
            'department_id' => 21,
            'name' => 'Puno',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 164,
            'department_id' => 21,
            'name' => 'Azangaro',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 165,
            'department_id' => 21,
            'name' => 'Carabaya',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 166,
            'department_id' => 21,
            'name' => 'Chucuito',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 167,
            'department_id' => 21,
            'name' => 'El Collao',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 168,
            'department_id' => 21,
            'name' => 'Huancane',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 169,
            'department_id' => 21,
            'name' => 'Lampa',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 170,
            'department_id' => 21,
            'name' => 'Melgar',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 171,
            'department_id' => 21,
            'name' => 'Moho',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 172,
            'department_id' => 21,
            'name' => 'San Antonio de Putin',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 173,
            'department_id' => 21,
            'name' => 'San Roman',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 174,
            'department_id' => 21,
            'name' => 'Sandia',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 175,
            'department_id' => 21,
            'name' => 'Yunguyo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 176,
            'department_id' => 22,
            'name' => 'Moyobamba',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 177,
            'department_id' => 22,
            'name' => 'Bellavista',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 178,
            'department_id' => 22,
            'name' => 'El Dorado',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 179,
            'department_id' => 22,
            'name' => 'Huallaga',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 180,
            'department_id' => 22,
            'name' => 'Lamas',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 181,
            'department_id' => 22,
            'name' => 'Mariscal Caceres',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 182,
            'department_id' => 22,
            'name' => 'Picota',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 183,
            'department_id' => 22,
            'name' => 'Rioja',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 184,
            'department_id' => 22,
            'name' => 'San Martin',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 185,
            'department_id' => 22,
            'name' => 'Tocache',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 186,
            'department_id' => 23,
            'name' => 'Tacna',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 187,
            'department_id' => 23,
            'name' => 'Candarave',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 188,
            'department_id' => 23,
            'name' => 'Jorge Basadre',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 189,
            'department_id' => 23,
            'name' => 'Tarata',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 190,
            'department_id' => 24,
            'name' => 'Tumbes',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 191,
            'department_id' => 24,
            'name' => 'Contralmirante Villa',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 192,
            'department_id' => 24,
            'name' => 'Zarumilla',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 193,
            'department_id' => 25,
            'name' => 'Coronel Portillo',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 194,
            'department_id' => 25,
            'name' => 'Atalaya',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 195,
            'department_id' => 25,
            'name' => 'Padre Abad',
        ]);
        factory(\App\Province::class, 1)->create([
            'id' => 196,
            'department_id' => 25,
            'name' => 'Purus',
        ]);

        //DISTRITOS
        factory(\App\District::class, 1)->create([
            'id' => 1,
            'province_id' => 1,
            'name' => 'Chachapoyas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 2,
            'province_id' => 1,
            'name' => 'Asuncion',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 3,
            'province_id' => 1,
            'name' => 'Balsas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 4,
            'province_id' => 1,
            'name' => 'Cheto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 5,
            'province_id' => 1,
            'name' => 'Chiliquin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 6,
            'province_id' => 1,
            'name' => 'Chuquibamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 7,
            'province_id' => 1,
            'name' => 'Granada',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 8,
            'province_id' => 1,
            'name' => 'Huancas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 9,
            'province_id' => 1,
            'name' => 'La Jalca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 10,
            'province_id' => 1,
            'name' => 'Leimebamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 11,
            'province_id' => 1,
            'name' => 'Levanto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 12,
            'province_id' => 1,
            'name' => 'Magdalena',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 13,
            'province_id' => 1,
            'name' => 'Mariscal Castilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 14,
            'province_id' => 1,
            'name' => 'Molinopampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 15,
            'province_id' => 1,
            'name' => 'Montevideo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 16,
            'province_id' => 1,
            'name' => 'Olleros',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 17,
            'province_id' => 1,
            'name' => 'Quinjalca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 18,
            'province_id' => 1,
            'name' => 'San Francisco de Daguas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 19,
            'province_id' => 1,
            'name' => 'San Isidro de Maino',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 20,
            'province_id' => 1,
            'name' => 'Soloco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 21,
            'province_id' => 1,
            'name' => 'Sonche',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 22,
            'province_id' => 2,
            'name' => 'Bagua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 23,
            'province_id' => 2,
            'name' => 'Aramango',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 24,
            'province_id' => 2,
            'name' => 'Copallin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 25,
            'province_id' => 2,
            'name' => 'El Parco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 26,
            'province_id' => 2,
            'name' => 'Imaza',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 27,
            'province_id' => 2,
            'name' => 'La Peca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 28,
            'province_id' => 3,
            'name' => 'Jumbilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 29,
            'province_id' => 3,
            'name' => 'Chisquilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 30,
            'province_id' => 3,
            'name' => 'Churuja',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 31,
            'province_id' => 3,
            'name' => 'Corosha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 32,
            'province_id' => 3,
            'name' => 'Cuispes',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 33,
            'province_id' => 3,
            'name' => 'Florida',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 34,
            'province_id' => 3,
            'name' => 'Jazan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 35,
            'province_id' => 3,
            'name' => 'Recta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 36,
            'province_id' => 3,
            'name' => 'San Carlos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 37,
            'province_id' => 3,
            'name' => 'Shipasbamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 38,
            'province_id' => 3,
            'name' => 'Valera',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 39,
            'province_id' => 3,
            'name' => 'Yambrasbamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 40,
            'province_id' => 4,
            'name' => 'Nieva',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 41,
            'province_id' => 4,
            'name' => 'El Cenepa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 42,
            'province_id' => 4,
            'name' => 'Rio Santiago',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 43,
            'province_id' => 5,
            'name' => 'Lamud',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 44,
            'province_id' => 5,
            'name' => 'Camporredondo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 45,
            'province_id' => 5,
            'name' => 'Cocabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 46,
            'province_id' => 5,
            'name' => 'Colcamar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 47,
            'province_id' => 5,
            'name' => 'Conila',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 48,
            'province_id' => 5,
            'name' => 'Inguilpata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 49,
            'province_id' => 5,
            'name' => 'Longuita',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 50,
            'province_id' => 5,
            'name' => 'Lonya Chico',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 51,
            'province_id' => 5,
            'name' => 'Luya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 52,
            'province_id' => 5,
            'name' => 'Luya Viejo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 53,
            'province_id' => 5,
            'name' => 'Maria',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 54,
            'province_id' => 5,
            'name' => 'Ocalli',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 55,
            'province_id' => 5,
            'name' => 'Ocumal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 56,
            'province_id' => 5,
            'name' => 'Pisuquia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 57,
            'province_id' => 5,
            'name' => 'Providencia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 58,
            'province_id' => 5,
            'name' => 'San Cristobal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 59,
            'province_id' => 5,
            'name' => 'San Francisco del Yeso',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 60,
            'province_id' => 5,
            'name' => 'San Jeronimo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 61,
            'province_id' => 5,
            'name' => 'San Juan de Lopecancha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 62,
            'province_id' => 5,
            'name' => 'Santa Catalina',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 63,
            'province_id' => 5,
            'name' => 'Santo Tomas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 64,
            'province_id' => 5,
            'name' => 'Tingo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 65,
            'province_id' => 5,
            'name' => 'Trita',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 66,
            'province_id' => 6,
            'name' => 'San Nicolas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 67,
            'province_id' => 6,
            'name' => 'Chirimoto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 68,
            'province_id' => 6,
            'name' => 'Cochamal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 69,
            'province_id' => 6,
            'name' => 'Huambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 70,
            'province_id' => 6,
            'name' => 'Limabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 71,
            'province_id' => 6,
            'name' => 'Longar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 72,
            'province_id' => 6,
            'name' => 'Mariscal Benavides',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 73,
            'province_id' => 6,
            'name' => 'Milpuc',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 74,
            'province_id' => 6,
            'name' => 'Omia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 75,
            'province_id' => 6,
            'name' => 'Santa Rosa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 76,
            'province_id' => 6,
            'name' => 'Totora',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 77,
            'province_id' => 6,
            'name' => 'Vista Alegre',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 78,
            'province_id' => 7,
            'name' => 'Bagua Grande',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 79,
            'province_id' => 7,
            'name' => 'Cajaruro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 80,
            'province_id' => 7,
            'name' => 'Cumba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 81,
            'province_id' => 7,
            'name' => 'El Milagro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 82,
            'province_id' => 7,
            'name' => 'Jamalca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 83,
            'province_id' => 7,
            'name' => 'Lonya Grande',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 84,
            'province_id' => 7,
            'name' => 'Yamon',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 85,
            'province_id' => 8,
            'name' => 'Huaraz',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 86,
            'province_id' => 8,
            'name' => 'Cochabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 87,
            'province_id' => 8,
            'name' => 'Colcabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 88,
            'province_id' => 8,
            'name' => 'Huanchay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 89,
            'province_id' => 8,
            'name' => 'Independencia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 90,
            'province_id' => 8,
            'name' => 'Jangas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 91,
            'province_id' => 8,
            'name' => 'La Libertad',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 92,
            'province_id' => 8,
            'name' => 'Olleros',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 93,
            'province_id' => 8,
            'name' => 'Pampas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 94,
            'province_id' => 8,
            'name' => 'Pariacoto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 95,
            'province_id' => 8,
            'name' => 'Pira',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 96,
            'province_id' => 8,
            'name' => 'Tarica',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 97,
            'province_id' => 9,
            'name' => 'Aija',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 98,
            'province_id' => 9,
            'name' => 'Coris',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 99,
            'province_id' => 9,
            'name' => 'Huacllan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 100,
            'province_id' => 9,
            'name' => 'La Merced',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 101,
            'province_id' => 9,
            'name' => 'Succha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 102,
            'province_id' => 10,
            'name' => 'Llamellin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 103,
            'province_id' => 10,
            'name' => 'Aczo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 104,
            'province_id' => 10,
            'name' => 'Chaccho',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 105,
            'province_id' => 10,
            'name' => 'Chingas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 106,
            'province_id' => 10,
            'name' => 'Mirgas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 107,
            'province_id' => 10,
            'name' => 'San Juan de Rontoy',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 108,
            'province_id' => 11,
            'name' => 'Chacas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 109,
            'province_id' => 11,
            'name' => 'Acochaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 110,
            'province_id' => 12,
            'name' => 'Chiquian',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 111,
            'province_id' => 12,
            'name' => 'Abelardo Pardo Lezameta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 112,
            'province_id' => 12,
            'name' => 'Antonio Raymondi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 113,
            'province_id' => 12,
            'name' => 'Aquia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 114,
            'province_id' => 12,
            'name' => 'Cajacay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 115,
            'province_id' => 12,
            'name' => 'Canis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 116,
            'province_id' => 12,
            'name' => 'Colquioc',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 117,
            'province_id' => 12,
            'name' => 'Huallanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 118,
            'province_id' => 12,
            'name' => 'Huasta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 119,
            'province_id' => 12,
            'name' => 'Huayllacayan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 120,
            'province_id' => 12,
            'name' => 'La Primavera',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 121,
            'province_id' => 12,
            'name' => 'Mangas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 122,
            'province_id' => 12,
            'name' => 'Pacllon',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 123,
            'province_id' => 12,
            'name' => 'San Miguel de Corpanqui',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 124,
            'province_id' => 12,
            'name' => 'Ticllos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 125,
            'province_id' => 13,
            'name' => 'Carhuaz',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 126,
            'province_id' => 13,
            'name' => 'Acopampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 127,
            'province_id' => 13,
            'name' => 'Amashca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 128,
            'province_id' => 13,
            'name' => 'Anta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 129,
            'province_id' => 13,
            'name' => 'Ataquero',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 130,
            'province_id' => 13,
            'name' => 'Marcara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 131,
            'province_id' => 13,
            'name' => 'Pariahuanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 132,
            'province_id' => 13,
            'name' => 'San Miguel de Aco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 133,
            'province_id' => 13,
            'name' => 'Shilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 134,
            'province_id' => 13,
            'name' => 'Tinco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 135,
            'province_id' => 13,
            'name' => 'Yungar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 136,
            'province_id' => 14,
            'name' => 'San Luis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 137,
            'province_id' => 14,
            'name' => 'San Nicolas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 138,
            'province_id' => 14,
            'name' => 'Yauya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 139,
            'province_id' => 15,
            'name' => 'Casma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 140,
            'province_id' => 15,
            'name' => 'Buena Vista Alta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 141,
            'province_id' => 15,
            'name' => 'Comandante Noel',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 142,
            'province_id' => 15,
            'name' => 'Yautan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 143,
            'province_id' => 16,
            'name' => 'Corongo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 144,
            'province_id' => 16,
            'name' => 'Aco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 145,
            'province_id' => 16,
            'name' => 'Bambas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 146,
            'province_id' => 16,
            'name' => 'Cusca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 147,
            'province_id' => 16,
            'name' => 'La Pampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 148,
            'province_id' => 16,
            'name' => 'Yanac',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 149,
            'province_id' => 16,
            'name' => 'Yupan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 150,
            'province_id' => 17,
            'name' => 'Huari',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 151,
            'province_id' => 17,
            'name' => 'Anra',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 152,
            'province_id' => 17,
            'name' => 'Cajay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 153,
            'province_id' => 17,
            'name' => 'Chavin de Huantar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 154,
            'province_id' => 17,
            'name' => 'Huacachi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 155,
            'province_id' => 17,
            'name' => 'Huacchis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 156,
            'province_id' => 17,
            'name' => 'Huachis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 157,
            'province_id' => 17,
            'name' => 'Huantar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 158,
            'province_id' => 17,
            'name' => 'Masin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 159,
            'province_id' => 17,
            'name' => 'Paucas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 160,
            'province_id' => 17,
            'name' => 'Ponto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 161,
            'province_id' => 17,
            'name' => 'Rahuapampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 162,
            'province_id' => 17,
            'name' => 'Rapayan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 163,
            'province_id' => 17,
            'name' => 'San Marcos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 164,
            'province_id' => 17,
            'name' => 'San Pedro de Chana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 165,
            'province_id' => 17,
            'name' => 'Uco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 166,
            'province_id' => 18,
            'name' => 'Huarmey',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 167,
            'province_id' => 18,
            'name' => 'Cochapeti',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 168,
            'province_id' => 18,
            'name' => 'Culebras',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 169,
            'province_id' => 18,
            'name' => 'Huayan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 170,
            'province_id' => 18,
            'name' => 'Malvas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 171,
            'province_id' => 19,
            'name' => 'Caraz',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 172,
            'province_id' => 19,
            'name' => 'Huallanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 173,
            'province_id' => 19,
            'name' => 'Huata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 174,
            'province_id' => 19,
            'name' => 'Huaylas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 175,
            'province_id' => 19,
            'name' => 'Mato',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 176,
            'province_id' => 19,
            'name' => 'Pamparomas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 177,
            'province_id' => 19,
            'name' => 'Pueblo Libre',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 178,
            'province_id' => 19,
            'name' => 'Santa Cruz',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 179,
            'province_id' => 19,
            'name' => 'Santo Toribio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 180,
            'province_id' => 19,
            'name' => 'Yuracmarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 181,
            'province_id' => 20,
            'name' => 'Piscobamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 182,
            'province_id' => 20,
            'name' => 'Casca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 183,
            'province_id' => 20,
            'name' => 'Eleazar Guzman Barron',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 184,
            'province_id' => 20,
            'name' => 'Fidel Olivas Escudero',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 185,
            'province_id' => 20,
            'name' => 'Llama',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 186,
            'province_id' => 20,
            'name' => 'Llumpa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 187,
            'province_id' => 20,
            'name' => 'Lucma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 188,
            'province_id' => 20,
            'name' => 'Musga',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 189,
            'province_id' => 21,
            'name' => 'Ocros',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 190,
            'province_id' => 21,
            'name' => 'Acas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 191,
            'province_id' => 21,
            'name' => 'Cajamarquilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 192,
            'province_id' => 21,
            'name' => 'Carhuapampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 193,
            'province_id' => 21,
            'name' => 'Cochas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 194,
            'province_id' => 21,
            'name' => 'Congas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 195,
            'province_id' => 21,
            'name' => 'Llipa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 196,
            'province_id' => 21,
            'name' => 'San Cristobal de Rajan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 197,
            'province_id' => 21,
            'name' => 'San Pedro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 198,
            'province_id' => 21,
            'name' => 'Santiago de Chilcas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 199,
            'province_id' => 22,
            'name' => 'Cabana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 200,
            'province_id' => 22,
            'name' => 'Bolognesi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 201,
            'province_id' => 22,
            'name' => 'Conchucos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 202,
            'province_id' => 22,
            'name' => 'Huacaschuque',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 203,
            'province_id' => 22,
            'name' => 'Huandoval',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 204,
            'province_id' => 22,
            'name' => 'Lacabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 205,
            'province_id' => 22,
            'name' => 'Llapo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 206,
            'province_id' => 22,
            'name' => 'Pallasca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 207,
            'province_id' => 22,
            'name' => 'Pampas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 208,
            'province_id' => 22,
            'name' => 'Santa Rosa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 209,
            'province_id' => 22,
            'name' => 'Tauca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 210,
            'province_id' => 23,
            'name' => 'Pomabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 211,
            'province_id' => 23,
            'name' => 'Huayllan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 212,
            'province_id' => 23,
            'name' => 'Parobamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 213,
            'province_id' => 23,
            'name' => 'Quinuabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 214,
            'province_id' => 24,
            'name' => 'Recuay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 215,
            'province_id' => 24,
            'name' => 'Catac',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 216,
            'province_id' => 24,
            'name' => 'Cotaparaco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 217,
            'province_id' => 24,
            'name' => 'Huayllapampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 218,
            'province_id' => 24,
            'name' => 'Llacllin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 219,
            'province_id' => 24,
            'name' => 'Marca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 220,
            'province_id' => 24,
            'name' => 'Pampas Chico',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 221,
            'province_id' => 24,
            'name' => 'Pararin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 222,
            'province_id' => 24,
            'name' => 'Tapacocha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 223,
            'province_id' => 24,
            'name' => 'Ticapampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 224,
            'province_id' => 25,
            'name' => 'Chimbote',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 225,
            'province_id' => 25,
            'name' => 'Caceres del Peru',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 226,
            'province_id' => 25,
            'name' => 'Coishco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 227,
            'province_id' => 25,
            'name' => 'Macate',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 228,
            'province_id' => 25,
            'name' => 'Moro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 229,
            'province_id' => 25,
            'name' => 'Nepeña',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 230,
            'province_id' => 25,
            'name' => 'Samanco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 231,
            'province_id' => 25,
            'name' => 'Santa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 232,
            'province_id' => 25,
            'name' => 'Nuevo Chimbote',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 233,
            'province_id' => 26,
            'name' => 'Sihuas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 234,
            'province_id' => 26,
            'name' => 'Acobamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 235,
            'province_id' => 26,
            'name' => 'Alfonso Ugarte',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 236,
            'province_id' => 26,
            'name' => 'Cashapampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 237,
            'province_id' => 26,
            'name' => 'Chingalpo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 238,
            'province_id' => 26,
            'name' => 'Huayllabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 239,
            'province_id' => 26,
            'name' => 'Quiches',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 240,
            'province_id' => 26,
            'name' => 'Ragash',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 241,
            'province_id' => 26,
            'name' => 'San Juan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 242,
            'province_id' => 26,
            'name' => 'Sicsibamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 243,
            'province_id' => 27,
            'name' => 'Yungay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 244,
            'province_id' => 27,
            'name' => 'Cascapara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 245,
            'province_id' => 27,
            'name' => 'Mancos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 246,
            'province_id' => 27,
            'name' => 'Matacoto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 247,
            'province_id' => 27,
            'name' => 'Quillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 248,
            'province_id' => 27,
            'name' => 'Ranrahirca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 249,
            'province_id' => 27,
            'name' => 'Shupluy',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 250,
            'province_id' => 27,
            'name' => 'Yanama',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 251,
            'province_id' => 28,
            'name' => 'Abancay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 252,
            'province_id' => 28,
            'name' => 'Chacoche',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 253,
            'province_id' => 28,
            'name' => 'Circa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 254,
            'province_id' => 28,
            'name' => 'Curahuasi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 255,
            'province_id' => 28,
            'name' => 'Huanipaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 256,
            'province_id' => 28,
            'name' => 'Lambrama',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 257,
            'province_id' => 28,
            'name' => 'Pichirhua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 258,
            'province_id' => 28,
            'name' => 'San Pedro de Cachora',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 259,
            'province_id' => 28,
            'name' => 'Tamburco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 260,
            'province_id' => 29,
            'name' => 'Andahuaylas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 261,
            'province_id' => 29,
            'name' => 'Andarapa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 262,
            'province_id' => 29,
            'name' => 'Chiara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 263,
            'province_id' => 29,
            'name' => 'Huancarama',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 264,
            'province_id' => 29,
            'name' => 'Huancaray',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 265,
            'province_id' => 29,
            'name' => 'Huayana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 266,
            'province_id' => 29,
            'name' => 'Kishuara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 267,
            'province_id' => 29,
            'name' => 'Pacobamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 268,
            'province_id' => 29,
            'name' => 'Pacucha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 269,
            'province_id' => 29,
            'name' => 'Pampachiri',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 270,
            'province_id' => 29,
            'name' => 'Pomacocha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 271,
            'province_id' => 29,
            'name' => 'San Antonio de Cachi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 272,
            'province_id' => 29,
            'name' => 'San Jeronimo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 273,
            'province_id' => 29,
            'name' => 'San Miguel de Chaccrampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 274,
            'province_id' => 29,
            'name' => 'Santa Maria de Chicmo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 275,
            'province_id' => 29,
            'name' => 'Talavera',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 276,
            'province_id' => 29,
            'name' => 'Tumay Huaraca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 277,
            'province_id' => 29,
            'name' => 'Turpo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 278,
            'province_id' => 29,
            'name' => 'Kaquiabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 279,
            'province_id' => 29,
            'name' => 'José María Arguedas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 280,
            'province_id' => 30,
            'name' => 'Antabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 281,
            'province_id' => 30,
            'name' => 'El Oro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 282,
            'province_id' => 30,
            'name' => 'Huaquirca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 283,
            'province_id' => 30,
            'name' => 'Juan Espinoza Medrano',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 284,
            'province_id' => 30,
            'name' => 'Oropesa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 285,
            'province_id' => 30,
            'name' => 'Pachaconas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 286,
            'province_id' => 30,
            'name' => 'Sabaino',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 287,
            'province_id' => 31,
            'name' => 'Chalhuanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 288,
            'province_id' => 31,
            'name' => 'Capaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 289,
            'province_id' => 31,
            'name' => 'Caraybamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 290,
            'province_id' => 31,
            'name' => 'Chapimarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 291,
            'province_id' => 31,
            'name' => 'Colcabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 292,
            'province_id' => 31,
            'name' => 'Cotaruse',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 293,
            'province_id' => 31,
            'name' => 'Huayllo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 294,
            'province_id' => 31,
            'name' => 'Justo Apu Sahuaraura',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 295,
            'province_id' => 31,
            'name' => 'Lucre',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 296,
            'province_id' => 31,
            'name' => 'Pocohuanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 297,
            'province_id' => 31,
            'name' => 'San Juan de Chacña',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 298,
            'province_id' => 31,
            'name' => 'Sañayca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 299,
            'province_id' => 31,
            'name' => 'Soraya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 300,
            'province_id' => 31,
            'name' => 'Tapairihua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 301,
            'province_id' => 31,
            'name' => 'Tintay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 302,
            'province_id' => 31,
            'name' => 'Toraya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 303,
            'province_id' => 31,
            'name' => 'Yanaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 304,
            'province_id' => 32,
            'name' => 'Tambobamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 305,
            'province_id' => 32,
            'name' => 'Cotabambas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 306,
            'province_id' => 32,
            'name' => 'Coyllurqui',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 307,
            'province_id' => 32,
            'name' => 'Haquira',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 308,
            'province_id' => 32,
            'name' => 'Mara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 309,
            'province_id' => 32,
            'name' => 'Challhuahuacho',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 310,
            'province_id' => 33,
            'name' => 'Chincheros',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 311,
            'province_id' => 33,
            'name' => 'Anco_Huallo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 312,
            'province_id' => 33,
            'name' => 'Cocharcas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 313,
            'province_id' => 33,
            'name' => 'Huaccana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 314,
            'province_id' => 33,
            'name' => 'Ocobamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 315,
            'province_id' => 33,
            'name' => 'Ongoy',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 316,
            'province_id' => 33,
            'name' => 'Uranmarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 317,
            'province_id' => 33,
            'name' => 'Ranracancha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 318,
            'province_id' => 33,
            'name' => 'Rocchacc',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 319,
            'province_id' => 33,
            'name' => 'El Porvenir',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 320,
            'province_id' => 33,
            'name' => 'Los Chankas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 321,
            'province_id' => 34,
            'name' => 'Chuquibambilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 322,
            'province_id' => 34,
            'name' => 'Curpahuasi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 323,
            'province_id' => 34,
            'name' => 'Gamarra',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 324,
            'province_id' => 34,
            'name' => 'Huayllati',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 325,
            'province_id' => 34,
            'name' => 'Mamara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 326,
            'province_id' => 34,
            'name' => 'Micaela Bastidas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 327,
            'province_id' => 34,
            'name' => 'Pataypampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 328,
            'province_id' => 34,
            'name' => 'Progreso',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 329,
            'province_id' => 34,
            'name' => 'San Antonio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 330,
            'province_id' => 34,
            'name' => 'Santa Rosa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 331,
            'province_id' => 34,
            'name' => 'Turpay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 332,
            'province_id' => 34,
            'name' => 'Vilcabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 333,
            'province_id' => 34,
            'name' => 'Virundo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 334,
            'province_id' => 34,
            'name' => 'Curasco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 335,
            'province_id' => 35,
            'name' => 'Arequipa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 336,
            'province_id' => 35,
            'name' => 'Alto Selva Alegre',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 337,
            'province_id' => 35,
            'name' => 'Cayma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 338,
            'province_id' => 35,
            'name' => 'Cerro Colorado',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 339,
            'province_id' => 35,
            'name' => 'Characato',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 340,
            'province_id' => 35,
            'name' => 'Chiguata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 341,
            'province_id' => 35,
            'name' => 'Jacobo Hunter',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 342,
            'province_id' => 35,
            'name' => 'La Joya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 343,
            'province_id' => 35,
            'name' => 'Mariano Melgar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 344,
            'province_id' => 35,
            'name' => 'Miraflores',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 345,
            'province_id' => 35,
            'name' => 'Mollebaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 346,
            'province_id' => 35,
            'name' => 'Paucarpata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 347,
            'province_id' => 35,
            'name' => 'Pocsi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 348,
            'province_id' => 35,
            'name' => 'Polobaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 349,
            'province_id' => 35,
            'name' => 'Quequeña',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 350,
            'province_id' => 35,
            'name' => 'Sabandia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 351,
            'province_id' => 35,
            'name' => 'Sachaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 352,
            'province_id' => 35,
            'name' => 'San Juan de Siguas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 353,
            'province_id' => 35,
            'name' => 'San Juan de Tarucani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 354,
            'province_id' => 35,
            'name' => 'Santa Isabel de Siguas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 355,
            'province_id' => 35,
            'name' => 'Santa Rita de Siguas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 356,
            'province_id' => 35,
            'name' => 'Socabaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 357,
            'province_id' => 35,
            'name' => 'Tiabaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 358,
            'province_id' => 35,
            'name' => 'Uchumayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 359,
            'province_id' => 35,
            'name' => 'Vitor',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 360,
            'province_id' => 35,
            'name' => 'Yanahuara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 361,
            'province_id' => 35,
            'name' => 'Yarabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 362,
            'province_id' => 35,
            'name' => 'Yura',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 363,
            'province_id' => 35,
            'name' => 'Jose Luis Bustamante y Rivero',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 364,
            'province_id' => 36,
            'name' => 'Camana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 365,
            'province_id' => 36,
            'name' => 'Jose Maria Quimper',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 366,
            'province_id' => 36,
            'name' => 'Mariano Nicolas Valcarcel',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 367,
            'province_id' => 36,
            'name' => 'Mariscal Caceres',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 368,
            'province_id' => 36,
            'name' => 'Nicolas de Pierola',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 369,
            'province_id' => 36,
            'name' => 'Ocoña',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 370,
            'province_id' => 36,
            'name' => 'Quilca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 371,
            'province_id' => 36,
            'name' => 'Samuel Pastor',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 372,
            'province_id' => 37,
            'name' => 'Caraveli',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 373,
            'province_id' => 37,
            'name' => 'Acari',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 374,
            'province_id' => 37,
            'name' => 'Atico',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 375,
            'province_id' => 37,
            'name' => 'Atiquipa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 376,
            'province_id' => 37,
            'name' => 'Bella Union',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 377,
            'province_id' => 37,
            'name' => 'Cahuacho',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 378,
            'province_id' => 37,
            'name' => 'Chala',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 379,
            'province_id' => 37,
            'name' => 'Chaparra',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 380,
            'province_id' => 37,
            'name' => 'Huanuhuanu',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 381,
            'province_id' => 37,
            'name' => 'Jaqui',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 382,
            'province_id' => 37,
            'name' => 'Lomas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 383,
            'province_id' => 37,
            'name' => 'Quicacha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 384,
            'province_id' => 37,
            'name' => 'Yauca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 385,
            'province_id' => 38,
            'name' => 'Aplao',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 386,
            'province_id' => 38,
            'name' => 'Andagua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 387,
            'province_id' => 38,
            'name' => 'Ayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 388,
            'province_id' => 38,
            'name' => 'Chachas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 389,
            'province_id' => 38,
            'name' => 'Chilcaymarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 390,
            'province_id' => 38,
            'name' => 'Choco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 391,
            'province_id' => 38,
            'name' => 'Huancarqui',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 392,
            'province_id' => 38,
            'name' => 'Machaguay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 393,
            'province_id' => 38,
            'name' => 'Orcopampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 394,
            'province_id' => 38,
            'name' => 'Pampacolca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 395,
            'province_id' => 38,
            'name' => 'Tipan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 396,
            'province_id' => 38,
            'name' => 'Uñon',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 397,
            'province_id' => 38,
            'name' => 'Uraca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 398,
            'province_id' => 38,
            'name' => 'Viraco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 399,
            'province_id' => 39,
            'name' => 'Chivay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 400,
            'province_id' => 39,
            'name' => 'Achoma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 401,
            'province_id' => 39,
            'name' => 'Cabanaconde',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 402,
            'province_id' => 39,
            'name' => 'Callalli',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 403,
            'province_id' => 39,
            'name' => 'Caylloma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 404,
            'province_id' => 39,
            'name' => 'Coporaque',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 405,
            'province_id' => 39,
            'name' => 'Huambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 406,
            'province_id' => 39,
            'name' => 'Huanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 407,
            'province_id' => 39,
            'name' => 'Ichupampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 408,
            'province_id' => 39,
            'name' => 'Lari',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 409,
            'province_id' => 39,
            'name' => 'Lluta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 410,
            'province_id' => 39,
            'name' => 'Maca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 411,
            'province_id' => 39,
            'name' => 'Madrigal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 412,
            'province_id' => 39,
            'name' => 'San Antonio de Chuca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 413,
            'province_id' => 39,
            'name' => 'Sibayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 414,
            'province_id' => 39,
            'name' => 'Tapay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 415,
            'province_id' => 39,
            'name' => 'Tisco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 416,
            'province_id' => 39,
            'name' => 'Tuti',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 417,
            'province_id' => 39,
            'name' => 'Yanque',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 418,
            'province_id' => 39,
            'name' => 'Majes',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 419,
            'province_id' => 40,
            'name' => 'Chuquibamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 420,
            'province_id' => 40,
            'name' => 'Andaray',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 421,
            'province_id' => 40,
            'name' => 'Cayarani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 422,
            'province_id' => 40,
            'name' => 'Chichas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 423,
            'province_id' => 40,
            'name' => 'Iray',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 424,
            'province_id' => 40,
            'name' => 'Rio Grande',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 425,
            'province_id' => 40,
            'name' => 'Salamanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 426,
            'province_id' => 40,
            'name' => 'Yanaquihua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 427,
            'province_id' => 41,
            'name' => 'Mollendo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 428,
            'province_id' => 41,
            'name' => 'Cocachacra',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 429,
            'province_id' => 41,
            'name' => 'Dean Valdivia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 430,
            'province_id' => 41,
            'name' => 'Islay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 431,
            'province_id' => 41,
            'name' => 'Mejia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 432,
            'province_id' => 41,
            'name' => 'Punta de Bombon',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 433,
            'province_id' => 42,
            'name' => 'Cotahuasi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 434,
            'province_id' => 42,
            'name' => 'Alca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 435,
            'province_id' => 42,
            'name' => 'Charcana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 436,
            'province_id' => 42,
            'name' => 'Huaynacotas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 437,
            'province_id' => 42,
            'name' => 'Pampamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 438,
            'province_id' => 42,
            'name' => 'Puyca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 439,
            'province_id' => 42,
            'name' => 'Quechualla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 440,
            'province_id' => 42,
            'name' => 'Sayla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 441,
            'province_id' => 42,
            'name' => 'Tauria',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 442,
            'province_id' => 42,
            'name' => 'Tomepampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 443,
            'province_id' => 42,
            'name' => 'Toro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 444,
            'province_id' => 43,
            'name' => 'Ayacucho',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 445,
            'province_id' => 43,
            'name' => 'Acocro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 446,
            'province_id' => 43,
            'name' => 'Acos Vinchos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 447,
            'province_id' => 43,
            'name' => 'Carmen Alto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 448,
            'province_id' => 43,
            'name' => 'Chiara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 449,
            'province_id' => 43,
            'name' => 'Ocros',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 450,
            'province_id' => 43,
            'name' => 'Pacaycasa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 451,
            'province_id' => 43,
            'name' => 'Quinua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 452,
            'province_id' => 43,
            'name' => 'San Jose de Ticllas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 453,
            'province_id' => 43,
            'name' => 'San Juan Bautista',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 454,
            'province_id' => 43,
            'name' => 'Santiago de Pischa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 455,
            'province_id' => 43,
            'name' => 'Socos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 456,
            'province_id' => 43,
            'name' => 'Tambillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 457,
            'province_id' => 43,
            'name' => 'Vinchos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 458,
            'province_id' => 43,
            'name' => 'Jesus Nazareno',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 459,
            'province_id' => 43,
            'name' => 'Andrés Avelino Cáceres Dorregaray',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 460,
            'province_id' => 44,
            'name' => 'Cangallo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 461,
            'province_id' => 44,
            'name' => 'Chuschi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 462,
            'province_id' => 44,
            'name' => 'Los Morochucos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 463,
            'province_id' => 44,
            'name' => 'Maria Parado de Bellido',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 464,
            'province_id' => 44,
            'name' => 'Paras',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 465,
            'province_id' => 44,
            'name' => 'Totos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 466,
            'province_id' => 45,
            'name' => 'Sancos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 467,
            'province_id' => 45,
            'name' => 'Carapo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 468,
            'province_id' => 45,
            'name' => 'Sacsamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 469,
            'province_id' => 45,
            'name' => 'Santiago de Lucanamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 470,
            'province_id' => 46,
            'name' => 'Huanta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 471,
            'province_id' => 46,
            'name' => 'Ayahuanco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 472,
            'province_id' => 46,
            'name' => 'Huamanguilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 473,
            'province_id' => 46,
            'name' => 'Iguain',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 474,
            'province_id' => 46,
            'name' => 'Luricocha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 475,
            'province_id' => 46,
            'name' => 'Santillana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 476,
            'province_id' => 46,
            'name' => 'Sivia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 477,
            'province_id' => 46,
            'name' => 'Llochegua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 478,
            'province_id' => 46,
            'name' => 'Canayre',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 479,
            'province_id' => 46,
            'name' => 'Uchuraccay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 480,
            'province_id' => 46,
            'name' => 'Pucacolpa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 481,
            'province_id' => 46,
            'name' => 'Chaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 482,
            'province_id' => 47,
            'name' => 'San Miguel',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 483,
            'province_id' => 47,
            'name' => 'Anco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 484,
            'province_id' => 47,
            'name' => 'Ayna',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 485,
            'province_id' => 47,
            'name' => 'Chilcas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 486,
            'province_id' => 47,
            'name' => 'Chungui',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 487,
            'province_id' => 47,
            'name' => 'Luis Carranza',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 488,
            'province_id' => 47,
            'name' => 'Santa Rosa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 489,
            'province_id' => 47,
            'name' => 'Tambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 490,
            'province_id' => 47,
            'name' => 'Samugari',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 491,
            'province_id' => 47,
            'name' => 'Anchihuay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 492,
            'province_id' => 47,
            'name' => 'Oronccoy',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 493,
            'province_id' => 48,
            'name' => 'Puquio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 494,
            'province_id' => 48,
            'name' => 'Aucara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 495,
            'province_id' => 48,
            'name' => 'Cabana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 496,
            'province_id' => 48,
            'name' => 'Carmen Salcedo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 497,
            'province_id' => 48,
            'name' => 'Chaviña',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 498,
            'province_id' => 48,
            'name' => 'Chipao',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 499,
            'province_id' => 48,
            'name' => 'Huac-Huas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 500,
            'province_id' => 48,
            'name' => 'Laramate',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 501,
            'province_id' => 48,
            'name' => 'Leoncio Prado',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 502,
            'province_id' => 48,
            'name' => 'Llauta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 503,
            'province_id' => 48,
            'name' => 'Lucanas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 504,
            'province_id' => 48,
            'name' => 'Ocaña',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 505,
            'province_id' => 48,
            'name' => 'Otoca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 506,
            'province_id' => 48,
            'name' => 'Saisa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 507,
            'province_id' => 48,
            'name' => 'San Cristobal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 508,
            'province_id' => 48,
            'name' => 'San Juan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 509,
            'province_id' => 48,
            'name' => 'San Pedro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 510,
            'province_id' => 48,
            'name' => 'San Pedro de Palco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 511,
            'province_id' => 48,
            'name' => 'Sancos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 512,
            'province_id' => 48,
            'name' => 'Santa Ana de Huaycahuacho',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 513,
            'province_id' => 48,
            'name' => 'Santa Lucia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 514,
            'province_id' => 49,
            'name' => 'Coracora',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 515,
            'province_id' => 49,
            'name' => 'Chumpi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 516,
            'province_id' => 49,
            'name' => 'Coronel Castañeda',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 517,
            'province_id' => 49,
            'name' => 'Pacapausa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 518,
            'province_id' => 49,
            'name' => 'Pullo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 519,
            'province_id' => 49,
            'name' => 'Puyusca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 520,
            'province_id' => 49,
            'name' => 'San Francisco de Ravacayco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 521,
            'province_id' => 49,
            'name' => 'Upahuacho',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 522,
            'province_id' => 50,
            'name' => 'Pausa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 523,
            'province_id' => 50,
            'name' => 'Colta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 524,
            'province_id' => 50,
            'name' => 'Corculla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 525,
            'province_id' => 50,
            'name' => 'Lampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 526,
            'province_id' => 50,
            'name' => 'Marcabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 527,
            'province_id' => 50,
            'name' => 'Oyolo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 528,
            'province_id' => 50,
            'name' => 'Pararca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 529,
            'province_id' => 50,
            'name' => 'San Javier de Alpabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 530,
            'province_id' => 50,
            'name' => 'San Jose de Ushua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 531,
            'province_id' => 50,
            'name' => 'Sara Sara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 532,
            'province_id' => 51,
            'name' => 'Querobamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 533,
            'province_id' => 51,
            'name' => 'Belen',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 534,
            'province_id' => 51,
            'name' => 'Chalcos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 535,
            'province_id' => 51,
            'name' => 'Chilcayoc',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 536,
            'province_id' => 51,
            'name' => 'Huacaña',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 537,
            'province_id' => 51,
            'name' => 'Morcolla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 538,
            'province_id' => 51,
            'name' => 'Paico',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 539,
            'province_id' => 51,
            'name' => 'San Pedro de Larcay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 540,
            'province_id' => 51,
            'name' => 'San Salvador de Quije',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 541,
            'province_id' => 51,
            'name' => 'Santiago de Paucaray',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 542,
            'province_id' => 51,
            'name' => 'Soras',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 543,
            'province_id' => 52,
            'name' => 'Huancapi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 544,
            'province_id' => 52,
            'name' => 'Alcamenca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 545,
            'province_id' => 52,
            'name' => 'Apongo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 546,
            'province_id' => 52,
            'name' => 'Asquipata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 547,
            'province_id' => 52,
            'name' => 'Canaria',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 548,
            'province_id' => 52,
            'name' => 'Cayara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 549,
            'province_id' => 52,
            'name' => 'Colca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 550,
            'province_id' => 52,
            'name' => 'Huamanquiquia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 551,
            'province_id' => 52,
            'name' => 'Huancaraylla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 552,
            'province_id' => 52,
            'name' => 'Huaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 553,
            'province_id' => 52,
            'name' => 'Sarhua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 554,
            'province_id' => 52,
            'name' => 'Vilcanchos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 555,
            'province_id' => 53,
            'name' => 'Vilcas Huaman',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 556,
            'province_id' => 53,
            'name' => 'Accomarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 557,
            'province_id' => 53,
            'name' => 'Carhuanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 558,
            'province_id' => 53,
            'name' => 'Concepcion',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 559,
            'province_id' => 53,
            'name' => 'Huambalpa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 560,
            'province_id' => 53,
            'name' => 'Independencia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 561,
            'province_id' => 53,
            'name' => 'Saurama',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 562,
            'province_id' => 53,
            'name' => 'Vischongo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 563,
            'province_id' => 54,
            'name' => 'Cajamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 564,
            'province_id' => 54,
            'name' => 'Asuncion',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 565,
            'province_id' => 54,
            'name' => 'Chetilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 566,
            'province_id' => 54,
            'name' => 'Cospan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 567,
            'province_id' => 54,
            'name' => 'Encañada',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 568,
            'province_id' => 54,
            'name' => 'Jesus',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 569,
            'province_id' => 54,
            'name' => 'Llacanora',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 570,
            'province_id' => 54,
            'name' => 'Los Baños del Inca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 571,
            'province_id' => 54,
            'name' => 'Magdalena',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 572,
            'province_id' => 54,
            'name' => 'Matara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 573,
            'province_id' => 54,
            'name' => 'Namora',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 574,
            'province_id' => 54,
            'name' => 'San Juan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 575,
            'province_id' => 55,
            'name' => 'Cajabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 576,
            'province_id' => 55,
            'name' => 'Cachachi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 577,
            'province_id' => 55,
            'name' => 'Condebamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 578,
            'province_id' => 55,
            'name' => 'Sitacocha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 579,
            'province_id' => 56,
            'name' => 'Celendin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 580,
            'province_id' => 56,
            'name' => 'Chumuch',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 581,
            'province_id' => 56,
            'name' => 'Cortegana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 582,
            'province_id' => 56,
            'name' => 'Huasmin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 583,
            'province_id' => 56,
            'name' => 'Jorge Chavez',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 584,
            'province_id' => 56,
            'name' => 'Jose Galvez',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 585,
            'province_id' => 56,
            'name' => 'Miguel Iglesias',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 586,
            'province_id' => 56,
            'name' => 'Oxamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 587,
            'province_id' => 56,
            'name' => 'Sorochuco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 588,
            'province_id' => 56,
            'name' => 'Sucre',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 589,
            'province_id' => 56,
            'name' => 'Utco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 590,
            'province_id' => 56,
            'name' => 'La Libertad de Pallan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 591,
            'province_id' => 57,
            'name' => 'Chota',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 592,
            'province_id' => 57,
            'name' => 'Anguia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 593,
            'province_id' => 57,
            'name' => 'Chadin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 594,
            'province_id' => 57,
            'name' => 'Chiguirip',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 595,
            'province_id' => 57,
            'name' => 'Chimban',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 596,
            'province_id' => 57,
            'name' => 'Choropampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 597,
            'province_id' => 57,
            'name' => 'Cochabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 598,
            'province_id' => 57,
            'name' => 'Conchan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 599,
            'province_id' => 57,
            'name' => 'Huambos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 600,
            'province_id' => 57,
            'name' => 'Lajas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 601,
            'province_id' => 57,
            'name' => 'Llama',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 602,
            'province_id' => 57,
            'name' => 'Miracosta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 603,
            'province_id' => 57,
            'name' => 'Paccha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 604,
            'province_id' => 57,
            'name' => 'Pion',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 605,
            'province_id' => 57,
            'name' => 'Querocoto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 606,
            'province_id' => 57,
            'name' => 'San Juan de Licupis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 607,
            'province_id' => 57,
            'name' => 'Tacabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 608,
            'province_id' => 57,
            'name' => 'Tocmoche',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 609,
            'province_id' => 57,
            'name' => 'Chalamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 610,
            'province_id' => 58,
            'name' => 'Contumaza',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 611,
            'province_id' => 58,
            'name' => 'Chilete',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 612,
            'province_id' => 58,
            'name' => 'Cupisnique',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 613,
            'province_id' => 58,
            'name' => 'Guzmango',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 614,
            'province_id' => 58,
            'name' => 'San Benito',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 615,
            'province_id' => 58,
            'name' => 'Santa Cruz de Toled',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 616,
            'province_id' => 58,
            'name' => 'Tantarica',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 617,
            'province_id' => 58,
            'name' => 'Yonan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 618,
            'province_id' => 59,
            'name' => 'Cutervo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 619,
            'province_id' => 59,
            'name' => 'Callayuc',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 620,
            'province_id' => 59,
            'name' => 'Choros',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 621,
            'province_id' => 59,
            'name' => 'Cujillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 622,
            'province_id' => 59,
            'name' => 'La Ramada',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 623,
            'province_id' => 59,
            'name' => 'Pimpingos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 624,
            'province_id' => 59,
            'name' => 'Querocotillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 625,
            'province_id' => 59,
            'name' => 'San Andres de Cutervo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 626,
            'province_id' => 59,
            'name' => 'San Juan de Cutervo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 627,
            'province_id' => 59,
            'name' => 'San Luis de Lucma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 628,
            'province_id' => 59,
            'name' => 'Santa Cruz',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 629,
            'province_id' => 59,
            'name' => 'Santo Domingo de La Capilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 630,
            'province_id' => 59,
            'name' => 'Santo Tomas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 631,
            'province_id' => 59,
            'name' => 'Socota',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 632,
            'province_id' => 59,
            'name' => 'Toribio Casanova',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 633,
            'province_id' => 60,
            'name' => 'Bambamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 634,
            'province_id' => 60,
            'name' => 'Chugur',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 635,
            'province_id' => 60,
            'name' => 'Hualgayoc',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 636,
            'province_id' => 61,
            'name' => 'Jaen',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 637,
            'province_id' => 61,
            'name' => 'Bellavista',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 638,
            'province_id' => 61,
            'name' => 'Chontali',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 639,
            'province_id' => 61,
            'name' => 'Colasay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 640,
            'province_id' => 61,
            'name' => 'Huabal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 641,
            'province_id' => 61,
            'name' => 'Las Pirias',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 642,
            'province_id' => 61,
            'name' => 'Pomahuaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 643,
            'province_id' => 61,
            'name' => 'Pucara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 644,
            'province_id' => 61,
            'name' => 'Sallique',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 645,
            'province_id' => 61,
            'name' => 'San Felipe',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 646,
            'province_id' => 61,
            'name' => 'San Jose del Alto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 647,
            'province_id' => 61,
            'name' => 'Santa Rosa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 648,
            'province_id' => 62,
            'name' => 'San Ignacio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 649,
            'province_id' => 62,
            'name' => 'Chirinos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 650,
            'province_id' => 62,
            'name' => 'Huarango',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 651,
            'province_id' => 62,
            'name' => 'La Coipa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 652,
            'province_id' => 62,
            'name' => 'Namballe',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 653,
            'province_id' => 62,
            'name' => 'San Jose de Lourdes',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 654,
            'province_id' => 62,
            'name' => 'Tabaconas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 655,
            'province_id' => 63,
            'name' => 'Pedro Galvez',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 656,
            'province_id' => 63,
            'name' => 'Chancay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 657,
            'province_id' => 63,
            'name' => 'Eduardo Villanueva',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 658,
            'province_id' => 63,
            'name' => 'Gregorio Pita',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 659,
            'province_id' => 63,
            'name' => 'Ichocan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 660,
            'province_id' => 63,
            'name' => 'Jose Manuel Quiroz',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 661,
            'province_id' => 63,
            'name' => 'Jose Sabogal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 662,
            'province_id' => 64,
            'name' => 'San Miguel',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 663,
            'province_id' => 64,
            'name' => 'Bolivar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 664,
            'province_id' => 64,
            'name' => 'Calquis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 665,
            'province_id' => 64,
            'name' => 'Catilluc',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 666,
            'province_id' => 64,
            'name' => 'El Prado',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 667,
            'province_id' => 64,
            'name' => 'La Florida',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 668,
            'province_id' => 64,
            'name' => 'Llapa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 669,
            'province_id' => 64,
            'name' => 'Nanchoc',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 670,
            'province_id' => 64,
            'name' => 'Niepos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 671,
            'province_id' => 64,
            'name' => 'San Gregorio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 672,
            'province_id' => 64,
            'name' => 'San Silvestre de Cochan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 673,
            'province_id' => 64,
            'name' => 'Tongod',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 674,
            'province_id' => 64,
            'name' => 'Union Agua Blanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 675,
            'province_id' => 65,
            'name' => 'San Pablo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 676,
            'province_id' => 65,
            'name' => 'San Bernardino',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 677,
            'province_id' => 65,
            'name' => 'San Luis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 678,
            'province_id' => 65,
            'name' => 'Tumbaden',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 679,
            'province_id' => 66,
            'name' => 'Santa Cruz',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 680,
            'province_id' => 66,
            'name' => 'Andabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 681,
            'province_id' => 66,
            'name' => 'Catache',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 682,
            'province_id' => 66,
            'name' => 'Chancaybaños',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 683,
            'province_id' => 66,
            'name' => 'La Esperanza',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 684,
            'province_id' => 66,
            'name' => 'Ninabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 685,
            'province_id' => 66,
            'name' => 'Pulan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 686,
            'province_id' => 66,
            'name' => 'Saucepampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 687,
            'province_id' => 66,
            'name' => 'Sexi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 688,
            'province_id' => 66,
            'name' => 'Uticyacu',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 689,
            'province_id' => 66,
            'name' => 'Yauyucan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 690,
            'province_id' => 67,
            'name' => 'Callao',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 691,
            'province_id' => 67,
            'name' => 'Bellavista',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 692,
            'province_id' => 67,
            'name' => 'Carmen de La Legua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 693,
            'province_id' => 67,
            'name' => 'La Perla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 694,
            'province_id' => 67,
            'name' => 'La Punta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 695,
            'province_id' => 67,
            'name' => 'Ventanilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 696,
            'province_id' => 67,
            'name' => 'Mi Peru',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 697,
            'province_id' => 68,
            'name' => 'Cusco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 698,
            'province_id' => 68,
            'name' => 'Ccorca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 699,
            'province_id' => 68,
            'name' => 'Poroy',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 700,
            'province_id' => 68,
            'name' => 'San Jeronimo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 701,
            'province_id' => 68,
            'name' => 'San Sebastian',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 702,
            'province_id' => 68,
            'name' => 'Santiago',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 703,
            'province_id' => 68,
            'name' => 'Saylla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 704,
            'province_id' => 68,
            'name' => 'Wanchaq',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 705,
            'province_id' => 69,
            'name' => 'Acomayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 706,
            'province_id' => 69,
            'name' => 'Acopia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 707,
            'province_id' => 69,
            'name' => 'Acos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 708,
            'province_id' => 69,
            'name' => 'Mosoc Llacta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 709,
            'province_id' => 69,
            'name' => 'Pomacanchi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 710,
            'province_id' => 69,
            'name' => 'Rondocan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 711,
            'province_id' => 69,
            'name' => 'Sangarara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 712,
            'province_id' => 70,
            'name' => 'Anta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 713,
            'province_id' => 70,
            'name' => 'Ancahuasi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 714,
            'province_id' => 70,
            'name' => 'Cachimayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 715,
            'province_id' => 70,
            'name' => 'Chinchaypujio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 716,
            'province_id' => 70,
            'name' => 'Huarocondo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 717,
            'province_id' => 70,
            'name' => 'Limatambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 718,
            'province_id' => 70,
            'name' => 'Mollepata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 719,
            'province_id' => 70,
            'name' => 'Pucyura',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 720,
            'province_id' => 70,
            'name' => 'Zurite',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 721,
            'province_id' => 71,
            'name' => 'Calca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 722,
            'province_id' => 71,
            'name' => 'Coya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 723,
            'province_id' => 71,
            'name' => 'Lamay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 724,
            'province_id' => 71,
            'name' => 'Lares',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 725,
            'province_id' => 71,
            'name' => 'Pisac',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 726,
            'province_id' => 71,
            'name' => 'San Salvador',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 727,
            'province_id' => 71,
            'name' => 'Taray',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 728,
            'province_id' => 71,
            'name' => 'Yanatile',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 729,
            'province_id' => 72,
            'name' => 'Yanaoca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 730,
            'province_id' => 72,
            'name' => 'Checca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 731,
            'province_id' => 72,
            'name' => 'Kunturkanki',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 732,
            'province_id' => 72,
            'name' => 'Langui',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 733,
            'province_id' => 72,
            'name' => 'Layo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 734,
            'province_id' => 72,
            'name' => 'Pampamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 735,
            'province_id' => 72,
            'name' => 'Quehue',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 736,
            'province_id' => 72,
            'name' => 'Tupac Amaru',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 737,
            'province_id' => 73,
            'name' => 'Sicuani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 738,
            'province_id' => 73,
            'name' => 'Checacupe',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 739,
            'province_id' => 73,
            'name' => 'Combapata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 740,
            'province_id' => 73,
            'name' => 'Marangani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 741,
            'province_id' => 73,
            'name' => 'Pitumarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 742,
            'province_id' => 73,
            'name' => 'San Pablo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 743,
            'province_id' => 73,
            'name' => 'San Pedro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 744,
            'province_id' => 73,
            'name' => 'Tinta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 745,
            'province_id' => 74,
            'name' => 'Santo Tomas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 746,
            'province_id' => 74,
            'name' => 'Capacmarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 747,
            'province_id' => 74,
            'name' => 'Chamaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 748,
            'province_id' => 74,
            'name' => 'Colquemarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 749,
            'province_id' => 74,
            'name' => 'Livitaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 750,
            'province_id' => 74,
            'name' => 'Llusco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 751,
            'province_id' => 74,
            'name' => 'Quiñota',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 752,
            'province_id' => 74,
            'name' => 'Velille',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 753,
            'province_id' => 75,
            'name' => 'Espinar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 754,
            'province_id' => 75,
            'name' => 'Condoroma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 755,
            'province_id' => 75,
            'name' => 'Coporaque',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 756,
            'province_id' => 75,
            'name' => 'Ocoruro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 757,
            'province_id' => 75,
            'name' => 'Pallpata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 758,
            'province_id' => 75,
            'name' => 'Pichigua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 759,
            'province_id' => 75,
            'name' => 'Suyckutambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 760,
            'province_id' => 75,
            'name' => 'Alto Pichigua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 761,
            'province_id' => 76,
            'name' => 'Santa Ana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 762,
            'province_id' => 76,
            'name' => 'Echarate',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 763,
            'province_id' => 76,
            'name' => 'Huayopata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 764,
            'province_id' => 76,
            'name' => 'Maranura',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 765,
            'province_id' => 76,
            'name' => 'Ocobamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 766,
            'province_id' => 76,
            'name' => 'Quellouno',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 767,
            'province_id' => 76,
            'name' => 'Kimbiri',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 768,
            'province_id' => 76,
            'name' => 'Santa Teresa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 769,
            'province_id' => 76,
            'name' => 'Vilcabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 770,
            'province_id' => 76,
            'name' => 'Pichari',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 771,
            'province_id' => 76,
            'name' => 'Inkawasi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 772,
            'province_id' => 76,
            'name' => 'Villa Virgen',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 773,
            'province_id' => 76,
            'name' => 'Villa Kintiarina',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 774,
            'province_id' => 76,
            'name' => 'Megantoni',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 775,
            'province_id' => 77,
            'name' => 'Paruro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 776,
            'province_id' => 77,
            'name' => 'Accha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 777,
            'province_id' => 77,
            'name' => 'Ccapi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 778,
            'province_id' => 77,
            'name' => 'Colcha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 779,
            'province_id' => 77,
            'name' => 'Huanoquite',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 780,
            'province_id' => 77,
            'name' => 'Omacha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 781,
            'province_id' => 77,
            'name' => 'Paccaritambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 782,
            'province_id' => 77,
            'name' => 'Pillpinto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 783,
            'province_id' => 77,
            'name' => 'Yaurisque',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 784,
            'province_id' => 78,
            'name' => 'Paucartambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 785,
            'province_id' => 78,
            'name' => 'Caicay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 786,
            'province_id' => 78,
            'name' => 'Challabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 787,
            'province_id' => 78,
            'name' => 'Colquepata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 788,
            'province_id' => 78,
            'name' => 'Huancarani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 789,
            'province_id' => 78,
            'name' => 'Kosñipata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 790,
            'province_id' => 79,
            'name' => 'Urcos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 791,
            'province_id' => 79,
            'name' => 'Andahuaylillas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 792,
            'province_id' => 79,
            'name' => 'Camanti',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 793,
            'province_id' => 79,
            'name' => 'Ccarhuayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 794,
            'province_id' => 79,
            'name' => 'Ccatca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 795,
            'province_id' => 79,
            'name' => 'Cusipata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 796,
            'province_id' => 79,
            'name' => 'Huaro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 797,
            'province_id' => 79,
            'name' => 'Lucre',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 798,
            'province_id' => 79,
            'name' => 'Marcapata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 799,
            'province_id' => 79,
            'name' => 'Ocongate',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 800,
            'province_id' => 79,
            'name' => 'Oropesa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 801,
            'province_id' => 79,
            'name' => 'Quiquijana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 802,
            'province_id' => 80,
            'name' => 'Urubamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 803,
            'province_id' => 80,
            'name' => 'Chinchero',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 804,
            'province_id' => 80,
            'name' => 'Huayllabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 805,
            'province_id' => 80,
            'name' => 'Machupicchu',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 806,
            'province_id' => 80,
            'name' => 'Maras',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 807,
            'province_id' => 80,
            'name' => 'Ollantaytambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 808,
            'province_id' => 80,
            'name' => 'Yucay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 809,
            'province_id' => 81,
            'name' => 'Huancavelica',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 810,
            'province_id' => 81,
            'name' => 'Acobambilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 811,
            'province_id' => 81,
            'name' => 'Acoria',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 812,
            'province_id' => 81,
            'name' => 'Conayca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 813,
            'province_id' => 81,
            'name' => 'Cuenca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 814,
            'province_id' => 81,
            'name' => 'Huachocolpa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 815,
            'province_id' => 81,
            'name' => 'Huayllahuara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 816,
            'province_id' => 81,
            'name' => 'Izcuchaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 817,
            'province_id' => 81,
            'name' => 'Laria',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 818,
            'province_id' => 81,
            'name' => 'Manta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 819,
            'province_id' => 81,
            'name' => 'Mariscal Caceres',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 820,
            'province_id' => 81,
            'name' => 'Moya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 821,
            'province_id' => 81,
            'name' => 'Nuevo Occoro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 822,
            'province_id' => 81,
            'name' => 'Palca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 823,
            'province_id' => 81,
            'name' => 'Pilchaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 824,
            'province_id' => 81,
            'name' => 'Vilca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 825,
            'province_id' => 81,
            'name' => 'Yauli',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 826,
            'province_id' => 81,
            'name' => 'Ascension',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 827,
            'province_id' => 81,
            'name' => 'Huando',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 828,
            'province_id' => 82,
            'name' => 'Acobamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 829,
            'province_id' => 82,
            'name' => 'Andabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 830,
            'province_id' => 82,
            'name' => 'Anta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 831,
            'province_id' => 82,
            'name' => 'Caja',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 832,
            'province_id' => 82,
            'name' => 'Marcas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 833,
            'province_id' => 82,
            'name' => 'Paucara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 834,
            'province_id' => 82,
            'name' => 'Pomacocha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 835,
            'province_id' => 82,
            'name' => 'Rosario',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 836,
            'province_id' => 83,
            'name' => 'Lircay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 837,
            'province_id' => 83,
            'name' => 'Anchonga',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 838,
            'province_id' => 83,
            'name' => 'Callanmarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 839,
            'province_id' => 83,
            'name' => 'Ccochaccasa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 840,
            'province_id' => 83,
            'name' => 'Chincho',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 841,
            'province_id' => 83,
            'name' => 'Congalla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 842,
            'province_id' => 83,
            'name' => 'Huanca-Huanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 843,
            'province_id' => 83,
            'name' => 'Huayllay Grande',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 844,
            'province_id' => 83,
            'name' => 'Julcamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 845,
            'province_id' => 83,
            'name' => 'San Antonio de Antaparco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 846,
            'province_id' => 83,
            'name' => 'Santo Tomas de Pata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 847,
            'province_id' => 83,
            'name' => 'Secclla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 848,
            'province_id' => 84,
            'name' => 'Castrovirreyna',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 849,
            'province_id' => 84,
            'name' => 'Arma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 850,
            'province_id' => 84,
            'name' => 'Aurahua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 851,
            'province_id' => 84,
            'name' => 'Capillas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 852,
            'province_id' => 84,
            'name' => 'Chupamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 853,
            'province_id' => 84,
            'name' => 'Cocas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 854,
            'province_id' => 84,
            'name' => 'Huachos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 855,
            'province_id' => 84,
            'name' => 'Huamatambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 856,
            'province_id' => 84,
            'name' => 'Mollepampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 857,
            'province_id' => 84,
            'name' => 'San Juan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 858,
            'province_id' => 84,
            'name' => 'Santa Ana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 859,
            'province_id' => 84,
            'name' => 'Tantara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 860,
            'province_id' => 84,
            'name' => 'Ticrapo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 861,
            'province_id' => 85,
            'name' => 'Churcampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 862,
            'province_id' => 85,
            'name' => 'Anco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 863,
            'province_id' => 85,
            'name' => 'Chinchihuasi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 864,
            'province_id' => 85,
            'name' => 'El Carmen',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 865,
            'province_id' => 85,
            'name' => 'La Merced',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 866,
            'province_id' => 85,
            'name' => 'Locroja',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 867,
            'province_id' => 85,
            'name' => 'Paucarbamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 868,
            'province_id' => 85,
            'name' => 'San Miguel de Mayocc',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 869,
            'province_id' => 85,
            'name' => 'San Pedro de Coris',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 870,
            'province_id' => 85,
            'name' => 'Pachamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 871,
            'province_id' => 85,
            'name' => 'Cosme',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 872,
            'province_id' => 86,
            'name' => 'Huaytara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 873,
            'province_id' => 86,
            'name' => 'Ayavi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 874,
            'province_id' => 86,
            'name' => 'Cordova',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 875,
            'province_id' => 86,
            'name' => 'Huayacundo Arma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 876,
            'province_id' => 86,
            'name' => 'Laramarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 877,
            'province_id' => 86,
            'name' => 'Ocoyo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 878,
            'province_id' => 86,
            'name' => 'Pilpichaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 879,
            'province_id' => 86,
            'name' => 'Querco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 880,
            'province_id' => 86,
            'name' => 'Quito-Arma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 881,
            'province_id' => 86,
            'name' => 'San Antonio de Cusicancha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 882,
            'province_id' => 86,
            'name' => 'San Francisco de Sangayaico',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 883,
            'province_id' => 86,
            'name' => 'San Isidro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 884,
            'province_id' => 86,
            'name' => 'Santiago de Chocorvos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 885,
            'province_id' => 86,
            'name' => 'Santiago de Quirahuara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 886,
            'province_id' => 86,
            'name' => 'Santo Domingo de Capillas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 887,
            'province_id' => 86,
            'name' => 'Tambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 888,
            'province_id' => 87,
            'name' => 'Pampas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 889,
            'province_id' => 87,
            'name' => 'Acostambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 890,
            'province_id' => 87,
            'name' => 'Acraquia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 891,
            'province_id' => 87,
            'name' => 'Ahuaycha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 892,
            'province_id' => 87,
            'name' => 'Colcabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 893,
            'province_id' => 87,
            'name' => 'Daniel Hernandez',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 894,
            'province_id' => 87,
            'name' => 'Huachocolpa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 895,
            'province_id' => 87,
            'name' => 'Huaribamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 896,
            'province_id' => 87,
            'name' => 'Ñahuimpuquio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 897,
            'province_id' => 87,
            'name' => 'Pazos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 898,
            'province_id' => 87,
            'name' => 'Quishuar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 899,
            'province_id' => 87,
            'name' => 'Salcabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 900,
            'province_id' => 87,
            'name' => 'Salcahuasi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 901,
            'province_id' => 87,
            'name' => 'San Marcos de Rocchac',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 902,
            'province_id' => 87,
            'name' => 'Surcubamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 903,
            'province_id' => 87,
            'name' => 'Tintay Puncu',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 904,
            'province_id' => 87,
            'name' => 'Quichuas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 905,
            'province_id' => 87,
            'name' => 'Andaymarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 906,
            'province_id' => 87,
            'name' => 'Roble',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 907,
            'province_id' => 87,
            'name' => 'Pichos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 908,
            'province_id' => 87,
            'name' => 'Santiago de Túcuma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 909,
            'province_id' => 88,
            'name' => 'Huanuco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 910,
            'province_id' => 88,
            'name' => 'Amarilis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 911,
            'province_id' => 88,
            'name' => 'Chinchao',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 912,
            'province_id' => 88,
            'name' => 'Churubamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 913,
            'province_id' => 88,
            'name' => 'Margos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 914,
            'province_id' => 88,
            'name' => 'Quisqui',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 915,
            'province_id' => 88,
            'name' => 'San Francisco de Cayran',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 916,
            'province_id' => 88,
            'name' => 'San Pedro de Chaulan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 917,
            'province_id' => 88,
            'name' => 'Santa Maria del Valle',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 918,
            'province_id' => 88,
            'name' => 'Yarumayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 919,
            'province_id' => 88,
            'name' => 'Pillco Marca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 920,
            'province_id' => 88,
            'name' => 'Yacus',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 921,
            'province_id' => 88,
            'name' => 'San Pablo de Pillao',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 922,
            'province_id' => 89,
            'name' => 'Ambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 923,
            'province_id' => 89,
            'name' => 'Cayna',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 924,
            'province_id' => 89,
            'name' => 'Colpas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 925,
            'province_id' => 89,
            'name' => 'Conchamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 926,
            'province_id' => 89,
            'name' => 'Huacar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 927,
            'province_id' => 89,
            'name' => 'San Francisco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 928,
            'province_id' => 89,
            'name' => 'San Rafael',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 929,
            'province_id' => 89,
            'name' => 'Tomay Kichwa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 930,
            'province_id' => 90,
            'name' => 'La Union',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 931,
            'province_id' => 90,
            'name' => 'Chuquis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 932,
            'province_id' => 90,
            'name' => 'Marias',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 933,
            'province_id' => 90,
            'name' => 'Pachas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 934,
            'province_id' => 90,
            'name' => 'Quivilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 935,
            'province_id' => 90,
            'name' => 'Ripan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 936,
            'province_id' => 90,
            'name' => 'Shunqui',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 937,
            'province_id' => 90,
            'name' => 'Sillapata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 938,
            'province_id' => 90,
            'name' => 'Yanas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 939,
            'province_id' => 91,
            'name' => 'Huacaybamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 940,
            'province_id' => 91,
            'name' => 'Canchabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 941,
            'province_id' => 91,
            'name' => 'Cochabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 942,
            'province_id' => 91,
            'name' => 'Pinra',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 943,
            'province_id' => 92,
            'name' => 'Llata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 944,
            'province_id' => 92,
            'name' => 'Arancay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 945,
            'province_id' => 92,
            'name' => 'Chavin de Pariarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 946,
            'province_id' => 92,
            'name' => 'Jacas Grande',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 947,
            'province_id' => 92,
            'name' => 'Jircan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 948,
            'province_id' => 92,
            'name' => 'Miraflores',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 949,
            'province_id' => 92,
            'name' => 'Monzon',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 950,
            'province_id' => 92,
            'name' => 'Punchao',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 951,
            'province_id' => 92,
            'name' => 'Puños',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 952,
            'province_id' => 92,
            'name' => 'Singa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 953,
            'province_id' => 92,
            'name' => 'Tantamayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 954,
            'province_id' => 93,
            'name' => 'Rupa-Rupa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 955,
            'province_id' => 93,
            'name' => 'Daniel Alomias Robles',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 956,
            'province_id' => 93,
            'name' => 'Hermilio Valdizan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 957,
            'province_id' => 93,
            'name' => 'Jose Crespo y Castillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 958,
            'province_id' => 93,
            'name' => 'Luyando',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 959,
            'province_id' => 93,
            'name' => 'Mariano Damaso Beraun',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 960,
            'province_id' => 93,
            'name' => 'Pucayacu',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 961,
            'province_id' => 93,
            'name' => 'Castillo Grande',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 962,
            'province_id' => 93,
            'name' => 'Pueblo Nuevo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 963,
            'province_id' => 93,
            'name' => 'Santo Domingo de Anda',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 964,
            'province_id' => 94,
            'name' => 'Huacrachuco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 965,
            'province_id' => 94,
            'name' => 'Cholon',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 966,
            'province_id' => 94,
            'name' => 'San Buenaventura',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 967,
            'province_id' => 94,
            'name' => 'La Morada',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 968,
            'province_id' => 94,
            'name' => 'Santa Rosa de Alto Yanajanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 969,
            'province_id' => 95,
            'name' => 'Panao',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 970,
            'province_id' => 95,
            'name' => 'Chaglla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 971,
            'province_id' => 95,
            'name' => 'Molino',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 972,
            'province_id' => 95,
            'name' => 'Umari',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 973,
            'province_id' => 96,
            'name' => 'Puerto Inca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 974,
            'province_id' => 96,
            'name' => 'Codo del Pozuzo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 975,
            'province_id' => 96,
            'name' => 'Honoria',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 976,
            'province_id' => 96,
            'name' => 'Tournavista',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 977,
            'province_id' => 96,
            'name' => 'Yuyapichis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 978,
            'province_id' => 97,
            'name' => 'Jesus',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 979,
            'province_id' => 97,
            'name' => 'Baños',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 980,
            'province_id' => 97,
            'name' => 'Jivia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 981,
            'province_id' => 97,
            'name' => 'Queropalca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 982,
            'province_id' => 97,
            'name' => 'Rondos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 983,
            'province_id' => 97,
            'name' => 'San Francisco de Asis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 984,
            'province_id' => 97,
            'name' => 'San Miguel de Cauri',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 985,
            'province_id' => 98,
            'name' => 'Chavinillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 986,
            'province_id' => 98,
            'name' => 'Cahuac',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 987,
            'province_id' => 98,
            'name' => 'Chacabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 988,
            'province_id' => 98,
            'name' => 'Aparicio Pomares',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 989,
            'province_id' => 98,
            'name' => 'Jacas Chico',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 990,
            'province_id' => 98,
            'name' => 'Obas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 991,
            'province_id' => 98,
            'name' => 'Pampamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 992,
            'province_id' => 98,
            'name' => 'Choras',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 993,
            'province_id' => 99,
            'name' => 'Ica',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 994,
            'province_id' => 99,
            'name' => 'La Tinguiña',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 995,
            'province_id' => 99,
            'name' => 'Los Aquijes',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 996,
            'province_id' => 99,
            'name' => 'Ocucaje',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 997,
            'province_id' => 99,
            'name' => 'Pachacutec',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 998,
            'province_id' => 99,
            'name' => 'Parcona',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 999,
            'province_id' => 99,
            'name' => 'Pueblo Nuevo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1000,
            'province_id' => 99,
            'name' => 'Salas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1001,
            'province_id' => 99,
            'name' => 'San Jose de los Molinos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1002,
            'province_id' => 99,
            'name' => 'San Juan Bautista',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1003,
            'province_id' => 99,
            'name' => 'Santiago',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1004,
            'province_id' => 99,
            'name' => 'Subtanjalla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1005,
            'province_id' => 99,
            'name' => 'Tate',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1006,
            'province_id' => 99,
            'name' => 'Yauca del Rosario',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1007,
            'province_id' => 100,
            'name' => 'Chincha Alta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1008,
            'province_id' => 100,
            'name' => 'Alto Laran',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1009,
            'province_id' => 100,
            'name' => 'Chavin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1010,
            'province_id' => 100,
            'name' => 'Chincha Baja',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1011,
            'province_id' => 100,
            'name' => 'El Carmen',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1012,
            'province_id' => 100,
            'name' => 'Grocio Prado',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1013,
            'province_id' => 100,
            'name' => 'Pueblo Nuevo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1014,
            'province_id' => 100,
            'name' => 'San Juan de Yanac',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1015,
            'province_id' => 100,
            'name' => 'San Pedro de Huacarpana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1016,
            'province_id' => 100,
            'name' => 'Sunampe',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1017,
            'province_id' => 100,
            'name' => 'Tambo de Mora',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1018,
            'province_id' => 101,
            'name' => 'Nazca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1019,
            'province_id' => 101,
            'name' => 'Changuillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1020,
            'province_id' => 101,
            'name' => 'El Ingenio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1021,
            'province_id' => 101,
            'name' => 'Marcona',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1022,
            'province_id' => 101,
            'name' => 'Vista Alegre',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1023,
            'province_id' => 102,
            'name' => 'Palpa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1024,
            'province_id' => 102,
            'name' => 'Llipata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1025,
            'province_id' => 102,
            'name' => 'Rio Grande',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1026,
            'province_id' => 102,
            'name' => 'Santa Cruz',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1027,
            'province_id' => 102,
            'name' => 'Tibillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1028,
            'province_id' => 103,
            'name' => 'Pisco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1029,
            'province_id' => 103,
            'name' => 'Huancano',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1030,
            'province_id' => 103,
            'name' => 'Humay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1031,
            'province_id' => 103,
            'name' => 'Independencia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1032,
            'province_id' => 103,
            'name' => 'Paracas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1033,
            'province_id' => 103,
            'name' => 'San Andres',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1034,
            'province_id' => 103,
            'name' => 'San Clemente',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1035,
            'province_id' => 103,
            'name' => 'Tupac Amaru Inca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1036,
            'province_id' => 104,
            'name' => 'Huancayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1037,
            'province_id' => 104,
            'name' => 'Carhuacallanga',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1038,
            'province_id' => 104,
            'name' => 'Chacapampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1039,
            'province_id' => 104,
            'name' => 'Chicche',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1040,
            'province_id' => 104,
            'name' => 'Chilca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1041,
            'province_id' => 104,
            'name' => 'Chongos Alto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1042,
            'province_id' => 104,
            'name' => 'Chupuro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1043,
            'province_id' => 104,
            'name' => 'Colca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1044,
            'province_id' => 104,
            'name' => 'Cullhuas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1045,
            'province_id' => 104,
            'name' => 'El Tambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1046,
            'province_id' => 104,
            'name' => 'Huacrapuquio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1047,
            'province_id' => 104,
            'name' => 'Hualhuas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1048,
            'province_id' => 104,
            'name' => 'Huancan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1049,
            'province_id' => 104,
            'name' => 'Huasicancha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1050,
            'province_id' => 104,
            'name' => 'Huayucachi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1051,
            'province_id' => 104,
            'name' => 'Ingenio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1052,
            'province_id' => 104,
            'name' => 'Pariahuanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1053,
            'province_id' => 104,
            'name' => 'Pilcomayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1054,
            'province_id' => 104,
            'name' => 'Pucara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1055,
            'province_id' => 104,
            'name' => 'Quichuay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1056,
            'province_id' => 104,
            'name' => 'Quilcas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1057,
            'province_id' => 104,
            'name' => 'San Agustin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1058,
            'province_id' => 104,
            'name' => 'San Jeronimo de Tunan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1059,
            'province_id' => 104,
            'name' => 'Saño',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1060,
            'province_id' => 104,
            'name' => 'Sapallanga',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1061,
            'province_id' => 104,
            'name' => 'Sicaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1062,
            'province_id' => 104,
            'name' => 'Santo Domingo de Acobamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1063,
            'province_id' => 104,
            'name' => 'Viques',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1064,
            'province_id' => 105,
            'name' => 'Concepcion',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1065,
            'province_id' => 105,
            'name' => 'Aco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1066,
            'province_id' => 105,
            'name' => 'Andamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1067,
            'province_id' => 105,
            'name' => 'Chambara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1068,
            'province_id' => 105,
            'name' => 'Cochas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1069,
            'province_id' => 105,
            'name' => 'Comas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1070,
            'province_id' => 105,
            'name' => 'Heroinas Toledo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1071,
            'province_id' => 105,
            'name' => 'Manzanares',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1072,
            'province_id' => 105,
            'name' => 'Mariscal Castilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1073,
            'province_id' => 105,
            'name' => 'Matahuasi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1074,
            'province_id' => 105,
            'name' => 'Mito',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1075,
            'province_id' => 105,
            'name' => 'Nueve de Julio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1076,
            'province_id' => 105,
            'name' => 'Orcotuna',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1077,
            'province_id' => 105,
            'name' => 'San Jose de Quero',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1078,
            'province_id' => 105,
            'name' => 'Santa Rosa de Ocopa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1079,
            'province_id' => 106,
            'name' => 'Chanchamayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1080,
            'province_id' => 106,
            'name' => 'Perene',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1081,
            'province_id' => 106,
            'name' => 'Pichanaqui',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1082,
            'province_id' => 106,
            'name' => 'San Luis de Shuaro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1083,
            'province_id' => 106,
            'name' => 'San Ramon',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1084,
            'province_id' => 106,
            'name' => 'Vitoc',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1085,
            'province_id' => 107,
            'name' => 'Jauja',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1086,
            'province_id' => 107,
            'name' => 'Acolla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1087,
            'province_id' => 107,
            'name' => 'Apata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1088,
            'province_id' => 107,
            'name' => 'Ataura',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1089,
            'province_id' => 107,
            'name' => 'Canchayllo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1090,
            'province_id' => 107,
            'name' => 'Curicaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1091,
            'province_id' => 107,
            'name' => 'El Mantaro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1092,
            'province_id' => 107,
            'name' => 'Huamali',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1093,
            'province_id' => 107,
            'name' => 'Huaripampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1094,
            'province_id' => 107,
            'name' => 'Huertas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1095,
            'province_id' => 107,
            'name' => 'Janjaillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1096,
            'province_id' => 107,
            'name' => 'Julcan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1097,
            'province_id' => 107,
            'name' => 'Leonor Ordoñez',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1098,
            'province_id' => 107,
            'name' => 'Llocllapampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1099,
            'province_id' => 107,
            'name' => 'Marco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1100,
            'province_id' => 107,
            'name' => 'Masma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1101,
            'province_id' => 107,
            'name' => 'Masma Chicche',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1102,
            'province_id' => 107,
            'name' => 'Molinos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1103,
            'province_id' => 107,
            'name' => 'Monobamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1104,
            'province_id' => 107,
            'name' => 'Muqui',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1105,
            'province_id' => 107,
            'name' => 'Muquiyauyo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1106,
            'province_id' => 107,
            'name' => 'Paca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1107,
            'province_id' => 107,
            'name' => 'Paccha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1108,
            'province_id' => 107,
            'name' => 'Pancan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1109,
            'province_id' => 107,
            'name' => 'Parco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1110,
            'province_id' => 107,
            'name' => 'Pomacancha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1111,
            'province_id' => 107,
            'name' => 'Ricran',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1112,
            'province_id' => 107,
            'name' => 'San Lorenzo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1113,
            'province_id' => 107,
            'name' => 'San Pedro de Chunan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1114,
            'province_id' => 107,
            'name' => 'Sausa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1115,
            'province_id' => 107,
            'name' => 'Sincos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1116,
            'province_id' => 107,
            'name' => 'Tunan Marca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1117,
            'province_id' => 107,
            'name' => 'Yauli',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1118,
            'province_id' => 107,
            'name' => 'Yauyos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1119,
            'province_id' => 108,
            'name' => 'Junin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1120,
            'province_id' => 108,
            'name' => 'Carhuamayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1121,
            'province_id' => 108,
            'name' => 'Ondores',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1122,
            'province_id' => 108,
            'name' => 'Ulcumayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1123,
            'province_id' => 109,
            'name' => 'Satipo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1124,
            'province_id' => 109,
            'name' => 'Coviriali',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1125,
            'province_id' => 109,
            'name' => 'Llaylla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1126,
            'province_id' => 109,
            'name' => 'Mazamari',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1127,
            'province_id' => 109,
            'name' => 'Pampa Hermosa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1128,
            'province_id' => 109,
            'name' => 'Pangoa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1129,
            'province_id' => 109,
            'name' => 'Rio Negro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1130,
            'province_id' => 109,
            'name' => 'Rio Tambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1131,
            'province_id' => 109,
            'name' => 'Vizcatán del Ene',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1132,
            'province_id' => 110,
            'name' => 'Tarma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1133,
            'province_id' => 110,
            'name' => 'Acobamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1134,
            'province_id' => 110,
            'name' => 'Huaricolca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1135,
            'province_id' => 110,
            'name' => 'Huasahuasi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1136,
            'province_id' => 110,
            'name' => 'La Union',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1137,
            'province_id' => 110,
            'name' => 'Palca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1138,
            'province_id' => 110,
            'name' => 'Palcamayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1139,
            'province_id' => 110,
            'name' => 'San Pedro de Cajas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1140,
            'province_id' => 110,
            'name' => 'Tapo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1141,
            'province_id' => 111,
            'name' => 'La Oroya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1142,
            'province_id' => 111,
            'name' => 'Chacapalpa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1143,
            'province_id' => 111,
            'name' => 'Huay-Huay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1144,
            'province_id' => 111,
            'name' => 'Marcapomacocha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1145,
            'province_id' => 111,
            'name' => 'Morococha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1146,
            'province_id' => 111,
            'name' => 'Paccha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1147,
            'province_id' => 111,
            'name' => 'Santa Barbara de Carhuacayan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1148,
            'province_id' => 111,
            'name' => 'Santa Rosa de Sacco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1149,
            'province_id' => 111,
            'name' => 'Suitucancha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1150,
            'province_id' => 111,
            'name' => 'Yauli',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1151,
            'province_id' => 112,
            'name' => 'Chupaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1152,
            'province_id' => 112,
            'name' => 'Ahuac',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1153,
            'province_id' => 112,
            'name' => 'Chongos Bajo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1154,
            'province_id' => 112,
            'name' => 'Huachac',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1155,
            'province_id' => 112,
            'name' => 'Huamancaca Chico',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1156,
            'province_id' => 112,
            'name' => 'San Juan de Yscos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1157,
            'province_id' => 112,
            'name' => 'San Juan de Jarpa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1158,
            'province_id' => 112,
            'name' => 'Tres de Diciembre',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1159,
            'province_id' => 112,
            'name' => 'Yanacancha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1160,
            'province_id' => 113,
            'name' => 'Trujillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1161,
            'province_id' => 113,
            'name' => 'El Porvenir',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1162,
            'province_id' => 113,
            'name' => 'Florencia de Mora',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1163,
            'province_id' => 113,
            'name' => 'Huanchaco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1164,
            'province_id' => 113,
            'name' => 'La Esperanza',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1165,
            'province_id' => 113,
            'name' => 'Laredo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1166,
            'province_id' => 113,
            'name' => 'Moche',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1167,
            'province_id' => 113,
            'name' => 'Poroto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1168,
            'province_id' => 113,
            'name' => 'Salaverry',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1169,
            'province_id' => 113,
            'name' => 'Simbal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1170,
            'province_id' => 113,
            'name' => 'Victor Larco Herrera',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1171,
            'province_id' => 114,
            'name' => 'Ascope',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1172,
            'province_id' => 114,
            'name' => 'Chicama',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1173,
            'province_id' => 114,
            'name' => 'Chocope',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1174,
            'province_id' => 114,
            'name' => 'Magdalena de Cao',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1175,
            'province_id' => 114,
            'name' => 'Paijan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1176,
            'province_id' => 114,
            'name' => 'Razuri',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1177,
            'province_id' => 114,
            'name' => 'Santiago de Cao',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1178,
            'province_id' => 114,
            'name' => 'Casa Grande',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1179,
            'province_id' => 115,
            'name' => 'Bolivar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1180,
            'province_id' => 115,
            'name' => 'Bambamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1181,
            'province_id' => 115,
            'name' => 'Condormarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1182,
            'province_id' => 115,
            'name' => 'Longotea',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1183,
            'province_id' => 115,
            'name' => 'Uchumarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1184,
            'province_id' => 115,
            'name' => 'Ucuncha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1185,
            'province_id' => 116,
            'name' => 'Chepen',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1186,
            'province_id' => 116,
            'name' => 'Pacanga',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1187,
            'province_id' => 116,
            'name' => 'Pueblo Nuevo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1188,
            'province_id' => 117,
            'name' => 'Julcan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1189,
            'province_id' => 117,
            'name' => 'Calamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1190,
            'province_id' => 117,
            'name' => 'Carabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1191,
            'province_id' => 117,
            'name' => 'Huaso',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1192,
            'province_id' => 118,
            'name' => 'Otuzco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1193,
            'province_id' => 118,
            'name' => 'Agallpampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1194,
            'province_id' => 118,
            'name' => 'Charat',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1195,
            'province_id' => 118,
            'name' => 'Huaranchal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1196,
            'province_id' => 118,
            'name' => 'La Cuesta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1197,
            'province_id' => 118,
            'name' => 'Mache',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1198,
            'province_id' => 118,
            'name' => 'Paranday',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1199,
            'province_id' => 118,
            'name' => 'Salpo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1200,
            'province_id' => 118,
            'name' => 'Sinsicap',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1201,
            'province_id' => 118,
            'name' => 'Usquil',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1202,
            'province_id' => 119,
            'name' => 'San Pedro de Lloc',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1203,
            'province_id' => 119,
            'name' => 'Guadalupe',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1204,
            'province_id' => 119,
            'name' => 'Jequetepeque',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1205,
            'province_id' => 119,
            'name' => 'Pacasmayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1206,
            'province_id' => 119,
            'name' => 'San Jose',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1207,
            'province_id' => 120,
            'name' => 'Tayabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1208,
            'province_id' => 120,
            'name' => 'Buldibuyo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1209,
            'province_id' => 120,
            'name' => 'Chillia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1210,
            'province_id' => 120,
            'name' => 'Huancaspata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1211,
            'province_id' => 120,
            'name' => 'Huaylillas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1212,
            'province_id' => 120,
            'name' => 'Huayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1213,
            'province_id' => 120,
            'name' => 'Ongon',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1214,
            'province_id' => 120,
            'name' => 'Parcoy',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1215,
            'province_id' => 120,
            'name' => 'Pataz',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1216,
            'province_id' => 120,
            'name' => 'Pias',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1217,
            'province_id' => 120,
            'name' => 'Santiago de Challas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1218,
            'province_id' => 120,
            'name' => 'Taurija',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1219,
            'province_id' => 120,
            'name' => 'Urpay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1220,
            'province_id' => 121,
            'name' => 'Huamachuco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1221,
            'province_id' => 121,
            'name' => 'Chugay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1222,
            'province_id' => 121,
            'name' => 'Cochorco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1223,
            'province_id' => 121,
            'name' => 'Curgos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1224,
            'province_id' => 121,
            'name' => 'Marcabal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1225,
            'province_id' => 121,
            'name' => 'Sanagoran',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1226,
            'province_id' => 121,
            'name' => 'Sarin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1227,
            'province_id' => 121,
            'name' => 'Sartimbamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1228,
            'province_id' => 122,
            'name' => 'Santiago de Chuco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1229,
            'province_id' => 122,
            'name' => 'Angasmarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1230,
            'province_id' => 122,
            'name' => 'Cachicadan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1231,
            'province_id' => 122,
            'name' => 'Mollebamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1232,
            'province_id' => 122,
            'name' => 'Mollepata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1233,
            'province_id' => 122,
            'name' => 'Quiruvilca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1234,
            'province_id' => 122,
            'name' => 'Santa Cruz de Chuca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1235,
            'province_id' => 122,
            'name' => 'Sitabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1236,
            'province_id' => 123,
            'name' => 'Cascas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1237,
            'province_id' => 123,
            'name' => 'Lucma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1238,
            'province_id' => 123,
            'name' => 'Compin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1239,
            'province_id' => 123,
            'name' => 'Sayapullo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1240,
            'province_id' => 124,
            'name' => 'Viru',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1241,
            'province_id' => 124,
            'name' => 'Chao',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1242,
            'province_id' => 124,
            'name' => 'Guadalupito',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1243,
            'province_id' => 125,
            'name' => 'Chiclayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1244,
            'province_id' => 125,
            'name' => 'Chongoyape',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1245,
            'province_id' => 125,
            'name' => 'Eten',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1246,
            'province_id' => 125,
            'name' => 'Eten Puerto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1247,
            'province_id' => 125,
            'name' => 'Jose Leonardo Ortiz',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1248,
            'province_id' => 125,
            'name' => 'La Victoria',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1249,
            'province_id' => 125,
            'name' => 'Lagunas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1250,
            'province_id' => 125,
            'name' => 'Monsefu',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1251,
            'province_id' => 125,
            'name' => 'Nueva Arica',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1252,
            'province_id' => 125,
            'name' => 'Oyotun',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1253,
            'province_id' => 125,
            'name' => 'Picsi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1254,
            'province_id' => 125,
            'name' => 'Pimentel',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1255,
            'province_id' => 125,
            'name' => 'Reque',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1256,
            'province_id' => 125,
            'name' => 'Santa Rosa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1257,
            'province_id' => 125,
            'name' => 'Saña',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1258,
            'province_id' => 125,
            'name' => 'Cayalti',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1259,
            'province_id' => 125,
            'name' => 'Patapo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1260,
            'province_id' => 125,
            'name' => 'Pomalca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1261,
            'province_id' => 125,
            'name' => 'Pucala',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1262,
            'province_id' => 125,
            'name' => 'Tuman',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1263,
            'province_id' => 126,
            'name' => 'Ferreñafe',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1264,
            'province_id' => 126,
            'name' => 'Cañaris',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1265,
            'province_id' => 126,
            'name' => 'Incahuasi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1266,
            'province_id' => 126,
            'name' => 'Manuel Antonio Mesones Muro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1267,
            'province_id' => 126,
            'name' => 'Pitipo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1268,
            'province_id' => 126,
            'name' => 'Pueblo Nuevo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1269,
            'province_id' => 127,
            'name' => 'Lambayeque',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1270,
            'province_id' => 127,
            'name' => 'Chochope',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1271,
            'province_id' => 127,
            'name' => 'Illimo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1272,
            'province_id' => 127,
            'name' => 'Jayanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1273,
            'province_id' => 127,
            'name' => 'Mochumi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1274,
            'province_id' => 127,
            'name' => 'Morrope',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1275,
            'province_id' => 127,
            'name' => 'Motupe',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1276,
            'province_id' => 127,
            'name' => 'Olmos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1277,
            'province_id' => 127,
            'name' => 'Pacora',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1278,
            'province_id' => 127,
            'name' => 'Salas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1279,
            'province_id' => 127,
            'name' => 'San Jose',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1280,
            'province_id' => 127,
            'name' => 'Tucume',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1281,
            'province_id' => 128,
            'name' => 'Lima',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1282,
            'province_id' => 128,
            'name' => 'Ancon',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1283,
            'province_id' => 128,
            'name' => 'Ate',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1284,
            'province_id' => 128,
            'name' => 'Barranco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1285,
            'province_id' => 128,
            'name' => 'Breña',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1286,
            'province_id' => 128,
            'name' => 'Carabayllo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1287,
            'province_id' => 128,
            'name' => 'Chaclacayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1288,
            'province_id' => 128,
            'name' => 'Chorrillos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1289,
            'province_id' => 128,
            'name' => 'Cieneguilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1290,
            'province_id' => 128,
            'name' => 'Comas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1291,
            'province_id' => 128,
            'name' => 'El Agustino',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1292,
            'province_id' => 128,
            'name' => 'Independencia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1293,
            'province_id' => 128,
            'name' => 'Jesus Maria',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1294,
            'province_id' => 128,
            'name' => 'La Molina',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1295,
            'province_id' => 128,
            'name' => 'La Victoria',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1296,
            'province_id' => 128,
            'name' => 'Lince',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1297,
            'province_id' => 128,
            'name' => 'Los Olivos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1298,
            'province_id' => 128,
            'name' => 'Lurigancho',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1299,
            'province_id' => 128,
            'name' => 'Lurin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1300,
            'province_id' => 128,
            'name' => 'Magdalena del Mar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1301,
            'province_id' => 128,
            'name' => 'Pueblo Libre',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1302,
            'province_id' => 128,
            'name' => 'Miraflores',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1303,
            'province_id' => 128,
            'name' => 'Pachacamac',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1304,
            'province_id' => 128,
            'name' => 'Pucusana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1305,
            'province_id' => 128,
            'name' => 'Puente Piedra',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1306,
            'province_id' => 128,
            'name' => 'Punta Hermosa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1307,
            'province_id' => 128,
            'name' => 'Punta Negra',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1308,
            'province_id' => 128,
            'name' => 'Rimac',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1309,
            'province_id' => 128,
            'name' => 'San Bartolo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1310,
            'province_id' => 128,
            'name' => 'San Borja',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1311,
            'province_id' => 128,
            'name' => 'San Isidro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1312,
            'province_id' => 128,
            'name' => 'San Juan de Lurigancho',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1313,
            'province_id' => 128,
            'name' => 'San Juan de Miraflores',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1314,
            'province_id' => 128,
            'name' => 'San Luis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1315,
            'province_id' => 128,
            'name' => 'San Martin de Porres',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1316,
            'province_id' => 128,
            'name' => 'San Miguel',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1317,
            'province_id' => 128,
            'name' => 'Santa Anita',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1318,
            'province_id' => 128,
            'name' => 'Santa Maria del Mar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1319,
            'province_id' => 128,
            'name' => 'Santa Rosa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1320,
            'province_id' => 128,
            'name' => 'Santiago de Surco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1321,
            'province_id' => 128,
            'name' => 'Surquillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1322,
            'province_id' => 128,
            'name' => 'Villa El Salvador',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1323,
            'province_id' => 128,
            'name' => 'Villa Maria del Triunfo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1324,
            'province_id' => 129,
            'name' => 'Barranca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1325,
            'province_id' => 129,
            'name' => 'Paramonga',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1326,
            'province_id' => 129,
            'name' => 'Pativilca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1327,
            'province_id' => 129,
            'name' => 'Supe',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1328,
            'province_id' => 129,
            'name' => 'Supe Puerto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1329,
            'province_id' => 130,
            'name' => 'Cajatambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1330,
            'province_id' => 130,
            'name' => 'Copa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1331,
            'province_id' => 130,
            'name' => 'Gorgor',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1332,
            'province_id' => 130,
            'name' => 'Huancapon',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1333,
            'province_id' => 130,
            'name' => 'Manas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1334,
            'province_id' => 131,
            'name' => 'Canta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1335,
            'province_id' => 131,
            'name' => 'Arahuay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1336,
            'province_id' => 131,
            'name' => 'Huamantanga',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1337,
            'province_id' => 131,
            'name' => 'Huaros',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1338,
            'province_id' => 131,
            'name' => 'Lachaqui',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1339,
            'province_id' => 131,
            'name' => 'San Buenaventura',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1340,
            'province_id' => 131,
            'name' => 'Santa Rosa de Quives',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1341,
            'province_id' => 132,
            'name' => 'San Vicente de Cañete',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1342,
            'province_id' => 132,
            'name' => 'Asia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1343,
            'province_id' => 132,
            'name' => 'Calango',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1344,
            'province_id' => 132,
            'name' => 'Cerro Azul',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1345,
            'province_id' => 132,
            'name' => 'Chilca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1346,
            'province_id' => 132,
            'name' => 'Coayllo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1347,
            'province_id' => 132,
            'name' => 'Imperial',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1348,
            'province_id' => 132,
            'name' => 'Lunahuana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1349,
            'province_id' => 132,
            'name' => 'Mala',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1350,
            'province_id' => 132,
            'name' => 'Nuevo Imperial',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1351,
            'province_id' => 132,
            'name' => 'Pacaran',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1352,
            'province_id' => 132,
            'name' => 'Quilmana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1353,
            'province_id' => 132,
            'name' => 'San Antonio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1354,
            'province_id' => 132,
            'name' => 'San Luis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1355,
            'province_id' => 132,
            'name' => 'Santa Cruz de Flores',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1356,
            'province_id' => 132,
            'name' => 'Zuñiga',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1357,
            'province_id' => 133,
            'name' => 'Huaral',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1358,
            'province_id' => 133,
            'name' => 'Atavillos Alto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1359,
            'province_id' => 133,
            'name' => 'Atavillos Bajo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1360,
            'province_id' => 133,
            'name' => 'Aucallama',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1361,
            'province_id' => 133,
            'name' => 'Chancay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1362,
            'province_id' => 133,
            'name' => 'Ihuari',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1363,
            'province_id' => 133,
            'name' => 'Lampian',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1364,
            'province_id' => 133,
            'name' => 'Pacaraos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1365,
            'province_id' => 133,
            'name' => 'San Miguel de Acos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1366,
            'province_id' => 133,
            'name' => 'Santa Cruz de Andamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1367,
            'province_id' => 133,
            'name' => 'Sumbilca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1368,
            'province_id' => 133,
            'name' => 'Veintisiete de Noviembre',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1369,
            'province_id' => 134,
            'name' => 'Matucana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1370,
            'province_id' => 134,
            'name' => 'Antioquia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1371,
            'province_id' => 134,
            'name' => 'Callahuanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1372,
            'province_id' => 134,
            'name' => 'Carampoma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1373,
            'province_id' => 134,
            'name' => 'Chicla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1374,
            'province_id' => 134,
            'name' => 'Cuenca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1375,
            'province_id' => 134,
            'name' => 'Huachupampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1376,
            'province_id' => 134,
            'name' => 'Huanza',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1377,
            'province_id' => 134,
            'name' => 'Huarochiri',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1378,
            'province_id' => 134,
            'name' => 'Lahuaytambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1379,
            'province_id' => 134,
            'name' => 'Langa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1380,
            'province_id' => 134,
            'name' => 'Laraos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1381,
            'province_id' => 134,
            'name' => 'Mariatana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1382,
            'province_id' => 134,
            'name' => 'Ricardo Palma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1383,
            'province_id' => 134,
            'name' => 'San Andres de Tupicocha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1384,
            'province_id' => 134,
            'name' => 'San Antonio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1385,
            'province_id' => 134,
            'name' => 'San Bartolome',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1386,
            'province_id' => 134,
            'name' => 'San Damian',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1387,
            'province_id' => 134,
            'name' => 'San Juan de Iris',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1388,
            'province_id' => 134,
            'name' => 'San Juan de Tantaranche',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1389,
            'province_id' => 134,
            'name' => 'San Lorenzo de Quinti',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1390,
            'province_id' => 134,
            'name' => 'San Mateo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1391,
            'province_id' => 134,
            'name' => 'San Mateo de Otao',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1392,
            'province_id' => 134,
            'name' => 'San Pedro de Casta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1393,
            'province_id' => 134,
            'name' => 'San Pedro de Huancayre',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1394,
            'province_id' => 134,
            'name' => 'Sangallaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1395,
            'province_id' => 134,
            'name' => 'Santa Cruz de Cocachacra',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1396,
            'province_id' => 134,
            'name' => 'Santa Eulalia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1397,
            'province_id' => 134,
            'name' => 'Santiago de Anchucaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1398,
            'province_id' => 134,
            'name' => 'Santiago de Tuna',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1399,
            'province_id' => 134,
            'name' => 'Santo Domingo de los Olleros',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1400,
            'province_id' => 134,
            'name' => 'Surco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1401,
            'province_id' => 135,
            'name' => 'Huacho',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1402,
            'province_id' => 135,
            'name' => 'Ambar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1403,
            'province_id' => 135,
            'name' => 'Caleta de Carquin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1404,
            'province_id' => 135,
            'name' => 'Checras',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1405,
            'province_id' => 135,
            'name' => 'Hualmay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1406,
            'province_id' => 135,
            'name' => 'Huaura',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1407,
            'province_id' => 135,
            'name' => 'Leoncio Prado',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1408,
            'province_id' => 135,
            'name' => 'Paccho',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1409,
            'province_id' => 135,
            'name' => 'Santa Leonor',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1410,
            'province_id' => 135,
            'name' => 'Santa Maria',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1411,
            'province_id' => 135,
            'name' => 'Sayan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1412,
            'province_id' => 135,
            'name' => 'Vegueta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1413,
            'province_id' => 136,
            'name' => 'Oyon',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1414,
            'province_id' => 136,
            'name' => 'Andajes',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1415,
            'province_id' => 136,
            'name' => 'Caujul',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1416,
            'province_id' => 136,
            'name' => 'Cochamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1417,
            'province_id' => 136,
            'name' => 'Navan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1418,
            'province_id' => 136,
            'name' => 'Pachangara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1419,
            'province_id' => 137,
            'name' => 'Yauyos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1420,
            'province_id' => 137,
            'name' => 'Alis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1421,
            'province_id' => 137,
            'name' => 'Ayauca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1422,
            'province_id' => 137,
            'name' => 'Ayaviri',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1423,
            'province_id' => 137,
            'name' => 'Azangaro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1424,
            'province_id' => 137,
            'name' => 'Cacra',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1425,
            'province_id' => 137,
            'name' => 'Carania',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1426,
            'province_id' => 137,
            'name' => 'Catahuasi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1427,
            'province_id' => 137,
            'name' => 'Chocos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1428,
            'province_id' => 137,
            'name' => 'Cochas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1429,
            'province_id' => 137,
            'name' => 'Colonia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1430,
            'province_id' => 137,
            'name' => 'Hongos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1431,
            'province_id' => 137,
            'name' => 'Huampara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1432,
            'province_id' => 137,
            'name' => 'Huancaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1433,
            'province_id' => 137,
            'name' => 'Huangascar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1434,
            'province_id' => 137,
            'name' => 'Huantan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1435,
            'province_id' => 137,
            'name' => 'Huañec',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1436,
            'province_id' => 137,
            'name' => 'Laraos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1437,
            'province_id' => 137,
            'name' => 'Lincha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1438,
            'province_id' => 137,
            'name' => 'Madean',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1439,
            'province_id' => 137,
            'name' => 'Miraflores',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1440,
            'province_id' => 137,
            'name' => 'Omas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1441,
            'province_id' => 137,
            'name' => 'Putinza',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1442,
            'province_id' => 137,
            'name' => 'Quinches',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1443,
            'province_id' => 137,
            'name' => 'Quinocay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1444,
            'province_id' => 137,
            'name' => 'San Joaquin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1445,
            'province_id' => 137,
            'name' => 'San Pedro de Pilas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1446,
            'province_id' => 137,
            'name' => 'Tanta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1447,
            'province_id' => 137,
            'name' => 'Tauripampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1448,
            'province_id' => 137,
            'name' => 'Tomas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1449,
            'province_id' => 137,
            'name' => 'Tupe',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1450,
            'province_id' => 137,
            'name' => 'Viñac',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1451,
            'province_id' => 137,
            'name' => 'Vitis',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1452,
            'province_id' => 138,
            'name' => 'Iquitos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1453,
            'province_id' => 138,
            'name' => 'Alto Nanay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1454,
            'province_id' => 138,
            'name' => 'Fernando Lores',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1455,
            'province_id' => 138,
            'name' => 'Indiana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1456,
            'province_id' => 138,
            'name' => 'Las Amazonas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1457,
            'province_id' => 138,
            'name' => 'Mazan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1458,
            'province_id' => 138,
            'name' => 'Napo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1459,
            'province_id' => 138,
            'name' => 'Punchana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1460,
            'province_id' => 138,
            'name' => 'Torres Causana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1461,
            'province_id' => 138,
            'name' => 'Belen',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1462,
            'province_id' => 138,
            'name' => 'San Juan Bautista',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1463,
            'province_id' => 139,
            'name' => 'Yurimaguas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1464,
            'province_id' => 139,
            'name' => 'Balsapuerto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1465,
            'province_id' => 139,
            'name' => 'Jeberos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1466,
            'province_id' => 139,
            'name' => 'Lagunas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1467,
            'province_id' => 139,
            'name' => 'Santa Cruz',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1468,
            'province_id' => 139,
            'name' => 'Teniente Cesar Lopez Rojas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1469,
            'province_id' => 140,
            'name' => 'Nauta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1470,
            'province_id' => 140,
            'name' => 'Parinari',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1471,
            'province_id' => 140,
            'name' => 'Tigre',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1472,
            'province_id' => 140,
            'name' => 'Trompeteros',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1473,
            'province_id' => 140,
            'name' => 'Urarinas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1474,
            'province_id' => 141,
            'name' => 'Ramon Castilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1475,
            'province_id' => 141,
            'name' => 'Pebas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1476,
            'province_id' => 141,
            'name' => 'Yavari',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1477,
            'province_id' => 141,
            'name' => 'San Pablo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1478,
            'province_id' => 142,
            'name' => 'Requena',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1479,
            'province_id' => 142,
            'name' => 'Alto Tapiche',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1480,
            'province_id' => 142,
            'name' => 'Capelo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1481,
            'province_id' => 142,
            'name' => 'Emilio San Martin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1482,
            'province_id' => 142,
            'name' => 'Maquia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1483,
            'province_id' => 142,
            'name' => 'Puinahua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1484,
            'province_id' => 142,
            'name' => 'Saquena',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1485,
            'province_id' => 142,
            'name' => 'Soplin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1486,
            'province_id' => 142,
            'name' => 'Tapiche',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1487,
            'province_id' => 142,
            'name' => 'Jenaro Herrera',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1488,
            'province_id' => 142,
            'name' => 'Yaquerana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1489,
            'province_id' => 143,
            'name' => 'Contamana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1490,
            'province_id' => 143,
            'name' => 'Inahuaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1491,
            'province_id' => 143,
            'name' => 'Padre Marquez',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1492,
            'province_id' => 143,
            'name' => 'Pampa Hermosa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1493,
            'province_id' => 143,
            'name' => 'Sarayacu',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1494,
            'province_id' => 143,
            'name' => 'Vargas Guerra',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1495,
            'province_id' => 144,
            'name' => 'Barranca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1496,
            'province_id' => 144,
            'name' => 'Cahuapanas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1497,
            'province_id' => 144,
            'name' => 'Manseriche',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1498,
            'province_id' => 144,
            'name' => 'Morona',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1499,
            'province_id' => 144,
            'name' => 'Pastaza',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1500,
            'province_id' => 144,
            'name' => 'Andoas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1501,
            'province_id' => 145,
            'name' => 'Putumayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1502,
            'province_id' => 145,
            'name' => 'Rosa Panduro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1503,
            'province_id' => 145,
            'name' => 'Teniente Manuel Clavero',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1504,
            'province_id' => 145,
            'name' => 'Yaguas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1505,
            'province_id' => 146,
            'name' => 'Tambopata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1506,
            'province_id' => 146,
            'name' => 'Inambari',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1507,
            'province_id' => 146,
            'name' => 'Las Piedras',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1508,
            'province_id' => 146,
            'name' => 'Laberinto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1509,
            'province_id' => 147,
            'name' => 'Manu',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1510,
            'province_id' => 147,
            'name' => 'Fitzcarrald',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1511,
            'province_id' => 147,
            'name' => 'Madre de Dios',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1512,
            'province_id' => 147,
            'name' => 'Huepetuhe',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1513,
            'province_id' => 148,
            'name' => 'Iñapari',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1514,
            'province_id' => 148,
            'name' => 'Iberia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1515,
            'province_id' => 148,
            'name' => 'Tahuamanu',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1516,
            'province_id' => 149,
            'name' => 'Moquegua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1517,
            'province_id' => 149,
            'name' => 'Carumas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1518,
            'province_id' => 149,
            'name' => 'Cuchumbaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1519,
            'province_id' => 149,
            'name' => 'Samegua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1520,
            'province_id' => 149,
            'name' => 'San Cristobal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1521,
            'province_id' => 149,
            'name' => 'Torata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1522,
            'province_id' => 150,
            'name' => 'Omate',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1523,
            'province_id' => 150,
            'name' => 'Chojata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1524,
            'province_id' => 150,
            'name' => 'Coalaque',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1525,
            'province_id' => 150,
            'name' => 'Ichuña',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1526,
            'province_id' => 150,
            'name' => 'La Capilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1527,
            'province_id' => 150,
            'name' => 'Lloque',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1528,
            'province_id' => 150,
            'name' => 'Matalaque',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1529,
            'province_id' => 150,
            'name' => 'Puquina',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1530,
            'province_id' => 150,
            'name' => 'Quinistaquillas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1531,
            'province_id' => 150,
            'name' => 'Ubinas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1532,
            'province_id' => 150,
            'name' => 'Yunga',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1533,
            'province_id' => 151,
            'name' => 'Ilo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1534,
            'province_id' => 151,
            'name' => 'El Algarrobal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1535,
            'province_id' => 151,
            'name' => 'Pacocha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1536,
            'province_id' => 152,
            'name' => 'Chaupimarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1537,
            'province_id' => 152,
            'name' => 'Huachon',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1538,
            'province_id' => 152,
            'name' => 'Huariaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1539,
            'province_id' => 152,
            'name' => 'Huayllay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1540,
            'province_id' => 152,
            'name' => 'Ninacaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1541,
            'province_id' => 152,
            'name' => 'Pallanchacra',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1542,
            'province_id' => 152,
            'name' => 'Paucartambo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1543,
            'province_id' => 152,
            'name' => 'San Francisco de Asis de Yarusyacan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1544,
            'province_id' => 152,
            'name' => 'Simon Bolivar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1545,
            'province_id' => 152,
            'name' => 'Ticlacayan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1546,
            'province_id' => 152,
            'name' => 'Tinyahuarco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1547,
            'province_id' => 152,
            'name' => 'Vicco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1548,
            'province_id' => 152,
            'name' => 'Yanacancha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1549,
            'province_id' => 153,
            'name' => 'Yanahuanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1550,
            'province_id' => 153,
            'name' => 'Chacayan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1551,
            'province_id' => 153,
            'name' => 'Goyllarisquizga',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1552,
            'province_id' => 153,
            'name' => 'Paucar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1553,
            'province_id' => 153,
            'name' => 'San Pedro de Pillao',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1554,
            'province_id' => 153,
            'name' => 'Santa Ana de Tusi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1555,
            'province_id' => 153,
            'name' => 'Tapuc',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1556,
            'province_id' => 153,
            'name' => 'Vilcabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1557,
            'province_id' => 154,
            'name' => 'Oxapampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1558,
            'province_id' => 154,
            'name' => 'Chontabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1559,
            'province_id' => 154,
            'name' => 'Huancabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1560,
            'province_id' => 154,
            'name' => 'Palcazu',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1561,
            'province_id' => 154,
            'name' => 'Pozuzo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1562,
            'province_id' => 154,
            'name' => 'Puerto Bermudez',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1563,
            'province_id' => 154,
            'name' => 'Villa Rica',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1564,
            'province_id' => 154,
            'name' => 'Constitución',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1565,
            'province_id' => 155,
            'name' => 'Piura',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1566,
            'province_id' => 155,
            'name' => 'Castilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1567,
            'province_id' => 155,
            'name' => 'Catacaos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1568,
            'province_id' => 155,
            'name' => 'Cura Mori',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1569,
            'province_id' => 155,
            'name' => 'El Tallan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1570,
            'province_id' => 155,
            'name' => 'La Arena',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1571,
            'province_id' => 155,
            'name' => 'La Union',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1572,
            'province_id' => 155,
            'name' => 'Las Lomas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1573,
            'province_id' => 155,
            'name' => 'Tambo Grande',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1574,
            'province_id' => 155,
            'name' => '26 de Octubre',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1575,
            'province_id' => 156,
            'name' => 'Ayabaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1576,
            'province_id' => 156,
            'name' => 'Frias',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1577,
            'province_id' => 156,
            'name' => 'Jilili',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1578,
            'province_id' => 156,
            'name' => 'Lagunas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1579,
            'province_id' => 156,
            'name' => 'Montero',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1580,
            'province_id' => 156,
            'name' => 'Pacaipampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1581,
            'province_id' => 156,
            'name' => 'Paimas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1582,
            'province_id' => 156,
            'name' => 'Sapillica',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1583,
            'province_id' => 156,
            'name' => 'Sicchez',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1584,
            'province_id' => 156,
            'name' => 'Suyo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1585,
            'province_id' => 157,
            'name' => 'Huancabamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1586,
            'province_id' => 157,
            'name' => 'Canchaque',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1587,
            'province_id' => 157,
            'name' => 'El Carmen de La Frontera',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1588,
            'province_id' => 157,
            'name' => 'Huarmaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1589,
            'province_id' => 157,
            'name' => 'Lalaquiz',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1590,
            'province_id' => 157,
            'name' => 'San Miguel de El Faique',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1591,
            'province_id' => 157,
            'name' => 'Sondor',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1592,
            'province_id' => 157,
            'name' => 'Sondorillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1593,
            'province_id' => 158,
            'name' => 'Chulucanas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1594,
            'province_id' => 158,
            'name' => 'Buenos Aires',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1595,
            'province_id' => 158,
            'name' => 'Chalaco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1596,
            'province_id' => 158,
            'name' => 'La Matanza',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1597,
            'province_id' => 158,
            'name' => 'Morropon',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1598,
            'province_id' => 158,
            'name' => 'Salitral',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1599,
            'province_id' => 158,
            'name' => 'San Juan de Bigote',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1600,
            'province_id' => 158,
            'name' => 'Santa Catalina de Mossa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1601,
            'province_id' => 158,
            'name' => 'Santo Domingo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1602,
            'province_id' => 158,
            'name' => 'Yamango',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1603,
            'province_id' => 159,
            'name' => 'Paita',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1604,
            'province_id' => 159,
            'name' => 'Amotape',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1605,
            'province_id' => 159,
            'name' => 'Arenal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1606,
            'province_id' => 159,
            'name' => 'Colan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1607,
            'province_id' => 159,
            'name' => 'La Huaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1608,
            'province_id' => 159,
            'name' => 'Tamarindo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1609,
            'province_id' => 159,
            'name' => 'Vichayal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1610,
            'province_id' => 160,
            'name' => 'Sullana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1611,
            'province_id' => 160,
            'name' => 'Bellavista',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1612,
            'province_id' => 160,
            'name' => 'Ignacio Escudero',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1613,
            'province_id' => 160,
            'name' => 'Lancones',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1614,
            'province_id' => 160,
            'name' => 'Marcavelica',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1615,
            'province_id' => 160,
            'name' => 'Miguel Checa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1616,
            'province_id' => 160,
            'name' => 'Querecotillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1617,
            'province_id' => 160,
            'name' => 'Salitral',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1618,
            'province_id' => 161,
            'name' => 'Pariñas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1619,
            'province_id' => 161,
            'name' => 'El Alto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1620,
            'province_id' => 161,
            'name' => 'La Brea',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1621,
            'province_id' => 161,
            'name' => 'Lobitos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1622,
            'province_id' => 161,
            'name' => 'Los Organos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1623,
            'province_id' => 161,
            'name' => 'Mancora',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1624,
            'province_id' => 162,
            'name' => 'Sechura',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1625,
            'province_id' => 162,
            'name' => 'Bellavista de La Union',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1626,
            'province_id' => 162,
            'name' => 'Bernal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1627,
            'province_id' => 162,
            'name' => 'Cristo Nos Valga',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1628,
            'province_id' => 162,
            'name' => 'Vice',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1629,
            'province_id' => 162,
            'name' => 'Rinconada Llicuar',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1630,
            'province_id' => 163,
            'name' => 'Puno',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1631,
            'province_id' => 163,
            'name' => 'Acora',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1632,
            'province_id' => 163,
            'name' => 'Amantani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1633,
            'province_id' => 163,
            'name' => 'Atuncolla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1634,
            'province_id' => 163,
            'name' => 'Capachica',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1635,
            'province_id' => 163,
            'name' => 'Chucuito',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1636,
            'province_id' => 163,
            'name' => 'Coata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1637,
            'province_id' => 163,
            'name' => 'Huata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1638,
            'province_id' => 163,
            'name' => 'Mañazo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1639,
            'province_id' => 163,
            'name' => 'Paucarcolla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1640,
            'province_id' => 163,
            'name' => 'Pichacani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1641,
            'province_id' => 163,
            'name' => 'Plateria',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1642,
            'province_id' => 163,
            'name' => 'San Antonio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1643,
            'province_id' => 163,
            'name' => 'Tiquillaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1644,
            'province_id' => 163,
            'name' => 'Vilque',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1645,
            'province_id' => 164,
            'name' => 'Azangaro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1646,
            'province_id' => 164,
            'name' => 'Achaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1647,
            'province_id' => 164,
            'name' => 'Arapa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1648,
            'province_id' => 164,
            'name' => 'Asillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1649,
            'province_id' => 164,
            'name' => 'Caminaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1650,
            'province_id' => 164,
            'name' => 'Chupa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1651,
            'province_id' => 164,
            'name' => 'Jose Domingo Choquehuanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1652,
            'province_id' => 164,
            'name' => 'Muñani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1653,
            'province_id' => 164,
            'name' => 'Potoni',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1654,
            'province_id' => 164,
            'name' => 'Saman',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1655,
            'province_id' => 164,
            'name' => 'San Anton',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1656,
            'province_id' => 164,
            'name' => 'San Jose',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1657,
            'province_id' => 164,
            'name' => 'San Juan de Salinas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1658,
            'province_id' => 164,
            'name' => 'Santiago de Pupuja',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1659,
            'province_id' => 164,
            'name' => 'Tirapata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1660,
            'province_id' => 165,
            'name' => 'Macusani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1661,
            'province_id' => 165,
            'name' => 'Ajoyani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1662,
            'province_id' => 165,
            'name' => 'Ayapata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1663,
            'province_id' => 165,
            'name' => 'Coasa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1664,
            'province_id' => 165,
            'name' => 'Corani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1665,
            'province_id' => 165,
            'name' => 'Crucero',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1666,
            'province_id' => 165,
            'name' => 'Ituata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1667,
            'province_id' => 165,
            'name' => 'Ollachea',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1668,
            'province_id' => 165,
            'name' => 'San Gaban',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1669,
            'province_id' => 165,
            'name' => 'Usicayos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1670,
            'province_id' => 166,
            'name' => 'Juli',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1671,
            'province_id' => 166,
            'name' => 'Desaguadero',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1672,
            'province_id' => 166,
            'name' => 'Huacullani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1673,
            'province_id' => 166,
            'name' => 'Kelluyo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1674,
            'province_id' => 166,
            'name' => 'Pisacoma',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1675,
            'province_id' => 166,
            'name' => 'Pomata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1676,
            'province_id' => 166,
            'name' => 'Zepita',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1677,
            'province_id' => 167,
            'name' => 'Ilave',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1678,
            'province_id' => 167,
            'name' => 'Capazo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1679,
            'province_id' => 167,
            'name' => 'Pilcuyo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1680,
            'province_id' => 167,
            'name' => 'Santa Rosa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1681,
            'province_id' => 167,
            'name' => 'Conduriri',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1682,
            'province_id' => 168,
            'name' => 'Huancane',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1683,
            'province_id' => 168,
            'name' => 'Cojata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1684,
            'province_id' => 168,
            'name' => 'Huatasani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1685,
            'province_id' => 168,
            'name' => 'Inchupalla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1686,
            'province_id' => 168,
            'name' => 'Pusi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1687,
            'province_id' => 168,
            'name' => 'Rosaspata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1688,
            'province_id' => 168,
            'name' => 'Taraco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1689,
            'province_id' => 168,
            'name' => 'Vilque Chico',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1690,
            'province_id' => 169,
            'name' => 'Lampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1691,
            'province_id' => 169,
            'name' => 'Cabanilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1692,
            'province_id' => 169,
            'name' => 'Calapuja',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1693,
            'province_id' => 169,
            'name' => 'Nicasio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1694,
            'province_id' => 169,
            'name' => 'Ocuviri',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1695,
            'province_id' => 169,
            'name' => 'Palca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1696,
            'province_id' => 169,
            'name' => 'Paratia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1697,
            'province_id' => 169,
            'name' => 'Pucara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1698,
            'province_id' => 169,
            'name' => 'Santa Lucia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1699,
            'province_id' => 169,
            'name' => 'Vilavila',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1700,
            'province_id' => 170,
            'name' => 'Ayaviri',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1701,
            'province_id' => 170,
            'name' => 'Antauta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1702,
            'province_id' => 170,
            'name' => 'Cupi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1703,
            'province_id' => 170,
            'name' => 'Llalli',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1704,
            'province_id' => 170,
            'name' => 'Macari',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1705,
            'province_id' => 170,
            'name' => 'Nuñoa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1706,
            'province_id' => 170,
            'name' => 'Orurillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1707,
            'province_id' => 170,
            'name' => 'Santa Rosa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1708,
            'province_id' => 170,
            'name' => 'Umachiri',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1709,
            'province_id' => 171,
            'name' => 'Moho',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1710,
            'province_id' => 171,
            'name' => 'Conima',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1711,
            'province_id' => 171,
            'name' => 'Huayrapata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1712,
            'province_id' => 171,
            'name' => 'Tilali',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1713,
            'province_id' => 172,
            'name' => 'Putina',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1714,
            'province_id' => 172,
            'name' => 'Ananea',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1715,
            'province_id' => 172,
            'name' => 'Pedro Vilca Apaza',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1716,
            'province_id' => 172,
            'name' => 'Quilcapuncu',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1717,
            'province_id' => 172,
            'name' => 'Sina',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1718,
            'province_id' => 173,
            'name' => 'Juliaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1719,
            'province_id' => 173,
            'name' => 'Cabana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1720,
            'province_id' => 173,
            'name' => 'Cabanillas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1721,
            'province_id' => 173,
            'name' => 'Caracoto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1722,
            'province_id' => 173,
            'name' => 'San Miguel',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1723,
            'province_id' => 174,
            'name' => 'Sandia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1724,
            'province_id' => 174,
            'name' => 'Cuyocuyo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1725,
            'province_id' => 174,
            'name' => 'Limbani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1726,
            'province_id' => 174,
            'name' => 'Patambuco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1727,
            'province_id' => 174,
            'name' => 'Phara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1728,
            'province_id' => 174,
            'name' => 'Quiaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1729,
            'province_id' => 174,
            'name' => 'San Juan del Oro',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1730,
            'province_id' => 174,
            'name' => 'Yanahuaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1731,
            'province_id' => 174,
            'name' => 'Alto Inambari',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1732,
            'province_id' => 174,
            'name' => 'San Pedro de Putina Punco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1733,
            'province_id' => 175,
            'name' => 'Yunguyo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1734,
            'province_id' => 175,
            'name' => 'Anapia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1735,
            'province_id' => 175,
            'name' => 'Copani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1736,
            'province_id' => 175,
            'name' => 'Cuturapi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1737,
            'province_id' => 175,
            'name' => 'Ollaraya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1738,
            'province_id' => 175,
            'name' => 'Tinicachi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1739,
            'province_id' => 175,
            'name' => 'Unicachi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1740,
            'province_id' => 176,
            'name' => 'Moyobamba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1741,
            'province_id' => 176,
            'name' => 'Calzada',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1742,
            'province_id' => 176,
            'name' => 'Habana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1743,
            'province_id' => 176,
            'name' => 'Jepelacio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1744,
            'province_id' => 176,
            'name' => 'Soritor',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1745,
            'province_id' => 176,
            'name' => 'Yantalo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1746,
            'province_id' => 177,
            'name' => 'Bellavista',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1747,
            'province_id' => 177,
            'name' => 'Alto Biavo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1748,
            'province_id' => 177,
            'name' => 'Bajo Biavo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1749,
            'province_id' => 177,
            'name' => 'Huallaga',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1750,
            'province_id' => 177,
            'name' => 'San Pablo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1751,
            'province_id' => 177,
            'name' => 'San Rafael',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1752,
            'province_id' => 178,
            'name' => 'San Jose de Sisa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1753,
            'province_id' => 178,
            'name' => 'Agua Blanca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1754,
            'province_id' => 178,
            'name' => 'San Martin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1755,
            'province_id' => 178,
            'name' => 'Santa Rosa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1756,
            'province_id' => 178,
            'name' => 'Shatoja',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1757,
            'province_id' => 179,
            'name' => 'Saposoa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1758,
            'province_id' => 179,
            'name' => 'Alto Saposoa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1759,
            'province_id' => 179,
            'name' => 'El Eslabon',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1760,
            'province_id' => 179,
            'name' => 'Piscoyacu',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1761,
            'province_id' => 179,
            'name' => 'Sacanche',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1762,
            'province_id' => 179,
            'name' => 'Tingo de Saposoa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1763,
            'province_id' => 180,
            'name' => 'Lamas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1764,
            'province_id' => 180,
            'name' => 'Alonso de Alvarado',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1765,
            'province_id' => 180,
            'name' => 'Barranquita',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1766,
            'province_id' => 180,
            'name' => 'Caynarachi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1767,
            'province_id' => 180,
            'name' => 'Cuñumbuqui',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1768,
            'province_id' => 180,
            'name' => 'Pinto Recodo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1769,
            'province_id' => 180,
            'name' => 'Rumisapa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1770,
            'province_id' => 180,
            'name' => 'San Roque de Cumbaza',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1771,
            'province_id' => 180,
            'name' => 'Shanao',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1772,
            'province_id' => 180,
            'name' => 'Tabalosos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1773,
            'province_id' => 180,
            'name' => 'Zapatero',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1774,
            'province_id' => 181,
            'name' => 'Juanjui',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1775,
            'province_id' => 181,
            'name' => 'Campanilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1776,
            'province_id' => 181,
            'name' => 'Huicungo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1777,
            'province_id' => 181,
            'name' => 'Pachiza',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1778,
            'province_id' => 181,
            'name' => 'Pajarillo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1779,
            'province_id' => 182,
            'name' => 'Picota',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1780,
            'province_id' => 182,
            'name' => 'Buenos Aires',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1781,
            'province_id' => 182,
            'name' => 'Caspisapa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1782,
            'province_id' => 182,
            'name' => 'Pilluana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1783,
            'province_id' => 182,
            'name' => 'Pucacaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1784,
            'province_id' => 182,
            'name' => 'San Cristobal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1785,
            'province_id' => 182,
            'name' => 'San Hilarion',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1786,
            'province_id' => 182,
            'name' => 'Shamboyacu',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1787,
            'province_id' => 182,
            'name' => 'Tingo de Ponasa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1788,
            'province_id' => 182,
            'name' => 'Tres Unidos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1789,
            'province_id' => 183,
            'name' => 'Rioja',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1790,
            'province_id' => 183,
            'name' => 'Awajun',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1791,
            'province_id' => 183,
            'name' => 'Elias Soplin Vargas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1792,
            'province_id' => 183,
            'name' => 'Nueva Cajamarca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1793,
            'province_id' => 183,
            'name' => 'Pardo Miguel',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1794,
            'province_id' => 183,
            'name' => 'Posic',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1795,
            'province_id' => 183,
            'name' => 'San Fernando',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1796,
            'province_id' => 183,
            'name' => 'Yorongos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1797,
            'province_id' => 183,
            'name' => 'Yuracyacu',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1798,
            'province_id' => 184,
            'name' => 'Tarapoto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1799,
            'province_id' => 184,
            'name' => 'Alberto Leveau',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1800,
            'province_id' => 184,
            'name' => 'Cacatachi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1801,
            'province_id' => 184,
            'name' => 'Chazuta',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1802,
            'province_id' => 184,
            'name' => 'Chipurana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1803,
            'province_id' => 184,
            'name' => 'El Porvenir',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1804,
            'province_id' => 184,
            'name' => 'Huimbayoc',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1805,
            'province_id' => 184,
            'name' => 'Juan Guerra',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1806,
            'province_id' => 184,
            'name' => 'La Banda de Shilcayo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1807,
            'province_id' => 184,
            'name' => 'Morales',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1808,
            'province_id' => 184,
            'name' => 'Papaplaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1809,
            'province_id' => 184,
            'name' => 'San Antonio',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1810,
            'province_id' => 184,
            'name' => 'Sauce',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1811,
            'province_id' => 184,
            'name' => 'Shapaja',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1812,
            'province_id' => 185,
            'name' => 'Tocache',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1813,
            'province_id' => 185,
            'name' => 'Nuevo Progreso',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1814,
            'province_id' => 185,
            'name' => 'Polvora',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1815,
            'province_id' => 185,
            'name' => 'Shunte',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1816,
            'province_id' => 185,
            'name' => 'Uchiza',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1817,
            'province_id' => 186,
            'name' => 'Tacna',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1818,
            'province_id' => 186,
            'name' => 'Alto de La Alianza',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1819,
            'province_id' => 186,
            'name' => 'Calana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1820,
            'province_id' => 186,
            'name' => 'Ciudad Nueva',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1821,
            'province_id' => 186,
            'name' => 'Inclan',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1822,
            'province_id' => 186,
            'name' => 'Pachia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1823,
            'province_id' => 186,
            'name' => 'Palca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1824,
            'province_id' => 186,
            'name' => 'Pocollay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1825,
            'province_id' => 186,
            'name' => 'Sama',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1826,
            'province_id' => 186,
            'name' => 'Coronel Gregorio Albarracin Lanchipa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1827,
            'province_id' => 186,
            'name' => 'La Yarada-Los Palos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1828,
            'province_id' => 187,
            'name' => 'Candarave',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1829,
            'province_id' => 187,
            'name' => 'Cairani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1830,
            'province_id' => 187,
            'name' => 'Camilaca',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1831,
            'province_id' => 187,
            'name' => 'Curibaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1832,
            'province_id' => 187,
            'name' => 'Huanuara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1833,
            'province_id' => 187,
            'name' => 'Quilahuani',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1834,
            'province_id' => 188,
            'name' => 'Locumba',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1835,
            'province_id' => 188,
            'name' => 'Ilabaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1836,
            'province_id' => 188,
            'name' => 'Ite',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1837,
            'province_id' => 189,
            'name' => 'Tarata',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1838,
            'province_id' => 189,
            'name' => 'Heroes Albarracin',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1839,
            'province_id' => 189,
            'name' => 'Estique',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1840,
            'province_id' => 189,
            'name' => 'Estique-Pampa',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1841,
            'province_id' => 189,
            'name' => 'Sitajara',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1842,
            'province_id' => 189,
            'name' => 'Susapaya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1843,
            'province_id' => 189,
            'name' => 'Tarucachi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1844,
            'province_id' => 189,
            'name' => 'Ticaco',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1845,
            'province_id' => 190,
            'name' => 'Tumbes',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1846,
            'province_id' => 190,
            'name' => 'Corrales',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1847,
            'province_id' => 190,
            'name' => 'La Cruz',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1848,
            'province_id' => 190,
            'name' => 'Pampas de Hospital',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1849,
            'province_id' => 190,
            'name' => 'San Jacinto',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1850,
            'province_id' => 190,
            'name' => 'San Juan de La Virgen',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1851,
            'province_id' => 191,
            'name' => 'Zorritos',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1852,
            'province_id' => 191,
            'name' => 'Casitas',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1853,
            'province_id' => 191,
            'name' => 'Canoas de Punta Sal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1854,
            'province_id' => 192,
            'name' => 'Zarumilla',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1855,
            'province_id' => 192,
            'name' => 'Aguas Verdes',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1856,
            'province_id' => 192,
            'name' => 'Matapalo',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1857,
            'province_id' => 192,
            'name' => 'Papayal',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1858,
            'province_id' => 193,
            'name' => 'Calleria',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1859,
            'province_id' => 193,
            'name' => 'Campoverde',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1860,
            'province_id' => 193,
            'name' => 'Iparia',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1861,
            'province_id' => 193,
            'name' => 'Masisea',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1862,
            'province_id' => 193,
            'name' => 'Yarinacocha',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1863,
            'province_id' => 193,
            'name' => 'Nueva Requena',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1864,
            'province_id' => 193,
            'name' => 'Manantay',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1865,
            'province_id' => 194,
            'name' => 'Raymondi',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1866,
            'province_id' => 194,
            'name' => 'Sepahua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1867,
            'province_id' => 194,
            'name' => 'Tahuania',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1868,
            'province_id' => 194,
            'name' => 'Yurua',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1869,
            'province_id' => 195,
            'name' => 'Padre Abad',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1870,
            'province_id' => 195,
            'name' => 'Irazola',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1871,
            'province_id' => 195,
            'name' => 'Curimana',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1872,
            'province_id' => 195,
            'name' => 'Neshuya',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1873,
            'province_id' => 195,
            'name' => 'Alexander von Humboldt',
        ]);
        factory(\App\District::class, 1)->create([
            'id' => 1874,
            'province_id' => 196,
            'name' => 'Purus',
        ]);






        //USUARIOS ALEATORIOS
        //ARRENDATARIOS
        factory(\App\User::class, 15)
            ->create([
                'role_id'=>\App\Role::LESSEE,
            ])
            ->each(function (\App\User $us) {
                factory(\App\Lessee::class, 1)
                    ->create([
                        'user_id'=>$us->id,
                    ]);
            });;
        //PROPIETARIOS
        factory(\App\User::class, 25)
            ->create([
                'role_id'=>\App\Role::PROPIETARY,
            ])
            ->each(function (\App\User $us) {
                factory(\App\Propietary::class, 1)
                    ->create([
                        'user_id'=>$us->id,
                    ]);
                factory(\App\Property::class, 2)
                    ->create([
                        'user_id'=>$us->id,
                    ])
                    ->each(function (\App\Property $pro) {
                        factory(\App\PropertyImage::class, 1)
                            ->create([
                                'property_id'=>$pro->id,
                                'order'=>1,
                            ]);
                        factory(\App\PropertyImage::class, 1)
                            ->create([
                                'property_id'=>$pro->id,
                                'order'=>2,
                            ]);
                    });
                ;
            });;
        //ARRENDATARIOS CON POSTULACIONES
        factory(\App\User::class, 15)
            ->create([
                'role_id'=>\App\Role::LESSEE,
            ])
            ->each(function (\App\User $us) {
                factory(\App\Lessee::class, 1)
                    ->create([
                        'user_id'=>$us->id,
                    ]);
                factory(\App\Application::class, 1)
                    ->create([
                        'user_id'=>$us->id,
                    ]);
            });

        //PETS
        factory(\App\Pet::class, 30)->create();

        //VISIT DATE
        factory(\App\VisitDate::class, 12)->create();

        //LEASE & CONTRACTS
        factory(\App\Lease::class, 5)
            ->create()
            ->each(function (\App\Lease $le) {
                factory(\App\Contract::class, 1)
                    ->create([
                        'lease_id'=>$le->id,
                    ]);
            });

        //PAYMENTS
        factory(\App\Payment::class, 10)->create();

        //SEARCHES
        factory(\App\Search::class, 25)->create();


    }
}
