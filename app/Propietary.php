<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Propietary
 *
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Propietary newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Propietary newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Propietary query()
 * @mixin \Eloquent
 */
class Propietary extends Model
{
    protected $fillable = [
        'user_id', 'biography',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
