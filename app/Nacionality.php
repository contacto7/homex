<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Nacionality
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nacionality newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nacionality newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nacionality query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nacionality whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nacionality whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nacionality whereName($value)
 * @mixin \Eloquent
 */
class Nacionality extends Model
{
    public $timestamps = false;

    public function users(){
        return $this->hasMany(User::class);
    }
}
