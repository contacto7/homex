<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Application
 *
 * @property-read \App\Lease $lease
 * @property-read \App\Property $property
 * @property-read \App\User $user
 * @property-read \App\VisitDate $visitDate
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application query()
 * @mixin \Eloquent
 */
class Application extends Model
{
    const INITIAL = 1;
    const APPROVED = 2;
    const DISAPPROVED = 3;

    const POSTULACION = 1;
    const PRE_APROBADO= 2;
    const VISITA = 3;
    const APROBADO = 4;
    const CONTRATO_FIRMADO = 5;

    protected $fillable = [
        'date','property_id','user_id', 'stage', 'stage_state',
    ];

    public function property(){
        return $this->belongsTo(Property::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function lease(){
        return $this->hasOne(Lease::class);
    }
    public function visitDate(){
        return $this->hasOne(VisitDate::class);
    }
}
