<?php

namespace App\Http\Requests;

use App\Contract;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
                return [
                    'type' => 'required|numeric',
                    'document' => 'nullable',
                    'state' =>
                        Rule::in([
                            Contract::ACTIVE,
                            Contract::INACTIVE,
                        ]),
                ];
            case 'POST':{
                return [
                    'lease_id' => [
                        'required',
                        Rule::exists('leases','id')
                    ],
                    'start_date' => 'required|date_format:Y-m-d',
                    'end_date' => 'required|date_format:Y-m-d',
                    'pay_date' => 'required|numeric',
                    'months_date' => 'required|numeric',
                    'type' => 'required|numeric',
                    'currency_id' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'amount' => 'required|numeric',
                    'document' => 'nullable',
                    'state' =>
                        Rule::in([
                            Contract::ACTIVE,
                            Contract::INACTIVE,
                ]),
                ];
            }
        }
    }



}
