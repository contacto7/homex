<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
                $rules = [];

                if($this->has('picture')){
                    $rules += [
                        'picture' => 'nullable',
                    ];
                }

                if($this->has('personal')){
                    $rules += [
                        'name' => 'required',
                        'last_name' => 'required',
                        'email' => 'required|email',
                        'cellphone' => 'nullable|numeric',
                    ];
                }

                if($this->has('document')){
                    $rules += [
                        'document_type_id' => [
                            'nullable',
                            Rule::exists('document_types','id')
                        ],
                        'document_number' => 'nullable|numeric',
                    ];
                }

                if($this->has('nacionality')){
                    $rules += [
                        'nacionality_id' => [
                            'nullable',
                            Rule::exists('nacionalities','id')
                        ],
                        'sex' =>
                            Rule::in([
                                User::FEMALE,
                                User::MALE,
                            ]),
                    ];
                }

                return $rules;
            case 'POST':{
                return [
                    'role_id' => [
                        'required',
                        Rule::exists('roles','id')
                    ],
                    'name' => 'required',
                    'last_name' => 'required',
                    'slug' => 'nullable',
                    'email' => 'required|email',
                    'password' => 'required|min:6',
                    'document_type_id' => [
                        'nullable',
                        Rule::exists('document_types','id')
                    ],
                    'document_number' => 'nullable',
                    'nacionality_id' => [
                        'nullable',
                        Rule::exists('nacionalities','id')
                    ],
                    'cellphone' => 'nullable',
                    'picture' => 'nullable',
                    'sex' =>
                        Rule::in([
                            User::FEMALE,
                            User::MALE,
                        ]),
                    'state' =>
                        Rule::in([
                            User::ACTIVE,
                            User::INACTIVE,
                        ]),
                    'remember_token' => 'nullable',
                ];
            }
        }
    }
}
