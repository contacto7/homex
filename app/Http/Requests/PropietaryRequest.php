<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PropietaryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
                $rules = [];

                if($this->has('life')){
                    $rules += [
                        'life' => 'nullable',
                    ];
                }

                return $rules;
            case 'POST':{
                return [
                    'user_id' => [
                        'required',
                        Rule::exists('users','id')
                    ],
                    'biography' => 'nullable',
                ];
            }


        }
    }
}
