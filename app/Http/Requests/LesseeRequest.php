<?php

namespace App\Http\Requests;

use App\Lessee;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LesseeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
                $rules = [];

                if($this->has('lifestyle')){
                    $rules += [
                        'lifestyle_type_id' => [
                            'nullable',
                            Rule::exists('lifestyle_types','id')
                        ],
                        'emotional_situation' => 'nullable|numeric',
                        'occupation_type_id' => [
                            'nullable',
                            Rule::exists('occupation_types','id')
                        ],
                        'monthly_salary' => 'nullable|numeric',
                    ];
                }

                if($this->has('location')){
                    $rules += [
                        'address' => 'nullable',
                        'district_id' => [
                            'nullable',
                            Rule::exists('districts','id')
                        ]
                    ];
                }

                if($this->has('bond')){
                    $rules += [
                        'quantity_members' => 'nullable|numeric',
                        'dependent_status' =>
                            [
                                'nullable',
                                Rule::in([
                                    Lessee::INDEPENDENT,
                                    Lessee::DEPENDENT,
                                ])
                            ],
                        'debtor_status' =>
                            [
                                'nullable',
                                Rule::in([
                                    Lessee::NOT_DEBTOR,
                                    Lessee::DEBTOR,
                                ])
                            ],
                        'other_postulation' =>
                            [
                                'nullable',
                                Rule::in([
                                    Lessee::NOT_OTHER_POSTULATION,
                                    Lessee::OTHER_POSTULATION,
                                ])
                            ],
                        'bond_interested_status' =>
                            [
                                'nullable',
                                Rule::in([
                                    Lessee::BOND_INTERESTED,
                                    Lessee::BOND_NOT_INTERESTED,
                                ])
                            ],
                    ];
                }

                return $rules;
            case 'POST':{
                return [
                    'user_id' => [
                        'required',
                        Rule::exists('users','id')
                    ],
                    'monthly_salary' => 'nullable|numeric',
                    'emotional_situation' => 'nullable|numeric',
                    'occupation_type_id' => [
                        'nullable',
                        Rule::exists('occupation_types','id')
                    ],
                    'occupation_center' => 'nullable',
                    'lifestyle_type_id' => [
                        'nullable',
                        Rule::exists('lifestyle_types','id')
                    ],
                    'address' => 'nullable',
                    'district_id' => [
                        'nullable',
                        Rule::exists('districts','id')
                    ],
                    'quantity_members' => 'nullable|numeric',
                    'dependent_status' =>
                        [
                            'nullable',
                            Rule::in([
                                Lessee::INDEPENDENT,
                                Lessee::DEPENDENT,
                            ])
                        ],
                    'debtor_status' =>
                        [
                            'nullable',
                            Rule::in([
                                Lessee::NOT_DEBTOR,
                                Lessee::DEBTOR,
                            ])
                        ],
                    'other_postulation' =>
                        [
                            'nullable',
                            Rule::in([
                                Lessee::NOT_OTHER_POSTULATION,
                                Lessee::OTHER_POSTULATION,
                            ])
                        ],
                    'bond_interested_status' =>
                        [
                            'nullable',
                            Rule::in([
                                Lessee::BOND_INTERESTED,
                                Lessee::BOND_NOT_INTERESTED,
                            ])
                        ],
                ];
            }


        }
    }

}
