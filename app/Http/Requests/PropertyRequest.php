<?php

namespace App\Http\Requests;

use App\Property;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
            case 'POST':{
                return [
                    'name' => 'required',
                    'description' => 'required',
                    'rooms' => 'required|numeric',
                    'district_id' => [
                        'required',
                        Rule::exists('districts','id')
                    ],
                    'property_type_id' => [
                        'required',
                        Rule::exists('property_types','id')
                    ],
                    'parking_slots' => 'required|numeric',
                    'total_area' => 'required|numeric',
                    'bathrooms' => 'required|numeric',
                    'half_bathrooms' => 'required|numeric',
                    'year' => 'required|numeric',
                    'currency_rental_amount' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'rental_amount' => 'required|numeric',
                    'currency_maintenance_amount' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'maintenance_amount' => 'nullable|numeric',
                    'currency_guaranty_amount' => [
                        'required',
                        Rule::exists('currencies','id')
                    ],
                    'guaranty_amount' => 'nullable|numeric',
                    'latitude' => 'required|numeric',
                    'longitude' => 'required|numeric',
                    'address' => 'required',
                    'pet_allowed' =>
                        Rule::in([
                            Property::PET_ALLOWED,
                            Property::PET_NOT_ALLOWED,
                        ]),
                    'state' =>
                        Rule::in([
                            Property::ACTIVE,
                            Property::INACTIVE,
                        ]),
                    'only_genre' =>
                        Rule::in([
                            Property::ALL,
                            Property::WOMEN_ONLY,
                            Property::MAN_ONLY,
                            Property::WOMAN_PREFERRED,
                            Property::MAN_PREFERRED,
                        ]),
                    'only_students' =>
                        Rule::in([
                            Property::ALL,
                            Property::STUDENTS_ONLY,
                            Property::STUDENTS_PREFERRED,
                        ]),
                    'young_bonus' =>
                        Rule::in([
                            Property::YB_QUALIFIED,
                            Property::YB_NOT_QUALIFIED,
                            Property::YB_PREFERRED,
                        ]),
                    'family_status' =>
                        Rule::in([
                            Property::ALL,
                            Property::COUPLE_WITH_KIDS_ONLY,
                            Property::COUPLE_WITH_KIDS_PREFERRED,
                        ]),
                    'age_range' =>
                        Rule::in([
                            Property::ALL,
                            Property::AGE_18_30_ONLY,
                            Property::AGE_31_45_ONLY,
                            Property::AGE_45_ONLY,
                            Property::AGE_18_30_PREFERRED,
                            Property::AGE_31_45_PREFERRED,
                            Property::AGE_45_PREFERRED,
                        ]),
                    'rent_limit_id' => [
                        'required',
                        Rule::exists('rent_limits','id')
                    ],
                ];
            }
        }
    }
}
