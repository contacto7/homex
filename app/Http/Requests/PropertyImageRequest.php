<?php

namespace App\Http\Requests;

use App\Property;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PropertyImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch($this->method()){
            case 'GET':
                return [];
            case 'DELETE':
                return [
                    'picture' => 'required',
                ];
            case 'PUT':
            case 'POST':{
                return [
                    'picture' => 'required',
                    'order' => 'required|numeric',
                    'property_id' => [
                        'required',
                        Rule::exists('properties','id')
                    ],
                ];
            }
        }
    }






}
