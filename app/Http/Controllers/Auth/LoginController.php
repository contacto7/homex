<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $tituloModal = "Ingreso fallido";

        $mensajeModal = "Su correo o contraseña son incorrectos.";

        $email = $request->input('email');

        return back()
            ->with('modalMessage',[$tituloModal, $mensajeModal])
            ->withInput()
            ->withErrors(['email' => $email]);
    }

    protected function authenticated(Request $request, $user)
    {
        if ( $user->role_id ===  Role::ADMIN) {// do your margic here
            //return redirect()->route('users.dashboard');
            return back();
        }

        return back();
    }
}
