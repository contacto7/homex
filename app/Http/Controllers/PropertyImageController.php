<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Requests\PropertyImageRequest;
use App\Property;
use App\PropertyImage;
use App\User;
use Illuminate\Http\Request;

class PropertyImageController extends Controller
{

    public function edit($property){

        //POLICIES
        $canEditProperty = self::canEditProperty($property);
        //END POLICIES

        if(!$canEditProperty){
            //dd($userRequest);
            $tituloModal = 'Actualizar datos';
            $mensajeModal = 'Usted no puede actualizar esta propiedad.';
            //return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
            return $mensajeModal;
        }

        $propertyImages = PropertyImage::with([])
            ->where('property_id', $property)
            ->orderBy('order', 'asc')
            ->get();


        //dd($property);
        return view('propertyImages.edit', compact('propertyImages', 'property'));

    }


    public function store(PropertyImageRequest $propertyImageRequest){

        $tituloModal = "Adición exitosa";
        $mensajeModal = "Se ha añadido la imagen exitosamente.";

        //POLICIES
        $canEditProperty = self::canEditProperty($propertyImageRequest->property_id);
        //END POLICIES

        if(!$canEditProperty){
            //dd($userRequest);
            $tituloModal = 'Actualizar datos';
            $mensajeModal = 'Usted no puede actualizar esta propiedad.';
            //return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
            return $mensajeModal;
        }

        try{

            if($propertyImageRequest->has('picture')){
                \Storage::delete('properties/'.$propertyImageRequest->picture);
                $picture = Helper::uploadFile('picture', 'properties');
                $propertyImageRequest->merge(['picture' => $picture ]);
            }

            $propertyImage = PropertyImage::create($propertyImageRequest->input());

        } catch(\Illuminate\Database\QueryException $e){
            $mensajeModal = 'Ha ocurrido un error agregando la imagen, por favor vuelva a intentarlo.<br>';
            $mensajeModal .= 'Código de error: ' . $e->getCode() ;
        }

        return back()->with('modalMessage',[$tituloModal, $mensajeModal]);

    }

    public function delete(PropertyImageRequest $propertyImageRequest, $propertyImage){

        $tituloModal = 'Eliminar imagen';
        $mensajeModal = 'Se logró eliminar la imagen con éxito.';

        //ENCONTRAMOS EL ID DE LA PROPIEDAD PARA SABER SI CUMPLE EL POLICY
        $propertyImage = PropertyImage::where('id', $propertyImage)->first();

        //POLICIES
        $canEditProperty = self::canEditProperty($propertyImage->property_id);
        //END POLICIES

        if(!$canEditProperty){
            //dd($userRequest);
            $tituloModal = 'Actualizar datos';
            $mensajeModal = 'Usted no puede actualizar esta propiedad.';
            //return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
            return $mensajeModal;
        }

        try{

            if($propertyImageRequest->has('picture')){
                \Storage::delete('properties/'.$propertyImageRequest->picture);
            }

            $propertyImage->delete();

        } catch(\Illuminate\Database\QueryException $e){
            $mensajeModal = 'Ha ocurrido un error eliminando la imagen, por favor vuelva a intentarlo.<br>';
            $mensajeModal .= 'Código de error: ' . $e->getCode() ;
        }

        return back()->with('modalMessage',[$tituloModal, $mensajeModal]);

    }


    public function canEditProperty($property){
        $propertyModel = Property::where('id',$property)->first();

        //dd($propertyModel);

        //POLICIES
        $canEditProperty = auth()->user()->can('editProperty', [Property::class, $propertyModel] );
        //END POLICIES

        return $canEditProperty;
    }


}
