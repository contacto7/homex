<?php

namespace App\Http\Controllers;

use App\Http\Requests\LesseeRequest;
use App\Lease;
use App\Lessee;
use Illuminate\Http\Request;

class LeaseController extends Controller
{

    public function list(){

        $leases = Lease::with([
            'application.property',
            'contracts'
        ])
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($leases);

        //dd($application);
        return view('leases.list', compact('leases'));

    }

    public function listLessee($id){

        $leases = Lease::with([
            'application.property',
            'contracts'
        ])
            ->whereHas("application", function($q) use($id){
                $q->where("user_id",$id);
            })
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($leases);

        //dd($application);
        return view('leases.listLessee', compact('leases'));

    }

    public function listPropietary($id){

        $leases = Lease::with([
            'application.property',
            'contracts'
        ])
            ->whereHas("application", function($q) use($id){
                $q->whereHas("property", function($sq) use($id){
                    $sq->where("user_id",$id);
                });
            })
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($leases);

        //dd($application);
        return view('leases.listPropietary', compact('leases'));

    }


}
