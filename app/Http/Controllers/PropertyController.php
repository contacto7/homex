<?php

namespace App\Http\Controllers;

use App\Application;
use App\District;
use App\Http\Requests\PropertyRequest;
use App\Http\Requests\PropietaryRequest;
use App\Mail\NewApplicationInPropertyMail;
use App\Property;
use App\PropertyImage;
use App\PropertyType;
use App\Search;
use App\User;
use Illuminate\Http\Request;

class PropertyController extends Controller
{

    public function admin(){
        $properties = new Property();
        $properties = $properties->newQuery();

        //DONDE EL ESTADO SEA ACTIVO
        $properties = $properties
            ->with([])
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($properties);
        return view('properties.admin', compact('properties') );
    }

    public function list(Request $request, Property $properties){
        $districts = $request->has('districts') ? $request->input('districts'): null;
        $types = $request->has('types') ? $request->input('types'): null;

        //ADICIONALES
        $young_bonus = $request->has('bono_renta_joven') ? $request->input('bono_renta_joven'): null;
        $only_students = $request->has('solo_estudiantes') ? $request->input('solo_estudiantes'): null;
        $pet_allowed = $request->has('mascotas_permitidas') ? $request->input('mascotas_permitidas'): null;
        $only_genre = $request->has('genero') ? $request->input('genero'): null;
        $min_amount = $request->has('precio_minimo') ? $request->input('precio_minimo'): null;
        $max_amount = $request->has('precio_maximo') ? $request->input('precio_maximo'): null;
        $rooms = $request->has('habitaciones') ? $request->input('habitaciones'): null;

        $properties = $properties->newQuery();

        //DEFINIMOS ARRAYS
        //SEPARAMOS POR COMAS
        $districts_arr = explode(",", $districts);
        $types_arr = explode(",", $types);
        //QUITAMOS LOS CEROS
        $districts_arr = array_filter($districts_arr);
        $types_arr = array_filter($types_arr);


        if ($districts) {
            $properties->whereIn('district_id', $districts_arr);
        }
        if ($types) {
            $properties->whereIn('property_type_id', $types_arr);
        }
        if ($young_bonus) {
            $properties->where('young_bonus', $young_bonus);
        }
        if ($only_students) {
            $properties->where('only_students', $only_students);
        }
        if ($pet_allowed) {
            $properties->where('pet_allowed', $pet_allowed);
        }
        if ($only_genre) {
            $properties->where('only_genre', $only_genre);
        }
        if ($min_amount) {
            $properties->where('rental_amount','>=', $min_amount);
        }
        if ($max_amount) {
            $properties->where('rental_amount','<=', $max_amount);
        }
        if ($rooms) {
            $properties->where('rooms', $rooms);
        }

        //DONDE EL ESTADO SEA ACTIVO
        $properties = $properties
            ->where('state', Property::ACTIVE)
            ->with([])
            ->orderBy('id', 'desc')
            ->paginate(12);


        //DISTRITOS
        $districts_list = District::with([])->orderBy('id', 'asc')->get();
        //TYPES
        $types_list = PropertyType::with([])->orderBy('id', 'asc')->get();

        //dd($properties);
        return view('properties.list', compact('properties','districts_arr','types_arr', 'districts_list', 'types_list','young_bonus','only_students','pet_allowed','only_genre','min_amount','max_amount','rooms') );
    }

    public function view($id){
        $property = Property::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $property_images = PropertyImage::with([])
            ->where('property_id',$id)
            ->orderBy('order', 'asc')
            ->latest()
            ->get();

        $related = $property->relatedProperties();

        //dd($companies);
        return view('properties.view', compact('property', 'related', 'property_images'));
    }

    public function myList($slug){
        $properties = Property::with([])
            ->whereHas("user", function($sq) use($slug){
                $sq->where("slug",$slug);
            })
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($companies);
        return view('properties.myList', compact('properties'));
    }

    public function application(Property $property){

        try {

            Application::create([
                'date'=>\Carbon\Carbon::now(),
                'property_id'=>$property->id,
                'user_id'=>auth()->user()->id,
            ]);

            $admin = User::whereId(User::CONTACT_MAIL_ID)->first();
            $propietary = $property->user;

            $recipients = [$admin,$propietary];

            \Mail::to($recipients)->send( new NewApplicationInPropertyMail( $property, auth()->user() ) );

            $tituloModal = "Registro exitoso";

            $mensajeModal = "El propietario ha recibido su notificación de interés en el inmueble. Un especialista de Homex se contactará con usted en 24 horas para determinar su mejor opción.";


            return back()->with('modalMessage',[$tituloModal, $mensajeModal]);

        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);

            if ($e->getCode() == 23000 ){

                $tituloModal = 'Aplicación fallida';
                $mensajeModal = 'Ya has aplicado al inmueble.';

                return back()->with('modalMessage',[$tituloModal, $mensajeModal]);

            }else{
                $tituloModal = 'Registro fallido';
                $mensajeModal = 'Ha ocurrido un error en el registro, por favor vuelva a intentarlo e ingrese los datos requeridos.<br>';
                $mensajeModal = 'Código de error: ' . $e->getCode() ;
                return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
            }

        }


    }
    public function userSearched($id){

        $searches = Search::where('user_id',$id)->with([
            'property.district.province',
            'user'
        ])->orderBy('id','desc')->paginate(12);

        //dd($properties);

        return view('properties.userSearched', compact('searches'));

    }

    public function addSearch(Request $request){


        $property_id = $request->has('property_id') ? $request->input('property_id'): null;

        //SI NO SE ESTÁ LOGGEADO, EL USUARIO QUE SE AÑADE A BUSQUEDA ES EL DE INVITADO
        $user_id = \Auth::check() ? auth()->user()->id:User::GUEST_USER;

        try {

            Search::create([
                'property_id'=>$property_id,
                'user_id'=>$user_id,
                'searched_at'=>\Carbon\Carbon::now(),
            ]);

            return "Se añadio";

        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);

            return "Error: ".$e;
        }


    }

    public function createSimilar(Property $property){
        $btnText = __("Crear propiedad");
        return view('properties.formSimilar', compact('property','btnText'));
    }

    public function create(){
        $property = new Property();

        $btnText = __("Crear propiedad");
        return view('properties.form', compact('property','btnText'));
    }

    public function store(PropertyRequest $propertyRequest){

        $propertyRequest->merge(['user_id' => auth()->user()->id ]);

        $tituloModal = "Registro exitoso";

        $mensajeModal = "Se ha registrado el inmueble.";

        //dd($accountingFlowRequest);

        try {

            $property = Property::create($propertyRequest->input());

            //return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
            return redirect()->route('propertyImages.edit', $property->id);

        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);

            $tituloModal = "Registro fallido";

            $mensajeModal = "No se ha podido registrar el inmueble. Por favor inténtelo de nuevo.";

            return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
        }


    }

    public function edit($id){
        $property = Property::with([])
            ->where('id',$id)
            ->orderBy('id', 'desc')
            ->first();

        $btnText = __("Actualizar propiedad");

        //dd($accounting);
        return view('properties.form', compact('property','btnText'));
    }

    public function update(PropertyRequest $propertyRequest, Property $property){

        $tituloModal = "Actualización de inmueble";

        $mensajeModal = "Se ha actualizado el inmueble exitosamente.";

        $young_bonus = $propertyRequest->input('young_bonus');
        $only_students = $propertyRequest->input('only_students');
        $pet_allowed = $propertyRequest->input('pet_allowed');

        if(!$young_bonus){
            $propertyRequest->merge(['young_bonus' => Property::YB_NOT_QUALIFIED ]);
        };
        if(!$only_students){
            $propertyRequest->merge(['only_students' => Property::ALL ]);
        };
        if(!$pet_allowed){
            $propertyRequest->merge(['pet_allowed' => Property::PET_NOT_ALLOWED ]);
        };

        try {

            $property->fill($propertyRequest->input())->save();

            //dd($customerRequest);
            return back()->with('modalMessage',[$tituloModal, $mensajeModal]);

        } catch(\Illuminate\Database\QueryException $e){

            $tituloModal = "Actualización fallido";

            $mensajeModal = "No se ha podido actualizar el inmueble. Por favor inténtelo de nuevo.";

            return back()->with('modalMessage',[$tituloModal, $mensajeModal]);

        }
    }



}
