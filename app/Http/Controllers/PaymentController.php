<?php

namespace App\Http\Controllers;

use App\Contract;
use App\Lease;
use App\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

    public function updatePayment(Request $request){

        $id = $request->has('id') ? $request->input('id'): null;
        $amount_payed = $request->has('amount_payed') ? $request->input('amount_payed'): null;

        $payment = new Payment();

        $payment = $payment::whereId($id)->first();

        $lease_id = $payment->contract->lease_id;

        //POLICIES
        $canStore = self::canStore($lease_id);
        //END POLICIES

        if(!$canStore){

            //CÓDIGO DE ERROR DE AUTORIZACION
            return 401;
        }

        try {

            if( $amount_payed >= $payment->amount_total ){
                $payment = $payment::whereId($id)->update([
                    'amount_payed'=>$amount_payed,
                    'state'=>Payment::PAYED,
                    'payed_date' => Carbon::now(),
                ]);

            }else{
                $payment = $payment::whereId($id)->update([
                    'amount_payed'=>$amount_payed,
                    'state'=>Payment::PARTIALLY_PAYED,
                ]);

            }

            return 1;

        } catch(\Illuminate\Database\QueryException $e){

            //dd($e);
            return $e;

        }
    }

    public function listLease($id){

        $payments = Payment::where('contract_id',$id)
            ->with(['contract'])
            ->orderBy('emitted_date','asc')
            ->paginate(12);

        $contract = Contract::where('id',$id)->first();

        $start_date = $contract->start_date;
        $lease_id = $contract->lease_id;

        //POLICIES
        $canStore = self::canStore($lease_id);
        //END POLICIES

        //dd($applications);

        return view('payments.listLease', compact('payments', 'start_date', 'canStore'));

    }

    public function canStore($lease){
        $leaseModel = Lease::where('id',$lease)->first();

        //dd($propertyModel);

        //POLICIES
        $canStore = auth()->user()->can('store', [Contract::class, $leaseModel] );
        //END POLICIES

        return $canStore;
    }


}
