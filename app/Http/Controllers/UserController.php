<?php

namespace App\Http\Controllers;

use App\District;
use App\DocumentType;
use App\Helpers\Helper;
use App\Http\Requests\UserRequest;
use App\Lessee;
use App\LifestyleType;
use App\Mail\RegistrationMail;
use App\Mail\RegistrationUserMail;
use App\Mail\ReportInterestedMail;
use App\Mail\ReportPayedMail;
use App\Nacionality;
use App\OccupationType;
use App\Payment;
use App\Property;
use App\Propietary;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserController extends Controller
{

    public function list(){
        $users = User::with([])
            ->orderBy('id', 'desc')
            ->paginate(12);

        //dd($companies);
        return view('users.list', compact('users' ));
    }

    public function profile($slug){

        $user = User::with([])
            ->where('slug',$slug)
            ->orderBy('id', 'desc')
            ->first();

        $documentTypes = DocumentType::get();
        $nacionalities = Nacionality::get();
        $lifestyleTypes = LifestyleType::get();
        $occupationTypes = OccupationType::get();
        $districts = District::get();

        //dd($companies);
        return view('users.profile', compact('user', 'documentTypes', 'nacionalities', 'lifestyleTypes', 'occupationTypes', 'districts'));
    }

    public function create(){
        $user = new User();

        $btnText = __("Crear usuario");
        return view('users.form', compact('user','btnText'));
    }

    public function store(UserRequest $userRequest){
        //dd($userRequest);
        //$userRequest->merge(['user_id' => auth()->user()->id ]);
        $slug = $userRequest->input('name').'-'.$userRequest->input('last_name').'-'.Str::random(4);

        $role = (int) $userRequest->input('role_id');
        $name = $userRequest->input('name');

        $password = bcrypt($userRequest->input('password'));

        $userRequest->merge(['remember_token' => Str::random(10) ]);
        $userRequest->merge(['slug' => $slug ]);
        $userRequest->merge(['password' => $password ]);

        /*MODAL MESSAGE*/
        $tituloModal = 'Registro Exitoso';
        $mensajeModal = 'Hola '.$name.', bienvenid@ a Homex!<br><br>';
        /*MODAL MESSAGE*/


        /*LESSEE INFORMATION*/

        $lesse = new Lessee();

        $bond_interested_status = $userRequest->input('bond_interested_status');
        $quantity_members = $userRequest->input('quantity_members');
        $dependent_status = $userRequest->input('dependent_status');
        $monthly_salary = $userRequest->input('monthly_salary');
        $debtor_status = $userRequest->input('debtor_status');
        $other_postulation = $userRequest->input('other_postulation');

        /**/

        try {

            $user = User::create($userRequest->input());

            //Inicializamos las variables de sesión

            \Auth::loginUsingId($user->id);

            //Creamos de acuerdo al ROL
            if($role === Role::PROPIETARY){
                Propietary::create(['user_id'=>$user->id]);
                $mensajeModal.='Publica y pronto encontraremos tu siguiente inquilino.';


            }elseif ($role === Role::LESSEE){

                if($bond_interested_status == 1){
                    $lesse = Lessee::create([
                        'user_id'=>$user->id,
                        'quantity_members'=>$quantity_members,
                        'dependent_status'=>$dependent_status,
                        'monthly_salary'=>$monthly_salary,
                        'debtor_status'=>$debtor_status,
                         'other_postulation'=>$other_postulation,
                        'bond_interested_status'=>1,
                    ]);
                }else{
                    $lesse = Lessee::create(['user_id'=>$user->id]);
                }

                $mensajeModal.='En esta plataforma pronto encontrarás tu siguiente hogar.';
            }

            //dd($customerRequest);
            //return new RegistrationMail($user, $lesse);
            //new RegistrationUserMail($user);

            $admin = User::whereId(User::CONTACT_MAIL_ID)->first();

            \Mail::to($admin)->send(new RegistrationMail($user, $lesse));
            \Mail::to($user)->send(new RegistrationUserMail($user));

            return back()->with('modalMessage',[$tituloModal, $mensajeModal]);

        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);

            if ($e->getCode() == 23000 ){
                $tituloModal = 'Registro fallido';
                $mensajeModal = 'Ya te encuentras registrado.<br> Si te olvidaste tu contraseña, <a href="'.route('password.request').'">ingresa aquí.</a>.';

                return back()->with('modalMessage',[$tituloModal, $mensajeModal]);

            }else{
                $tituloModal = 'Registro fallido';
                $mensajeModal = 'Ha ocurrido un error en el registro, por favor vuelva a intentarlo e ingrese los datos requeridos.<br>';
                $mensajeModal = 'Código de error: ' . $e->getCode() ;
                return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
            }

        }

    }

    public function update(UserRequest $userRequest, User $user){

        //dd($userRequest);
        $tituloModal = 'Actualizar datos';
        $mensajeModal = 'Se actualizó correctamente su información personal.';

        //POLICIES
        $canEditUser = auth()->user()->can('editUser', [User::class, $user] );
        //END POLICIES

        if(!$canEditUser){
            //dd($userRequest);
            $tituloModal = 'Actualizar datos';
            $mensajeModal = 'Usted no puede actualizar este usuario.';
            return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
        }

        try{

            if($userRequest->has('picture')){
                \Storage::delete('users/'.$user->picture);
                $picture = Helper::uploadFile('picture', 'users');
                $userRequest->merge(['picture' => $picture ]);
            }

            $user->fill($userRequest->input())->save();

        } catch(\Illuminate\Database\QueryException $e){
            $mensajeModal = 'Ha ocurrido un error en la actualización, por favor vuelva a intentarlo e ingrese los datos requeridos.<br>';
            $mensajeModal .= 'Código de error: ' . $e->getCode() ;
        }
        return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
    }

    public function reportInterested($user_wanted){

        $tituloModal = 'Solicitar reporte';
        $mensajeModal = 'Se registró correctamente su interés en el reporte. Nuestros encargados se pondrán en contacto con usted.';

        $admin = User::whereId(User::CONTACT_MAIL_ID)->first();
        $user_wanting = User::whereId(auth()->user()->id)->first();
        $user_wanted = User::whereId($user_wanted)->first();


        try{
            \Mail::to($admin)->send(new ReportInterestedMail($user_wanted, $user_wanting));

        } catch(\Illuminate\Database\QueryException $e){
            $mensajeModal = 'Ha ocurrido un error en la solicitud, por favor vuelva a intentarlo e ingrese los datos requeridos.<br>';
            $mensajeModal .= 'Código de error: ' . $e->getCode() ;
        }
        return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
    }

    public function reportPayed($user_wanted){

        $tituloModal = 'Solicitar reporte';
        $mensajeModal = 'Nuestros encargados se pondrán en contacto con usted para verificar el pago y birndarle el reporte solicitado.';

        $admin = User::whereId(User::CONTACT_MAIL_ID)->first();
        $user_wanting = User::whereId(auth()->user()->id)->first();
        $user_wanted = User::whereId($user_wanted)->first();

        try{
            \Mail::to($admin)->send(new ReportPayedMail($user_wanted, $user_wanting));

        } catch(\Illuminate\Database\QueryException $e){
            $mensajeModal = 'Ha ocurrido un error en la solicitud, por favor vuelva a intentarlo e ingrese los datos requeridos.<br>';
            $mensajeModal .= 'Código de error: ' . $e->getCode() ;
        }
        return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
    }


}
