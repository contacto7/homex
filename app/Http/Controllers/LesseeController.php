<?php

namespace App\Http\Controllers;

use App\Http\Requests\LesseeRequest;
use App\Lessee;
use App\User;
use Illuminate\Http\Request;

class LesseeController extends Controller
{

    public function update(LesseeRequest $lesseeRequest, Lessee $lessee){

        //dd($userRequest);
        $tituloModal = 'Actualizar datos';
        $mensajeModal = 'Se actualizó correctamente su información personal.';

        //USUARIO QUE SE MODIFICARA
        $user=$lessee->user;

        //POLICIES
        $canEditUser = auth()->user()->can('editUser', [User::class, $user] );
        //END POLICIES

        if(!$canEditUser){
            //dd($userRequest);
            $tituloModal = 'Actualizar datos';
            $mensajeModal = 'Usted no puede actualizar este usuario.';
            return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
        }

        try{

            $lessee->fill($lesseeRequest->input())->save();

        } catch(\Illuminate\Database\QueryException $e){
            $mensajeModal = 'Ha ocurrido un error en la actualización, por favor vuelva a intentarlo e ingrese los datos requeridos.<br>';
            $mensajeModal = 'Código de error: ' . $e->getCode() ;
        }
        return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
    }
}
