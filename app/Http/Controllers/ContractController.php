<?php

namespace App\Http\Controllers;

use App\Contract;
use App\Helpers\Helper;
use App\Http\Requests\ContractRequest;
use App\Lease;
use App\Mail\LeaseCreatedMail;
use App\Payment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ContractController extends Controller
{

    public function myList($user){
        $properties = Contract::with([])
            ->where('user_id',$user)
            ->orderBy('id', 'desc')
            ->get();

        //dd($companies);
        return view('properties.myList', compact('properties'));
    }

    public function listLease($id){

        $contracts = Contract::with([])
            ->where('lease_id', $id)
            ->orderBy('id', 'desc')
            ->paginate(12);

        $lease_id = $id;

        //POLICIES
        $canStore = self::canStore($lease_id);
        //END POLICIES

        //dd($application);
        return view('contracts.listLease', compact('contracts', 'lease_id', 'canStore'));
    }

    public function getDownload($id){
        return response()->download(storage_path("app/public/contracts/{$id}"));
    }

    public function create($lease_id){
        $contract = new Contract();

        $btnText = __("Crear contrato");
        return view('contracts.form', compact('contract','btnText', 'lease_id'));
    }

    public function store(ContractRequest $contractRequest){

        $tituloModal = "Registro exitoso";

        $mensajeModal = "Se ha registrado el contrato.";

        //dd($accountingFlowRequest);

        //POLICIES
        $canStore = self::canStore($contractRequest->lease_id);
        //END POLICIES

        if(!$canStore){
            //dd($userRequest);
            $mensajeModal = 'Usted no puede crear este contrato.';
            //return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
            return $mensajeModal;
        }


        try {

            if($contractRequest->has('document')){
                $document = Helper::uploadFile('document', 'contracts');
                $contractRequest->merge(['document' => $document ]);
            }

            $contract = Contract::create($contractRequest->input());

            self::insertPayments($contractRequest, $contract->id);

            $admin = User::whereId(User::CONTACT_MAIL_ID)->first();
            $lessee = $contract->lease->application->user;
            $propietary = $contract->lease->application->property->user;

            $recipients = [$admin, $lessee, $propietary];

            //dd($recipients);

            \Mail::to($recipients)->send(new LeaseCreatedMail($contractRequest->lease_id));

            return redirect()->route('payments.listLease', ["id" => $contract->id ]);

        } catch(\Illuminate\Database\QueryException $e){
            //dd($e);

            $tituloModal = "Registro fallido";

            $mensajeModal = "No se ha podido registrar el contrato. Por favor inténtelo de nuevo.";

            return back()->with('modalMessage',[$tituloModal, $mensajeModal]);
        }


    }

    public function edit($contract){
        $contract = Contract::with([])
            ->where('id',$contract)
            ->orderBy('id', 'desc')
            ->first();

        $lease_id = $contract->lease_id;

        //POLICIES
        $canStore = self::canStore($lease_id);
        //END POLICIES

        if(!$canStore){
            $mensajeError = 'Usted no puede editar este contrato.';
            return $mensajeError;
        }

        $btnText = __("Actualizar contrato");
        return view('contracts.form', compact('contract','btnText','lease_id'));
    }

    public function update(ContractRequest $contractRequest, Contract $contract){

        $tituloModal = "Actualización de contrato";

        $mensajeModal = "Se ha actualizado el contrato exitosamente.";

        //POLICIES
        $canStore = self::canStore($contract->lease_id);
        //END POLICIES

        if(!$canStore){
            $mensajeError = 'Usted no puede editar este contrato.';
            return $mensajeError;
        }

        try {

            if($contractRequest->has('document')){
                \Storage::delete('contracts/'.$contractRequest->document);
                $document = Helper::uploadFile('document', 'contracts');
                $contractRequest->merge(['document' => $document ]);
            }

            $contract->fill($contractRequest->input())->save();

            //dd($customerRequest);
            return back()->with('modalMessage',[$tituloModal, $mensajeModal]);

        } catch(\Illuminate\Database\QueryException $e){

            $tituloModal = "Actualización fallido";

            $mensajeModal = "No se ha podido actualizar el contrato. Por favor inténtelo de nuevo.";

            return back()->with('modalMessage',[$tituloModal, $mensajeModal]);

        }


    }

    public function canStore($lease){
        $leaseModel = Lease::where('id',$lease)->first();

        //dd($propertyModel);

        //POLICIES
        $canStore = auth()->user()->can('store', [Contract::class, $leaseModel] );
        //END POLICIES

        return $canStore;
    }

    public function insertPayments($contractRequest, $contract_id){

        $pay_date =  $contractRequest->input('pay_date');
        $start_date =  $contractRequest->input('start_date');
        $months_date =  $contractRequest->input('months_date');
        $currency_id =  $contractRequest->input('currency_id');
        $amount =  $contractRequest->input('amount');

        /*Creamos los pagos*/
        $payments = array();

        //Creamos la primera fecha de pago
        $start_pay_date = \Carbon\Carbon::createFromFormat('Y-m-d', $start_date, 'America/Lima');
        $start_date = \Carbon\Carbon::createFromFormat('Y-m-d', $start_date, 'America/Lima');

        //Cambiamos el día
        $start_pay_date = $start_pay_date->day($pay_date);


        //dd($start_pay_date);

        //El dia de pago empieza el mismo mes
        $i = 0;

        //Si el día de pago ya pasó al día de inicio del contrato, se cuenta a partir del otro mes
        //dd("Dia de inicio de contrato: ".$start_date->day.". Dia de pago: ".$pay_date);

        if($start_date->day > $pay_date){
            $i++;
            $months_date++;
        }

        for ($i; $i < $months_date; $i++) {

            $temporal_date = clone $start_pay_date;

            $temporal_date = $temporal_date->addMonth($i);
            //dd($temporal_date);


            $payment = array(
                'contract_id'=>$contract_id,
                'emitted_date'=> $temporal_date,
                'payed_date'=> null,
                'currency_id'=> $currency_id,
                'amount_payed'=> 0,
                'amount_total'=> $amount,
                'state'=> Payment::NOT_PAYED,
            );

            array_push($payments, $payment);
        }
        //dd($payments);

        Payment::insert($payments); // Eloquent approach
        //\DB::table('payments')->insert($data); // Query Builder approach
        /*Creamos los pagos*/

        //return back()->with('modalMessage',[$tituloModal, $mensajeModal]);

        $payments = Payment::where('contract_id',$contract_id)
            ->with([])
            ->orderBy('emitted_date','asc')
            ->paginate(12);

    }



}
