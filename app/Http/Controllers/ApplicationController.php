<?php

namespace App\Http\Controllers;

use App\Application;
use App\Lease;
use App\Mail\AprovedApplicationMail;
use App\User;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function list(){

        $applications =
            Application::select('*', \DB::raw('count(*) as postulaciones'))
                ->with([
                    'property.district.province',
                    'user'
                ])
                ->orderBy('property_id','desc')
                ->groupBy('property_id')
                ->paginate(12);

        //dd($applications);

        //dd($application);
        return view('applications.list', compact('applications'));

    }
    public function listLessee($id){

        $applications = Application::where('user_id',$id)->with([
            'property.district.province',
            'user'
        ])->latest()->paginate(12);

        //dd($applications);

        //dd($application);
        return view('applications.listLessee', compact('applications'));

    }
    public function listPropietary($id){

        $applications =
            Application::whereHas("property", function($q) use($id){
                $q->where("user_id",$id);
            })
                ->select('*', \DB::raw('count(*) as postulaciones'))
                ->with([
                'property.district.province',
                'user'
            ])
                ->orderBy('property_id','desc')
                ->groupBy('property_id')
                ->paginate(12);

        //dd($applications);

        //dd($application);
        return view('applications.listPropietary', compact('applications'));

    }

    public function listProperty($id){

        $applications = Application::where('property_id',$id)->with([
            'property.district.province',
            'user'
        ])
            ->orderByDesc('stage')
            ->orderByRaw('FIELD(stage_state, 2,1,3)')
            ->paginate(12);

        //dd($applications);

        //dd($application);
        return view('applications.listProperty', compact('applications'));

    }

    public function status($id){

        $application = Application::where('id',$id)->with([
            'property.district.province',
            'user'
        ])->first();

        //dd($application);
        return view('applications.status', compact('application'));

    }

    public function updatePostulation(Request $request){

        $id = $request->has('id') ? $request->input('id'): null;
        $stage = $request->has('stage') ? $request->input('stage'): null;
        $stage_state = $request->has('stage_state') ? $request->input('stage_state'): null;

        try {

            $application = new Application();

            $application::whereId($id)->update([
                'stage'=>$stage,
                'stage_state'=>$stage_state,
            ]);

            $property_id = $application::whereId($id)->get('property_id')->first()->property_id;


            if($stage == Application::CONTRATO_FIRMADO && $stage_state == Application::APPROVED){

                $lease = new Lease();

                $lease::create([
                    'application_id'=>$id
                ]);

            }elseif($stage == Application::APROBADO && $stage_state == Application::APPROVED){


                $admin = User::whereId(User::CONTACT_MAIL_ID)->first();

                \Mail::to($admin)->send(new AprovedApplicationMail($property_id));

            }



            //dd($customerRequest);
            return 1;

        } catch(\Illuminate\Database\QueryException $e){

            //dd($e);
            return $e;

        }
    }


}
