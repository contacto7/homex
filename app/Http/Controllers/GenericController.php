<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GenericController extends Controller
{

    public function aboutUs()
    {
        return view('aboutus');
    }

    public function faq()
    {
        return view('faq');
    }

    public function privacyPolicies()
    {
        return view('privacyPolicies');
    }

    public function termsAndConditions()
    {
        return view('termsAndConditions');
    }

    public function deleteHelp()
    {
        try{
            \Cookie::queue('hide', 'helpMessage', 360);
            return 1;
        } catch(\Illuminate\Database\QueryException $e){
            return $e;
        }

    }
}
