<?php

namespace App\Console\Commands;

use App\Mail\RememberPaymentsdMail;
use App\Payment;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RememberPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remember:payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enviar un correo a las personas que aún no pagan su renta mensual';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $users=User::whereHas("applications", function($sq1) {
            $sq1->whereHas("lease", function($sq2) {
                $sq2->whereHas("contracts", function($sq3) {
                    $sq3->whereHas("payments", function($sq4) {
                        $sq4->where('emitted_date','<=', Carbon::now('America/Lima'))
                            ->whereColumn('amount_payed','<','amount_total');
                    });
                });
            });
        })->get();

        /*
         * SE DIVIDE POR 95 TODA LA COLECCION CON CHUNK PARA QUE
         * AL ENVIAR LOS CORREOS SE ENVIÉN CON RECEPTORES DE 95
         *
         * ESTO SE HACE DEBIDO A QUE GMAIL TIENE RESTRICCIÓN DE
         * ENVÍO MEDIANTE SOFTWARE, REFERENCIA DE GOOGLE:
         * https://support.google.com/a/answer/166852?hl=es
         *
         * */
        foreach ($users->chunk(95) as $chunk){

            \Mail::to($chunk)->send(new RememberPaymentsdMail());

        }

        return true;

    }
}
