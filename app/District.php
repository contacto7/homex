<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\District
 *
 * @property-read \App\Province $province
 * @method static \Illuminate\Database\Eloquent\Builder|\App\District newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\District newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\District query()
 * @mixin \Eloquent
 */
class District extends Model
{
    protected $fillable = [
        'name', 'province_id'
    ];

    public function province(){
        return $this->belongsTo(Province::class);
    }
}
