<?php

namespace App\Mail;

use App\Property;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewApplicationInPropertyMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Property $property, User $user_interested)
    {
        $this->property = $property;
        $this->user_interested = $user_interested;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject("Nuevo postulante en inmueble")
            ->markdown("emails.new_application_in_property")
            ->with('property', $this->property)
            ->with('user_interested', $this->user_interested);
    }
}
