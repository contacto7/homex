<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportPayedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user_wanted, User $user_wanting)
    {
        //
        $this->user_wanted = $user_wanted;
        $this->user_wanting = $user_wanting;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject("Solicitud de reporte PAGADO")
            ->markdown("emails.report_payed")
            ->with('user_wanted', $this->user_wanted)
            ->with('user_wanting', $this->user_wanting);
    }
}
