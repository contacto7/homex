<?php

namespace App\Mail;

use App\Lessee;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegistrationMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var User
     */
    private $user;
    /**
     * @var Lessee
     */
    private $lessee;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param Lessee $lessee
     */
    public function __construct(User $user, Lessee $lessee)
    {
        //
        $this->user = $user;
        $this->lessee = $lessee;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject("Se ha registrado un usuario nuevo")
            ->markdown("emails.registration_mail")
            ->with('user', $this->user)
            ->with('lessee', $this->lessee);
    }
}
