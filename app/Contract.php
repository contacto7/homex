<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Contract
 *
 * @property-read \App\Lease $lease
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Contract query()
 * @mixin \Eloquent
 */
class Contract extends Model
{
    const ACTIVE = 1;
    const INACTIVE = 2;

    //http://comprarmicasa.com/tres-tipos-de-contratos-de-arrendamiento-de-vivienda-en-el-peru/
    const LIVING_DESTINED = 1;
    const LIVING_DESTINED_WITH_BUYING_OPTION = 2;
    const LIVING_FINANCE_DESTINED = 3;

    //
    protected $fillable = [
        'start_date','end_date','pay_date', 'months_date',
        'type', 'currency_id','amount','lease_id', 'document', 'state',
    ];

    public function lease(){
        return $this->belongsTo(Lease::class);
    }
    public function payments(){
        return $this->hasMany(Payment::class);
    }

}
