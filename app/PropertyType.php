<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PropertyType
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Property[] $properties
 * @property-read int|null $properties_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PropertyType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PropertyType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PropertyType query()
 * @mixin \Eloquent
 */
class PropertyType extends Model
{
    protected $fillable = [
        'name'
    ];

    public function properties(){
        return $this->hasMany(Property::class);
    }
}
