<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Pet
 *
 * @property-read \App\Lessee $lessee
 * @property-read \App\PetType $petType
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pet query()
 * @mixin \Eloquent
 */
class Pet extends Model
{
    const SMALL = 1;
    const MEDIUM =2;
    const BIG = 3;

    protected $fillable = [
        'size' ,'pet_type_id','lessees_id'
    ];

    public function petType(){
        return $this->belongsTo(PetType::class);
    }
    public function lessee(){
        return $this->belongsTo(Lessee::class);
    }
}
