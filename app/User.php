<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

/**
 * App\User
 *
 * @property int $id
 * @property int $role_id
 * @property string $name
 * @property string $last_name
 * @property string $slug
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property int $document_type_id
 * @property string $document_number
 * @property int $nacionality_id
 * @property string $cellphone
 * @property string|null $picture
 * @property string $sex
 * @property string $state
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $stripe_id
 * @property string|null $card_brand
 * @property string|null $card_last_four
 * @property string|null $trial_ends_at
 * @property-read \App\DocumentType $documentType
 * @property-read \App\Lessee $lessee
 * @property-read \App\Nacionality $nacionality
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Role $role
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCardBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCardLastFour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCellphone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDocumentTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereNacionalityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTrialEndsAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    const GUEST_USER = 2;

    const ACTIVE = 1;
    const INACTIVE = 2;

    const FEMALE = 1;
    const MALE = 2;

    const SINGLE = 1;
    const COUPLE = 2;
    const MARRIED = 3;
    const WITH_FAMILY = 4;

    const CONTACT_MAIL_ID = 1;

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id','office_id','last_name', 'slug', 'email_verified_at',
        'picture','state','name', 'email', 'password', 'document_type_id',
        'document_number', 'nacionality_id', 'cellphone', 'sex'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function pathAttachment(){
        return "/images/users/".$this->picture;
    }

    /*++
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
    */

    public function setSlugAttribute(){
        $fake_id = rand(1000000,9999999);

        $slug_name = Str::slug($this->attributes['name']." ".$this->attributes['last_name']." ".$fake_id, '-');

        $this->attributes['slug'] = $slug_name;
    }

    public static function navigation(){
        return auth()->check() ? auth()->user()->role->name : 'guest';
    }


    public function role(){
        return $this->belongsTo(Role::class);
    }
    public function documentType(){
        return $this->belongsTo(DocumentType::class);
    }
    public function nacionality(){
        return $this->belongsTo(Nacionality::class);
    }
    public function lessee(){
        return $this->hasOne(Lessee::class);
    }
    public function propietary(){
        return $this->hasOne(Propietary::class);
    }
    public function applications(){
        return $this->hasMany(Application::class);
    }

}
