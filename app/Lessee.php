<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Lessee
 *
 * @property-read \App\District $district
 * @property-read \App\LifestyleType $lifestyleType
 * @property-read \App\OccupationType $occupationType
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Pet[] $pets
 * @property-read int|null $pets_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Lessee newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Lessee newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Lessee query()
 * @mixin \Eloquent
 */
class Lessee extends Model
{
    const SINGLE = 1;
    const COUPLE = 2;
    const FAMILY = 3;
    const INDEPENDENT = 1;
    const DEPENDENT = 2;
    const NOT_DEBTOR = 1;
    const DEBTOR = 2;
    const NOT_OTHER_POSTULATION = 1;
    const OTHER_POSTULATION = 2;
    const BOND_INTERESTED = 1;
    const BOND_NOT_INTERESTED = 2;

    protected $fillable = [
        'user_id', 'monthly_salary', 'emotional_situation',
        'occupation_type_id','occupation_center','lifestyle_type_id','address',
        'district_id', 'quantity_members', 'dependent_status', 'debtor_status',
        'other_postulation','bond_interested_status',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function occupationType(){
        return $this->belongsTo(OccupationType::class);
    }

    public function lifestyleType(){
        return $this->belongsTo(LifestyleType::class);
    }

    public function district(){
        return $this->belongsTo(District::class);
    }

    public function pets(){
        return $this->hasMany(Pet::class);
    }
}
