<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\RentLimit
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Property[] $properties
 * @property-read int|null $properties_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentLimit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentLimit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\RentLimit query()
 * @mixin \Eloquent
 */
class RentLimit extends Model
{
    protected $fillable = [
        'lower_rent','upper_rent'
    ];

    public function properties(){
        return $this->hasMany(Property::class);
    }
}
