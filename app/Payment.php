<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Payment
 *
 * @property-read \App\Lease $lease
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment query()
 * @mixin \Eloquent
 */
class Payment extends Model
{
    const NOT_PAYED = 1;
    const PAYED = 2;
    const PARTIALLY_PAYED = 3;
    //
    protected $fillable = [
        'lease_id','emitted_date','payed_date', 'currency_id',
        'amount_payed','amount_total','state',
    ];

    public function contract(){
        return $this->belongsTo(Contract::class);
    }

}
