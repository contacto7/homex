<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\LifestyleType
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Lessee[] $lessees
 * @property-read int|null $lessees_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LifestyleType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LifestyleType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\LifestyleType query()
 * @mixin \Eloquent
 */
class LifestyleType extends Model
{
    protected $fillable = [
        'name','description'
    ];

    public function lessees(){
        return $this->hasMany(Lessee::class);
    }
}
