<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Lease
 *
 * @property-read \App\Application $application
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Contract[] $contracts
 * @property-read int|null $contracts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Payment[] $payments
 * @property-read int|null $payments_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Lease newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Lease newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Lease query()
 * @mixin \Eloquent
 */
class Lease extends Model
{
    protected $fillable = [
        'application_id'
    ];

    public function application(){
        return $this->belongsTo(Application::class);
    }
    public function contracts(){
        return $this->hasMany(Contract::class);
    }
    public function lastContract(){
        return Contract::
            where('lease_id', $this->id)
            ->latest()
            ->first();

    }
}
