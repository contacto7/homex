<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PropertyImage
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PropertyImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PropertyImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PropertyImage query()
 * @mixin \Eloquent
 */
class PropertyImage extends Model
{

    protected $fillable = [
        'picture','order','property_id',
    ];

    public function pathAttachment(){
        return "/images/properties/".$this->picture;
    }

    public function property(){
        return $this->belongsTo(Property::class);
    }
}
