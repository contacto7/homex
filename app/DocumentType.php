<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\DocumentType
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentType whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\DocumentType whereName($value)
 * @mixin \Eloquent
 */
class DocumentType extends Model
{
    public $timestamps = false;

    public function users(){
        return $this->hasMany(User::class);
    }
}
