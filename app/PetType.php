<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\PetType
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Pet[] $pets
 * @property-read int|null $pets_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PetType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PetType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PetType query()
 * @mixin \Eloquent
 */
class PetType extends Model
{
    protected $fillable = [
        'name'
    ];

    public function pets(){
        return $this->hasMany(Pet::class);
    }
}
