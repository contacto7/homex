<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Province
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\District[] $districts
 * @property-read int|null $districts_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Province newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Province newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Province query()
 * @mixin \Eloquent
 */
class Province extends Model
{
    protected $fillable = [
        'name', 'department_id'
    ];

    public function department(){
        return $this->belongsTo(Department::class);
    }

    public function districts(){
        return $this->hasMany(District::class);
    }
}
