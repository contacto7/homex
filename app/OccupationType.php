<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\OccupationType
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Lessee[] $lessees
 * @property-read int|null $lessees_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OccupationType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OccupationType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OccupationType query()
 * @mixin \Eloquent
 */
class OccupationType extends Model
{
    protected $fillable = [
        'name'
    ];

    public function lessees(){
        return $this->hasMany(Lessee::class);
    }
}
