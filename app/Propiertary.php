<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Propiertary
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Propiertary newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Propiertary newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Propiertary query()
 * @mixin \Eloquent
 */
class Propiertary extends Model
{
    //
}
