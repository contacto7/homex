<?php

namespace App\Policies;

use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function editUser(User $user_auth, $user_profile){
        return $user_auth->id === $user_profile->id || $user_auth->role_id === Role::ADMIN;
    }

    public function solicitarReporte(User $user){
        return $user->role_id === Role::PROPIETARY;
    }

}
