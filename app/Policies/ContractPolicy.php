<?php

namespace App\Policies;

use App\Lease;
use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContractPolicy
{
    use HandlesAuthorization;

    public function store(User $user, Lease $lease){
        //Vemos si el estudiante ya está inscrito en el curso
        return $user->id === $lease->application->property->user_id || $user->role_id === Role::ADMIN;
    }


}
