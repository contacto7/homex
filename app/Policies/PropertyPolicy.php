<?php

namespace App\Policies;

use App\Property;
use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PropertyPolicy
{
    use HandlesAuthorization;

    public function opt_for_properties(User $user){
        //SI ES PROPIETARIO, NO PODRA POSTULAR
        return $user->role_id === Role::LESSEE;
    }

    public function editProperty(User $user, Property $property){
        //Vemos si el estudiante ya está inscrito en el curso
        return $user->id === $property->user_id || $user->role_id === Role::ADMIN;
    }

    public function apply(User $user, Property $property){
        //Vemos si el arrendatario ya está postuló
        //return ! $user->applications->contains($user->id);
        return ! $property->applications->where('user_id',$user->id)->count();
    }

    public function create(User $user){
        //SI ES PROPIETARIO, NO PODRA POSTULAR
        return $user->role_id === Role::LESSEE;
    }
}
