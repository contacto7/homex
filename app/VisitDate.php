<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\VisitDate
 *
 * @property-read \App\Application $application
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VisitDate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VisitDate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\VisitDate query()
 * @mixin \Eloquent
 */
class VisitDate extends Model
{
    protected $fillable = [
        'day','duration','application_id',
    ];


    public function application(){
        return $this->belongsTo(Application::class);
    }
}
