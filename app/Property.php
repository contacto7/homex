<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Property
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Application[] $applications
 * @property-read int|null $applications_count
 * @property-read \App\District $district
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PropertyImage[] $propertyImages
 * @property-read int|null $property_images_count
 * @property-read \App\PropertyType $propertyType
 * @property-read \App\RentLimit $rentLimit
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Search[] $searches
 * @property-read int|null $searches_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Property newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Property newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Property query()
 * @mixin \Eloquent
 */
class Property extends Model
{
    const ALL = 1;
    const ACTIVE = 1;
    const INACTIVE = 2;
    const PET_ALLOWED = 1;
    const PET_NOT_ALLOWED = 2;
    const WOMEN_ONLY = 2;
    const MAN_ONLY = 3;
    const WOMAN_PREFERRED = 4;
    const MAN_PREFERRED = 5;
    const STUDENTS_ONLY = 2;
    const STUDENTS_PREFERRED = 3;
    const YB_QUALIFIED = 1;
    const YB_NOT_QUALIFIED = 2;
    const YB_PREFERRED = 3;
    const COUPLE_WITH_KIDS_ONLY = 2;
    const COUPLE_WITH_KIDS_PREFERRED = 3;
    const AGE_18_30_ONLY = 2;
    const AGE_31_45_ONLY = 3;
    const AGE_45_ONLY = 4;
    const AGE_18_30_PREFERRED = 5;
    const AGE_31_45_PREFERRED = 6;
    const AGE_45_PREFERRED = 7;

    protected $fillable = [
        'name','description','rooms', 'user_id', 'district_id',
        'property_type_id','parking_slots','total_area',
        'bathrooms','half_bathrooms','date', 'currency_rental_amount',
        'rental_amount','currency_maintenance_amount', 'maintenance_amount',
        'currency_guaranty_amount', 'guaranty_amount',
        'latitude','longitude','address', 'pet_allowed',
        'state','only_genre','only_students', 'young_bonus', 'family_status',
        'age_range', 'rent_limit_id',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function currencyRentalAmount(){
        return $this->belongsTo(Currency::class, 'currency_rental_amount');
    }
   public function currencyMaintenanceAmount(){
        return $this->belongsTo(Currency::class, 'currency_maintenance_amount');
    }
   public function currencyGuarantyAmount(){
        return $this->belongsTo(Currency::class, 'currency_guaranty_amount');
    }
    public function district(){
        return $this->belongsTo(District::class);
    }
    public function propertyType(){
        return $this->belongsTo(PropertyType::class);
    }
    public function rentLimit(){
        return $this->belongsTo(RentLimit::class);
    }

    public function searches(){
        return $this->hasMany(Search::class);
    }
    public function applications(){
        return $this->hasMany(Application::class);
    }
    public function propertyImages(){
        return $this->hasMany(PropertyImage::class);
    }

    //RELACIONADOS POR
    //Distrito
    //

    public function relatedProperties(){
        return Property::
            where('district_id',$this->district->id)
            ->where('id','!=',$this->id)
            ->where('state', Property::ACTIVE)
            ->latest()
            ->limit(6)
            ->get();
    }

    public function principalImage(){
        return PropertyImage::
            where('property_id',$this->id)
            ->where('order',1)
            ->latest()
            ->first();
    }

    public function visits(){
        return Search::
            select('*', \DB::raw('count(*) as visitas'))
            ->where('property_id',$this->id)
            ->orderBy('property_id','desc')
            ->groupBy('property_id')
            ->first();
        //dd($applications);
    }

}
