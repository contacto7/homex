<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.min.js" integrity="sha256-aS5HnZXPFUnMTBhNEiZ+fKMsekyUqwm30faj/Qh/gIA=" crossorigin="anonymous"></script>
@stack('scripts')

<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"  rel="stylesheet" />

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.css" integrity="sha256-U6YrSIhyGybBdtKMg2+iJsIbyHtNY3Yj9gacnUG2jNo=" crossorigin="anonymous" />
    @stack('styles')
</head>
<body>
@include('partials.navigation')
@yield('jumbotron')
<div id="app">
    @yield('title')
    <main class="py-4">
        @if(session('message'))
            <div class="row justify-content-center text-center">
                <div class="col-md-10">
                    <div class="alert alert-{{ session('message')[0] }} alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <h4 class="alert-heading">{{ __("Mensaje informativo") }}</h4>
                        <p>{{ session('message')[1] }}</p>
                    </div>
                </div>
            </div>
        @endif
        @yield('content')
    </main>
</div>
@include('partials.modals.modalIngresar')
@include('partials.modals.modalRegistrarse')
@include('partials.modals.modalBonoRentaJoven')
@include('partials.modals.modalMensaje')

@include('partials.footer')
<script>

    $('.carousel').carousel({
        interval: 10000000
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover({html:true});
    });
    $(document).on("click",".btn-registro",function(){
        $('#modalIngresar').modal('hide');
        $('#modalRegistrarse').modal('show');
    });
    $('body').on('hidden.bs.modal', function () {
        if($('.modal.show').length > 0)
        {
            $('body').addClass('modal-open');
        }
    });
    $(document).on("click",".close-help-message",function(){
        $('.help-message-wrapp').hide();

        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: '{{ route('deleteHelp') }}',
            success: function (data) {
                if(data == 1){
                    console.log("Se eliminó la ayuda");
                }else{
                    console.log("Ocurrió un error al eliminar la ayuda. Error:"+data);
                }

            },error:function(data){
                console.log(data);
            }
        });


    });
</script>
</body>
</html>
