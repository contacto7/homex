@component('mail::message')

Se ha registrado un nuevo postulante en el inmueble <a href="{{ route('properties.view',$property->id) }}">{{ $property->name }}</a>:

<a href="{{ route('users.profile',$user_interested->slug) }}">{{ $user_interested->name." ".$user_interested->last_name }}</a>.

<small>Para poder ver el perfil completo, debe iniciar sesión.</small>

Gracias,

{{ env("APP_NAME", "Homex") }}

@endcomponent
