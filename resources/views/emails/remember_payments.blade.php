@component('mail::message')
    Estimado(a),

    Se le recuerda que tiene un pendiente de pago por concepto de renta mensual de su inmueble.

    Si ya canceló, por favor ignore este mensaje y solicite a su arrendador actualizar el pago en la plataforma.

    Gracias,

    {{ env("APP_NAME", "Homex") }}
@endcomponent
