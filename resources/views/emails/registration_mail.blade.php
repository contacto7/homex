@component('mail::message')
@php

$role = (int) $user->role_id;

$bond_interested_status = 0;
$quantity_members = "";
$dependent_status = "";
$monthly_salary = "";
$debtor_status = "";
$other_postulation = "";

if (isset($lessee->bond_interested_status)){
$bond_interested_status = $lessee->bond_interested_status;
$quantity_members = $lessee->quantity_members;
$dependent_status = $lessee->dependent_status;
$monthly_salary = $lessee->monthly_salary;
$debtor_status = $lessee->debtor_status;
$other_postulation = $lessee->other_postulation;
}


if($role === \App\Role::PROPIETARY){
    $role_description = "Propietario";
}elseif ($role === \App\Role::LESSEE){
    $role_description = "Arrendatario";
}


@endphp
# Nuevo usuario - {{ $role_description }}
@if($role === \App\Role::LESSEE && $bond_interested_status == 1)
##Interesado en bono renta joven
@endif

<table class="table-data">
    <tr>
        <td>Nombres</td>
        <td>: {{ $user->name }}</td>
    </tr>
    <tr>
        <td>Apellidos</td>
        <td>: {{ $user->last_name }}</td>
    </tr>
    <tr>
        <td>Correo</td>
        <td>: {{ $user->email }}</td>
    </tr>
    <tr>
        <td>Teléfono</td>
        <td>: {{ $user->cellphone }}</td>
    </tr>
    @if($role === \App\Role::LESSEE && $bond_interested_status == 1)
        <tr>
            <td>Familiares habitantes</td>
            <td>: {{ $quantity_members }}</td>
        </tr>
        <tr>
            <td>Independiente</td>
            <td>:
                @if($dependent_status == \App\Lessee::INDEPENDENT)
                    Si
                @elseif($dependent_status == \App\Lessee::DEPENDENT)
                    No
                @endif
            </td>
        </tr>
        <tr>
            <td>Ingresos mensuales</td>
            <td>: {{ $monthly_salary }}</td>
        </tr>
        <tr>
            <td>Registrado como deudor moroso</td>
            <td>:
                @if($debtor_status == \App\Lessee::NOT_DEBTOR)
                    No
                @elseif($debtor_status == \App\Lessee::DEBTOR)
                    Si
                @endif
            </td>
        </tr>
        <tr>
            <td>Otras postulaciones</td>
            <td>:
                @if($other_postulation == \App\Lessee::NOT_OTHER_POSTULATION)
                    No
                @elseif($other_postulation == \App\Lessee::OTHER_POSTULATION)
                    Si
                @endif
            </td>
        </tr>
        <tr>
            <td>Nacionalidad</td>
            <td>: {{ $user->nacionality->name }}</td>
        </tr>
    @endif
</table>


@component('mail::button', ['url' => url('/users/profile/' . $user->slug)])
        Ver usuario
@endcomponent

Gracias,<br>
{{ env("APP_NAME", "Homex") }}

@endcomponent
