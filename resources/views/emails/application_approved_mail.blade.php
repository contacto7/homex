@component('mail::message')
# NUEVO ALQUILER APROBADO:

@component('mail::button', ['url' => route('applications.listProperty', $property_id) ])
    Ver postulación
@endcomponent

Gracias,<br>
{{ env("APP_NAME", "Homex") }}
@endcomponent
