@component('mail::message')
Hola {{ $user->name }},

Te has registrado con éxito a {{ config('app.name') }}.

De parte de nuestro equipo te damos la bienvenida, y esperamos que pronto encuentres tu
@if($user->role_id == \App\Role::PROPIETARY)
inquilino
@elseif($user->role_id == \App\Role::LESSEE)
hogar
@endif
ideal.

@component('mail::button', ['url' => url('/users/profile/' . $user->slug)])
    Ver mi cuenta
@endcomponent

Gracias,<br>
{{ env("APP_NAME", "Homex") }}

@endcomponent
