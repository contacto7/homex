@component('mail::message')
Estimado(a),

Se ha registrado un nuevo contrato, podrá ver la lista de los contratos de la propiedad en el siguiente link:

@component('mail::button', ['url' => route('contracts.listLease', $lease_id) ])
    Ver contratos
@endcomponent

Gracias,<br>
{{ env("APP_NAME", "Homex") }}
@endcomponent
