@component('mail::message')
# USUARIO QUE DESEA EL REPORTE:
## {{ $user_wanting->name." ".$user_wanting->last_name }}

<table class="table-data">
    <tr>
        <td>Correo</td>
        <td>: {{ $user_wanting->email }}</td>
    </tr>
    <tr>
        <td>Teléfono</td>
        <td>: {{ $user_wanting->cellphone }}</td>
    </tr>
    <tr>
        <td>Perfil</td>
        <td>
            <a href="{{ route('users.profile',$user_wanting->slug) }}">VER</a>
        </td>
    </tr>
</table>
# USUARIO DEL QUE SE DESEA:
## {{ $user_wanted->name." ".$user_wanted->last_name }}

<table class="table-data">
    <tr>
        <td>Correo</td>
        <td>: {{ $user_wanted->email }}</td>
    </tr>
    <tr>
        <td>Teléfono</td>
        <td>: {{ $user_wanted->cellphone }}</td>
    </tr>
    <tr>
        <td>Perfil</td>
        <td>
            <a href="{{ route('users.profile',$user_wanted->slug) }}">VER</a>
        </td>
    </tr>
</table>

Gracias,<br>
{{ env("APP_NAME", "Homex") }}

@endcomponent
