@extends('layouts.app')

@section('jumbotron')
    @include('partials.jumbotronCreate')
@endsection

@section('content')
    <div class="container container-accounting-admin">
        <form
            method="POST"
            action="{{ ! $contract->id ? route('contracts.store'): route('contracts.update', $contract->id) }}"
            enctype="multipart/form-data"
        >
            @if($contract->id)
                @method('PUT')
            @endif

            @csrf


            @if(!$contract->id)
            <div class="form-group" style="display: none">
                <label for="lease_id">Arrendamiento</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('lease_id') ? 'is-invalid': '' }}"
                    name="lease_id"
                    id="lease_id"
                    value="{{ $lease_id ?: $contract->lease_id }}"
                    required
                >
            </div>

            <div class="form-group" >
                <label for="start_date">Inicio de contrato:</label>
                <div class="input-group date" id="start_date_tp" data-target-input="nearest">
                    <input
                        type="text"
                        name="start_date"
                        id="start_date"
                        class="form-control datetimepicker-input"
                        value="{{ old('start_date') ?: $contract->start_date }}"
                        data-target="#start_date_tp"
                        data-toggle="datetimepicker"
                    />
                    <div class="input-group-append" data-target="#start_date_tp" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="end_date">Fin de contrato:</label>
                <div class="input-group date" id="end_date_tp" data-target-input="nearest">
                    <input
                        type="text"
                        name="end_date"
                        id="end_date"
                        class="form-control datetimepicker-input"
                        value="{{ old('end_date') ?: $contract->end_date }}"
                        data-target="#end_date_tp"
                        data-toggle="datetimepicker"
                    />
                    <div class="input-group-append" data-target="#end_date_tp" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <label for="pay_date">Día de pago</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('pay_date') ? 'is-invalid': '' }}"
                    name="pay_date"
                    id="pay_date"
                    min="0"
                    placeholder=""
                    value="{{ old('pay_date') ?: $contract->pay_date }}"
                    required
                >
            </div>
            <div class="form-group">
                <label for="months_date">Meses de contrato</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('months_date') ? 'is-invalid': '' }}"
                    name="months_date"
                    id="months_date"
                    min="0"
                    placeholder=""
                    value="{{ old('months_date') ?: $contract->months_date }}"
                    required
                >
            </div>

            <div class="form-group">
                <label for="currency_id">Moneda*</label>
                <select
                    class="form-control {{ $errors->has('currency_id') ? 'is-invalid': '' }}"
                    name="currency_id"
                    id="currency_id"
                    required
                >
                    @foreach(\App\Currency::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('currency_id') === $id || $contract->currency_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="amount">Importe de pago mensual</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('amount') ? 'is-invalid': '' }}"
                    name="amount"
                    id="amount"
                    min="0"
                    placeholder=""
                    value="{{ old('amount') ?: $contract->amount }}"
                    required
                >
            </div>


            @endif
            <div class="form-group">
                <label for="state">ESTADO</label>
                <select
                    class="form-control {{ $errors->has('state') ? 'is-invalid': '' }}"
                    name="state"
                    id="state"
                >
                    <option
                        {{
                            (int) old('state') === \App\Contract::ACTIVE
                            ||
                            (int) $contract->state === \App\Contract::ACTIVE
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Contract::ACTIVE }}"
                    >Activo</option>
                    <option
                        {{
                            (int) old('state') === \App\Contract::INACTIVE
                            ||
                            (int) $contract->state === \App\Contract::INACTIVE
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Contract::INACTIVE }}"
                    >Inactivo</option>
                </select>
            </div>
            <div class="form-group">
                <label for="type">Tipo de contrato</label>
                <select
                    class="form-control {{ $errors->has('type') ? 'is-invalid': '' }}"
                    name="type"
                    id="type"
                >
                    <option
                        {{
                            (int) old('type') === \App\Contract::LIVING_DESTINED
                            ||
                            (int) $contract->type === \App\Contract::LIVING_DESTINED
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Contract::LIVING_DESTINED }}"
                    >Arrendamiento de inmueble destinado a vivienda</option>
                    <option
                        {{
                            (int) old('type') === \App\Contract::LIVING_DESTINED_WITH_BUYING_OPTION
                            ||
                            (int) $contract->type === \App\Contract::LIVING_DESTINED_WITH_BUYING_OPTION
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Contract::LIVING_DESTINED_WITH_BUYING_OPTION }}"
                    >Arrendamiento con opción de compra de inmueble destinado a vivienda</option>
                    <option
                        {{
                            (int) old('type') === \App\Contract::LIVING_FINANCE_DESTINED
                            ||
                            (int) $contract->type === \App\Contract::LIVING_FINANCE_DESTINED
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Contract::LIVING_FINANCE_DESTINED }}"
                    >Arrendamiento-financiero de inmueble destinado a vivienda</option>
                </select>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <input
                        type="file"
                        class="custom-file-input form-control"
                        name="document"
                        id="document"
                        placeholder=""
                    >
                    <label  class="custom-file-label" for="document" data-browse="Elegir">

                        @if($contract->id)
                        Actualizar documento
                        @else
                        Seleccionar contrato
                        @endif
                    </label>
                    <div  class="profile-filename"></div>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>

            </div>

        </form>

    </div>
@endsection

@push('styles')
    <link rel="stylesheet"  href="{{ asset('css/tempusdominus-bootstrap-4.min.css') }}">
@endpush
@push('scripts')
    <script type="text/javascript"  src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/tempusdominus-bootstrap-4.min.js') }}"></script>

    <script>
        $(function () {
            $('#start_date_tp').datetimepicker({
                format: 'YYYY-MM-DD',
                locale: 'es'
            });
            $('#end_date_tp').datetimepicker({
                format: 'YYYY-MM-DD',
                locale: 'es'
            });
            $('#document').change(function() {
                let filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
                $('.custom-file-label').html(filename);
            });
        });

    </script>
@endpush
