@extends('layouts.app')


@section('title')
    @include('partials.genericJumbotron', [
        'title' => __("Contratos"),
        'icon' => "file-o"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if($canStore)
            <div class="col-12 mb-4" style="width: 100%">
                <a href="{{ route('contracts.create', $lease_id) }}" class="btn btn-homex-dark">Agregar contrato</a>
            </div>
            @endif
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">Contrato</th>
                    <th scope="col">Duración</th>
                    <th scope="col">Pago</th>
                    <th scope="col">Meses</th>
                    <th scope="col">Alquiler</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($contracts as $contract)
                    <tr>
                        <td>


                            @switch($contract->type)
                                @case(\App\Contract::LIVING_DESTINED)
                                <span>Arrendamiento de inmueble destinado a vivienda.</span>
                                @break
                                @case(\App\Contract::LIVING_DESTINED_WITH_BUYING_OPTION)
                                <span>Arrendamiento con opción de compra de inmueble destinado a vivienda.</span>
                                @break
                                @case(\App\Contract::LIVING_FINANCE_DESTINED)
                                <span>Arrendamiento-financiero de inmueble destinado a vivienda.</span>
                                @break
                                @default
                                <span>Otro.</span>
                            @endswitch
                            @if($contract->type == \App\Contract::ACTIVE)
                                <span>Estado activo.</span>
                            @elseif($contract->type == \App\Contract::INACTIVE)
                                <span>Estado inactivo.</span>
                            @endif
                            @if($contract->document)
                                <a href="{{ route('contracts.download', $contract->document) }}">
                                    Descargar contrato
                                </a>
                            @endif

                        </td>
                        <td>
                            {{ date('d/m/Y', strtotime($contract->start_date)) }}<br>
                            {{ date('d/m/Y', strtotime($contract->end_date)) }}
                        </td>
                        <td>{{ $contract->pay_date }} del mes</td>
                        <td>{{ $contract->months_date }}</td>
                        <td>S/{{ number_format($contract->amount, 2, '.', "'") }}</td>
                        <td>
                            <a
                                class="btn-homex btn-homex-clear m-0 mb-1"
                                href="{{ route('payments.listLease', $contract->id ) }}"
                            >Pagos</a>
                            @if($canStore)
                            <a
                                class="btn-homex btn-homex-clear m-0"
                                href="{{ route('contracts.edit', $contract->id ) }}"
                            >Editar</a>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay contratos disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center">
            {{ $contracts->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection

@push('scripts')
    <script>

        $(document).on('click', '.modificar', function(){


        });

    </script>
@endpush
