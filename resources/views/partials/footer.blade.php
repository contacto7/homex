<div class="row">
    <div class="col-md-12">
        <div class="card card-footer">
            <div
                class="row text-center d-flex align-items-center py-3 px-4 my-3"
            >
                <div class="col-sm-6 col-md-3">
                    <div class="footer-col-wrapp">
                        <div class="footer-titulo">
                            Homex
                        </div>
                        <div class="footer-subtitulo">
                            Contacto
                        </div>
                        <a class="footer-texto" href="mailto:contacto@homex.com" target="_blank">
                            contacto@homex.pe
                        </a>
                        <div class="footer-texto">
                            Lima-Perú
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="footer-col-wrapp">
                        <div class="footer-subtitulo">
                            Servicio al cliente
                        </div>
                        <a class="footer-texto" href="{{ route('aboutUs') }}">
                            {{ __("Nosotros") }}
                        </a><br>
                        <a class="footer-texto" href="{{ route('termsAndConditions') }}">
                            Términos y condiciones
                        </a><br>
                        <a class="footer-texto" href="{{ route('privacyPolicies') }}">
                            Política privacidad
                        </a><br>
                        <a class="footer-texto" href="{{ route('faq') }}">
                            Preguntas Frecuentes
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="footer-col-wrapp">
                        <div class="footer-subtitulo">
                            Búsquedas populares
                        </div>
                        <a class="footer-texto" href="{{ route('properties.list', ['types'=>1,  'districts'=>1]) }}">
                            Departamentos en Miraflores
                        </a><br>
                        <a class="footer-texto" href="{{ route('properties.list', ['types'=>1,  'districts'=>2]) }}">
                            Departamentos en Barranco
                        </a><br>
                        <a class="footer-texto" href="{{ route('properties.list', ['types'=>1,  'districts'=>13]) }}">
                            Departamentos en San Isidro
                        </a><br>
                        <a class="footer-texto" href="{{ route('properties.list', ['types'=>1,  'districts'=>3]) }}">
                            Departamentos en San Miguel
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="footer-col-wrapp">
                        <div class="footer-subtitulo">
                            Links relevantes
                        </div>
                        <a class="footer-texto" href="https://www.gob.pe/institucion/vivienda/noticias/52150-inician-las-inscripciones-para-bono-renta-joven" rel="nofollow">
                            Bono Renta Joven
                        </a><br>
                        <a class="footer-texto" href="https://www.gob.pe/vivienda" rel="nofollow">
                            Ministerio De Vivienda
                        </a><br>
                        <a class="footer-texto" href="https://www.mivivienda.com.pe/PortalWEB/" rel="nofollow">
                            Fondo MiVivienda
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@if(Cookie::get('hide') != 'helpMessage')

    <div class="help-message-wrapp text-center">
        <div class="help-message">
            <div class="help-close-wrapp close-help-message">
                <span class="fa fa-close"></span>
            </div>
            <div id="helpCarousel" class="carousel slide"  data-ride="carousel">
                <!-- Carousel items -->
                <div class="carousel-inner" id="my_text_slider">
                    <div class="carousel-item active">
                        <div class="help-message-intro">Hola, ¿te podemos ayudar?</div>
                        <button
                            class="btn btn-outline-info"
                            data-target="#helpCarousel"
                            data-slide-to="1"
                        >SI</button>
                        <button
                            class="btn btn-outline-info close-help-message"
                        >No</button>
                    </div>
                    <div class="carousel-item">
                        <div class="help-message-intro">Deseo:</div>
                        <a class="btn btn-outline-info" data-toggle="modal" data-target="#modalHelpPost" href="#modalHelpPost">Publicar</a>
                        <a class="btn btn-outline-info" data-toggle="modal" data-target="#modalHelpLease" href="#modalHelpLease">Arrendar</a>
                    </div>
                    <div class="carousel-item">think the Code</div>
                </div>
            </div>

        </div>
    </div>
    @include('partials.modals.modalHelpPost')
    @include('partials.modals.modalHelpLease')

@endif
