<li>
    <a
        class="nav-link btn-publicar"
        href="{{ route('properties.create') }}"
    >{{ __("Publicar inmueble")  }}</a>
</li>
<li class="nav-item dropdown">
    <a id="navbarDropdown"
       class="nav-link dropdown-toggle"
       href="#" role="button"
       data-toggle="dropdown"
       aria-haspopup="true"
       aria-expanded="false"
    >
        {{ auth()->user()->name }} <span class="caret"></span>
    </a>

    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('leases.listPropietary', auth()->user()->id ) }}">
            {{ __("Arrendamientos")  }}
        </a>
        <a class="dropdown-item" href="{{ route('applications.listPropietary', auth()->user()->id ) }}">
            {{ __("Postulaciones")  }}
        </a>
        <a class="dropdown-item" href="{{ route('properties.myList', auth()->user()->slug ) }}">
            {{ __("Mis viviendas")  }}
        </a>
        <a class="dropdown-item" href="{{ route('properties.userSearched', auth()->user()->id ) }}">
            {{ __("Búsquedas")  }}
        </a>
        <hr>
        <a class="dropdown-item" href="{{ route('users.profile', auth()->user()->slug) }}">
            {{ __("Mi perfil") }}
        </a>
        <a class="dropdown-item" href="{{ route('logout') }}"
           onclick="event.preventDefault();
           document.getElementById('logout-form').submit();"
        >
            {{ __("Cerrar sesión") }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</li>

