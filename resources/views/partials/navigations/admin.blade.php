<li class="nav-item dropdown">
    <a id="navbarDropdown"
       class="nav-link dropdown-toggle"
       href="#" role="button"
       data-toggle="dropdown"
       aria-haspopup="true"
       aria-expanded="false"
    >
        Administrar <span class="caret"></span>
    </a>

    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('users.list') }}">
            {{ __("Usuarios")  }}
        </a>
        <a class="dropdown-item" href="{{ route('leases.list') }}">
            {{ __("Arrendamientos")  }}
        </a>
        <a class="dropdown-item" href="{{ route('applications.list') }}">
            {{ __("Postulaciones")  }}
        </a>
        <a class="dropdown-item" href="{{ route('properties.admin') }}">
            {{ __("Viviendas")  }}
        </a>

    </div>
</li>
@include('partials.navigations.logged')
