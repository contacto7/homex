<div class="row profile-group">
    <div class="col-sm-12">
        <div class="profile-title">Estilo de vida:
            @can('editUser', [\App\User::class, $user] )
            <a class="edit-btn"
               data-toggle="modal"
               data-target="#modalEditLifestyle"
               href="#modalEditLifestyle">
                <span class="fa fa-pencil"></span> Editar
            </a>
            @endcan
        </div>
    </div>

    @if($user->lessee->lifestyle_type_id)
        <div class="col-sm-6">
            <div class="profile-field">{{ $user->lessee->lifestyleType->name }}</div>
        </div>
    @endif
    @if($user->lessee->emotional_situation)
    <div class="col-sm-6">
        <div class="profile-field">
            @if($user->lessee->emotional_situation == \App\Lessee::SINGLE)
                Solter@
            @elseif($user->lessee->emotional_situation == \App\Lessee::COUPLE)
                Con pareja
            @elseif($user->lessee->emotional_situation == \App\Lessee::FAMILY)
                Con familia
            @endif
        </div>
    </div>
    @endif
    @if($user->lessee->occupationType)
    <div class="col-sm-6">
        <div class="profile-field">
            {{ $user->lessee->occupationType->name }}
            @if($user->lessee->occupation_center)
                en {{ $user->lessee->occupation_center }}
            @endif
        </div>
    </div>
    @endif

    @if($user->lessee->monthly_salary)
    <div class="col-sm-6">
        <div class="profile-field">S/. {{ number_format($user->lessee->monthly_salary, 2, '.', "'") }}</div>
    </div>
    @endif

</div>

<div class="row profile-group">
    <div class="col-sm-12">
        <div class="profile-title">Vivienda:
            @can('editUser', [\App\User::class, $user] )
            <a class="edit-btn"
               data-toggle="modal"
               data-target="#modalEditAddress"
               href="#modalEditAddress">
                <span class="fa fa-pencil"></span> Editar
            </a>
            @endcan
        </div>
    </div>

    @if($user->lessee->address)
        <div class="col-sm-6">
            <div class="profile-field">{{ $user->lessee->address }}</div>
        </div>
    @endif
    @if($user->lessee->district_id)
        <div class="col-sm-6">
            <div class="profile-field">{{ $user->lessee->district->name }}</div>
        </div>
    @endif

</div>

<div class="row profile-group">
    <div class="col-sm-12">
        <div class="profile-title">Bono renta joven:
            @can('editUser', [\App\User::class, $user] )
            <a class="edit-btn"
               data-toggle="modal"
               data-target="#modalEditBond"
               href="#modalEditBond">
                <span class="fa fa-pencil"></span> Editar
            </a>
            @endcan
        </div>
    </div>

    @if($user->lessee->bond_interested_status)
        <div class="col-sm-6">
            <div class="profile-field">
                @if($user->lessee->bond_interested_status == \App\Lessee::BOND_INTERESTED)
                    Interesado en bono renta joven
                @elseif($user->lessee->bond_interested_status == \App\Lessee::BOND_NOT_INTERESTED)
                    No interesado en bono renta joven
                @endif
            </div>
        </div>
    @endif

    @if($user->lessee->quantity_members)
        <div class="col-sm-6">
            <div class="profile-field">Viviremos {{ $user->lessee->quantity_members }} personas</div>
        </div>
    @endif

    @if($user->lessee->dependent_status)
        <div class="col-sm-6">
            <div class="profile-field">
                @if($user->lessee->dependent_status == \App\Lessee::INDEPENDENT)
                    Independiente
                @elseif($user->lessee->dependent_status == \App\Lessee::DEPENDENT)
                    Dependiente
                @endif
            </div>
        </div>
    @endif

    @if($user->lessee->debtor_status)
        <div class="col-sm-6">
            <div class="profile-field">
                @if($user->lessee->dependent_status == \App\Lessee::NOT_DEBTOR)
                    No deudor judicial moroso
                @elseif($user->lessee->dependent_status == \App\Lessee::DEBTOR)
                    Deudor judicial moroso
                @endif
            </div>
        </div>
    @endif

    @if($user->lessee->other_postulation)
        <div class="col-sm-6">
            <div class="profile-field">
                @if($user->lessee->other_postulation == \App\Lessee::NOT_OTHER_POSTULATION)
                    No postulé a otro programa de ayuda para viviendas
                @elseif($user->lessee->other_postulation == \App\Lessee::OTHER_POSTULATION)
                    Postulé a otro programa de ayuda para viviendas
                @endif
            </div>
        </div>
    @endif

</div>
