<div class="row profile-group">
    <div class="col-sm-12">
        <div class="profile-title">Déjanos una nota que nos pueda ayudar a servirte mejor:
            @can('editUser', [\App\User::class, $user] )
            <a class="edit-btn"
               data-toggle="modal"
               data-target="#modalEditBiography"
               href="#modalEditBiography">
                <span class="fa fa-pencil"></span> Editar
            </a>
            @endcan
        </div>
    </div>

    @if($user->propietary->biography)
        <div class="col-sm-6">
            <div class="profile-field">{!! $user->propietary->biography !!}</div>
        </div>
    @endif
</div>

