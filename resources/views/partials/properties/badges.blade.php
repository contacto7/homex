<div class="card-property-badges">
    @if( $property->young_bonus == \App\Property::YB_QUALIFIED )
    <div class="card-property-badge">
        <img
            src="{{ asset('image/iconos/bono-renta-joven.png') }}"
            class="card-property-badge-image"
            alt="Bono renta joven"
            data-toggle="tooltip"
            data-placement="top"
            title="Bono renta joven"
        >
    </div>
    @endif
    @if( $property->only_students == \App\Property::STUDENTS_ONLY )
    <div class="card-property-badge">
        <img
            src="{{ asset('image/iconos/para-estudiantes.png') }}"
            class="card-property-badge-image"
            alt="para estudiantes"
            data-toggle="tooltip"
            data-placement="top"
            title="Para estudiantes"
        >
    </div>
    @endif
    @if( $property->pet_allowed == \App\Property::PET_ALLOWED )
    <div class="card-property-badge">
        <img
            src="{{ asset('image/iconos/acepta-mascotas.png') }}"
            class="card-property-badge-image"
            alt="acepta mascotas"
            data-toggle="tooltip"
            data-placement="top"
            title="Acepta mascotas"
        >
    </div>
    @endif

</div>
