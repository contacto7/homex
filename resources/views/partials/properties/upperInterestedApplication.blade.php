@auth

    @can('opt_for_properties', \App\Property::class)

        @can('apply', $property)
            <a
                class="btn-homex-primary jumbotron-property-btn"
                href="{{ route('properties.application', ['property' =>$property->id ]) }}"
            >Me interesa</a>
        @else
            <a
                class="btn-homex-primary jumbotron-property-btn"
                href="{{ route('applications.status', $property->applications->where('user_id',auth()->user()->id)->last()->id ) }}"
            >Ver postulación</a>
        @endcan

    @else

        @can('editProperty', $property)
            <a
                class="btn-homex-primary jumbotron-property-btn"
                href="{{ route('properties.edit', ['property'=>$property->id ]) }}"
            >Editar</a>
            <a
                class="btn-homex-primary jumbotron-property-btn"
                href="{{ route('properties.createSimilar', ['property'=>$property->id ] ) }}"
            >Publicar similar</a>
        @else
            <a
                class="btn-homex-primary jumbotron-property-btn"
                href="{{ route('properties.myList', ['user'=>auth()->user()->slug ] ) }}"
            >Mis viviendas</a>
        @endcan

    @endcan

@else
    <a
        class="btn-homex-primary jumbotron-property-btn"
        data-toggle="modal"
        data-target="#modalIngresar"
        href="#modalIngresar"
    >Me interesa</a>
@endauth
