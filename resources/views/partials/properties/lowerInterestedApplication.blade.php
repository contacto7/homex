@auth

    @can('opt_for_properties', \App\Property::class)

        @can('apply', $property)
            <a
                class="btn-homex btn-homex-clear"
                href="{{ route('properties.application', ['property' =>$property->id ]) }}"
            >ME INTERESA</a>
        @else

            <a
                class="btn-homex btn-homex-clear"
                href="{{ route('applications.status', $property->applications->where('user_id',auth()->user()->id)->last()->id ) }}"
            >VER POSTULACION</a>

        @endcan

    @else

        @can('editProperty', $property)
            <a
                class="btn-homex btn-homex-clear"
                href="{{ route('properties.edit', ['property'=>$property->id ]) }}"
            >EDITAR</a>
            <a
                class="btn-homex btn-homex-clear"
                href="{{ route('properties.createSimilar', ['property'=>$property->id ] ) }}"
            >Publicar similar</a>
        @else
            <a
                class="btn-homex btn-homex-clear"
                href="{{ route('properties.myList', ['user'=>auth()->user()->slug ] ) }}"
            >Mis viviendas</a>
        @endcan

    @endcan

@else
    <a
        class="btn-homex btn-homex-clear"
        data-toggle="modal"
        data-target="#modalIngresar"
        href="#modalIngresar"
    >ME INTERESA</a>
@endauth


