<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="filtros-bread-wrapp mt-3 mb-5">
                <span class="pr-4">Filtros aplicados:</span>
                @foreach($types_arr ?: []  as $type)
                    @php
                    $type_temp_arr = array_diff( (array) $types_arr, (array) $type );
                    @endphp
                    <a
                        class="filtro-bread filtro-array"
                        href="{{ route('properties.list',
                        [
                        'types'=>implode(',',$type_temp_arr),
                        'districts'=>implode(',',$districts_arr),
                        'bono_renta_joven'=>$young_bonus,
                        'solo_estudiantes'=>$only_students,
                        'mascotas_permitidas'=>$pet_allowed,
                        'genero'=>$only_genre,
                        'precio_minimo'=>$min_amount,
                        'precio_maximo'=>$max_amount,
                        'habitaciones'=>$rooms,
                        ]

                        ) }}"
                        data-id="{{ $type }}"
                        data-type="1"
                        id="filtro-1-{{ $type }}"
                    ></a>
                @endforeach
                @foreach($districts_arr ?: [] as $district)
                    @php
                        $district_temp_arr = array_diff( (array) $districts_arr, (array) $district );
                    @endphp
                    <a
                        class="filtro-bread filtro-array"
                        href="{{ route('properties.list',
                        [
                        'types'=>implode(',',$types_arr),
                        'districts'=>implode(',',$district_temp_arr),
                        'bono_renta_joven'=>$young_bonus,
                        'solo_estudiantes'=>$only_students,
                        'mascotas_permitidas'=>$pet_allowed,
                        'genero'=>$only_genre,
                        'precio_minimo'=>$min_amount,
                        'precio_maximo'=>$max_amount,
                        'habitaciones'=>$rooms,
                        ]

                        ) }}"
                        data-id="{{ $district }}"
                        data-type="2"
                        id="filtro-2-{{ $district }}"
                    ></a>
                @endforeach
                @if($young_bonus)
                    <a
                        class="filtro-bread"
                        href="{{ route('properties.list',
                        [
                        'types'=>implode(',',$types_arr),
                        'districts'=>implode(',',$districts_arr),
                        'solo_estudiantes'=>$only_students,
                        'mascotas_permitidas'=>$pet_allowed,
                        'genero'=>$only_genre,
                        'precio_minimo'=>$min_amount,
                        'precio_maximo'=>$max_amount,
                        'habitaciones'=>$rooms,
                        ]

                        ) }}"
                    >Bono Renta Joven X</a>
                @endif
                @if($only_students)
                    <a
                        class="filtro-bread"
                        href="{{ route('properties.list',
                        [
                        'types'=>implode(',',$types_arr),
                        'districts'=>implode(',',$districts_arr),
                        'bono_renta_joven'=>$young_bonus,
                        'mascotas_permitidas'=>$pet_allowed,
                        'genero'=>$only_genre,
                        'precio_minimo'=>$min_amount,
                        'precio_maximo'=>$max_amount,
                        'habitaciones'=>$rooms,
                        ]

                        ) }}"
                    >Sólo estudiantes X</a>
                @endif
                @if($pet_allowed)
                    <a
                        class="filtro-bread"
                        href="{{ route('properties.list',
                        [
                        'types'=>implode(',',$types_arr),
                        'districts'=>implode(',',$districts_arr),
                        'bono_renta_joven'=>$young_bonus,
                        'solo_estudiantes'=>$only_students,
                        'genero'=>$only_genre,
                        'precio_minimo'=>$min_amount,
                        'precio_maximo'=>$max_amount,
                        'habitaciones'=>$rooms,
                        ]

                        ) }}"
                    >Mascotas permitidas X</a>
                @endif
                @if($only_genre)
                    <a
                        class="filtro-bread"
                        href="{{ route('properties.list',
                        [
                        'types'=>implode(',',$types_arr),
                        'districts'=>implode(',',$districts_arr),
                        'bono_renta_joven'=>$young_bonus,
                        'solo_estudiantes'=>$only_students,
                        'mascotas_permitidas'=>$pet_allowed,
                        'precio_minimo'=>$min_amount,
                        'precio_maximo'=>$max_amount,
                        'habitaciones'=>$rooms,
                        ]

                        ) }}"
                    >Para
                        @if($only_genre == \App\Property::WOMEN_ONLY)
                            Mujeres
                        @elseif($only_genre == \App\Property::MAN_ONLY)
                            Hombres
                        @endif
                        X</a>
                @endif
                @if($min_amount)
                    <a
                        class="filtro-bread"
                        href="{{ route('properties.list',
                        [
                        'types'=>implode(',',$types_arr),
                        'districts'=>implode(',',$districts_arr),
                        'bono_renta_joven'=>$young_bonus,
                        'solo_estudiantes'=>$only_students,
                        'mascotas_permitidas'=>$pet_allowed,
                        'genero'=>$only_genre,
                        'precio_maximo'=>$max_amount,
                        'habitaciones'=>$rooms,
                        ]

                        ) }}"
                    >&gt; S/. {{ $min_amount }} X</a>
                @endif
                @if($max_amount)
                    <a
                        class="filtro-bread"
                        href="{{ route('properties.list',
                        [
                        'types'=>implode(',',$types_arr),
                        'districts'=>implode(',',$districts_arr),
                        'bono_renta_joven'=>$young_bonus,
                        'solo_estudiantes'=>$only_students,
                        'mascotas_permitidas'=>$pet_allowed,
                        'genero'=>$only_genre,
                        'precio_minimo'=>$min_amount,
                        'habitaciones'=>$rooms,
                        ]

                        ) }}"
                    >&lt; S/. {{ $max_amount }} X</a>
                @endif
                @if($rooms)
                    <a
                        class="filtro-bread"
                        href="{{ route('properties.list',
                        [
                        'types'=>implode(',',$types_arr),
                        'districts'=>implode(',',$districts_arr),
                        'bono_renta_joven'=>$young_bonus,
                        'solo_estudiantes'=>$only_students,
                        'mascotas_permitidas'=>$pet_allowed,
                        'genero'=>$only_genre,
                        'precio_minimo'=>$min_amount,
                        'precio_maximo'=>$max_amount,
                        ]

                        ) }}"
                    >{{ $rooms }} Habitaciones X</a>
                @endif

                <a class="btn-homex-clear filtro-adicional"
                   data-toggle="modal"
                   data-target="#modalFiltroBusqueda"
                   href="#modalFiltroBusqueda"
                >Agregar Filtros</a>

            </div>

        </div>
    </div>
</div>
