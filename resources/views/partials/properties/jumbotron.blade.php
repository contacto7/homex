<div class="jumbotron-propiedad">
    <div class="container">
        <div class="col-md-12">
            <div class="row text-white text-center d-flex my-2">
                <div class="col-sm-12 col-md-7 col-lg-6 py-4 ">
                    <div class="jumbotron-property-img-wrapp">

                        @include('partials.properties.carousel')

                    </div>
                </div>
                <div class="col-sm-12 col-md-5 col-lg-6 text-left jumbotron-property-description px-md-3 px-lg-5 py-4 my-auto">
                    <div class="jumbotron-property-name pb-1">{{ $property->name }}</div>
                    <div class="jumbotron-property-address pb-1"><span class="fa fa-map-marker"></span> {{ $property->address }}</div>
                    <div class="jumbotron-property-area pb-1"><span class="fa fa-stop"></span> Área total {{ $property->total_area }} m<sup>2</sup></div>
                    <div class="jumbotron-property-rooms pb-1"><span class="fa fa-home"></span> {{ $property->rooms }} habitaciones</div>
                    <div class="jumbotron-property-price pb-2"><span class="fa fa-heart"></span> Renta mensual</div>
                    <div class="jumbotron-property-amount pb-2">{{ $property->currencyRentalAmount->symbol }}{{ number_format($property->rental_amount, 2, '.', "'") }} x mes</div>
                    <div class="text-center">
                        @include('partials.properties.upperInterestedApplication')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
