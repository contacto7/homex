<a
    class="card card-properties"
    href="{{ route('properties.view', $property->id) }}"
>
    <img
        class="card-img-top"
        @if($property->principalImage())
            src="{{ $property->principalImage()->pathAttachment() }}"
        @else
            src="{{ asset('image/iconos/property.jpg') }}"
        @endif
        alt="{{ $property->name }}"
    />
    <div class="card-body">
        <b class="card-title">
            {{ Str::limit($property->name, 40, '...') }}</b>
        <div class="row justify-content-center">
            {{-- Añadir parcial para mostrar los badges --}}
            @include('partials.properties.badges')
        </div>
        <div class="card-text">
            <div class="card-property-price">{{ $property->currencyRentalAmount->symbol }}{{ number_format($property->rental_amount, 2, '.', "'") }} x mes</div>
            <div class="card-property-location">{{ $property->district->name }},
                {{ $property->district->province->name }}</div>
            <div class="card-property-area">Área total {{ $property->total_area }} m<sup>2</sup></div>
        </div>
    </div>
</a>

@can('editProperty', [\App\Property::class, $property])
    <a
        class="card-property-edit"
        href="{{ route('properties.edit', $property->id) }}"
        data-toggle="tooltip"
        data-placement="top"
        title="Editar propiedad"

    >
        <span class="fa fa-pencil"></span>
    </a>
    <a
        class="card-property-edit card-property-add"
        href="{{ route('properties.createSimilar', ['property'=>$property->id ]) }}"
        data-toggle="tooltip"
        data-placement="top"
        title="Publicar similar"
    >
        <span class="fa fa-plus"></span>
    </a>
@endcan
