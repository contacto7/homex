<div class="pt-0 mt-4 mb-4">
    <h2 class="property-detail-title-related mt-2">
        {{ __("Inmuebles relacionados") }}
    </h2>
</div>
<div class="container-fluid">
    <div class="row">
        @forelse($related as $relatedProperty)
            <div class="col-md-6 listing-block">
                <div class="media media-related-card">
                    <div class="fav-box">

                        <div class="">
                            @if( $property->young_bonus == \App\Property::YB_QUALIFIED )
                                <div class="card-property-badge">
                                    <img
                                        src="{{ asset('image/iconos/bono-renta-joven.png') }}"
                                        class="card-property-badge-image"
                                        alt="Bono renta joven"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="Bono renta joven"
                                    >
                                </div>
                            @endif
                            @if( $property->only_students == \App\Property::STUDENTS_ONLY )
                                <div class="card-property-badge">
                                    <img
                                        src="{{ asset('image/iconos/para-estudiantes.png') }}"
                                        class="card-property-badge-image"
                                        alt="para estudiantes"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="Para estudiantes"
                                    >
                                </div>
                            @endif
                            @if( $property->pet_allowed == \App\Property::PET_ALLOWED )
                                <div class="card-property-badge">
                                    <img
                                        src="{{ asset('image/iconos/acepta-mascotas.png') }}"
                                        class="card-property-badge-image"
                                        alt="acepta mascotas"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="Acepta mascotas"
                                    >
                                </div>
                            @endif
                        </div>


                    </div>
                    <a
                        href="{{ route('properties.view', $relatedProperty->id) }}"
                    >
                        <img
                            class="d-flex align-self-lg-start media-related-img"
                            src="{{ $relatedProperty->principalImage()->pathAttachment() }}"
                            alt="{{ $relatedProperty->name }}"
                        >
                    </a>
                    <div class="media-body">
                        <div class="price">
                            <small>{{ $relatedProperty->name }}</small>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="alert alert-dark">
                {{ __("No hay propiedades relacionadas") }}
            </div>
        @endforelse
    </div>
</div>
