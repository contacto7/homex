<div class="row">
    <div class="col-md-12">
        <div class="card card-jumbotron" style="background-image: url('{{ asset('image/banner-inicio.jpg') }}')">
            <form
                class="row text-white text-center d-flex align-items-center py-5 px-4 my-5"
                method="get"
                action="{{ route('properties.list') }}"
            >
                <div class="col-12">
                    <div class="text-left">
                        <div class="form-group">
                            <h3 for="" class="font-weight-bold pl-3">Deseo buscar:</h3>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="text-left">
                        <div class="form-group">
                            <label for="types" class="font-weight-bold pl-3">Inmueble:</label>
                            <select
                                class="form-control {{ $errors->has('types') ? 'is-invalid': '' }}"
                                name="types"
                                id="types"
                            ></select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="text-left">
                        <div class="form-group">
                            <label for="input-district-search" class="font-weight-bold pl-3">Distrito:</label>
                            <input
                                class="form-control"
                                style="color: #69bde2;text-transform: uppercase;font-weight: bold;"
                                id="input-district-search"
                                data-search="districts"
                                data-id="0"
                                data-status="0"
                                type="text"
                                placeholder="Ingrese el distrito..."
                            >
                            <input
                                class="form-control"
                                id="districts"
                                name="districts"
                                type="hidden"
                            >
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="btn-buscar-jumbotron-wrapp mt-3">
                        <button
                            type="submit"
                            class="btn-homex btn-homex-dark btn-result m-0"
                        >
                            Buscar
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<script>
    function changeId($obj) {
        let district_id = $obj.getSelectedItemData().id;
        let district_name = $obj.getSelectedItemData().name;

        $("#districts").val(district_id);

        console.log("Id: "+ district_id);
        console.log("Nombre: "+ district_name);
    }

    var district_options = [
        @foreach(\App\District::with('province')->get() as $district)
        {id: "{{ $district->id }}",name:"{{ $district->name.", ".$district->province->name }}"},
        @endforeach
    ];
    var options = {
        data: district_options,
        getValue: "name",
        list: {
            match:{
                enabled:true
            },
            onSelectItemEvent: function() {
                $obj = $("#input-district-search");

                changeId($obj);
            }
        }
    };

    $("#input-district-search").easyAutocomplete(options);

</script>
