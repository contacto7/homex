<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">

            <div class="nav-buscar-home-wrapp justify-content-center d-flex flex-wrap">
                <div class="nav-buscar-home-inn align-self-center">

                    <div class="title m-b-sm">
                        HOMEX
                    </div>

                    <div class="mensaje-subtitulo-wrapp">
                        <div class="mensaje-subtitulo">
                            Hazlo fácil, alquila con Homex
                        </div>
                    </div>
                    <div
                        class="btn-homex btn-homex-primary mt-5"
                        data-target="#carouselExampleIndicators"
                        data-slide-to="1"
                    >
                        Buscar
                    </div>

                </div>

            </div>

        </div>
        <div class="carousel-item">

            <div class="nav-buscar-home-wrapp justify-content-center d-flex flex-wrap">
                <div class="nav-buscar-home-inn align-self-center">


                    <div class="mensaje-subtitulo-wrapp">
                        <div class="mensaje-subtitulo">
                            <span class="btn-anterior-wrapp">
                                <sup
                                    class="btn"
                                    data-target="#carouselExampleIndicators"
                                    data-slide-to="0"
                                >
                                    <span class="fa fa-long-arrow-left"></span>
                                </sup>
                            </span>
                            DESEO ALQUILAR:
                        </div>
                    </div>
                    <div
                        class="btn-homex btn-homex-primary btn-inicio-buscar-med btn-select"
                        data-search="property_type_id"
                        data-id="1"
                        data-status="0"
                    >
                        Departamento
                    </div>
                    <div
                        class="btn-homex btn-homex-primary btn-inicio-buscar-med btn-select"
                        data-search="property_type_id"
                        data-id="2"
                        data-status="0"
                    >
                        Casa
                    </div>
                    <div
                        class="btn-homex btn-homex-primary btn-inicio-buscar-med btn-select"
                        data-search="property_type_id"
                        data-id="5"
                        data-status="0"
                    >
                        Oficina
                    </div>
                    <div
                        class="btn-homex btn-homex-primary btn-inicio-buscar-med btn-select"
                        data-search="property_type_id"
                        data-id="3"
                        data-status="0"
                    >
                        Dúplex
                    </div>
                    <div
                        class="btn-homex btn-homex-primary btn-inicio-buscar-med btn-select"
                        data-search="property_type_id"
                        data-id="4"
                        data-status="0"
                    >
                        Tríplex
                    </div>


                    <div class="btn-siguiente-wrapp mt-5">
                        <div
                            class="btn-homex btn-homex-dark"
                            data-target="#carouselExampleIndicators"
                            data-slide-to="2"
                        >
                            Siguiente
                        </div>
                    </div>


                </div>

            </div>

        </div>
        <div class="carousel-item">

            <div class="nav-buscar-home-wrapp justify-content-center d-flex flex-wrap">
                <div class="nav-buscar-home-inn align-self-center">

                    <div class="mensaje-subtitulo-wrapp">
                        <div class="mensaje-subtitulo">

                            <span class="btn-anterior-wrapp">
                                <sup
                                    class="btn"
                                    data-target="#carouselExampleIndicators"
                                    data-slide-to="1"
                                >
                                    <span class="fa fa-long-arrow-left"></span>
                                </sup>
                            </span>
                            DESEO VIVIR EN:
                        </div>
                    </div>
                    <div
                        class="btn-homex btn-homex-primary btn-inicio-buscar-med btn-select"
                        data-search="district_id"
                        data-id="1302"
                        data-status="0"
                    >
                        Miraflores
                    </div>
                    <div
                        class="btn-homex btn-homex-primary btn-inicio-buscar-med btn-select"
                        data-search="district_id"
                        data-id="1284"
                        data-status="0"
                    >
                        Barranco
                    </div>
                    <div
                        class="btn-homex btn-homex-primary btn-inicio-buscar-med btn-select"
                        data-search="district_id"
                        data-id="1311"
                        data-status="0"
                    >
                        San Isidro
                    </div>
                    <div
                        class="btn-homex btn-homex-primary btn-inicio-buscar-med btn-select"
                        data-search="district_id"
                        data-id="1296"
                        data-status="0"
                    >
                        Lince
                    </div>
                    <div
                        class="btn-homex btn-homex-primary btn-inicio-buscar-med btn-select"
                        data-search="district_id"
                        data-id="1316"
                        data-status="0"
                    >
                        San Miguel
                    </div>
                    <div
                        class="btn-homex btn-homex-primary btn-inicio-buscar-med btn-select"
                        data-search="district_id"
                        data-id="1301"
                        data-status="0"
                    >
                        Pueblo Libre
                    </div>
                    <input
                        class="btn-homex btn-homex-primary btn-inicio-buscar-med"
                        id="input-district-search"
                        data-search="district_id"
                        data-id="0"
                        data-status="0"
                        type="text"
                        placeholder="Ingrese el distrito..."
                    >

                    <div class="btn-siguiente-wrapp mt-5">
                        <div
                            class="btn-homex btn-homex-dark btn-result"
                        >
                            Ver Resultados
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>
