<div class="modal" id="modalEditBond">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Estilo de vida</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body modal-ajax-content">

                <form method="POST" action="{{ route('lessees.update', $user->lessee->id) }}">

                    @method('PUT')

                    @csrf

                    <div class="form-group row" style="display: none">
                        <label for="bond" class="col-md-4 col-form-label text-md-right">Bono</label>

                        <div class="col-md-6">
                            <input id="bond" type="text" class="form-control" name="bond" value="bond" required>
                            @error('bond')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="quantity_members" class="col-md-4 col-form-label text-md-right">{{ __('Número de familiares que habitarán') }}</label>

                        <div class="col-md-6">
                            <input id="quantity_members" type="number"  inputmode="numeric" pattern="[0-9]*"  class="form-control " name="quantity_members" value="{{ old('quantity_members')?:$user->lessee->quantity_members }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="bond_interested_status" class="col-md-4 col-form-label text-md-right">¿Interesado en el bono renta joven?</label>
                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control"
                                name="bond_interested_status"
                                id="bond_interested_status"
                            >
                                <option
                                    {{ (int) old('bond_interested_status') == ""  ? 'selected' : '' }}
                                    value=""
                                >{{ __("Seleccionar") }}</option>
                                <option
                                    {{ (int) old('bond_interested_status') === \App\Lessee::BOND_INTERESTED || $user->lessee->bond_interested_status == \App\Lessee::BOND_INTERESTED ? 'selected' : '' }}
                                    value="{{ \App\Lessee::BOND_INTERESTED }}"
                                >{{ __("Si") }}</option>
                                <option
                                    {{ (int) old('bond_interested_status') === \App\Lessee::BOND_NOT_INTERESTED || $user->lessee->bond_interested_status == \App\Lessee::BOND_NOT_INTERESTED ? 'selected' : '' }}
                                    value="{{ \App\Lessee::BOND_NOT_INTERESTED }}"
                                >{{ __("No") }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="dependent_status" class="col-md-4 col-form-label text-md-right">¿Eres independiente?</label>
                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control"
                                name="dependent_status"
                                id="dependent_status"
                            >
                                <option
                                    {{ (int) old('dependent_status') == ""  ? 'selected' : '' }}
                                    value=""
                                >{{ __("Seleccionar") }}</option>
                                <option
                                    {{ (int) old('dependent_status') === \App\Lessee::INDEPENDENT || $user->lessee->dependent_status == \App\Lessee::INDEPENDENT ? 'selected' : '' }}
                                    value="{{ \App\Lessee::INDEPENDENT }}"
                                >{{ __("Si") }}</option>
                                <option
                                    {{ (int) old('dependent_status') === \App\Lessee::DEPENDENT || $user->lessee->dependent_status == \App\Lessee::DEPENDENT ? 'selected' : '' }}
                                    value="{{ \App\Lessee::DEPENDENT }}"
                                >{{ __("No") }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="debtor_status" class="col-md-4 col-form-label text-md-right">¿Estás en el registro de deudores morosos?</label>

                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control"
                                name="debtor_status"
                                id="debtor_status"
                            >
                                <option
                                    {{ (int) old('debtor_status') == ""  ? 'selected' : '' }}
                                    value=""
                                >{{ __("Seleccionar") }}</option>
                                <option
                                    {{ (int) old('debtor_status') === \App\Lessee::NOT_DEBTOR || $user->lessee->debtor_status == \App\Lessee::NOT_DEBTOR  ? 'selected' : '' }}
                                    value="{{ \App\Lessee::NOT_DEBTOR }}"
                                >{{ __("No") }}</option>
                                <option
                                    {{ (int) old('debtor_status') === \App\Lessee::DEBTOR || $user->lessee->debtor_status == \App\Lessee::DEBTOR  ? 'selected' : '' }}
                                    value="{{ \App\Lessee::DEBTOR }}"
                                >{{ __("Si") }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="other_postulation" class="col-md-4 col-form-label text-md-right">¿Has postulado a otro programa de ayuda para vivienda?</label>
                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control"
                                name="other_postulation"
                                id="other_postulation"
                            >
                                <option
                                    {{ (int) old('other_postulation') == ""  ? 'selected' : '' }}
                                    value=""
                                >{{ __("Seleccionar") }}</option>
                                <option
                                    {{ (int) old('other_postulation') === \App\Lessee::NOT_OTHER_POSTULATION || $user->lessee->other_postulation == \App\Lessee::NOT_OTHER_POSTULATION ? 'selected' : '' }}
                                    value="{{ \App\Lessee::NOT_OTHER_POSTULATION }}"
                                >{{ __("No") }}</option>
                                <option
                                    {{ (int) old('other_postulation') === \App\Lessee::OTHER_POSTULATION || $user->lessee->other_postulation == \App\Lessee::OTHER_POSTULATION ? 'selected' : '' }}
                                    value="{{ \App\Lessee::OTHER_POSTULATION }}"
                                >{{ __("Si") }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-12 text-center">
                            <button type="submit" class="btn-homex btn-homex-clear" style="width: auto">
                                {{ __('Actualizar') }}
                            </button>
                        </div>
                    </div>
                </form>


            </div>

        </div>
    </div>
</div>
