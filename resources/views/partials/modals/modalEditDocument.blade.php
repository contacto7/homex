<div class="modal" id="modalEditDocument">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Documento de identidad</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body modal-ajax-content">

                <form method="POST" action="{{ route('users.update', $user->id) }}">

                    @method('PUT')

                    @csrf

                    <div class="form-group row" style="display: none">
                        <label for="document" class="col-md-4 col-form-label text-md-right">Document</label>

                        <div class="col-md-6">
                            <input id="document" type="text" class="form-control" name="document" value="document" required>
                            @error('document')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="document_type_id" class="col-md-4 col-form-label text-md-right">{{ __('Tipo de documento') }}</label>

                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control"
                                name="document_type_id"
                                id="document_type_id"
                            >
                                @foreach($documentTypes as $documentType)
                                    <option
                                        {{ (int) old('document_type_id') === $documentType->id || $user->document_type_id == $documentType->id ? 'selected' : '' }}
                                        value="{{ $documentType->id }}"
                                    >{{ $documentType->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="document_number" class="col-md-4 col-form-label text-md-right">Número de documento</label>

                        <div class="col-md-6">
                            <input id="document_number" type="text" class="form-control" name="document_number" value="{{ old('document_number')?:$user->document_number }}">
                        </div>
                    </div>


                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-12 text-center">
                            <button type="submit" class="btn-homex btn-homex-clear" style="width: auto">
                                {{ __('Actualizar') }}
                            </button>
                        </div>
                    </div>
                </form>


            </div>

        </div>
    </div>
</div>
