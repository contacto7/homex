<div class="modal" id="modalEditPicture">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Documento de identidad</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body modal-ajax-content">

                <form method="POST" action="{{ route('users.update', $user->id) }}" enctype="multipart/form-data">

                    @method('PUT')

                    @csrf

                    <div class="form-group">
                        <div class="col-md-12">
                            <input
                                type="file"
                                class="custom-file-input form-control {{ $errors->has('picture') ? 'is-invalid': '' }}"
                                name="picture"
                                id="picture"
                                placeholder=""
                                value="{{ old('picture') ?: $user->picture }}"
                            >
                            <label  class="custom-file-label" for="picture" data-browse="Elegir">
                                Seleccionar foto
                            </label>
                            <div  class="profile-filename"></div>
                            @if($errors->has('picture'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('picture') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-12 text-center">
                            <button type="submit" class="btn-homex btn-homex-clear" style="width: auto">
                                {{ __('Actualizar') }}
                            </button>
                        </div>
                    </div>
                </form>


            </div>

        </div>
    </div>
</div>
