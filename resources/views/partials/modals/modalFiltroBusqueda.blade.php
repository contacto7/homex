<div class="modal" id="modalFiltroBusqueda">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Agregar filtros</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body modal-ajax-content">

                <div class="form">

                    <div id="accordion" class="accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn-homex btn-homex-primary" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Tipo de inmueble:
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="p-2 text-center" id="checkbox-type-wrapp"></div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn-homex btn-homex-primary collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Distritos
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="p-2 text-center" id="distritos-input-wrapp">

                                    <div class="form-group row">
                                        <div class="col-12">
                                            <input type="text" inputmode="numeric" class="form-control" name="amount-min" id="amount-min">
                                        </div>
                                    </div>

                                </div>
                                <div class="p-2 text-center" id="checkbox-district-wrapp"></div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn-homex btn-homex-primary collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Perfil del arrendatario:
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">

                                <div class="p-2">
                                    <div class="form-group row">
                                        <label for="amount-min" class="col-md-4 col-form-label text-md-right">Precio min.:</label>
                                        <div class="col-md-6">
                                            <input type="number"  inputmode="numeric" pattern="[0-9]*"  class="form-control" name="amount-min" id="amount-min">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="amount-max" class="col-md-4 col-form-label text-md-right">Precio max.:</label>
                                        <div class="col-md-6">
                                            <input type="number"  inputmode="numeric" pattern="[0-9]*"  class="form-control" name="amount-max" id="amount-max">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="rooms" class="col-md-4 col-form-label text-md-right">Dormitorios:</label>
                                        <div class="col-md-6">
                                            <input type="number"  inputmode="numeric" pattern="[0-9]*"  class="form-control" name="rooms" id="rooms">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="" class="col-md-4 col-form-label text-md-right">Extras:</label>
                                        <div class="col-md-6">
                                            <div class="form-check form-check">
                                                <input
                                                    class="form-check-input chkbx-input"
                                                    type="checkbox"
                                                    data-search="young_bonus"
                                                    data-id="{{ \App\Property::YB_QUALIFIED }}"
                                                    id="young_bonus">
                                                <label
                                                    class="form-check-label checkbox-filter-label"
                                                    for="young_bonus"
                                                >Bono renta joven </label>
                                            </div>
                                            <div class="form-check form-check">
                                                <input
                                                    class="form-check-input chkbx-input"
                                                    type="checkbox"
                                                    data-search="only_students"
                                                    data-id="{{ \App\Property::STUDENTS_ONLY }}"
                                                    id="only_students">
                                                <label
                                                    class="form-check-label checkbox-filter-label"
                                                    for="only_students"
                                                >Para estudiantes </label>
                                            </div>
                                            <div class="form-check form-check">
                                                <input
                                                    class="form-check-input chkbx-input"
                                                    type="checkbox"
                                                    data-search="pet_allowed"
                                                    data-id="{{ \App\Property::PET_ALLOWED }}"
                                                    id="pet_allowed">
                                                <label
                                                    class="form-check-label checkbox-filter-label"
                                                    for="pet_allowed"
                                                >Se permiten mascotas </label>
                                            </div>
                                            <div class="form-check form-check">
                                                <input
                                                    class="form-check-input chkbx-input"
                                                    type="checkbox"
                                                    data-search="only_genre"
                                                    data-id="{{ \App\Property::WOMEN_ONLY }}"
                                                    id="only_genre">
                                                <label
                                                    class="form-check-label checkbox-filter-label"
                                                    for="only_genre"
                                                >Para mujeres </label>
                                            </div>
                                            <div class="form-check form-check">
                                                <input
                                                    class="form-check-input chkbx-input"
                                                    type="checkbox"
                                                    data-search="only_genre"
                                                    data-id="{{ \App\Property::MAN_ONLY }}"
                                                    id="only_genre_h">
                                                <label
                                                    class="form-check-label checkbox-filter-label"
                                                    for="only_genre_h"
                                                >Para hombres </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn-homex btn-homex-clear btn-filtrar-adicionales" style="width: auto">
                                {{ __('Filtrar') }}
                            </button>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</div>
