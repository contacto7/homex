<div class="modal" id="modalInfoReporte">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{ __('Rporte de Perfil de Postulante') }}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div>
                    <div class="pb-2">El “Reporte de Perfil de Postulante” es un documento elaborado por Homex, con información de fuente pública, privada y redes sociales, a fin de determinar el perfil personal, crediticio y social del postulante, que pueda permitir al propietario tomar una  decisión informada sobre su futuro inquilino. El documento es entregado en el email del propietario dentro de un plazo máximo de 24 horas de solicitado.</div>
                </div>
            </div>

        </div>
    </div>
</div>
