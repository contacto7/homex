<div class="modal" id="modalRegistrarse">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{ __('Registrarme') }}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body modal-ajax-content">

                <form
                    method="POST"
                    action="{{ route('users.store') }}"
                    novalidate>
                    @csrf

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombres') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('Apellidos') }}</label>

                        <div class="col-md-6">
                            <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name">

                            @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="cellphone" class="col-md-4 col-form-label text-md-right">{{ __('Celular') }}</label>

                        <div class="col-md-6">
                            <input id="cellphone" type="text" class="form-control @error('cellphone') is-invalid @enderror" name="cellphone" value="{{ old('cellphone') }}" required autocomplete="cellphone">

                            @error('cellphone')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="role_id" class="col-md-4 col-form-label text-md-right">Deseo</label>
                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control {{ $errors->has('role_id') ? 'is-invalid': '' }}"
                                name="role_id"
                                id="role_id"
                            >

                                <option
                                    {{ (int) old('role_id') === \App\Role::PROPIETARY ? 'selected' : '' }}
                                    value="{{ \App\Role::PROPIETARY }}"
                                >{{ __("Publicar") }}</option>
                                <option
                                    {{ (int) old('role_id') === \App\Role::LESSEE ? 'selected' : '' }}
                                    value="{{ \App\Role::LESSEE }}"
                                >{{ __("Alquilar") }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row interested-chk-row">
                        <label for="bond_interested_status" class="col-md-4 col-form-label text-md-right"></label>

                        <div class="col-md-6">
                            <input type="checkbox" class="form-check-input ml-0" id="bond_interested_status" name="bond_interested_status" value="1">
                            <label class="form-check-label ml-3 mr-3" for="bond_interested_status">Estoy interesado en el bono renta joven</label>

                        </div>
                    </div>


                    <div class="form-group row lessee-row">
                        <label for="nacionality_id" class="col-md-4 col-form-label text-md-right">{{ __('Nacionalidad') }}</label>

                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control {{ $errors->has('nacionality_id') ? 'is-invalid': '' }}"
                                name="nacionality_id"
                                id="nacionality_id"
                            >
                                @foreach(\App\Nacionality::pluck('name', 'id') as $id => $name)
                                    <option
                                        {{ (int) old('nacionality_id') === $id || "170" == $id ? 'selected' : '' }}
                                        value="{{ $id }}"
                                    >{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row lessee-row">
                        <label for="quantity_members" class="col-md-4 col-form-label text-md-right">{{ __('Número de familiares que habitarán') }}</label>

                        <div class="col-md-6">
                            <input id="quantity_members" type="number"  inputmode="numeric" pattern="[0-9]*"  class="form-control @error('quantity_members') is-invalid @enderror" name="quantity_members" value="{{ old('quantity_members') }}">

                            @error('quantity_members')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row lessee-row">
                        <label for="dependent_status" class="col-md-4 col-form-label text-md-right">¿Eres independiente?</label>
                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control {{ $errors->has('dependent_status') ? 'is-invalid': '' }}"
                                name="dependent_status"
                                id="dependent_status"
                            >
                                <option
                                    {{ (int) old('dependent_status') == ""  ? 'selected' : '' }}
                                    value=""
                                >{{ __("Seleccionar") }}</option>
                                <option
                                    {{ (int) old('dependent_status') === \App\Lessee::INDEPENDENT ? 'selected' : '' }}
                                    value="{{ \App\Lessee::INDEPENDENT }}"
                                >{{ __("Si") }}</option>
                                <option
                                    {{ (int) old('dependent_status') === \App\Lessee::DEPENDENT ? 'selected' : '' }}
                                    value="{{ \App\Lessee::DEPENDENT }}"
                                >{{ __("No") }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row lessee-row">
                        <label for="monthly_salary" class="col-md-4 col-form-label text-md-right">{{ __('Ingresos mensuales') }}</label>

                        <div class="col-md-6">
                            <input id="monthly_salary" type="number"  inputmode="numeric" pattern="[0-9]*"  class="form-control @error('monthly_salary') is-invalid @enderror" name="monthly_salary" value="{{ old('monthly_salary') }}">

                            @error('monthly_salary')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row lessee-row">
                        <label for="debtor_status" class="col-md-4 col-form-label text-md-right">¿Estás en el registro de deudores morosos?</label>

                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control {{ $errors->has('debtor_status') ? 'is-invalid': '' }}"
                                name="debtor_status"
                                id="debtor_status"
                            >
                                <option
                                    {{ (int) old('debtor_status') == ""  ? 'selected' : '' }}
                                    value=""
                                >{{ __("Seleccionar") }}</option>
                                <option
                                    {{ (int) old('debtor_status') === \App\Lessee::NOT_DEBTOR ? 'selected' : '' }}
                                    value="{{ \App\Lessee::NOT_DEBTOR }}"
                                >{{ __("No") }}</option>
                                <option
                                    {{ (int) old('debtor_status') === \App\Lessee::DEBTOR ? 'selected' : '' }}
                                    value="{{ \App\Lessee::DEBTOR }}"
                                >{{ __("Si") }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row lessee-row">
                        <label for="other_postulation" class="col-md-4 col-form-label text-md-right">¿Has postulado a otro programa de ayuda para vivienda?</label>
                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control {{ $errors->has('other_postulation') ? 'is-invalid': '' }}"
                                name="other_postulation"
                                id="other_postulation"
                            >
                                <option
                                    {{ (int) old('other_postulation') == ""  ? 'selected' : '' }}
                                    value=""
                                >{{ __("Seleccionar") }}</option>
                                <option
                                    {{ (int) old('other_postulation') === \App\Lessee::NOT_OTHER_POSTULATION ? 'selected' : '' }}
                                    value="{{ \App\Lessee::NOT_OTHER_POSTULATION }}"
                                >{{ __("No") }}</option>
                                <option
                                    {{ (int) old('other_postulation') === \App\Lessee::OTHER_POSTULATION ? 'selected' : '' }}
                                    value="{{ \App\Lessee::OTHER_POSTULATION }}"
                                >{{ __("Si") }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn-homex btn-homex-clear">
                                {{ __('Registrar') }}
                            </button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
<script>

    $("#bond_interested_status").on( 'change', function() {
        bondInputVisibility();
    });
    $("#role_id").on( 'change', function() {
        roleInterestedVisibility();
        bondInputVisibility();
    });

    function bondInputVisibility() {
        if( $("#bond_interested_status").is(':checked') ) {
            // Hacer algo si el checkbox ha sido seleccionado
            $(".lessee-row").css('display','flex');
        } else {
            // Hacer algo si el checkbox ha sido deseleccionado
            $(".lessee-row").hide();
        }
    }
    function roleInterestedVisibility() {
        if( $("#role_id").val() == 3 ) {
            // Hacer algo si el checkbox ha sido seleccionado
            $(".interested-chk-row").css('display','flex');
        } else {
            $("#bond_interested_status").prop('checked',false)
            // Hacer algo si el checkbox ha sido deseleccionado
            $(".interested-chk-row").hide();
        }
    }
</script>
