<div class="modal" id="modalHelpPost">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{ __('Ayuda para propietarios') }}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div>
                    <div class="pb-2">Para publicar un inmueble, debes seguir los siguientes pasos:</div>
                    <ol>
                        <li>Registrarte en Homex, seleccionando "Publicar" en el campo "Deseo".</li>
                        <li>Hacer click en "Publicar inmueble".</li>
                        <li>Llenar los campos solicitados, y darle click a "Crear propiedad".</li>
                        <li>En la pestaña "Imágenes", agregar fotos de la propiedad.</li>
                        <li>Listo, puedes revisar tus propiedades seleccionando tu nombre en la barra de navegación superior y seleccionando "Mis viviendas" .</li>
                    </ol>
                </div>
            </div>

        </div>
    </div>
</div>
