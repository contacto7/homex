<div class="modal" id="modalHelpLease">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{ __('Ayuda para arrendatarios') }}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div>
                    <div class="pb-2">Para buscar tu inmueble ideal y postular por el arrendamiento, debes seguir los siguientes pasos:</div>
                    <ol>
                        <li>Registrarte en Homex, seleccionando "Alquilar" en el campo "Deseo".</li>
                        <li>Seleccionar los filtros de búsqueda que deseas aplicar.</li>
                        <li>Dale click a las propiedades donde te gustaría vivir.</li>
                        <li>En el detalle de la propiedad, si te gustó realmente, dale click a "Me interesa". Al darle click a "Me interesa", el propietario sabrá que deseas alquilar su propiedad.</li>
                        <li>Podrás ver tu postulación a la propiedad, seleccionando tu nombre en la barra de navegación superior, luego dando click a "Postulaciones".</li>
                        <li>Eso es todo, si llegan a un acuerdo, podrás ver el arrendamiento, el contrato y los pagos, dándole click a tu nombre en la
                            barra de navegación superior, y seleccionando "Arrendamientos".</li>

                    </ol>
                </div>
            </div>

        </div>
    </div>
</div>
