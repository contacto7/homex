<div class="modal" id="modalSolicitarReporte">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Solicitar reporte</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body modal-ajax-content">
                <div class="">
                    <div class="form-group row">
                        <label for="document" class="col-md-12 col-form-label">En el reporte obtendrás información
                            que te ayudará a elegir al inquilino correcto. Para obtener el reporte debes depositar el importe
                            de S/.150.00 en nuestra cuenta. Puedes elegir pagar en los siguientes bancos:
                        </label>
                    </div>
                </div>
                <div class="">
                    <div class="bancos-tabs">

                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a
                                    class="nav-item nav-link active"
                                    id="nav-bcp-tab"
                                    data-toggle="tab"
                                    href="#nav-bcp"
                                    role="tab"
                                    aria-controls="nav-bcp"
                                    aria-selected="true"
                                >
                                    <img
                                        src="{{ asset('image/bancos/bcp.jpg') }}"
                                        class="banco-logo-reporte"
                                        alt="logo de banco"
                                    >


                                </a>
                                <a
                                    class="nav-item nav-link"
                                    id="nav-ib-tab"
                                    data-toggle="tab"
                                    href="#nav-ib"
                                    role="tab"
                                    aria-controls="nav-ib"
                                    aria-selected="true"
                                >
                                    <img
                                        src="{{ asset('image/bancos/interbank.png') }}"
                                        class="banco-logo-reporte"
                                        alt="logo de banco"
                                    >
                                </a>
                                <a
                                    class="nav-item nav-link"
                                    id="nav-bbva-tab"
                                    data-toggle="tab"
                                    href="#nav-bbva"
                                    role="tab"
                                    aria-controls="nav-bbva"
                                    aria-selected="true"
                                >
                                    <img
                                        src="{{ asset('image/bancos/bbva.jpg') }}"
                                        class="banco-logo-reporte"
                                        alt="logo de banco"
                                    >
                                </a>



                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">

                            <div class="tab-pane fade show active" id="nav-bcp" role="tabpanel" aria-labelledby="nav-bcp-tab">

                                <div class="tab-pane-inn requisitos-wrapp">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="h5 tit-tab-brjmodal">
                                                <span class="yape-txt">YAPÉANOS</span>, escaneando el siguiente código:
                                            </div>
                                            <div class="text-center p-4 m-2">

                                                <img
                                                    src="{{ asset('image/bancos/qr-yape.jpeg') }}"
                                                    class="banco-logo-reporte"
                                                    alt="logo de banco"
                                                >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="h5 tit-tab-brjmodal">
                                                O deposita en nuestra cuenta bancaria BCP
                                            </div>
                                            <p class="dscr-tab-brjmodal">
                                                <b>Ahorros soles:</b> 213-1513551353153153153<br>
                                                <b>CCI:</b>  222-245644654445644466465-12
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="nav-ib" role="tabpanel" aria-labelledby="nav-ib-tab">

                                <div class="tab-pane-inn requisitos-wrapp">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="h5 tit-tab-brjmodal">
                                                Proximamente en Interbank
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="nav-bbva" role="tabpanel" aria-labelledby="nav-bbva-tab">

                                <div class="tab-pane-inn requisitos-wrapp">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="h5 tit-tab-brjmodal">
                                                Proximamente en el BBVA
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <form method="POST" action="{{ route('users.reportInterested', $user->id) }}">
                    @csrf

                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-12 text-center">
                            <button type="submit" class="btn-homex btn-homex-primary" style="width: auto">
                                {{ __('Estoy interesado') }}
                            </button>
                        </div>
                    </div>
                </form>
                <form method="POST" action="{{ route('users.reportPayed', $user->id) }}">
                    @csrf

                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-12 text-center">
                            <button type="submit" class="btn-homex btn-homex-clear" style="width: auto">
                                {{ __('Ya realicé el pago!') }}
                            </button>
                        </div>
                    </div>
                </form>


            </div>

        </div>
    </div>
</div>
