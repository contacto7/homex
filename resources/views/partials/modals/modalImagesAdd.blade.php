<div class="modal" id="modalImagesAdd">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Añadir imagen</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body modal-ajax-content">
                <form
                    method="POST"
                    action="{{ route('propertyImages.store') }}"
                    novalidate
                    enctype="multipart/form-data"
                >
                    @csrf

                    <div class="form-group" style="display: none">
                        <label for="property_id">Propiedad</label>
                        <input
                            type="text"
                            class="form-control"
                            name="property_id"
                            id="property_id"
                            placeholder=""
                            value="{{ $property }}"
                            required
                        >
                    </div>


                    <div class="form-group">
                        <div class="col-md-12">
                            <input
                                type="file"
                                class="custom-file-input form-control"
                                name="picture"
                                id="picture"
                            >
                            <label  class="custom-file-label" for="picture" data-browse="Elegir">
                                Seleccionar foto
                            </label>
                            <div  class="profile-filename"></div>
                        </div>
                    </div>

                    <div class="form-group" style="display: none">
                        <label for="order">Orden</label>
                        <input
                            type="number"  inputmode="numeric" pattern="[0-9]*"
                            class="form-control"
                            name="order"
                            id="order"
                            placeholder=""
                            value="{{ ++$last_order }}"
                            required
                        >
                        @if($errors->has('order'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('order') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">
                            {{ __("Agregar") }}
                        </button>
                    </div>

                </form>

            </div>

        </div>
    </div>
</div>
