<div class="modal" id="modalBonoRentaJoven">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">{{ __('Bono renta joven') }}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a
                            class="nav-item nav-link active"
                            id="nav-req-tab"
                            data-toggle="tab"
                            href="#nav-req"
                            role="tab"
                            aria-controls="nav-req"
                            aria-selected="true"
                        >Requisitos</a>
                        <a
                            class="nav-item nav-link"
                            id="nav-func-tab"
                            data-toggle="tab"
                            href="#nav-func"
                            role="tab"
                            aria-controls="nav-func"
                            aria-selected="false"
                        >¿Cómo funciona?</a>
                        <a
                            class="nav-item nav-link"
                            id="nav-help-tab"
                            data-toggle="tab"
                            href="#nav-help"
                            role="tab"
                            aria-controls="nav-help"
                            aria-selected="false"
                        >¡Te ayudamos!</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">

                    <div class="tab-pane fade show active" id="nav-req" role="tabpanel" aria-labelledby="nav-req-tab">

                        <div class="tab-pane-inn requisitos-wrapp">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="h5 tit-tab-brjmodal">
                                        Arrendador
                                    </div>
                                    <p class="dscr-tab-brjmodal">
                                        <ul class="list-tab-brjmodal">
                                            <li>Monto de la renta mensual.</li>
                                            <li>Características del inmueble.</li>
                                        </ul>
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <div class="h5 tit-tab-brjmodal">
                                        Arrendatario
                                    </div>
                                    <p class="dscr-tab-brjmodal">
                                        <ul class="list-tab-brjmodal">
                                            <li>Grupos familiares de mínimo 2 personas.</li>
                                            <li>Peruanos.</li>
                                            <li>No dependientes.</li>
                                            <li>Escasos recursos.</li>
                                            <li>No deudores judiciales morosos.</li>
                                            <li>No haber postulado a otro programa de ayuda para viviendas.</li>
                                        </ul>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="nav-func" role="tabpanel" aria-labelledby="nav-func-tab">

                        <div class="tab-pane-inn funcionamiento-wrapp">

                            <div class="list-group">
                                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">Paso 1</h5>
                                        <div>Tú</div>
                                    </div>
                                    <p class="mb-1">Postulas.</p>
                                </a>
                                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">Paso 2</h5>
                                        <div>Ministerio de vivienda</div>
                                    </div>
                                    <p class="mb-1">Aprueba el otorgamiento del bono.</p>
                                </a>
                                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">Paso 3</h5>
                                        <div>Tú y arrendador</div>
                                    </div>
                                    <p class="mb-1">Firman el contrato de arrendamiento.</p>
                                </a>
                                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">Paso 4</h5>
                                        <div>Tú</div>
                                    </div>
                                    <p class="mb-1">Depositas el importe del arrendamiento.</p>
                                </a>
                                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">Paso 5</h5>
                                        <div>Fondo mi vivienda</div>
                                    </div>
                                    <p class="mb-1">Deposita el monto de subsidio.</p>
                                </a>
                                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">Paso 6</h5>
                                        <div>Arrendador</div>
                                    </div>
                                    <p class="mb-1">Retira el importe de la renta.</p>
                                </a>
                            </div>

                        </div>

                    </div>
                    <div class="tab-pane fade" id="nav-help" role="tabpanel" aria-labelledby="nav-help-tab">

                        <div class="tab-pane-inn funcionamiento-wrapp">

                            <div class="h5">¿Ya cuentas con el bono para vivienda?</div>
                            <div class="mb-3">
                                Registrate y encuentra tu hogar aquí.
                            </div>
                            <div class="h5">¿Aún no cuentas con el bono para vivienda?</div>
                            <div class="">
                                Registrate, nuestro equipo evaluará tu perfil y realizará las gestiones necesarias ante el ministerio de vivienda por ti!
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
