<div class="modal" id="modalEditAddress">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Ubicación</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body modal-ajax-content">

                <form method="POST" action="{{ route('lessees.update', $user->lessee->id) }}">

                    @method('PUT')

                    @csrf

                    <div class="form-group row" style="display: none">
                        <label for="location" class="col-md-4 col-form-label text-md-right">Location</label>

                        <div class="col-md-6">
                            <input id="location" type="text" class="form-control" name="location" value="location" required>
                            @error('location')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="address" class="col-md-4 col-form-label text-md-right">Dirección</label>

                        <div class="col-md-6">
                            <input id="address" type="text" class="form-control" name="address" value="{{ old('address')?:$user->lessee->address }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="district_id" class="col-md-4 col-form-label text-md-right">{{ __('Distrito') }}</label>

                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control"
                                name="district_id"
                                id="district_id"
                            >
                                @foreach($districts as $district)
                                    <option
                                        {{ (int) old('district_id') === $district->id || $user->lessee->district_id == $district->id ? 'selected' : '' }}
                                        value="{{ $district->id }}"
                                    >{{ $district->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-12 text-center">
                            <button type="submit" class="btn-homex btn-homex-clear" style="width: auto">
                                {{ __('Actualizar') }}
                            </button>
                        </div>
                    </div>
                </form>


            </div>

        </div>
    </div>
</div>
