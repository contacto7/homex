<div class="modal" id="modalEditNationality">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Nacionalidad y sexo</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body modal-ajax-content">

                <form method="POST" action="{{ route('users.update', $user->id) }}">

                    @method('PUT')

                    @csrf

                    <div class="form-group row" style="display: none">
                        <label for="nacionality" class="col-md-4 col-form-label text-md-right">Nacionality</label>

                        <div class="col-md-6">
                            <input id="nacionality" type="text" class="form-control" name="nacionality" value="nacionality" required>
                            @error('nacionality')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="nacionality_id" class="col-md-4 col-form-label text-md-right">{{ __('Nacionalidad') }}</label>

                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control"
                                name="nacionality_id"
                                id="nacionality_id"
                            >
                                @foreach($nacionalities as $nacionality)
                                    <option
                                        {{ (int) old('nacionality_id') === $nacionality->id || $user->nacionality_id == $nacionality->id ? 'selected' : '' }}
                                        value="{{ $nacionality->id }}"
                                    >{{ $nacionality->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="sex" class="col-md-4 col-form-label text-md-right">{{ __('Sexo') }}</label>

                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control"
                                name="sex"
                                id="sex"
                            >
                                <option
                                    {{ (int) old('sex') === \App\User::FEMALE || $user->sex == \App\User::FEMALE ? 'selected' : '' }}
                                    value="{{ \App\User::FEMALE }}"
                                >{{ __("Mujer") }}</option>
                                <option
                                    {{ (int) old('sex') === \App\User::MALE || $user->sex == \App\User::MALE ? 'selected' : '' }}
                                    value="{{ \App\User::MALE }}"
                                >{{ __("Hombre") }}</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-12 text-center">
                            <button type="submit" class="btn-homex btn-homex-clear" style="width: auto">
                                {{ __('Actualizar') }}
                            </button>
                        </div>
                    </div>
                </form>


            </div>

        </div>
    </div>
</div>
