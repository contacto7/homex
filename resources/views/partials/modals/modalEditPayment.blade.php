<div class="modal" id="modalEditPayment">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Actualizar Pago</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body modal-ajax-content">

                <div>
                    <div class="form-group row" style="display: none">
                        <label for="id" class="col-md-4 col-form-label text-md-right">Id</label>

                        <div class="col-md-6">
                            <input id="id" type="number"  inputmode="numeric" pattern="[0-9]*"  class="form-control" name="id" value="{{ old('id') }}" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="amount_payed" class="col-md-4 col-form-label text-md-right">Monto pagado</label>

                        <div class="col-md-6">
                            <input id="amount_payed" type="number"  inputmode="numeric" pattern="[0-9]*"  class="form-control" name="amount_payed" value="{{ old('amount_payed') }}" required>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn-homex btn-homex-clear btn-modificar-postulacion" style="width: auto">
                                {{ __('Modificar') }}
                            </button>
                            <div class="mensaje-ajax"></div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</div>
