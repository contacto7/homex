<div class="modal" id="modalEditLifestyle">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Estilo de vida</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body modal-ajax-content">

                <form method="POST" action="{{ route('lessees.update', $user->lessee->id) }}">

                    @method('PUT')

                    @csrf

                    <div class="form-group row" style="display: none">
                        <label for="lifestyle" class="col-md-4 col-form-label text-md-right">Lifestyle</label>

                        <div class="col-md-6">
                            <input id="lifestyle" type="text" class="form-control" name="lifestyle" value="lifestyle" required>
                            @error('lifestyle')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lifestyle_type_id" class="col-md-4 col-form-label text-md-right">{{ __('Estilo de vida') }}</label>

                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control"
                                name="lifestyle_type_id"
                                id="lifestyle_type_id"
                            >
                                @foreach($lifestyleTypes as $lifestyleType)
                                    <option
                                        {{ (int) old('lifestyle_type_id') === $lifestyleType->id || $user->lessee->lifestyle_type_id == $lifestyleType->id ? 'selected' : '' }}
                                        value="{{ $lifestyleType->id }}"
                                    >{{ $lifestyleType->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="emotional_situation" class="col-md-4 col-form-label text-md-right">{{ __('Situación emocional') }}</label>

                        <div class="col-md-6">
                            <select
                                class="form-control"
                                name="emotional_situation"
                                id="emotional_situation"
                            >
                                <option
                                    {{ (int) old('emotional_situation') === \App\Lessee::SINGLE || $user->lessee->emotional_situation == \App\Lessee::SINGLE ? 'selected' : '' }}
                                    value="{{ \App\Lessee::SINGLE }}"
                                >{{ __("Soltero(a)") }}</option>
                                <option
                                    {{ (int) old('emotional_situation') === \App\Lessee::COUPLE || $user->lessee->emotional_situation == \App\Lessee::COUPLE ? 'selected' : '' }}
                                    value="{{ \App\Lessee::COUPLE }}"
                                >{{ __("Con pareja") }}</option>
                                <option
                                    {{ (int) old('emotional_situation') === \App\Lessee::FAMILY || $user->lessee->emotional_situation == \App\Lessee::FAMILY ? 'selected' : '' }}
                                    value="{{ \App\Lessee::FAMILY }}"
                                >{{ __("En familia") }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="occupation_type_id" class="col-md-4 col-form-label text-md-right">{{ __('Ocupación') }}</label>

                        <div class="col-md-6">
                            <select
                                style="text-transform: capitalize"
                                class="form-control"
                                name="occupation_type_id"
                                id="occupation_type_id"
                            >
                                @foreach($occupationTypes as $occupationType)
                                    <option
                                        {{ (int) old('occupation_type_id') === $occupationType->id || $user->lessee->occupation_type_id == $occupationType->id ? 'selected' : '' }}
                                        value="{{ $occupationType->id }}"
                                    >{{ $occupationType->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="occupation_center" class="col-md-4 col-form-label text-md-right">Centro de trabajo</label>

                        <div class="col-md-6">
                            <input id="occupation_center" type="text" class="form-control" name="occupation_center" value="{{ old('occupation_center')?:$user->lessee->occupation_center }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="monthly_salary" class="col-md-4 col-form-label text-md-right">Salario mensual</label>

                        <div class="col-md-6">
                            <input id="monthly_salary" type="number"  inputmode="numeric" pattern="[0-9]*"  class="form-control" name="monthly_salary" value="{{ old('monthly_salary')?:$user->lessee->monthly_salary }}">
                        </div>
                    </div>


                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-12 text-center">
                            <button type="submit" class="btn-homex btn-homex-clear" style="width: auto">
                                {{ __('Actualizar') }}
                            </button>
                        </div>
                    </div>
                </form>


            </div>

        </div>
    </div>
</div>
