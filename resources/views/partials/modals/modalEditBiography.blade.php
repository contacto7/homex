<div class="modal" id="modalEditBiography">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Nota</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body modal-ajax-content">

                <form method="POST" action="{{ route('propietaries.update', $user->propietary->id) }}">

                    @method('PUT')

                    @csrf

                    <div class="form-group row" style="display: none">
                        <label for="life" class="col-md-4 col-form-label text-md-right">Vida</label>

                        <div class="col-md-6">
                            <input id="life" type="text" class="form-control" name="life" value="life" required>
                            @error('life')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="biography" class="col-md-4 col-form-label text-md-right">{{ __('Descripción') }}</label>

                        <div class="col-md-6">
                            <textarea id="biography" class="form-control " name="biography">{{ old('biography')?:$user->propietary->biography }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-12 text-center">
                            <button type="submit" class="btn-homex btn-homex-clear" style="width: auto">
                                {{ __('Actualizar') }}
                            </button>
                        </div>
                    </div>
                </form>


            </div>

        </div>
    </div>
</div>
