<div class="modal" id="modalPostulacion">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Actualizar Postulación</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body modal-ajax-content">

                <div>
                    <div class="form-group row" style="display: none">
                        <label for="id" class="col-md-4 col-form-label text-md-right">Id</label>

                        <div class="col-md-6">
                            <input id="id" type="number"  inputmode="numeric" pattern="[0-9]*"  class="form-control @error('id') is-invalid @enderror" name="id" value="{{ old('id') }}" required>

                            @error('id')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="stage" class="col-md-4 col-form-label text-md-right">Etapa</label>

                        <div class="col-md-6">
                            <select
                                id="stage"
                                name="stage"
                                class="form-control @error('stage') is-invalid @enderror"
                                required
                            >
                                <option value="{{ \App\Application::POSTULACION }}">Postulación</option>
                                <option value="{{ \App\Application::PRE_APROBADO }}">Pre-aprobado</option>
                                <option value="{{ \App\Application::VISITA }}">Visita</option>
                                <option value="{{ \App\Application::APROBADO }}">Alquiler aprobado</option>
                                <option value="{{ \App\Application::CONTRATO_FIRMADO }}">Contrato firmado</option>
                            </select>

                            @error('stage')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="stage_state" class="col-md-4 col-form-label text-md-right">Estado</label>

                        <div class="col-md-6">
                            <select
                                id="stage_state"
                                name="stage_state"
                                class="form-control @error('stage_state') is-invalid @enderror"
                                required
                            >
                                <option value="{{ \App\Application::INITIAL }}">No revisado</option>
                                <option value="{{ \App\Application::APPROVED }}">Aprobado</option>
                                <option value="{{ \App\Application::DISAPPROVED }}">Descartado</option>
                            </select>

                            @error('stage_state')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn-homex btn-homex-clear btn-modificar-postulacion" style="width: auto">
                                {{ __('Modificar') }}
                            </button>
                            <div class="mensaje-ajax"></div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</div>
