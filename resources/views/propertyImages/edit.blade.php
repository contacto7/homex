@extends('layouts.app')


@section('title')
    @include('partials.genericJumbotron', [
        'title' => __("Imágenes"),
        'icon' => "list-alt"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <a
                class="btn btn-homex-dark"
                data-toggle="modal"
                data-target="#modalImagesAdd"
                href="#modalImagesAdd"
            >Agregar imagen</a>
            <div class="p-2"> *Las imágenes se guardan automáticamente</div>
        </div>
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Imagen</th>
                    <th scope="col">Creación</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @php $temp =1; @endphp
                @php $last_order =0; @endphp
                @forelse($propertyImages as $propertyImage)
                    <tr>
                        <td>{{ $temp }}</td>
                        <td>
                            <img
                                class="profile-img"
                                src="{{ $propertyImage->pathAttachment() }}"
                                alt="{{ $propertyImage->picture }}"
                                style="width: 100px"
                            />

                        </td>
                        <td>{{ $propertyImage->created_at }}</td>

                        <td>

                            <form
                                method="POST"
                                action="{{ route('propertyImages.delete', $propertyImage->id) }}"
                                novalidate
                            >
                                @method('DELETE')

                                @csrf
                                <div class="form-group" style="display: none">
                                    <input
                                        type="text"
                                        class="form-control"
                                        name="picture"
                                        value="{{ $propertyImage->picture }}"
                                        required
                                    >
                                </div>
                                <div class="form-group">
                                    <button
                                        type="submit"
                                        class="btn-homex btn-homex-primary error m-1"
                                    >
                                        <span class="fa fa-trash"></span>
                                    </button>
                                </div>

                            </form>
                        </td>
                    </tr>
                    @php $temp ++;$last_order=$propertyImage->order; @endphp
                @empty
                    <tr>
                        <td>{{ __("No hay imágenes disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
        <div class="row">
            <a
                class="btn btn-homex-dark"
                href="{{ route('properties.myList', auth()->user()->slug) }}"
            >Ver mis propiedades</a>
        </div>



    </div>

    @include('partials.modals.modalImagesAdd')
@endsection

@push('scripts')
    <script>
        $(function() {
            $('#picture').change(function() {
                let filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
                $('.custom-file-label').html(filename);
            });
        });

    </script>
@endpush
