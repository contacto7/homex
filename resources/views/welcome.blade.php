<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ env("APP_NAME", "Homex") }}</title>

        <!-- Bootstrap js -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.min.js" integrity="sha256-aS5HnZXPFUnMTBhNEiZ+fKMsekyUqwm30faj/Qh/gIA=" crossorigin="anonymous"></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Bootstrap css -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"  rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.css" integrity="sha256-U6YrSIhyGybBdtKMg2+iJsIbyHtNY3Yj9gacnUG2jNo=" crossorigin="anonymous" />

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #fff;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            body{
                background-image: url({{ asset('image/banner-inicio.jpg') }});
                background-position: center center;
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-size: cover;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-left {
                position: absolute;
                left: 10px;
                top: 18px;
                max-width: 30vw;
                text-align: center;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .btn-homex{
                font-weight: bold;
                padding: 10px;
                cursor: pointer;
                -webkit-transition: all 0.4s ease;
                -moz-transition: all 0.4s ease;
                -o-transition: all 0.4s ease;
                transition: all 0.4s ease;
                border: 0;
                display: inline-block;
                max-width: 300px;
                width: 100%;
                margin: 10px;
                text-decoration: none;
                border-radius: 10px;
            }
            .btn-homex-primary{
                color: rgb(105, 189, 226);
                background-color: rgb(255,255,255);
            }
            input.btn-homex-primary::placeholder{
                color: rgb(105, 189, 226);
            }
            .btn-homex-primary:not(input):hover, .btn-homex-primary:not(input):focus{
                color: rgb(255, 255, 255);
                background-color: rgb(146,221,234);
            }
            .btn-homex-primary-active{
                color: rgb(255, 255, 255);
                background-color: rgb(146,221,234);
            }
            .btn-homex-dark{
                background-color: rgb(106,210,214);
                color: rgb(255,255,255);
            }
            .btn-homex-dark:not(input):hover, .btn-homex-dark:not(input):focus{
                color: rgb(255, 255, 255);
                background-color: rgb(100,190,193);
            }
            .btn-homex-dark-active{
                color: rgb(255, 255, 255);
                background-color: rgb(146,221,234);
            }

            .title {
                font-size: 84px;
                font-weight: 100;
            }

            .mensaje-subtitulo-wrapp{
                padding-bottom: 40px;
                font-size: 16px;
                width: 100%;
            }

            .iniciar-sesion-links a{
                color: #fff;
                padding: 0 1vw;
                font-size: calc(13px + 0.5vw);
                font-weight: 600;
                text-decoration: none;
                text-transform: uppercase;
            }

            .mensaje-subtitulo{
                color: #fff;
                padding: 0 25px;
                font-size: 18px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .mensaje-subsubtitulo{
                color: #fff;
                padding: 0 25px;
                font-size: 16px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .mensaje-subsubtitulo{
                color: #fff;
                padding: 0 25px;
                font-size: 16px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .mensaje-inicio-3{
                color: #fff;
                padding: 0 25px;
                font-size: 16px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .modal{
                color: #888
            }

            .tab-pane-inn{
                padding: 15px;
            }

            #modalBonoRentaJoven .nav-link{
                padding: 0.5rem 0.5rem;
                font-size: calc(11px + 0.5vw);
            }

            .lessee-row,.interested-chk-row{
                display: none;
            }

            /**/
            .btn-inicio-buscar-med{
                width: calc(50% - 20px);
            }
            .btn-siguiente-wrapp{
                width: 100%;
                text-align: center;
            }

            .nav-buscar-home-wrapp{
                height: 600px;
            }
            .nav-buscar-home-inn{
                width: 100%;
                display: inline-flex;
                flex-wrap: wrap;
                justify-content: center;
            }
            .easy-autocomplete{
                width: calc(50% - 20px) !important;
                max-width: 400px !important;
                color: rgb(105, 189, 226);
            }

            .easy-autocomplete input{
                width: 100%;
                color: rgb(105, 189, 226);
                border: none;
                padding: 10px;
                outline: 0;
                border-radius: 0;
                text-transform: uppercase;
            }

            .form-wrapper{
                width: 500px;
            }
            /**/
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-left iniciar-sesion-links">
                    <a
                        href="{{ route('aboutUs') }}"
                    >Hola, te contamos que hacemos</a>
                </div>
                <div class="top-right iniciar-sesion-links">
                    @auth
                        <a
                            href="{{ route("users.profile", auth()->user()->slug) }}"
                        >{{ auth()->user()->name }}</a>
                    @else



                        @if (Route::has('register'))
                            <a
                                data-toggle="modal"
                                data-target="#modalBonoRentaJoven"
                                href="#modalBonoRentaJoven"
                                style="display: none"
                            >Bono renta joven</a>
                            <a
                                data-toggle="modal"
                                data-target="#modalIngresar"
                                href="#modalIngresar"
                            >Ingresar</a>
                            <a
                                data-toggle="modal"
                                data-target="#modalRegistrarse"
                                href="#modalRegistrarse"
                            >Únete a Homex</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="mensaje-inicio-3 pt-4 pl-sm-5 pr-sm-5">
                    <div
                        class="buscar-inicio-wrapp"
                    >
                        <div
                            class="buscar-inicio-inn"
                        >
                            @include('partials.home.search')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('partials.modals.modalIngresar')
        @include('partials.modals.modalRegistrarse')
        @include('partials.modals.modalBonoRentaJoven')
        @include('partials.modals.modalMensaje')

    </body>
</html>
<script>
    $('.carousel').carousel({
        interval: 10000000
    });

    $(".btn-select").click(function(){
        $(this).toggleClass("btn-homex-primary-active");

        changeStatus($(this));



    });

    function changeStatus($obj) {
        let data_search = $obj.data("search");
        let data_id = parseInt($obj.data("id"));
        let data_status = parseInt($obj.data("status"));

        console.log("Sarch: "+ data_search);
        console.log("Id: "+ data_id);
        console.log("Status: "+ data_status);

        if(data_status){
            $obj.data("status",0);
        }else{
            $obj.data("status",1);
        }
    }
    function changeId($obj) {
        let district_id = $obj.getSelectedItemData().id;
        let district_name = $obj.getSelectedItemData().name;

        $obj.data("id",district_id);
        $obj.data("status","1");

        console.log("Id: "+ district_id);
        console.log("Nombre: "+ district_name);
    }

    var district_options = [
        @foreach(\App\District::with('province')->get() as $district)
        {id: "{{ $district->id }}",name:"{{ $district->name.", ".$district->province->name }}"},
        @endforeach
    ];
    var options = {
        data: district_options,
        getValue: "name",
        list: {
            match:{
              enabled:true
            },
            onSelectItemEvent: function() {
                $obj = $("#input-district-search");

                changeId($obj);
            }
        }
    };

    $("#input-district-search").easyAutocomplete(options);

    $(".btn-result").click(function(){
        let url_busqueda="/properties/list?";
        let types="types=";
        let districts="&districts=";

        $( ".btn-homex" ).each(function() {
            $obj = $(this);

            let data_search = $obj.data("search");
            let data_id = parseInt($obj.data("id"));
            let data_status = parseInt($obj.data("status"));

            let url_busqueda;

            if(data_status){
                console.log("Sarch: "+ data_search);
                console.log("Id: "+ data_id);
                console.log("Status: "+ data_status);

                if(data_search === "property_type_id"){
                    types+=(data_id+",");
                }else if(data_search === "district_id"){
                    districts+=(data_id+",");
                }

            }
        });


        url_busqueda+=types;
        url_busqueda+=districts;

        console.log("-------------------------------------");
        console.log(url_busqueda);

        // similar behavior as clicking on a link
        window.location.href = url_busqueda;

    });
</script>
