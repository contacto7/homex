@extends('layouts.app')

@section('jumbotron')
    @include('partials.genericJumbotron', [
        'title' => __("TÉRMINOS Y CONDICIONES"),
        'icon' => "th-list"
    ])
@endsection

@section('content')
    <div class="pl-5 pr-5">
        <div class="about-us-wrapper">
            <h2>Visión</h2>
            <p>
                Una comunidad ordenada, que viva en armonía con su entorno y con el medio ambiente.
            </p>
            <h2>Misión</h2>
            <p>
                Lograr la convivencia pacifica entre propietarios y arrendatarios.
            </p>
            <h2>Objetivo</h2>
            <p>
                Que todos encuentren un lugar ideal en donde vivir.
            </p>
        </div>
    </div>
@endsection

@push('scripts')
    <script>


    </script>
@endpush
