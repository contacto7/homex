@extends('layouts.app')


@section('title')
    @include('partials.genericJumbotron', [
        'title' => __("Cronograma de pagos"),
        'icon' => "home"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">Período de Pago</th>
                    <th scope="col">Fecha de pago</th>
                    <th scope="col">Importe de pago</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Pagado</th>
                    <th scope="col">Pagado en</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @php $temp_month = 0; @endphp
                @forelse($payments as $payment)
                    <tr>
                        <td>
                            {{ date('d/m/Y', strtotime($start_date." + $temp_month month" )) }} -
                            @php $temp_month++; @endphp
                            {{ date('d/m/Y', strtotime($start_date." + $temp_month month - 1 day")) }}

                        </td>
                        <td>{{ date('d/m/Y', strtotime($payment->emitted_date )) }}</td>
                        <td>
                            S/{{ number_format($payment->amount_total, 2, '.', "'") }}
                        </td>
                        <td>
                            @if($payment->state == \App\Payment::NOT_PAYED)
                                @if(\Carbon\Carbon::now() >=  $payment->emitted_date)
                                    <b class="error state-{{ $payment->id }}">Renta Impaga</b>
                                @else
                                    <span class="state-{{ $payment->id }}">No pagado</span>
                                @endif
                            @elseif($payment->state == \App\Payment::PAYED)
                                <span class="state-{{ $payment->id }}">Pagado</span>
                            @elseif($payment->state == \App\Payment::PARTIALLY_PAYED)
                                <span class="state-{{ $payment->id }}">Pagado parcialmente</span>
                            @endif
                        </td>
                        <td>
                            S/<span class="amount-payed-{{ $payment->id }}">{{ number_format($payment->amount_payed, 2, '.', "'") }}</span>
                        </td>
                        <td>
                            @if($payment->payed_date)
                                {{ date('d/m/Y', strtotime($payment->payed_date )) }}
                            @endif
                        </td>
                        <td>
                            @if($canStore)
                            <a
                                class="btn-homex btn-homex-clear m-1 btn-postulacion"
                                data-toggle="modal"
                                data-target="#modalEditPayment"
                                data-id="{{ $payment->id }}"
                                href="#modalEditPayment"
                            ><span class="fa fa-pencil"></span>  </a>
                            @endif
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay arrendamientos disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center">
            {{ $payments->appends(request()->except('page'))->links() }}
        </div>
    </div>
    @include('partials.modals.modalEditPayment')
@endsection

@push('scripts')
    <script>

        $(document).on('click', '.btn-postulacion', function(){

            $(".mensaje-ajax").html("");

            let id = $(this).data('id');

            console.log(id);

            $("#id").val(id);
        });

        $(document).on('click', '.btn-modificar-postulacion', function(){
            let id = $("#id").val();
            let amount_payed = $("#amount_payed").val();

            console.log(id);
            console.log(amount_payed);

            //Enviamos una solicitud con el ruc de la empresa
            $.ajax({
                type: 'GET', //THIS NEEDS TO BE GET
                url: '{{ route('payments.updatePayment') }}',
                data: {
                    id: id,
                    amount_payed: amount_payed,
                },
                success: function (data) {
                    if(data == 1){
                        $(".mensaje-ajax").html("Se actualizó correctamente la postulación.");

                        window.location.href=window.location.href;

                    }else if(data == 401){
                        $(".mensaje-ajax").html("Usted no tiene permiso para actualizar este pago.");
                    }else{
                        $(".mensaje-ajax").html("Ocurrió un error al actualizar. Error:"+data);
                    }
                    console.log(data);
                },error:function(data){
                    console.log(data);
                }
            });



        });


    </script>
@endpush

