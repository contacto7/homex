@extends('layouts.app')


@section('title')
    @include('partials.genericJumbotron', [
        'title' => __("Postulaciones"),
        'icon' => "list-alt"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <a class="h3 pt-2 pb-4" href="{{ route('properties.view', $applications[0]->property->id) }}">
                {{ $applications[0]->property->name }}
            </a>
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Postulante</th>
                    <th scope="col">Postulado</th>
                    <th scope="col">Etapa</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($applications as $application)
                    <tr>
                        <td>{{ $application->id }}</td>
                        <td>
                            <a href="{{ route('users.profile',$application->user->slug) }}">
                                {{ $application->user->name." ".$application->user->last_name }}
                            </a>
                        </td>
                        <td>
                            {{ date('d/m/Y', strtotime($application->date)) }}</td>
                        <td>
                            @switch($application->stage)
                                @case(1)
                                <span class="stage-name-{{ $application->id }}">Postulación</span>
                                @break
                                @case(2)
                                <span class="stage-name-{{ $application->id }}">Pre-aprobado</span>
                                @break
                                @case(3)
                                <span class="stage-name-{{ $application->id }}">Visita</span>
                                @break
                                @case(4)
                                <span class="stage-name-{{ $application->id }}">Alquiler aprobado</span>
                                @break
                                @case(5)
                                <span class="stage-name-{{ $application->id }}">Contrato firmado</span>
                                @break

                                @default
                                <span class="stage-name-{{ $application->id }}">Inicial</span>
                            @endswitch
                        </td>
                        <td>
                            @switch($application->stage_state)
                                @case(\App\Application::INITIAL)
                                <span class="state-name-{{ $application->id }}">No revisado</span>
                                @break
                                @case(\App\Application::APPROVED)
                                <span class="state-name-{{ $application->id }}">Aprobado</span>
                                @break
                                @case(\App\Application::DISAPPROVED)
                                <span class="state-name-{{ $application->id }}">Descartado</span>
                                @break

                                @default
                                <span class="state-name-{{ $application->id }}">No revisado</span>
                            @endswitch
                        </td>

                        <td>
                            <a
                                class="btn-homex btn-homex-clear m-1 btn-postulacion"
                                data-toggle="modal"
                                data-target="#modalPostulacion"
                                data-id="{{ $application->id }}"
                                data-stage="{{ $application->stage }}"
                                data-state="{{ $application->stage_state }}"
                                href="#modalPostulacion"
                            >Postulación</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay postulaciones disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center">
            {{ $applications->appends(request()->except('page'))->links() }}
        </div>
    </div>
    @include('partials.modals.modalPostulacion')
@endsection

@push('scripts')
    <script>

        $(document).on('click', '.btn-postulacion', function(){

            $(".mensaje-ajax").html("");

            let id = $(this).data('id');
            let stage = $(this).data('stage');
            let state = $(this).data('state');

            console.log(id);
            console.log(stage);
            console.log(state);

            $("#id").val(id);
            $('#stage option[value='+stage+']').prop('selected', true);
            $('#stage_state option[value='+state+']').prop('selected', true);
        });

        $(document).on('click', '.btn-modificar-postulacion', function(){
            let id = $("#id").val();
            let stage = $("#stage").val();
            let stage_state = $("#stage_state").val();
            let confirm_contract = true;

            console.log(id);
            console.log(stage);
            console.log(stage_state);

            if(stage == 5 && stage_state == 2){
                confirm_contract = confirm("¿Esta seguro de convertir esta postulación en arrendamiento?");
            }

            if(confirm_contract){
                //Enviamos una solicitud con el ruc de la empresa
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: '{{ route('applications.updatePostulation') }}',
                    data: {
                        id: id,
                        stage: stage,
                        stage_state: stage_state,
                    },
                    success: function (data) {
                        if(data == 1){
                            $(".mensaje-ajax").html("Se actualizó correctamente la postulación.");

                            if(stage == 5 && stage_state == 2){
                                $(".mensaje-ajax").append(" Podrá ver este nuevo arrendamiento en su lista de alquileres.");
                            }

                            $(".stage-name-"+id).html(stageName(stage));
                            $(".state-name-"+id).html(stateName(stage_state));

                        }else{
                            $(".mensaje-ajax").html("Ocurrió un error al actualizar. Error:"+data);
                        }

                    },error:function(data){
                        console.log(data);
                    }
                });
            }else{

                $(".mensaje-ajax").html("No se actualizó postulación.");
            }



        });

        function stateName(state) {

            state = parseInt(state);

            let state_name = "No revisado";

            switch (state) {
                case 1:
                    state_name = "No revisado";
                    break;
                case 2:
                    state_name = "Alquiler aprobado";
                    break;
                case 3:
                    state_name = "Descartado";
                    break;
            }

            return state_name;
        }

        function stageName(stage) {

            stage = parseInt(stage);

            let stage_name = "Postulación";

            switch (stage) {
                case 1:
                    stage_name = "Postulación";
                    break;
                case 2:
                    stage_name = "Pre-aprobado";
                    break;
                case 3:
                    stage_name = "Visita";
                    break;
                case 4:
                    stage_name = "Aprobado";
                    break;
                case 5:
                    stage_name = "Contrato firmado";
                    break;
            }

            return stage_name;

        }

    </script>
@endpush
