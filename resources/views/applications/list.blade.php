@extends('layouts.app')


@section('title')
    @include('partials.genericJumbotron', [
        'title' => __("Postulaciones"),
        'icon' => "list-alt"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Inmueble</th>
                    <th scope="col">Últ. Post.</th>
                    <th scope="col">Cant.</th>
                    <th scope="col">Monto</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($applications as $application)
                    <tr>
                        <td>{{ $application->property->id }}</td>
                        <td>{{ $application->property->name }}</td>
                        <td>
                            {{ date('d/m/Y', strtotime($application->date)) }}
                        </td>
                        <td>
                            {{ $application->postulaciones }}
                        </td>
                        <td>S/{{ number_format($application->property->rental_amount, 2, '.', "'") }}</td>
                        <td>
                            <a
                                class="btn-homex btn-homex-clear m-1"
                                href="{{ route('applications.listProperty', $application->property->id ) }}"
                            >Postulaciones</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay postulaciones disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center">
            {{ $applications->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection

@push('scripts')
    <script>

        $(document).on('click', '.modificar', function(){


        });

    </script>
@endpush
