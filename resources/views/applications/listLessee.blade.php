@extends('layouts.app')


@section('title')
    @include('partials.genericJumbotron', [
        'title' => __("Postulaciones"),
        'icon' => "list-alt"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Inmueble</th>
                    <th scope="col">Postulado</th>
                    <th scope="col">Etapa</th>
                    <th scope="col">Monto</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($applications as $application)
                    <tr>
                        <td>{{ $application->id }}</td>
                        <td>{{ $application->property->name }}</td>
                        <td>
                            {{ date('d/m/Y', strtotime($application->date)) }}</td>
                        <td>
                            @switch($application->stage)
                                @case(1)
                                <span>Postulación</span>
                                @break
                                @case(2)
                                <span>Pre-aprobado</span>
                                @break
                                @case(3)
                                <span>Visita</span>
                                @break
                                @case(4)
                                <span>Alquiler aprobado</span>
                                @break
                                @case(5)
                                <span>Contrato firmado</span>
                                @break

                                @default
                                <span>Inicial</span>
                            @endswitch
                        </td>
                        <td>S/{{ number_format($application->property->rental_amount, 2, '.', "'") }}</td>
                        <td>
                            <a
                                class="btn-homex btn-homex-clear m-1"
                                href="{{ route('applications.status', $application->id ) }}"
                            >Postulación</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay postulaciones disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center">
            {{ $applications->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection

@push('scripts')
    <script>

        $(document).on('click', '.modificar', function(){


        });

    </script>
@endpush
