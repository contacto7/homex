@extends('layouts.app')

@section('jumbotron')
    @include('partials.genericJumbotron', [
        'title' => __("Mi postulación"),
        'icon' => "home"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="col-xs-12">
            <div class="property-detail-wrapp">
                <h2 class="property-detail-title">
                    {{ $application->property->name }}
                </h2>
                <div class="property-detail-subtitle">
                    {{ $application->property->address }}
                    {{ $application->property->district->name }}, {{ $application->property->district->province->name }}.
                    <a href="{{ route('properties.view', $application->property->id) }}">Ver inmueble.</a>
                </div>

                <h3 class="property-detail-sub3 mt-4 mb-4">Estado</h3>

                <div class="application-status-wrapp">
                    <div class="application-status">

                        <!-- FASE 1 -->
                        <div class="application-status-phase phase-1">
                            <div class="asp-title">
                                Postulación
                            </div>
                            <div class="asp-indicator-wrapp">
                                <div class="asp-indicator">
                                    <span class="fa fa-check"></span>
                                    <span class="fa fa-times"></span>
                                </div>
                            </div>
                        </div>
                        <!-- FASE 2 -->
                        <div class="application-status-phase phase-2">
                            <div class="asp-title">
                                Pre-aprobado
                            </div>
                            <div class="asp-indicator-wrapp">
                                <div class="asp-indicator">
                                    <span class="fa fa-check"></span>
                                    <span class="fa fa-times"></span>
                                </div>
                            </div>
                        </div>
                        <!-- FASE 3 -->
                        <div class="application-status-phase phase-3">
                            <div class="asp-title">
                                Visita
                            </div>
                            <div class="asp-indicator-wrapp">
                                <div class="asp-indicator">
                                    <span class="fa fa-check"></span>
                                    <span class="fa fa-times"></span>
                                </div>
                            </div>
                        </div>
                        <!-- FASE 4 -->
                        <div class="application-status-phase phase-4">
                            <div class="asp-title">
                                Alquiler aprobado
                            </div>
                            <div class="asp-indicator-wrapp">
                                <div class="asp-indicator">
                                    <span class="fa fa-check"></span>
                                    <span class="fa fa-times"></span>
                                </div>
                            </div>
                        </div>
                        <!-- FASE 5 -->
                        <div class="application-status-phase phase-5">
                            <div class="asp-title">
                                Contrato firmado
                            </div>
                            <div class="asp-indicator-wrapp">
                                <div class="asp-indicator">
                                    <span class="fa fa-check"></span>
                                    <span class="fa fa-times"></span>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        </div>

    </div>
@endsection

@push('scripts')
    <script>

        var application = @json($application);
        var aprobado = 2;
        var desaprobado = 3;

        $(function() {

            //console.log(application);
            console.log(application['stage']);
            console.log(application['stage_state']);

            var stage = application['stage'];
            //DISMINUIMOS PARA QUE EMPIECE EN CERO
            stage--;
            var stage_state = application['stage_state'];

            $( ".application-status-phase" ).each(function(index, element) {

                if(index < stage){
                    $( this ).addClass( "phase-approved" );
                }else if(index == stage){

                    if(!index){
                        $( this ).addClass( "phase-approved" );
                    }


                    if(stage_state == aprobado){
                        $( this ).addClass( "phase-approved" );
                    }else if(stage_state == desaprobado){
                        $( this ).addClass( "phase-disapproved" );
                    }

                }
            });


        });


    </script>
@endpush
