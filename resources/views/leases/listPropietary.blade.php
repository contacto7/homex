@extends('layouts.app')


@section('title')
    @include('partials.genericJumbotron', [
        'title' => __("Alquileres"),
        'icon' => "home"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">Inmueble</th>
                    <th scope="col">Creado</th>
                    <th scope="col">Monto</th>
                    <th scope="col">Contrato</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($leases as $lease)
                    <tr>
                        <td>{{ $lease->application->property->name }}</td>
                        <td>
                            {{ date('d/m/Y', strtotime($lease->created_at)) }}
                        </td>
                        <td>
                            @if($lease->lastContract())
                                S/{{ number_format($lease->lastContract()->amount, 2, '.', "'") }}
                            @else
                                S/{{ number_format($lease->application->property->rental_amount, 2, '.', "'") }}
                            @endif
                        </td>
                        <td>
                            @if($lease->lastContract())
                                {{ $lease->lastContract()->state == 1 ? 'Activo':'Inactivo' }}
                            @else
                                Sin contrato
                            @endif
                        </td>
                        <td>
                            <a
                                class="btn-homex btn-homex-clear mb-1 mt-0 p-2"
                                href="{{ route('contracts.listLease', $lease->id ) }}"
                            >Contratos</a>
                            <a
                                class="btn-homex btn-homex-clear mb-1 mt-0 p-2"
                                href="{{ route('properties.view', $lease->application->property->id ) }}"
                            >Inmueble</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay arrendamientos disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center">
            {{ $leases->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection

@push('scripts')
    <script>

        $(document).on('click', '.modificar', function(){


        });

    </script>
@endpush
