@extends('layouts.app')

@section('jumbotron')
    @include('partials.genericJumbotron', [
        'title' => __("PREGUNTAS FRECUENTES"),
        'icon' => "question-circle"
    ])
@endsection

@section('content')
    <div class="pl-5 pr-5">
        <div class="about-us-wrapper">
            <h2>¿Que hacemos?</h2>
            <p>
                Somos la única comunidad en el Perú que ayuda exclusivamente a a arrendador y arrendatario a poder encontrarse e interactuar de una manera ágil, segura, transparente e informada, para la toma de una decisión de arrendamiento.
            </p>
            <h2>¿Como lo hacemos?</h2>
            <p>
                Conectamos a los arrendadores y arrendatarios a través de la tecnología, realizamos un estudio detallado del perfil del futuro inquilino, que incluye su historial crediticio, antecedentes, y demás información relevante para la toma de decisiones.
            </p>
            <h2>¿Cuáles son mis beneficios?</h2>
            <h3><i>Para el arrendador:</i></h3>
            <ul>
                <li>Publicitar su inmueble a toda la comunidad.</li>
                <li>Contar con el Reporte de Perfil del futuro inquilino, mediante el Reporte correspondiente.</li>
                <li>Uso del contrato de arrendamiento, bajo el formato que corresponda.</li>
                <li>Gestión mensual del pago de las deudas.</li>
                <li>Alertas de pago a los arrendatarios.</li>
                <li>Envió de comunicaciones en caso de incumplimiento en el pago.</li>
            </ul>
            <h3><i>Para el arrendatario:</i></h3>
            <ul>
                <li>Seleccionar el inmueble de forma rápida y eficiente.</li>
                <li>Acreditar su buen perfil como inquilino, mediante el reporte correspondiente.</li>
                <li>Uso del contrato de arrendamiento, bajo el formato correspondiente.</li>
                <li>Recordatorios de pago para evitar intereses o penalidades.</li>
            </ul>
            <h2>¿Como funciona?</h2>
            <p>
                Regístrate en nuestra plataforma y formarás parte de esta comunidad.
            </p>
        </div>
    </div>
@endsection

@push('scripts')
    <script>


    </script>
@endpush
