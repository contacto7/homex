@extends('layouts.app')

@section('jumbotron')
    @include('partials.jumbotronCreate')
@endsection

@section('content')
    <div class="container container-accounting-admin">
        <form
            method="POST"
            action="{{ ! $property->id ? route('properties.store'): route('properties.update', $property->id) }}"
            novalidate
        >
            @if($property->id)
                @method('PUT')
            @endif

            @csrf

            <div class="form-group">
                <label for="name">Nombre de propiedad</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('name') ? 'is-invalid': '' }}"
                    name="name"
                    id="name"
                    placeholder=""
                    value="{{ old('name') ?: $property->name }}"
                    required
                >
                @if($errors->has('name'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="description">Descripción</label>
                <textarea
                    type="text"
                    class="form-control {{ $errors->has('description') ? 'is-invalid': '' }}"
                    name="description"
                    id="description"
                    required
                >{{ old('description') ?: $property->description }}</textarea>
                @if($errors->has('description'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="rooms">Cuartos</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('rooms') ? 'is-invalid': '' }}"
                    name="rooms"
                    id="rooms"
                    placeholder=""
                    value="{{ old('rooms') ?: $property->rooms }}"
                    required
                >
                @if($errors->has('rooms'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('rooms') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <label for="district_id">Distrito</label>
                <select
                    style="text-transform: capitalize"
                    class="form-control {{ $errors->has('district_id') ? 'is-invalid': '' }}"
                    name="district_id"
                    id="district_id"
                >
                    @foreach(\App\District::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('district_id') === $id || $property->district_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="property_type_id">Tipo de propiedad</label>
                <select
                    style="text-transform: capitalize"
                    class="form-control {{ $errors->has('property_type_id') ? 'is-invalid': '' }}"
                    name="property_type_id"
                    id="property_type_id"
                >
                    @foreach(\App\PropertyType::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('property_type_id') === $id || $property->property_type_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="parking_slots">Estacionamientos</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('parking_slots') ? 'is-invalid': '' }}"
                    name="parking_slots"
                    id="parking_slots"
                    placeholder=""
                    value="{{ old('parking_slots') ?: $property->parking_slots }}"
                    required
                >
                @if($errors->has('parking_slots'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('parking_slots') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="total_area">Área total</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('total_area') ? 'is-invalid': '' }}"
                    name="total_area"
                    id="total_area"
                    placeholder=""
                    value="{{ old('total_area') ?: $property->total_area }}"
                    required
                >
                @if($errors->has('total_area'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('total_area') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="covered_area">Área construida</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('covered_area') ? 'is-invalid': '' }}"
                    name="covered_area"
                    id="covered_area"
                    placeholder=""
                    value="{{ old('covered_area') ?: $property->covered_area }}"
                    required
                >
                @if($errors->has('covered_area'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('covered_area') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="bathrooms">Baños completos</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('bathrooms') ? 'is-invalid': '' }}"
                    name="bathrooms"
                    id="bathrooms"
                    placeholder=""
                    value="{{ old('bathrooms') ?: $property->bathrooms }}"
                    required
                >
                @if($errors->has('bathrooms'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('bathrooms') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="half_bathrooms">Medios baños</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('half_bathrooms') ? 'is-invalid': '' }}"
                    name="half_bathrooms"
                    id="half_bathrooms"
                    placeholder=""
                    value="{{ old('half_bathrooms') ?: $property->half_bathrooms }}"
                    required
                >
                @if($errors->has('half_bathrooms'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('half_bathrooms') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="year">Año de construcción</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('year') ? 'is-invalid': '' }}"
                    name="year"
                    id="year"
                    placeholder=""
                    value="{{ old('year') ?: $property->year }}"
                    required
                >
                @if($errors->has('year'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('year') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="rental_amount">Renta mensual</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('rental_amount') ? 'is-invalid': '' }}"
                    name="rental_amount"
                    id="rental_amount"
                    placeholder=""
                    value="{{ old('rental_amount') ?: $property->rental_amount }}"
                    required
                >
                @if($errors->has('rental_amount'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('rental_amount') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group" style="display: none">
                <label for="latitude">Latitud</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('latitude') ? 'is-invalid': '' }}"
                    name="latitude"
                    id="latitude"
                    placeholder=""
                    value="1"
                    required
                >
                @if($errors->has('latitude'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('latitude') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group" style="display: none">
                <label for="longitude">Longitud</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('longitude') ? 'is-invalid': '' }}"
                    name="longitude"
                    id="longitude"
                    placeholder=""
                    value="1"
                    required
                >
                @if($errors->has('longitude'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('longitude') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group">
                <label for="address">Dirección</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('address') ? 'is-invalid': '' }}"
                    name="address"
                    id="address"
                    placeholder=""
                    value="{{ old('address') ?: $property->address }}"
                    required
                >
                @if($errors->has('address'))
                    <span class="invalid-feedback">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
                @endif
            </div>

            <div class="form-group">
                <label for="only_genre">Permitidos</label>
                <select
                    class="form-control {{ $errors->has('only_genre') ? 'is-invalid': '' }}"
                    name="only_genre"
                    id="only_genre"
                >
                    <option
                        {{
                            (int) old('only_genre') === \App\Property::ALL
                            ||
                            (int) $property->only_genre === \App\Property::ALL
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::ALL }}"
                    >Ambos géneros</option>
                    <option
                        {{
                            (int) old('only_genre') === \App\Property::WOMEN_ONLY
                            ||
                            (int) $property->only_genre === \App\Property::WOMEN_ONLY
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::WOMEN_ONLY }}"
                    >Sólo mujeres</option>
                    <option
                        {{
                            (int) old('only_genre') === \App\Property::MAN_ONLY
                            ||
                            (int) $property->only_genre === \App\Property::MAN_ONLY
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::MAN_ONLY }}"
                    >Sólo hombres</option>
                </select>
            </div>


            <div class="form-group">
                <label for="" class="">Detalles adicionales</label>
                <div class="col-md-6">
                    <div class="form-check">
                        <input
                            class="form-check-input"
                            type="checkbox"
                            data-search="young_bonus"
                            data-id="{{ \App\Property::YB_QUALIFIED }}"
                            id="young_bonus">
                        <label
                            class="form-check-label checkbox-filter-label"
                            for="young_bonus"
                        >Bono renta joven </label>
                    </div>
                    <div class="form-check">
                        <input
                            class="form-check-input"
                            type="checkbox"
                            data-search="only_students"
                            data-id="{{ \App\Property::STUDENTS_ONLY }}"
                            id="only_students">
                        <label
                            class="form-check-label checkbox-filter-label"
                            for="only_students"
                        >Para estudiantes </label>
                    </div>
                    <div class="form-check">
                        <input
                            class="form-check-input"
                            type="checkbox"
                            data-search="pet_allowed"
                            data-id="{{ \App\Property::PET_ALLOWED }}"
                            id="pet_allowed">
                        <label
                            class="form-check-label checkbox-filter-label"
                            for="pet_allowed"
                        >Se permiten mascotas </label>
                    </div>
                </div>
            </div>
            <div class="form-group" style="display: none">
                <label for="rent_limit_id">Límite de renta</label>
                <select
                    style="text-transform: capitalize"
                    class="form-control {{ $errors->has('rent_limit_id') ? 'is-invalid': '' }}"
                    name="rent_limit_id"
                    id="rent_limit_id"
                >
                    @php $temp = 0; @endphp
                    @foreach(\App\RentLimit::orderByDesc('id')->get() as $rentLimit)
                        <option
                            {{ $temp == 0 ? 'select':'' }}
                            value="{{ $rentLimit->id }}"
                        >{{ $rentLimit->lower_rent." - ".$rentLimit->upper_rent }}</option>
                        @php $temp++; @endphp
                    @endforeach
                </select>
            </div>



            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>
            </div>

        </form>

    </div>
@endsection

@push('scripts')
    <script>
        $(function() {
            $('#picture').change(function() {
                let filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
                $('.custom-file-label').html(filename);
            });
        });

    </script>
@endpush
