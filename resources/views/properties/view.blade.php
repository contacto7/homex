@extends('layouts.app')

@section('jumbotron')
    @include('partials.properties.jumbotron')
@endsection

@section('content')
    <div class="container">
        <div class="col-xs-12">
            <div class="property-detail-wrapp">
                <h2 class="property-detail-title">{{ $property->name }}</h2>
                <div class="property-detail-subtitle">{{ $property->description }}</div>
                <h3 class="property-detail-sub3 mt-2">Detalles</h3>

                <div class="property-detail-row">
                    <div class="property-detail-characteristic">Dirección: </div>
                    <div class="property-detail-description">{{ $property->address }}</div>
                </div>
                <div class="property-detail-row">
                    <div class="property-detail-characteristic">Ubicación: </div>
                    <div class="property-detail-description">{{ $property->district->name }}, {{ $property->district->province->name }}</div>
                </div>
                <div class="property-detail-row">
                    <div class="property-detail-characteristic">Año de construcción: </div>
                    <div class="property-detail-description">{{ date('Y', strtotime($property->year)) }}</div>
                </div>
                <div class="property-detail-row">
                    <div class="property-detail-characteristic">Área total: </div>
                    <div class="property-detail-description">{{ $property->total_area }} m<sup>2</sup></div>
                </div>
                <div class="property-detail-row">
                    <div class="property-detail-characteristic">Número de habitaciones: </div>
                    <div class="property-detail-description">{{ $property->rooms }}</div>
                </div>
                <div class="property-detail-row">
                    <div class="property-detail-characteristic">Baños completos: </div>
                    <div class="property-detail-description">{{ $property->bathrooms }}</div>
                </div>
                <div class="property-detail-row">
                    <div class="property-detail-characteristic">Medios baños: </div>
                    <div class="property-detail-description">{{ $property->half_bathrooms }}</div>
                </div>
                <div class="property-detail-row">
                    <div class="property-detail-characteristic">Número de estacionamientos: </div>
                    <div class="property-detail-description">{{ $property->parking_slots }}</div>
                </div>
                <div class="property-detail-row">
                    <div class="property-detail-characteristic">Monto de renta mensual: </div>
                    <div class="property-detail-description">{{ $property->currencyRentalAmount->symbol }}{{ $property->rental_amount }}</div>
                </div>
                <div class="property-detail-row">
                    <div class="property-detail-characteristic">Monto de mantenimiento mensual: </div>
                    <div class="property-detail-description">{{ $property->currencyMaintenanceAmount->symbol }}{{ $property->rental_amount }}</div>
                </div>
                <div class="property-detail-row">
                    <div class="property-detail-characteristic">Monto de la garantía: </div>
                    <div class="property-detail-description">{{ $property->currencyGuarantyAmount->symbol }}{{ $property->guaranty_amount }}</div>
                </div>


                <div class="property-detail-row">
                    <div class="property-detail-characteristic">Añadido: </div>
                    <div class="property-detail-description">{{ date('d-m-Y', strtotime($property->created_at)) }}</div>
                </div>

                <h3 class="property-detail-sub3 mt-2">Perfil del arrendatario</h3>

                @if($property->pet_allowed == \App\Property::PET_ALLOWED)
                <div class="property-detail-row">
                    <div class="property-detail-characteristic">Mascotas permitidas</div>
                </div>
                @endif
                @if($property->only_genre == \App\Property::WOMEN_ONLY)
                <div class="property-detail-row">
                    <div class="property-detail-characteristic">Para mujeres</div>
                </div>
                @elseif($property->only_genre == \App\Property::MAN_ONLY)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">Para varones</div>
                    </div>
                @elseif($property->only_genre == \App\Property::WOMAN_PREFERRED)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">De preferencia para mujeres</div>
                    </div>
                @elseif($property->only_genre == \App\Property::MAN_PREFERRED)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">De preferencia para varones</div>
                    </div>
                @endif
                @if($property->only_students == \App\Property::STUDENTS_ONLY)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">Para estudiantes</div>
                    </div>
                @elseif($property->only_genre == \App\Property::STUDENTS_PREFERRED)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">Estudiantes de preferencia</div>
                    </div>
                @endif
                @if($property->young_bonus == \App\Property::YB_QUALIFIED)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">CALIFICADO PARA BONO RENTA JOVEN</div>
                    </div>
                @elseif($property->only_genre == \App\Property::YB_PREFERRED)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">Calificado para bono renta joven de preferencia</div>
                    </div>
                @endif
                @if($property->family_status == \App\Property::COUPLE_WITH_KIDS_ONLY)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">Sólo parejas con niños</div>
                    </div>
                @elseif($property->family_status == \App\Property::COUPLE_WITH_KIDS_PREFERRED)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">Parejas con niños de preferencia</div>
                    </div>
                @endif
                @if($property->age_range == \App\Property::AGE_18_30_ONLY)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">Sólo para personas de 18 a 30 años</div>
                    </div>
                @elseif($property->age_range == \App\Property::AGE_31_45_ONLY)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">Sólo para personas de 31 a 45 años</div>
                    </div>
                @elseif($property->age_range == \App\Property::AGE_45_ONLY)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">Sólo para personas mayores a 45 años</div>
                    </div>
                @elseif($property->age_range == \App\Property::AGE_18_30_PREFERRED)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">Para personas de 18 a 30 años de preferencia</div>
                    </div>
                @elseif($property->age_range == \App\Property::AGE_31_45_PREFERRED)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">Para personas de 31 a 45 años de preferencia</div>
                    </div>
                @elseif($property->age_range == \App\Property::AGE_45_PREFERRED)
                    <div class="property-detail-row">
                        <div class="property-detail-characteristic">Para personas mayores a 45 de preferencia</div>
                    </div>
                @endif

            </div>

        </div>

        <div class="col-xs-12 text-center">
            @include('partials.properties.lowerInterestedApplication')
        </div>

        <div class="col-xs-12">
            <div class="propiedades-relacionadas-wrapp">
                @include('partials.properties.related')
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>

        $(function() {
            //DEFINIMOS LOS ARREGLOS DISTRITOS Y TIPOS DE INMUEBLES COMO JSON
            var property_id = {{ $property->id }};
            var property_images = @json($property_images);
            let carousel_id = "carouselPropertyImages";

            console.log(property_images);

            populateCarouselIndicators(property_images, carousel_id);
            populateCarouselItems(property_images, carousel_id);
            anadirBusqueda(property_id);

            function populateCarouselIndicators(class_json, carousel_id) {
                let carousel = "#"+carousel_id;
                let indicators = $(carousel + ' .carousel-indicators');
                let temp_indicators = 0;

                //VACIAMOS EL ELEMENTO
                indicators.empty();

                //LE AGREGAMOS LAS OPCIONES AL RECORRER EL JSON CLASS
                $.each(class_json, function (key, element) {

                    let indicator = $('<li></li>').attr({
                        "data-target":carousel,
                        "data-slide-to":temp_indicators
                    });

                    if(!temp_indicators){
                        indicator.addClass("active");
                    }

                    indicator.appendTo(indicators);


                    //console.log(element.id);
                    temp_indicators++;
                });
            }

            function populateCarouselItems(class_json, carousel_id) {
                let carousel = "#"+carousel_id;
                let items = $(carousel + ' .carousel-inner');
                let temp_items = 0;

                //VACIAMOS EL ELEMENTO
                items.empty();

                //LE AGREGAMOS LAS OPCIONES AL RECORRER EL JSON CLASS
                $.each(class_json, function (key, element) {

                    let item = $('<div></div>').addClass('carousel-item');

                    if(!temp_items){
                        item.addClass("active");
                    }
                    //console.log(element.id);

                    //AÑADIMOS LA IMAGEN
                    item.append($('<img/>').attr({
                        "class": "jumnbotron-property-img",
                        "alt": "jumnbotron-property-img",
                        "src": "/images/properties/"+element.picture
                    }));

                    item.appendTo(items);

                    temp_items++;
                });

                //SI NO HAY ITEMS
                if(!temp_items){
                    items.html("No hay imágenes disponibles.");
                }
            }


            function anadirBusqueda(property_id){
                $.ajax({
                    type: 'GET', //THIS NEEDS TO BE GET
                    url: '{{ route('properties.addSearch') }}',
                    data: {
                        property_id: property_id
                    },
                    success: function (data) {
                        if(data.includes("Error")){
                            console.log("Ocurrió algún problema agregando la búsqueda.");
                        }else{
                            console.log("Se agregó la búsqueda.");
                        }

                    },error:function(){
                        console.log(data);
                    }
                });
            }


        });

    </script>
@endpush
