@extends('layouts.app')

@section('jumbotron')
    @include('partials.jumbotron', [
        'title' => __("Accede a cualquier curso de inmediato"),
        'icon' => "th"
    ])
@endsection

@section('content')
    @include('partials.properties.breadcrumb')
    <div class="pl-5 pr-5">
        <div class="row justify-content-center">
            @forelse($properties as $property)
                <div class="col-lg-4 col-md-6 col-sm-12 d-flex align-items-stretch">
                    @include('partials.properties.card_properties')
                </div>
            @empty
                {{ __("No hay propiedades disponibles") }}
            @endforelse
        </div>
        <div class="row justify-content-center">
            {{ $properties->appends(request()->except('page'))->links() }}
        </div>
    </div>
    @include('partials.modals.modalFiltroBusqueda')
@endsection

@push('scripts')
<script>

$(function() {

    //DEFINIMOS LOS ARREGLOS DISTRITOS Y TIPOS DE INMUEBLES COMO JSON
    var types = @json($types_list);
    var districts = @json($districts_list);

    var types_select = "types";
    var districts_select = "districts";

    //CHECKBOX ATTRS
    ////PROPERTY TYPES
    var checkbox_wrapp_1 = "checkbox-type-wrapp";
    var checkbox_filter_1 = "property_type_id";
    ////DISTRICTS
    var checkbox_wrapp_2 = "checkbox-district-wrapp";
    var checkbox_filter_2 = "district_id";

    populateSelectOptions(types , types_select);
    populateSelectOptions(districts , districts_select);

    populateCheckBox(types , checkbox_wrapp_1, checkbox_filter_1);
    populateCheckBox(districts , checkbox_wrapp_2, checkbox_filter_2);


    //LLENAR LOS FILTROS
    $( ".filtro-array" ).each(function() {
        var $element = $(this);
        let filter_id = parseInt($element.data('id'));
        let filter_type = parseInt($element.data('type'));
        let filter_name;

        console.log(filter_type);

        if(filter_type === 1){
            filter_name = getNameFromJson(types, filter_id);
        }else if(filter_type === 2){
            filter_name = getNameFromJson(districts, filter_id);
        }


        $("#filtro-"+filter_type+"-"+filter_id).html(filter_name+" x");
    });

    //AGREGAMOS LOS FILTROS A LOS PARÁMETROS - CHECKBOX
    $(".btn-filtrar-adicionales").click(function(){
        let url_busqueda="/properties/list?";
        let types="types=";
        let districts="&districts=";
        let young_bonus = "&bono_renta_joven=";
        let only_students = "&solo_estudiantes=";
        let pet_allowed = "&mascotas_permitidas=";
        let only_genre = "&genero=";
        let max_amount = "&precio_maximo=";
        let min_amount = "&precio_minimo=";
        let rooms = "&habitaciones=";

        $( ".chkbx-input" ).each(function() {
            $obj = $(this);
            console.log("CHECKBOX");

            let data_search = $obj.data("search");
            let data_id = parseInt($obj.data("id"));
            let data_status = $obj.is(':checked');

            let url_busqueda;

            if(data_status){
                console.log("Sarch: "+ data_search);
                console.log("Id: "+ data_id);
                console.log("Status: "+ data_status);

                if(data_search === "property_type_id"){
                    types+=(data_id+",");
                }else if(data_search === "district_id"){
                    districts+=(data_id+",");
                }else if(data_search === "young_bonus"){
                    young_bonus+=(data_id);
                }else if(data_search === "only_students"){
                    only_students+=(data_id);
                }else if(data_search === "pet_allowed"){
                    pet_allowed+=(data_id);
                }else if(data_search === "only_genre"){
                    only_genre+=(data_id);
                }

            }
        });

        //AGREGAMOS LOS FILTROS A LOS PARÁMETROS - INPUT
        let min_amount_input = $("#amount-min").val();
        let max_amount_input = $("#amount-max").val();
        let rooms_input = $("#rooms").val();

        if(min_amount_input > 0){
            min_amount+=min_amount_input;
        }
        if(max_amount_input > 0){
            max_amount+=max_amount_input;
        }
        if(rooms_input > 0){
            rooms+=rooms_input;
        }

        //CONSTRUIMOS LA URL
        url_busqueda+=types;
        url_busqueda+=districts;
        url_busqueda+=young_bonus;
        url_busqueda+=only_students;
        url_busqueda+=pet_allowed;
        url_busqueda+=only_genre;
        url_busqueda+=max_amount;
        url_busqueda+=min_amount;
        url_busqueda+=rooms;

        console.log(url_busqueda);

        //REDIRIGIMOS A LA URL CON LOS FILTROS CONSTRUIDOS
        window.location.href = url_busqueda;

    });

    //PARA MUJERES o PARA HOMBRES EXCLUYENTES
    $('#only_genre,#only_genre_h').change(function() {
        var genero = $(this).data("id");
        var checked_check = $(this).prop("checked");

        if(genero == 2 && checked_check){
            $("#only_genre_h").prop("checked", false);
        }else if(genero == 3 && checked_check){
            $("#only_genre").prop("checked", false);
        }
    });

    function populateSelectOptions(class_json , select_id) {
        let dropdown = $('#'+select_id);

        //VACIAMOS EL ELEMENTO
        dropdown.empty();

        //LE AGREGAMOS LA OPCION DE SELECCIONAR
        dropdown.append('<option selected="true" value="">Seleccionar</option>');
        dropdown.prop('selectedIndex', 0);

        //LE AGREGAMOS LAS OPCIONES AL RECORRER EL JSON CLASS
        $.each(class_json, function (key, element) {
            dropdown.append($('<option></option>').attr('value', element.id).text(element.name));
            //console.log(element.id);
        });
    }

    function populateCheckBox(class_json , checkbox_wrapp_id, data_search) {
        let checkbox_wrapp = $('#'+checkbox_wrapp_id);

        //VACIAMOS EL ELEMENTO
        checkbox_wrapp.empty();

        //CREAMOS EL CHECK FORM, Y DENTRO AGREGAMOS
        ////EL INPUT CREADO
        ////EL LABEL CREADO
        let outer_form_check_temp = $('<div></div>').attr('class', 'form-check form-check-inline');
        let input_form_check_temp = $('<input></input>').attr({
            class: "form-check-input chkbx-input",
            type: "checkbox",
            "data-search":data_search
        });
        let label_form_check_temp = $('<label></label>').attr({
            class: "form-check-label checkbox-filter-label"
        });

        //LE AGREGAMOS LAS OPCIONES AL RECORRER EL JSON CLASS
        $.each(class_json, function (key, element) {
            //COPIAMOS LOS ELEMENTOS CREADOS
             let outer_form_check = outer_form_check_temp.clone();
             let input_form_check = input_form_check_temp.clone();
             let label_form_check = label_form_check_temp.clone();

             //AGARRAMOS LOS DATOS DEL MODELO
            let data_id = element.id;
            let data_name = element.name;

            //AGREGAMOS LOS ATRIBUTOS FALTANTES AL INPUT Y AL LABEL
            input_form_check.attr({
                "data-id": data_id,
                id:data_search+data_name,
            });
            label_form_check.attr({
                "for": data_search+data_name
            }).text(data_name);

            //outer_form_check.clone().appendTo(checkbox_wrapp);
            checkbox_wrapp.append(outer_form_check);
            outer_form_check.append(input_form_check);
            outer_form_check.append(label_form_check);
        });
    }

    function getNameFromJson(class_json, filter_id) {
        let filter_name = "name-not-found";
        $.each(class_json, function(i, v) {
            if (v.id == filter_id) {
                filter_name = v.name;
            }
        });
        return filter_name;
    }

});

</script>
@endpush
