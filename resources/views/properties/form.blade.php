@extends('layouts.app')

@section('jumbotron')
    @include('partials.jumbotronCreate')
@endsection

@section('content')
    <div class="container container-accounting-admin">
        <form
            method="POST"
            action="{{ ! $property->id ? route('properties.store'): route('properties.update', $property->id) }}"
            novalidate
            class="row"
        >
            @if($property->id)
                @method('PUT')
            @endif

            @csrf

            <div class="form-group col-sm-12">
                <label for="name">Título de anuncio</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('name') ? 'is-invalid': '' }}"
                    name="name"
                    id="name"
                    placeholder=""
                    value="{{ old('name') ?: $property->name }}"
                    required
                >
                @if($errors->has('name'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group col-sm-12">
                <label for="description">Descripción (Máximo 500 caracteres).</label> <div class="d-inline-block badge" id="dscr-msg"></div>
                <textarea
                    type="text"
                    class="form-control {{ $errors->has('description') ? 'is-invalid': '' }}"
                    name="description"
                    id="description"
                    required
                >{{ old('description') ?: $property->description }}</textarea>
                @if($errors->has('description'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group col-sm-6">
                <label for="rooms">Número de habitaciones</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('rooms') ? 'is-invalid': '' }}"
                    name="rooms"
                    id="rooms"
                    min="0"
                    placeholder=""
                    value="{{ old('rooms') ?: $property->rooms }}"
                    required
                >
                @if($errors->has('rooms'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('rooms') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group col-sm-6">
                <label for="district_id">Distrito</label>
                <select
                    style="text-transform: capitalize"
                    class="form-control {{ $errors->has('district_id') ? 'is-invalid': '' }}"
                    name="district_id"
                    id="district_id"
                >
                    @foreach(\App\District::with('province')->get() as $district)
                        <option
                            {{ (int) old('district_id') === $district->id || $property->district_id === $district->id ? 'selected' : '' }}
                            value="{{ $district->id }}"
                        >{{ $district->name.", ".$district->province->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-sm-6">
                <label for="property_type_id">Tipo de propiedad</label>
                <select
                    style="text-transform: capitalize"
                    class="form-control {{ $errors->has('property_type_id') ? 'is-invalid': '' }}"
                    name="property_type_id"
                    id="property_type_id"
                >
                    @foreach(\App\PropertyType::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('property_type_id') === $id || $property->property_type_id === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-sm-6">
                <label for="parking_slots">Número de estacionamientos</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('parking_slots') ? 'is-invalid': '' }}"
                    name="parking_slots"
                    id="parking_slots"
                    min="0"
                    placeholder=""
                    value="{{ old('parking_slots') ?: $property->parking_slots }}"
                    required
                >
                @if($errors->has('parking_slots'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('parking_slots') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group col-sm-6">
                <label for="total_area">Área total (m<sup>2</sup>)</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('total_area') ? 'is-invalid': '' }}"
                    name="total_area"
                    id="total_area"
                    placeholder=""
                    value="{{ old('total_area') ?: $property->total_area }}"
                    required
                >
                @if($errors->has('total_area'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('total_area') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group col-sm-6">
                <label for="bathrooms">Baños completos</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('bathrooms') ? 'is-invalid': '' }}"
                    name="bathrooms"
                    id="bathrooms"
                    min="0"
                    placeholder=""
                    value="{{ old('bathrooms') ?: $property->bathrooms }}"
                    required
                >
                @if($errors->has('bathrooms'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('bathrooms') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group col-sm-6">
                <label for="half_bathrooms">Medios baños</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('half_bathrooms') ? 'is-invalid': '' }}"
                    name="half_bathrooms"
                    id="half_bathrooms"
                    min="0"
                    placeholder=""
                    value="{{ old('half_bathrooms') ?: $property->half_bathrooms }}"
                    required
                >
                @if($errors->has('half_bathrooms'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('half_bathrooms') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group col-sm-6">
                <label for="year">Año de construcción</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('year') ? 'is-invalid': '' }}"
                    name="year"
                    id="year"
                    min="1900"
                    placeholder=""
                    value="{{ old('year') ?: $property->year }}"
                    required
                >
                @if($errors->has('year'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('year') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group col-sm-6">
                <label for="currency_rental_amount">Moneda de renta mensual</label>
                <select
                    class="form-control {{ $errors->has('currency_rental_amount') ? 'is-invalid': '' }}"
                    name="currency_rental_amount"
                    id="currency_rental_amount"
                    required
                >
                    @foreach(\App\Currency::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('currency_rental_amount') === $id || $property->currency_rental_amount === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-sm-6">
                <label for="rental_amount">Monto de renta mensual</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('rental_amount') ? 'is-invalid': '' }}"
                    name="rental_amount"
                    id="rental_amount"
                    min="0"
                    placeholder=""
                    value="{{ old('rental_amount') ?: $property->rental_amount }}"
                    required
                >
                @if($errors->has('rental_amount'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('rental_amount') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group col-sm-6">
                <label for="currency_maintenance_amount">Moneda de monto de mantenimiento mensual</label>
                <select
                    class="form-control {{ $errors->has('currency_maintenance_amount') ? 'is-invalid': '' }}"
                    name="currency_maintenance_amount"
                    id="currency_maintenance_amount"
                    required
                >
                    @foreach(\App\Currency::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('currency_maintenance_amount') === $id || $property->currency_maintenance_amount === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-sm-6">
                <label for="maintenance_amount">Monto de mantenimiento mensual</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('maintenance_amount') ? 'is-invalid': '' }}"
                    name="maintenance_amount"
                    id="maintenance_amount"
                    min="0"
                    placeholder=""
                    value="{{ old('maintenance_amount') ?: $property->maintenance_amount }}"
                    required
                >
                @if($errors->has('maintenance_amount'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('maintenance_amount') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group col-sm-6">
                <label for="currency_guaranty_amount">Moneda de monto de la garantía</label>
                <select
                    class="form-control {{ $errors->has('currency_guaranty_amount') ? 'is-invalid': '' }}"
                    name="currency_guaranty_amount"
                    id="currency_guaranty_amount"
                    required
                >
                    @foreach(\App\Currency::pluck('name', 'id') as $id => $name)
                        <option
                            {{ (int) old('currency_guaranty_amount') === $id || $property->currency_guaranty_amount === $id ? 'selected' : '' }}
                            value="{{ $id }}"
                        >{{ $name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-sm-6">
                <label for="guaranty_amount">Monto de la garantía</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('guaranty_amount') ? 'is-invalid': '' }}"
                    name="guaranty_amount"
                    id="guaranty_amount"
                    min="0"
                    placeholder=""
                    value="{{ old('guaranty_amount') ?: $property->guaranty_amount }}"
                    required
                >
                @if($errors->has('guaranty_amount'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('guaranty_amount') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group col-sm-6" style="display: none">
                <label for="latitude">Latitud</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('latitude') ? 'is-invalid': '' }}"
                    name="latitude"
                    id="latitude"
                    placeholder=""
                    value="1"
                    required
                >
                @if($errors->has('latitude'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('latitude') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group col-sm-6" style="display: none">
                <label for="longitude">Longitud</label>
                <input
                    type="number"  inputmode="numeric" pattern="[0-9]*"
                    class="form-control {{ $errors->has('longitude') ? 'is-invalid': '' }}"
                    name="longitude"
                    id="longitude"
                    placeholder=""
                    value="1"
                    required
                >
                @if($errors->has('longitude'))
                    <span class="invalid-feedback">
                    <strong>{{ $errors->first('longitude') }}</strong>
                </span>
                @endif
            </div>

            <div class="form-group col-sm-6">
                <label for="address">Dirección</label>
                <input
                    type="text"
                    class="form-control {{ $errors->has('address') ? 'is-invalid': '' }}"
                    name="address"
                    id="address"
                    placeholder=""
                    value="{{ old('address') ?: $property->address }}"
                    required
                >
                @if($errors->has('address'))
                    <span class="invalid-feedback">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
                @endif
            </div>

            <div class="form-group col-sm-6">
                <label for="only_genre">Permitidos</label>
                <select
                    class="form-control {{ $errors->has('only_genre') ? 'is-invalid': '' }}"
                    name="only_genre"
                    id="only_genre"
                >
                    <option
                        {{
                            (int) old('only_genre') === \App\Property::ALL
                            ||
                            (int) $property->only_genre === \App\Property::ALL
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::ALL }}"
                    >Ambos géneros</option>
                    <option
                        {{
                            (int) old('only_genre') === \App\Property::WOMEN_ONLY
                            ||
                            (int) $property->only_genre === \App\Property::WOMEN_ONLY
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::WOMEN_ONLY }}"
                    >Sólo mujeres</option>
                    <option
                        {{
                            (int) old('only_genre') === \App\Property::MAN_ONLY
                            ||
                            (int) $property->only_genre === \App\Property::MAN_ONLY
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::MAN_ONLY }}"
                    >Sólo hombres</option>
                    <option
                        {{
                            (int) old('only_genre') === \App\Property::WOMAN_PREFERRED
                            ||
                            (int) $property->only_genre === \App\Property::WOMAN_PREFERRED
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::WOMAN_PREFERRED }}"
                    >Mujeres de preferencia</option>
                    <option
                        {{
                            (int) old('only_genre') === \App\Property::MAN_PREFERRED
                            ||
                            (int) $property->only_genre === \App\Property::MAN_PREFERRED
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::MAN_PREFERRED }}"
                    >Hombres de preferencia</option>
                </select>
            </div>

            <div class="form-group col-sm-6">
                <label for="young_bonus">Bono renta joven</label>
                <select
                    class="form-control {{ $errors->has('young_bonus') ? 'is-invalid': '' }}"
                    name="young_bonus"
                    id="young_bonus"
                >
                    <option
                        {{
                            (int) old('young_bonus') === \App\Property::YB_QUALIFIED
                            ||
                            (int) $property->young_bonus === \App\Property::YB_QUALIFIED
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::YB_QUALIFIED }}"
                    >Sólo calificados</option>
                    <option
                        {{
                            (int) old('young_bonus') === \App\Property::YB_NOT_QUALIFIED
                            ||
                            (int) $property->young_bonus === \App\Property::YB_NOT_QUALIFIED
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::YB_NOT_QUALIFIED }}"
                    >Indiferente</option>
                    <option
                        {{
                            (int) old('young_bonus') === \App\Property::YB_PREFERRED
                            ||
                            (int) $property->young_bonus === \App\Property::YB_PREFERRED
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::YB_PREFERRED }}"
                    >Calificados de preferencia</option>
                </select>
            </div>


            <div class="form-group col-sm-6">
                <label for="only_students">Perfil académico</label>
                <select
                    class="form-control {{ $errors->has('only_students') ? 'is-invalid': '' }}"
                    name="only_students"
                    id="only_students"
                >
                    <option
                        {{
                            (int) old('only_students') === \App\Property::ALL
                            ||
                            (int) $property->only_students === \App\Property::ALL
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::ALL }}"
                    >Indiferente</option>
                    <option
                        {{
                            (int) old('only_students') === \App\Property::STUDENTS_ONLY
                            ||
                            (int) $property->only_students === \App\Property::STUDENTS_ONLY
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::STUDENTS_ONLY }}"
                    >Sólo estudiantes</option>
                    <option
                        {{
                            (int) old('only_students') === \App\Property::STUDENTS_PREFERRED
                            ||
                            (int) $property->only_students === \App\Property::STUDENTS_PREFERRED
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::STUDENTS_PREFERRED }}"
                    >Estudiantes de preferencia</option>
                </select>
            </div>

            <div class="form-group col-sm-6">
                <label for="family_status">Perfil familiar</label>
                <select
                    class="form-control {{ $errors->has('family_status') ? 'is-invalid': '' }}"
                    name="family_status"
                    id="family_status"
                >
                    <option
                        {{
                            (int) old('family_status') === \App\Property::ALL
                            ||
                            (int) $property->family_status === \App\Property::ALL
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::ALL }}"
                    >Indiferente</option>
                    <option
                        {{
                            (int) old('family_status') === \App\Property::COUPLE_WITH_KIDS_ONLY
                            ||
                            (int) $property->family_status === \App\Property::COUPLE_WITH_KIDS_ONLY
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::COUPLE_WITH_KIDS_ONLY }}"
                    >Sólo parejas con hijo</option>
                    <option
                        {{
                            (int) old('family_status') === \App\Property::COUPLE_WITH_KIDS_PREFERRED
                            ||
                            (int) $property->family_status === \App\Property::COUPLE_WITH_KIDS_PREFERRED
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::COUPLE_WITH_KIDS_PREFERRED }}"
                    >Parejas con hijo de preferencia</option>
                </select>
            </div>
            <div class="form-group col-sm-6">
                <label for="age_range">Perfil de edades</label>
                <select
                    class="form-control {{ $errors->has('age_range') ? 'is-invalid': '' }}"
                    name="age_range"
                    id="age_range"
                >
                    <option
                        {{
                            (int) old('age_range') === \App\Property::ALL
                            ||
                            (int) $property->age_range === \App\Property::ALL
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::ALL }}"
                    >Indiferente</option>
                    <option
                        {{
                            (int) old('age_range') === \App\Property::AGE_18_30_ONLY
                            ||
                            (int) $property->age_range === \App\Property::AGE_18_30_ONLY
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::AGE_18_30_ONLY }}"
                    >Sólo de 18 a 30 años</option>
                    <option
                        {{
                            (int) old('age_range') === \App\Property::AGE_31_45_ONLY
                            ||
                            (int) $property->age_range === \App\Property::AGE_31_45_ONLY
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::AGE_31_45_ONLY }}"
                    >Sólo de 31 a 45 años</option>
                    <option
                        {{
                            (int) old('age_range') === \App\Property::AGE_45_ONLY
                            ||
                            (int) $property->age_range === \App\Property::AGE_45_ONLY
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::AGE_45_ONLY }}"
                    >Sólo mayores de 45 años</option>
                    <option
                        {{
                            (int) old('age_range') === \App\Property::AGE_18_30_PREFERRED
                            ||
                            (int) $property->age_range === \App\Property::AGE_18_30_PREFERRED
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::AGE_18_30_PREFERRED }}"
                    >De 18 a 30 años de preferencia</option>
                    <option
                        {{
                            (int) old('age_range') === \App\Property::AGE_31_45_PREFERRED
                            ||
                            (int) $property->age_range === \App\Property::AGE_31_45_PREFERRED
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::AGE_31_45_PREFERRED }}"
                    >De 31 a 45 años de preferencia</option>
                    <option
                        {{
                            (int) old('age_range') === \App\Property::AGE_45_PREFERRED
                            ||
                            (int) $property->age_range === \App\Property::AGE_45_PREFERRED
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::AGE_45_PREFERRED }}"
                    >Mayores de 45 años de preferencia</option>
                </select>
            </div>


            <div class="form-group col-sm-6">
                <label for="" class="">Características adicionales</label>
                <div class="col-md-12">
                    <div class="form-check">
                        <input
                            class="form-check-input"
                            type="checkbox"
                            value="{{ \App\Property::PET_ALLOWED }}"
                            id="pet_allowed"
                            name="pet_allowed"
                            {{ $property->pet_allowed == \App\Property::PET_ALLOWED ? "checked": $property->pet_allowed  }}
                        >
                        <label
                            class="form-check-label checkbox-filter-label"
                            for="pet_allowed"
                        >Se permiten mascotas </label>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-6" style="display: none">
                <label for="rent_limit_id">Límite de renta</label>
                <select
                    style="text-transform: capitalize"
                    class="form-control {{ $errors->has('rent_limit_id') ? 'is-invalid': '' }}"
                    name="rent_limit_id"
                    id="rent_limit_id"
                >
                    @php $temp = 0; @endphp
                    @foreach(\App\RentLimit::orderByDesc('id')->get() as $rentLimit)
                        <option
                            {{ $temp == 0 ? 'select':'' }}
                            value="{{ $rentLimit->id }}"
                        >{{ $rentLimit->lower_rent." - ".$rentLimit->upper_rent }}</option>
                        @php $temp++; @endphp
                    @endforeach
                </select>
            </div>

            <div class="form-group col-sm-6">
                <label for="state">ESTADO</label>
                <select
                    class="form-control {{ $errors->has('state') ? 'is-invalid': '' }}"
                    name="state"
                    id="state"
                >
                    <option
                        {{
                            (int) old('state') === \App\Property::ACTIVE
                            ||
                            (int) $property->state === \App\Property::ACTIVE
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::ACTIVE }}"
                    >Activo</option>
                    <option
                        {{
                            (int) old('state') === \App\Property::INACTIVE
                            ||
                            (int) $property->state === \App\Property::INACTIVE
                            ?
                            'selected' : ''
                        }}
                        value="{{ \App\Property::INACTIVE }}"
                    >Inactivo</option>
                </select>
            </div>



            <div class="form-group text-center" style="width: 100%;">
                <button type="submit" class="btn btn-danger">
                    {{ __($btnText) }}
                </button>

                @if($property->id)
                    <a class="btn btn-homex-dark" href="{{ route('propertyImages.edit', $property->id) }}">
                        {{ __("Editar fotos") }}
                    </a>
                @endif
            </div>

        </form>

    </div>
@endsection

@push('scripts')
    <script>

        $(function() {

            //DEFINIMOS LOS ARREGLOS DISTRITOS Y TIPOS DE INMUEBLES COMO JSONs



            $( "#description" ).on('input', function() {
                var string_length =  $(this).val().length;
                var string = $(this).val();

                $("#dscr-msg").html("<div class='p-2'>"+string_length+" caracteres.<div>");

                if (string_length > 500) {
                    $("#dscr-msg").html("<div class='alert-danger p-2'>Has sobrepasado los 500 caracteres. Por favor, reduce la descripción.<div>");

                    let text;

                    text = string.substring(0, 500);

                    $(this).val(text);
                }
            });
        });

    </script>
@endpush


