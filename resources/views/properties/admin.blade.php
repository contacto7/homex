@extends('layouts.app')


@section('title')
    @include('partials.genericJumbotron', [
        'title' => __("Viviendas"),
        'icon' => "home"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">Inmueble</th>
                    <th scope="col">Visitas</th>
                    <th scope="col">Creado</th>
                    <th scope="col">Usuario</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($properties as $property)
                    <tr>
                        <td>{{ $property->name }}</td>
                        <td>
                            @if($property->visits())
                                {{ $property->visits()->visitas }}
                            @else
                                0
                            @endif
                        </td>
                        <td>{{ date('d/m/Y', strtotime($property->created_at)) }}</td>
                        <td>{{ $property->user->name }}</td>
                        <td>
                            <a
                                class="btn btn-outline-info"
                                href="{{ route('properties.view', $property->id) }}"
                                data-toggle="tooltip"
                                data-placement="top"
                                title="Ver vivienda"
                            >
                                <i class="fa fa-home"></i>
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay arrendamientos disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center">
            {{ $properties->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection
