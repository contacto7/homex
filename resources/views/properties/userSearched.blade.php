@extends('layouts.app')


@section('title')
    @include('partials.genericJumbotron', [
        'title' => __("Búsquedas"),
        'icon' => "clock-o"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Inmueble</th>
                    <th scope="col">Monto</th>
                    <th scope="col">Buscado</th>
                    <th scope="col">Ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($searches as $search)
                    <tr>
                        <td>{{ $search->id }}</td>
                        <td>{{ $search->property->name }}</td>
                        <td>S/{{ number_format($search->property->rental_amount, 2, '.', "'") }}</td>
                        <td>
                            {{ date('d/m/Y', strtotime($search->searched_at)) }}</td>
                        <td>
                            <a
                                class="btn-homex btn-homex-clear m-1"
                                href="{{ route('properties.view', $search->property->id) }}"
                            >Inmueble</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay búsquedas disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center">
            {{ $searches->appends(request()->except('page'))->links() }}
        </div>
    </div>
@endsection

@push('scripts')
    <script>

        $(document).on('click', '.modificar', function(){


        });

    </script>
@endpush
