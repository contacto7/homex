@extends('layouts.app')

@section('jumbotron')
    @include('partials.genericJumbotron', [
        'title' => __("Mis viviendas"),
        'icon' => "home"
    ])
@endsection

@section('content')
    <div class="pl-5 pr-5">
        <div class="row justify-content-center">

        <div class="col-12 mb-4" style="width: 100%">
            <a href="{{ route('properties.create') }}" class="btn btn-homex-dark">Agregar propiedad</a>

        </div>

        @forelse($properties as $property)
                <div class="col-lg-3 col-md-4 col-sm-6 d-flex align-items-stretch">
                    @include('partials.properties.card_properties')
                </div>
            @empty
                {{ __("No hay propiedades disponibles") }}
            @endforelse
        </div>
        <div class="row justify-content-center">
            {{ $properties->appends(request()->except('page'))->links() }}
        </div>
    </div>
    @include('partials.modals.modalFiltroBusqueda')
@endsection

@push('scripts')
    <script>

        $(function() {
        });

    </script>
@endpush
