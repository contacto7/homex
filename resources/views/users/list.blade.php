@extends('layouts.app')


@section('title')
    @include('partials.genericJumbotron', [
        'title' => __("Usuarios"),
        'icon' => "user"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <table class="table table-striped table-light">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Teléfono</th>
                    <th scope="col">ver</th>
                </tr>
                </thead>
                <tbody>
                @forelse($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->last_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->cellphone }}</td>
                        <td>
                            <div class="btn-group">
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('users.profile', $user->slug) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Perfil"
                                >
                                    <i class="fa fa-user"></i>
                                </a>
                                @if($user->role_id == \App\Role::PROPIETARY)
                                <a
                                    class="btn btn-outline-info"
                                    href="{{ route('properties.myList', $user->slug) }}"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Viviendas"
                                >
                                    <i class="fa fa-home"></i>
                                </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>{{ __("No hay usuarios disponibles")}}</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="row justify-content-center">
            {{ $users->links() }}
        </div>
    </div>
@endsection
