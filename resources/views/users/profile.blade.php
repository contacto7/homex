@extends('layouts.app')

@section('jumbotron')
    @include('partials.genericJumbotron', [
        'title' => __("MI PERFIL"),
        'icon' => "user"
    ])
@endsection

@section('content')
    <div class="container">
        <div class="profile-wrapp">
            <div class="row profile-group">
                <div class="col-sm-12">
                    <div class="profile-img-wrapp">

                        <img
                            class="profile-img"
                            src="{{ $user->pathAttachment() }}"
                            alt="{{ $user->name }}"
                        />
                        @can('editUser', [\App\User::class, $user] )
                        <div>
                            <a class="edit-btn d-inline-block"
                               data-toggle="modal"
                               data-target="#modalEditPicture"
                               href="#modalEditPicture">
                                <span class="fa fa-pencil"></span> Editar
                            </a>
                        </div>
                        @endcan

                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="profile-title text-center">
                        @if(!$errors->isEmpty())
                            <span class="error">¡Por favor ingrese los datos requeridos correctamente!</span>
                        @endif
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="profile-title">
                        Información personal:
                        @can('editUser', [\App\User::class, $user] )
                        <a class="edit-btn"
                           data-toggle="modal"
                           data-target="#modalEditPersonalInfo"
                           href="#modalEditPersonalInfo">
                            <span class="fa fa-pencil"></span> Editar
                        </a>
                        @endcan
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="profile-field">{{ $user->name }}</div>
                </div>
                <div class="col-sm-6">
                    <div class="profile-field">{{ $user->last_name }}</div>
                </div>
                <div class="col-sm-6">
                    @auth()
                        @if(auth()->user()->role_id == \App\Role::ADMIN || $user->id == auth()->user()->id)
                        <div class="profile-field">{{ $user->email }}</div>
                        @else
                        <div class="profile-field">*******@*****.com</div>
                        @endif
                    @elseauth
                        <div class="profile-field">*******@*****.com</div>
                    @endauth
                </div>
                <div class="col-sm-6">
                    @auth()
                        @if(auth()->user()->role_id == \App\Role::ADMIN || $user->id == auth()->user()->id)
                            <div class="profile-field">{{ $user->cellphone }}</div>
                        @else
                            <div class="profile-field">9*********</div>
                        @endif
                    @elseauth
                        <div class="profile-field">9*********</div>
                    @endauth
                </div>

            </div>
            <div class="row profile-group">
                <div class="col-sm-12">
                    <div class="profile-title">Documento:
                        @can('editUser', [\App\User::class, $user] )
                        <a class="edit-btn"
                           data-toggle="modal"
                           data-target="#modalEditDocument"
                           href="#modalEditDocument">
                            <span class="fa fa-pencil"></span> Editar
                        </a>
                        @endcan
                    </div>
                </div>
                @if($user->documentType)
                <div class="col-sm-6">
                    <div class="profile-field">{{ $user->documentType->name }}</div>
                </div>
                @endif
                @if($user->document_number)
                <div class="col-sm-6">
                    <div class="profile-field">{{ $user->document_number }}</div>
                </div>
                @endif

            </div>
            <div class="row profile-group">
                <div class="col-sm-12">
                    <div class="profile-title">Nacionalidad y sexo:
                        @can('editUser', [\App\User::class, $user] )
                        <a class="edit-btn"
                           data-toggle="modal"
                           data-target="#modalEditNationality"
                           href="#modalEditNationality">
                            <span class="fa fa-pencil"></span> Editar
                        </a>
                        @endcan
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="profile-field">{{ $user->nacionality->name }}</div>
                </div>
                <div class="col-sm-6">
                    <div class="profile-field">
                        Sexo:
                        @if($user->sex == \App\User::FEMALE)
                            Mujer
                        @elseif($user->sex == \App\User::MALE)
                            Hombre
                        @endif
                    </div>
                </div>

            </div>

            @auth
                @if($user->role_id == \App\Role::PROPIETARY)
                    @include('partials.users.propietary')
                @elseif($user->role_id == \App\Role::LESSEE)
                    @include('partials.users.lessee')
                @endif
            @endauth



            @auth()
                @if(auth()->user()->role_id == \App\Role::ADMIN || $user->id == auth()->user()->id)

                @elseif($user->role_id == \App\Role::LESSEE )
                    <div class=" mt-4 text-center">

                        <div class="reporte-pregunta-wrapp">
                            <a class="d-inline-block mb-3"
                               data-toggle="modal"
                               data-target="#modalInfoReporte"
                               href="#modalInfoReporte"
                            >
                                ¿Qué es el reporte?
                            </a>
                        </div>
                        <a class="btn btn-danger"
                           data-toggle="modal"
                           data-target="#modalSolicitarReporte"
                           href="#modalSolicitarReporte"
                        >
                            SOLICITAR REPORTE
                        </a>
                    </div>
                @endif
            @endauth



        </div>
    </div>

    @can('editUser', [\App\User::class, $user] )
        @include('partials.modals.modalEditPicture')
        @include('partials.modals.modalEditPersonalInfo')
        @include('partials.modals.modalEditDocument')
        @include('partials.modals.modalEditNationality')

        @if($user->role_id == \App\Role::LESSEE)
            @include('partials.modals.modalEditLifestyle')
            @include('partials.modals.modalEditAddress')
            @include('partials.modals.modalEditBond')
        @elseif($user->role_id == \App\Role::PROPIETARY)
            @include('partials.modals.modalEditBiography')
        @endif
    @endcan

    @can('solicitarReporte', [\App\User::class, $user] )
        @include('partials.modals.modalSolicitarReporte')
        @include('partials.modals.modalInfoReporte')
    @endcan

@endsection


@push('scripts')
    <script>
        $(function() {
            $('#picture').change(function() {
                let filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
                $('.custom-file-label').html(filename);
            });
        });

    </script>
@endpush


